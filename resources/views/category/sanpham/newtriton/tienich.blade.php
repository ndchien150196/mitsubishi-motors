<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>AN TOÀN BỊ ĐỘNG</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide120"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="Trang bị 7 túi khí an toàn" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> Trang bị 7 túi khí an toàn</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="./Newtriton _ Mitsubishi Quảng Ninh - Đại lý Mitsubishi Motors tại Việt Nam _ Phân phối xe Mitsubishi Mirage, Attrage, Triton, Outlander Sport, Pajero Sport chính hãng_files/Untitled-7.png"
                                    alt="Trang bị 7 túi khí an toàn"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> Trang bị 7 túi khí an toàn</span></h3>
                                <div class="opContent">
                                    <p>Khi xảy ra va chạm, 7 túi khí an toàn sẽ bảo vệ tài xế và hành khách ở mức tối ưu 7 túi khí bao gồm: 2 túi khí phía trước, 2 túi khí bên, 2 túi khi rèm và 1 túi khí ở chân tài xế.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide121" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Khung xe RISE thép siêu cường" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Khung xe RISE thép siêu cường</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="./Newtriton _ Mitsubishi Quảng Ninh - Đại lý Mitsubishi Motors tại Việt Nam _ Phân phối xe Mitsubishi Mirage, Attrage, Triton, Outlander Sport, Pajero Sport chính hãng_files/SU-rise.jpg"
                                    alt="Khung xe RISE thép siêu cường"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Khung xe RISE thép siêu cường</span></h3>
                                <div class="opContent">
                                    <p>Công nghệ khung xe đặc trưng của Mitsubishi mang đến khả năng hấp thụ hiệu quả năng lượng va chạm từ mọi hướng và phân tán lực tác động giúp bảo vệ hành khách</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="./Newtriton _ Mitsubishi Quảng Ninh - Đại lý Mitsubishi Motors tại Việt Nam _ Phân phối xe Mitsubishi Mirage, Attrage, Triton, Outlander Sport, Pajero Sport chính hãng_files/Untitled-7.png"
                        alt="Trang bị 7 túi khí an toàn"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Trang bị 7 túi khí an toàn</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Khi xảy ra va chạm, 7 túi khí an toàn sẽ bảo vệ tài xế và hành khách ở mức tối ưu 7 túi khí bao gồm: 2 túi khí phía trước, 2 túi khí bên, 2 túi khi rèm và 1 túi khí ở chân tài xế.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="./Newtriton _ Mitsubishi Quảng Ninh - Đại lý Mitsubishi Motors tại Việt Nam _ Phân phối xe Mitsubishi Mirage, Attrage, Triton, Outlander Sport, Pajero Sport chính hãng_files/SU-rise.jpg"
                        alt="Khung xe RISE thép siêu cường"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khung xe RISE thép siêu cường</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Công nghệ khung xe đặc trưng của Mitsubishi mang đến khả năng hấp thụ hiệu quả năng lượng va chạm từ mọi hướng và phân tán lực tác động giúp bảo vệ hành khách</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>