@extends("master")
@section("content")
<section id="err_404">
   	<picture>
   		<source media="(max-width: 991px)" srcset="{{asset('/img/404-banner-mb.jpg')}}">
       <source media="(min-width: 992px)" srcset="{{asset('/img/404-banner-dt.jpg')}}">
       <img class="img-full" src="{{asset('/img/404-banner-dt.jpg')}}" alt="Lỗi">
   	</picture>
</section>
@endsection