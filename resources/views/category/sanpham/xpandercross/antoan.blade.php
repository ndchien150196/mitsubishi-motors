<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>AN TOÀN</span>
        <small> <!--                                --> </small>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide100"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="An toàn tiên tiến tự tin trên mọi hành trình" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> An toàn tiên tiến tự tin trên mọi hành trình</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Rise.jpg')}}" alt="An toàn tiên tiến tự tin trên mọi hành trình" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> An toàn tiên tiến tự tin trên mọi hành trình</span></h3>
                                <div class="opContent">
                                    <p>Công nghệ khung xe RISE đặc trưng của Mitsubishi cùng hoàng loạt các công nghệ an toàn, giúp người lái tự tin điều khiển xe vượt mọi hành trình.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide101" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống cân bằng điện tử ASC" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Hệ thống cân bằng điện tử ASC</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/ASC.jpg')}}" alt="Hệ thống cân bằng điện tử ASC" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Hệ thống cân bằng điện tử ASC</span></h3>
                                <div class="opContent">
                                    <p>
                                        Kiểm soát lực phanh, nâng cao sự ổn định trong điều kiện đường trơn trượt
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide102" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống kiểm soát lực kéo TCL" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Hệ thống kiểm soát lực kéo TCL</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/TCL.jpg')}}" alt="Hệ thống kiểm soát lực kéo TCL" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Hệ thống kiểm soát lực kéo TCL</span></h3>
                                <div class="opContent">
                                    <p>Giúp tăng sự ổn định của xe và kiểm soát xe tốt hơn trong điều kiện thời tiết bất lợi và thiếu lực kéo.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide103" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống phanh ABS – EBD" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Hệ thống phanh ABS – EBD</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/ABS-EBD.jpg')}}" alt="Hệ thống phanh ABS – EBD" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Hệ thống phanh ABS – EBD</span></h3>
                                <div class="opContent">
                                    <p>
                                        Các hệ thống phanh an toàn ABS, EBD mang lại khả năng an toàn chủ động vượt trội cho Xpander Cross.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide104" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống hỗ trợ lực phanh khẩn cấp BA" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Hệ thống hỗ trợ lực phanh khẩn cấp BA</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/BA.jpg')}}" alt="Hệ thống hỗ trợ lực phanh khẩn cấp BA" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Hệ thống hỗ trợ lực phanh khẩn cấp BA</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống hỗ trợ phanh khẩn cấp giúp tăng cường lực phanh cho người lái trong trường hợp đạp phanh khẩn cấp, đảm bảo an toàn cho người và xe.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide105" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống hỗ trợ khởi hành ngang dốc HSA" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Hệ thống hỗ trợ khởi hành ngang dốc HSA</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/HSA.jpg')}}" alt="Hệ thống hỗ trợ khởi hành ngang dốc HSA" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Hệ thống hỗ trợ khởi hành ngang dốc HSA</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống tự động hỗ trợ phanh giúp xe không bị trượt về phía sau khi người lái chuyển từ chân phanh sang chân ga trong trường hợp dừng và khởi hành ngang dốc.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="6" class="option-item null slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide106" data-slick-index="6" aria-hidden="true">
                        <a href="javascript:;" title="Hệ thống cảnh báo phanh khẩn cấp (ESS)" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">6. Hệ thống cảnh báo phanh khẩn cấp (ESS)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/ESS.jpg')}}" alt="Hệ thống cảnh báo phanh khẩn cấp (ESS)" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">6. Hệ thống cảnh báo phanh khẩn cấp (ESS)</span></h3>
                                <div class="opContent">
                                    <p>
                                        Khi ABS được kích hoạt, đèn cảnh báo nguy hiểm sẽ tự động bật để báo hiệu cho các xe đi sau.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Rise.jpg')}}" alt="An toàn tiên tiến tự tin trên mọi hành trình" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">An toàn tiên tiến tự tin trên mọi hành trình</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Công nghệ khung xe RISE đặc trưng của Mitsubishi cùng hoàng loạt các công nghệ an toàn, giúp người lái tự tin điều khiển xe vượt mọi hành trình.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/ASC.jpg')}}" alt="Hệ thống cân bằng điện tử ASC" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống cân bằng điện tử ASC</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Kiểm soát lực phanh, nâng cao sự ổn định trong điều kiện đường trơn trượt
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/TCL.jpg')}}" alt="Hệ thống kiểm soát lực kéo TCL" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống kiểm soát lực kéo TCL</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Giúp tăng sự ổn định của xe và kiểm soát xe tốt hơn trong điều kiện thời tiết bất lợi và thiếu lực kéo.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/ABS-EBD.jpg')}}" alt="Hệ thống phanh ABS – EBD" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống phanh ABS – EBD</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Các hệ thống phanh an toàn ABS, EBD mang lại khả năng an toàn chủ động vượt trội cho Xpander Cross.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/BA.jpg')}}" alt="Hệ thống hỗ trợ lực phanh khẩn cấp BA" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống hỗ trợ lực phanh khẩn cấp BA</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Hệ thống hỗ trợ phanh khẩn cấp giúp tăng cường lực phanh cho người lái trong trường hợp đạp phanh khẩn cấp, đảm bảo an toàn cho người và xe.</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/HSA.jpg')}}" alt="Hệ thống hỗ trợ khởi hành ngang dốc HSA" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống hỗ trợ khởi hành ngang dốc HSA</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Hệ thống tự động hỗ trợ phanh giúp xe không bị trượt về phía sau khi người lái chuyển từ chân phanh sang chân ga trong trường hợp dừng và khởi hành ngang dốc.</p>
                    </div>
                </div>
            </div>
            <div data-item="6" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/ESS.jpg')}}" alt="Hệ thống cảnh báo phanh khẩn cấp (ESS)" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống cảnh báo phanh khẩn cấp (ESS)</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Khi ABS được kích hoạt, đèn cảnh báo nguy hiểm sẽ tự động bật để báo hiệu cho các xe đi sau.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>