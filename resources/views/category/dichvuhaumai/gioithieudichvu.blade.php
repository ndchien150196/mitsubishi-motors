@extends("master")
@section("content") 
<div class="second-block">
    <div class="grid-fluid">
        <h1 class="title-block">
            <span>Giới thiệu dịch vụ</span>
        </h1>
        <div class="grid-content">
            <div class="mobile-block services-block">
                <p class="description-box text-center">
                    Mitsubishi Motors Việt Nam mang đến dịch vụ chuyên nghiệp cho quý khách hàng. Chúng tôi cam kết duy trì chất lượng tốt nhất, độ tin cậy và hiệu suất hoạt động cao nhất cho chiếc của quý khách. Hãy để chúng tôi chăm sóc
                    chiếc xe của quý khách với hệ thống dịch vụ ủy quyền chuyên nghiệp cùng với phụ tùng chính hiệu từ Mitsubishi Motors.
                </p>
                <ul class="services-list">
                    <li>
                        <a href="{{url('dich-vu-hau-mai',['chinh-sach-bao-hanh'])}}" title="Chính sách bảo hành" class="services-item">
                            <img alt="1" src="{{asset('img/dichvu/waranty.jpg')}}"/>
                            <div class="overlay">
                                <div class="content">
                                    <img alt="Chính sách bảo hành" src="{{asset('img/dichvu/icon-waranty.svg')}}"/>
                                    <h3 class="title">Chính sách bảo hành</h3>
                                    <p class="more-link">Xem chi tiết<i class="fa fa-angle-double-right"></i></p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('dich-vu-hau-mai',['bao-duong-dinh-ky'])}}" title="Bảo dưỡng định kỳ" class="services-item">
                            <img
                                alt="2"
                                src="{{asset('img/dichvu/maintenance.jpg')}}"/>
                            <div class="overlay">
                                <div class="content">
                                    <img alt="Bảo dưỡng định kỳ" src="{{asset('img/dichvu/icon-maintenance.svg')}}"/>
                                    <h3 class="title">Bảo dưỡng định kỳ</h3>
                                    <p class="more-link">Xem chi tiết<i class="fa fa-angle-double-right"></i></p>
                                </div>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('dich-vu-hau-mai',['phu-tung-chinh-hieu'])}}" title="Phụ tùng chính hiệu" class="services-item">
                            <img alt="3" src="{{asset('img/dichvu/sparepart.jpg')}}"/>
                            <div class="overlay">
                                <div class="content">
                                    <img alt="Phụ tùng chính hiệu" src="{{asset('img/dichvu/icon-sparepart.svg')}}"/>
                                    <h3 class="title">Phụ tùng chính hiệu</h3>
                                    <p class="more-link">Xem chi tiết<i class="fa fa-angle-double-right"></i></p>
                                </div>
                            </div>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection
