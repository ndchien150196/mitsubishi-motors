<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>NỘI THẤT RỘNG RÃI &amp; TIỆN NGHI</span>
        <small> <!--                                --> </small>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" style="opacity: 1; height: 612px; transform: translate3d(0px, 0px, 0px);" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide80"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="Khoang nội thất tiện nghi 7 chỗ đích thực" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> Khoang nội thất tiện nghi 7 chỗ đích thực</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Interior1.2.jpg')}}" alt="Khoang nội thất tiện nghi 7 chỗ đích thực" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> Khoang nội thất tiện nghi 7 chỗ đích thực</span></h3>
                                <div class="opContent">
                                    <p>
                                        Thiết kế nội thất theo triết lý Omotenashi hiện đại, tinh tế và tiện dụng đi cùng với ghế da cao cấp hai tông màu giúp mang đến sự thoải mái cho mọi hành khách ngay cả trên những hành
                                        trình dài.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide81" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Ghế da cao cấp 2 tông màu" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Ghế da cao cấp 2 tông màu</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Ghe-da-2-mau.jpg')}}" alt="Ghế da cao cấp 2 tông màu" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Ghế da cao cấp 2 tông màu</span></h3>
                                <div class="opContent">
                                    <p>
                                        Xpander Cross được trang bị ghế da cao cấp 2 tông màu không chỉ tạo ấn tượng ngay từ ánh nhìn đầu tiên mà đem lại thoải mái trên hành trình dài. Hàng ghế thứ hai có thể trượt lên xuống,
                                        tăng diện tích cho hàng ghế thứ 3.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide82" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Màn hình cảm ứng 7-inch" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Màn hình cảm ứng 7-inch</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Man-hinh-AC-AA.jpg')}}" alt="Màn hình cảm ứng 7-inch" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Màn hình cảm ứng 7-inch</span></h3>
                                <div class="opContent">
                                    <p>Màn hình cảm ứng 7-inch có khả năng kết nối Apple CarPlay, Android Auto và Weblink (Android). Bên cạnh đó, hệ thống giải trí có thể kết nối Bluetooth, USB.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide83" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống điều khiển hành trình Cruise Control" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Hệ thống điều khiển hành trình Cruise Control</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Cruise-Control.jpg')}}" alt="Hệ thống điều khiển hành trình Cruise Control" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Hệ thống điều khiển hành trình Cruise Control</span></h3>
                                <div class="opContent">
                                    <p>Cruise Control giúp duy trì tốc độ ổn định mà không phải đặt chân lên bàn đạp ga, giúp việc lái xe trở nên thoải mái và thư giãn hơn, đặc biệt trên hành trình dài.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide84" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Khả năng sắp xếp ghế linh hoạt" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Khả năng sắp xếp ghế linh hoạt</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Ghe-linh-hoat.gif')}}" alt="Khả năng sắp xếp ghế linh hoạt" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Khả năng sắp xếp ghế linh hoạt</span></h3>
                                <div class="opContent">
                                    <p>Với 7 chỗ ngồi linh hoạt, người lái có thể gập ghế để tăng kích thước khoang chở đồ hoặc phục vụ các mục đích chuyên chở khác nhau.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide85" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Khả năng cách âm vượt trội" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Khả năng cách âm vượt trội</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Cach-am.jpg')}}" alt="Khả năng cách âm vượt trội" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Khả năng cách âm vượt trội</span></h3>
                                <div class="opContent">
                                    <p>Vật liệu cách âm và hấp thụ âm được trang bị trên khắp thân xe, kết hợp với kính chắn gió cách âm giúp cabin luôn yên tĩnh vượt trội.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Interior1.2.jpg')}}" alt="Khoang nội thất tiện nghi 7 chỗ đích thực" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khoang nội thất tiện nghi 7 chỗ đích thực</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Thiết kế nội thất theo triết lý Omotenashi hiện đại, tinh tế và tiện dụng đi cùng với ghế da cao cấp hai tông màu giúp mang đến sự thoải mái cho mọi hành khách ngay cả trên những hành trình dài.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Ghe-da-2-mau.jpg')}}" alt="Ghế da cao cấp 2 tông màu" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Ghế da cao cấp 2 tông màu</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Xpander Cross được trang bị ghế da cao cấp 2 tông màu không chỉ tạo ấn tượng ngay từ ánh nhìn đầu tiên mà đem lại thoải mái trên hành trình dài. Hàng ghế thứ hai có thể trượt lên xuống, tăng diện tích
                            cho hàng ghế thứ 3.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Man-hinh-AC-AA.jpg')}}" alt="Màn hình cảm ứng 7-inch" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Màn hình cảm ứng 7-inch</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Màn hình cảm ứng 7-inch có khả năng kết nối Apple CarPlay, Android Auto và Weblink (Android). Bên cạnh đó, hệ thống giải trí có thể kết nối Bluetooth, USB.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Cruise-Control.jpg')}}" alt="Hệ thống điều khiển hành trình Cruise Control" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống điều khiển hành trình Cruise Control</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Cruise Control giúp duy trì tốc độ ổn định mà không phải đặt chân lên bàn đạp ga, giúp việc lái xe trở nên thoải mái và thư giãn hơn, đặc biệt trên hành trình dài.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Ghe-linh-hoat.gif')}}" alt="Khả năng sắp xếp ghế linh hoạt" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khả năng sắp xếp ghế linh hoạt</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Với 7 chỗ ngồi linh hoạt, người lái có thể gập ghế để tăng kích thước khoang chở đồ hoặc phục vụ các mục đích chuyên chở khác nhau.</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Cach-am.jpg')}}" alt="Khả năng cách âm vượt trội" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khả năng cách âm vượt trội</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Vật liệu cách âm và hấp thụ âm được trang bị trên khắp thân xe, kết hợp với kính chắn gió cách âm giúp cabin luôn yên tĩnh vượt trội.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>