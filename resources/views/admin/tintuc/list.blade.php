@extends("app")

@section("content")

<div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row"></div>
    <div class="row">
        <div class="col-sm-12">
            <table id="mitsubishi" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="mitsubishi_info">
                <thead>
                    <tr role="row">
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">ID</th>
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Tiêu Đề</th>
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Chuyên Mục</th>
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Avatar</th>
                        <!-- <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Nội Dung</th> -->
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Trạng Thái</th>
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Thời Gian</th>
                        <th class="text-center">
                        	<a href="{{route('admin.tintuc.them')}}" class="btn btn-default"><i class="fa fa-plus">Thêm</i></a>
                        </th>
                    </tr>
                </thead>
                <tbody class="">
                    @foreach($tintuc as $k => $u)
                        <tr role="row" class="odd">
                            <td class="text-center">{{$u->id}}</td>
                            <td class="text-center">{!! $u->title !!}</td>
                            <td class="text-center">
                                {{\App\Common::category($u->category)}}
                            </td>
                            <td class="text-center">
                                <a href="{{$u->avatar}}" target="_blank"><img src="{{$u->avatar}}" height="50"></a>
                            </td>
                            <td class="text-center">
                                @if($u->status == 1)
                                    Xuất Bản
                                @else
                                    Nháp / Gỡ Bài
                                @endif
                            </td>
                            <td class="text-center">
                                {{ \Carbon\Carbon::createFromTimestamp(strtotime($u->created_at))->format('H:i d-m-Y')}}
                            </td>
                            <td class="text-center">
                                <a href="{{route('admin.tintuc.edit', ['slug' => $u->slug])}}"><span class="fa fa-edit"></span></a>
                                <a href="{{route('admin.tintuc.delete', ['slug' => $u->slug])}}"><span class="fa fa-trash"></span></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection