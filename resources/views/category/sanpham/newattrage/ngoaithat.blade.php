<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>NGOẠI THẤT HIỆN ĐẠI</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide70"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="Diện mạo ấn tượng và nổi bật trong nội thị​" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> Diện mạo ấn tượng và nổi bật trong nội thị​</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Attrage-Exterior.png')}}"
                                    alt="Diện mạo ấn tượng và nổi bật trong nội thị​"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> Diện mạo ấn tượng và nổi bật trong nội thị​</span></h3>
                                <div class="opContent">
                                    <p>Thừa hưởng những ưu điểm nổi bật từ thiết kế Dynamic Shield, ngoại hình của Attrage mang đến hình ảnh khỏe khoắn, trẻ trung phù hợp với nhu cầu di chuyển linh hoạt trong đô thị.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide71" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Đèn chiếu sáng phía trước Bi-LED" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Đèn chiếu sáng phía trước Bi-LED</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Đèn-Bi-LED.jpg')}}"
                                    alt="Đèn chiếu sáng phía trước Bi-LED"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li><span>CVT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Đèn chiếu sáng phía trước Bi-LED</span></h3>
                                <div class="opContent">
                                    <p>Công nghệ LED cho khả năng chiếu sáng vượt trội với thiết kế sắc sảo, kết hợp tinh tế bên cạnh thiết kế hiện đại của phần đầu xe.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide72" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Đèn chiếu sáng phía sau LED" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Đèn chiếu sáng phía sau LED</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Attrage-Den-hau-LED.png')}}"
                                    alt="Đèn chiếu sáng phía sau LED"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li><span>CVT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Đèn chiếu sáng phía sau LED</span></h3>
                                <div class="opContent">
                                    <p>Thiết kế đèn xe đặc trưng mang lại ấn tượng mạnh cho phần đuôi xe.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide73" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Gương chiếu hậu chỉnh điện &amp; gập điện*" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Gương chiếu hậu chỉnh điện &amp; gập điện*</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Attrage-Guong-chieu-hau.png')}}"
                                    alt="Gương chiếu hậu chỉnh điện &amp; gập điện*"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Gương chiếu hậu chỉnh điện &amp; gập điện*</span></h3>
                                <div class="opContent">
                                    <p>Gương chiếu hậu với kích thước lớn tăng độ an toàn cho người lái, tích hợp chức năng chỉnh điện và gập điện đem đến sự tiện dụng trong quá trình sử dụng. (Trang bị tùy chọn)</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide74" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Mâm xe 15-inch thiết kế mới" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Mâm xe 15-inch thiết kế mới</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Attrage-Mam-xe_.png')}}"
                                    alt="Mâm xe 15-inch thiết kế mới"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Mâm xe 15-inch thiết kế mới</span></h3>
                                <div class="opContent">
                                    <p>Mâm đúc hợp kim&nbsp;nhôm&nbsp;với thiết kế mới hiện đại và thể thao</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide75" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Nút bấm mở cốp sau" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Nút bấm mở cốp sau</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Attrage-Nut-bam-mo-cop.png')}}"
                                    alt="Nút bấm mở cốp sau"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li><span>CVT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Nút bấm mở cốp sau</span></h3>
                                <div class="opContent">
                                    <p>Nút bấm mở cốp sau tích hợp hệ thống KOS thông minh</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Attrage-Exterior.png')}}"
                        alt="Diện mạo ấn tượng và nổi bật trong nội thị​"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Diện mạo ấn tượng và nổi bật trong nội thị​</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Thừa hưởng những ưu điểm nổi bật từ thiết kế Dynamic Shield, ngoại hình của Attrage mang đến hình ảnh khỏe khoắn, trẻ trung phù hợp với nhu cầu di chuyển linh hoạt trong đô thị.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Đèn-Bi-LED.jpg')}}"
                        alt="Đèn chiếu sáng phía trước Bi-LED"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li><span>CVT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Đèn chiếu sáng phía trước Bi-LED</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Công nghệ LED cho khả năng chiếu sáng vượt trội với thiết kế sắc sảo, kết hợp tinh tế bên cạnh thiết kế hiện đại của phần đầu xe.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Attrage-Den-hau-LED.png')}}"
                        alt="Đèn chiếu sáng phía sau LED"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li><span>CVT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Đèn chiếu sáng phía sau LED</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Thiết kế đèn xe đặc trưng mang lại ấn tượng mạnh cho phần đuôi xe.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Attrage-Guong-chieu-hau.png')}}"
                        alt="Gương chiếu hậu chỉnh điện &amp; gập điện*"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Gương chiếu hậu chỉnh điện &amp; gập điện*</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Gương chiếu hậu với kích thước lớn tăng độ an toàn cho người lái, tích hợp chức năng chỉnh điện và gập điện đem đến sự tiện dụng trong quá trình sử dụng. (Trang bị tùy chọn)</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Attrage-Mam-xe_.png')}}"
                        alt="Mâm xe 15-inch thiết kế mới"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Mâm xe 15-inch thiết kế mới</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Mâm đúc hợp kim&nbsp;nhôm&nbsp;với thiết kế mới hiện đại và thể thao</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Attrage-Nut-bam-mo-cop.png')}}"
                        alt="Nút bấm mở cốp sau"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li><span>CVT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Nút bấm mở cốp sau</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Nút bấm mở cốp sau tích hợp hệ thống KOS thông minh</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>