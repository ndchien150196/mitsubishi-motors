<!--Banner-->
<div class="pr-banner">
    <picture>
        <!--if IE 9video(style='display: none;')-->
        <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/07/RN-Cross_BannerWeb_1920x800.jpg" alt="Bản Lĩnh Định Phong Cách" media="(min-width: 992px)" />
        <!--if IE 9-->
        <img srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/07/RN-Cross_BannerWeb_750x600.jpg" alt="Bản Lĩnh Định Phong Cách" class="res-img" />
    </picture>
    <div class="bn-content">
        <picture>
            <!--if IE 9video(style='display: none;')
                                -->
            <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/08/Logo1.png" alt="Xpander Cross logo" media="(min-width: 992px)" />
            <!--if IE 9-->
            <img srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/08/Logo1.png" alt="Xpander Cross logo" />
        </picture>
        <h2 class="bn-title visible-xs visible-sm">Bản Lĩnh Định Phong Cách</h2>
        <h2 class="bn-title hidden-xs hidden-sm">Bản Lĩnh Định Phong Cách</h2>
        <p class="summary"></p>
    </div>
</div>
<!--End - Banner-->