@extends("master")
@section("js")
    <script src="{{asset('js/slick.min.js')}}"></script>
@endsection
@section("content") 
<div class="page-product">
    @include('category.sanpham.xpandercross.banner')
    @include('category.sanpham.xpandercross.navigation')

    
    <div class="pr-content">
        @include('category.sanpham.xpandercross.dacdiemnoibat')

        <!--===================-->

        @include('category.sanpham.xpandercross.thongsokythuat')
        
        <!--===================-->

        @include('category.sanpham.xpandercross.ngoaithat')

        @include('category.sanpham.xpandercross.noithatrongrai')

        @include('category.sanpham.xpandercross.vanhanh')

        @include('category.sanpham.xpandercross.antoan')

        <!--End - Accessories-->
        <!--===================-->
        <!--===================-->
    </div>
    <!--.pr-content-->

    <!--===================-->
</div>

@endsection
@section('afftercontent')

<div d2-estimate-popup=""></div>
<!--#product_extimate-->

<section id="product_specs" class="mitsu-modal product-specs">
    <div class="grid-inner">
        <div data-href="#product_specs" class="modal-close"></div>
        <h3 class="title-block pr-title-block">
            <a title="So sánh giữa các phiên bản">So sánh giữa các phiên bản</a>
        </h3>
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <div class="list-steps">
                    <div class="steps-header">
                        <table>
                            <colgroup>
                                <col width="35%" />
                                <col width="65%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <div class="stepes-pagination">
                                            <ul>
                                                <li><a href="javascript:;" data-href="#st_thongsokythuat" class="stepes-active active">Thông số kỹ thuật</a></li>
                                                <li><a href="javascript:;" data-href="#st_trangthietbi" class="stepes-active">Trang thiết bị</a></li>
                                            </ul>
                                        </div>
                                    </th>
                                    <th>
                                        <img src="{{asset('img/menu/Cross.png')}}" alt="Xpander Cross Xpander Cross" />
                                        <label>Xpander Cross</label><span>{{$product['Xpander_Cross']}} VNĐ</span>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="st_thongsokythuat" class="stepes step-1 active">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />
                                    <col width="65%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><b> KÍCH THƯỚC </b></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kích thước tổng thể (DxRxC) (mm)</td>
                                        <td>4.500 x 1.800 x 1.750</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoảng cách hai cầu xe (mm)</td>
                                        <td>2.775</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoảng cách hai bánh xe trước/sau (mm)</td>
                                        <td>1.520/1.510</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Bán kính quay vòng nhỏ nhất (m)</td>
                                        <td>5,2</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoảng sáng gầm xe (mm)</td>
                                        <td>225</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Trọng lượng không tải (Kg)</td>
                                        <td>1.275</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Số chỗ ngồi</td>
                                        <td>7</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> ĐỘNG CƠ </b></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Loại động cơ</td>
                                        <td>1.5L MIVEC</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dung Tích Xylanh (cc)</td>
                                        <td>1.499</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Công suất cực đại (ps/rpm)</td>
                                        <td>104/6.000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mômen xoắn cực đại (Nm/rpm)</td>
                                        <td>141/4.000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dung tích thùng nhiên liệu (L)</td>
                                        <td>45</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hộp số</td>
                                        <td>Số tự động 4 cấp</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Truyền động</td>
                                        <td>1 cầu - 2WD</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Trợ lực lái</td>
                                        <td>Điện</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống treo trước</td>
                                        <td>McPherson với lò xo cuộn</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống treo sau</td>
                                        <td>Thanh xoắn</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lốp xe trước/sau</td>
                                        <td>Mâm hợp kim, 205/55R17</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Phanh trước/sau</td>
                                        <td>Đĩa/Tang trống</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mức tiêu hao nhiên liệu (L/100km)</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Kết hợp</i></td>
                                        <td>6,9</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Trong đô thị</i></td>
                                        <td>8,7</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Ngoài đô thị</i></td>
                                        <td>5,9</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="st_trangthietbi" class="stepes step-2">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />

                                    <col width="65%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><b> NGOẠI THẤT </b></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống đèn chiếu sáng phía trước</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i> - Đèn chiếu xa </i></td>
                                        <td>LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i> - Đèn chiếu gần </i></td>
                                        <td>LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn định vị dạng LED</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn chào mừng và đèn hỗ trợ chiếu sáng khi rời xe</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn sương mù trước/sau</td>
                                        <td>Đèn sương mù trước LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn báo phanh thứ ba trên cao</td>
                                        <td>LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kính chiếu hậu</td>
                                        <td>Mạ crôm, gập điện, chỉnh điện, Tích hợp đèn báo rẽ</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tay nắm cửa ngoài</td>
                                        <td>Mạ crôm</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lưới tản nhiệt</td>
                                        <td>Màu đen, thiết kế thể thao</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gạt nước kính trước</td>
                                        <td>Gạt mưa gián đoạn</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gạt nước kính sau và sưởi kính sau</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mâm đúc hợp kim</td>
                                        <td>17 inch - 2 tông màu</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> NỘI THÂT </b></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Vô lăng và cần số bọc da</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Nút điều khiển âm thanh trên vô lăng</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Điện thoại rảnh tay trên vô lăng</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống kiểm soát hành trình</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Vô lăng điều chỉnh 4 hướng</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Điều hòa nhiệt độ</td>
                                        <td>Chỉnh tay, 2 giàn lạnh</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chất liệu ghế</td>
                                        <td>Da cao cấp 2 tông màu</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Ghế tài xế</td>
                                        <td>Chỉnh tay 6 hướng</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hàng ghế thứ hai gập 60:40</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hàng ghế thứ ba gập 50:50</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tay nắm cửa trong mạ crôm</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kính cửa điều khiển điện</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Màn hình hiển thị đa thông tin</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Móc gắn ghế an toàn trẻ em</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống âm thanh</td>
                                        <td>Màn hình cảm ứng Apple CarPlay &amp; Android Auto, USB/Bluetooth</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Số lượng loa</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Sấy kính trước/sau</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cửa gió điều hòa hàng ghế sau</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gương chiếu hậu trong chống chói chỉnh tay</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> AN TOÀN </b></td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Túi khí an toàn</td>
                                        <td>Túi khí đôi</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cơ cấu căng đai tự động</td>
                                        <td>Hàng ghế trước</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống chống bó cứng phanh (ABS)</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống phân phối lực phanh điện tử EBD</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống trợ lực phanh khẩn cấp BA</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống cân bằng điện tử (ASC)</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống khởi hành ngang dốc (HSA)</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chìa khóa thông minh và khởi động bằng nút bấm</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoá cửa từ xa</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cảnh báo phanh khẩn cấp (ESS)</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chức năng chống trộm</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chìa khóa mã hóa chống trộm</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Camera lùi</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khóa cửa trung tâm</td>
                                        <td>Có</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br />
                    <br />
                    <p class="text-left">(*) Mức tiêu hao nhiên liệu chứng nhận bởi Cục Đăng Kiểm Việt Nam. Các thông số kỹ thuật có thể thay đổi mà không báo trước</p>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('jsfooter')
<script src="{{asset('js/sanpham/TweenMax.min.js')}}"></script>
<script src="{{asset('js/sanpham/ScrollToPlugin.min.js')}}"></script>
<script src="{{asset('js/sanpham/jquery.fancybox.js')}}"></script>
<script src="{{asset('js/product-page.js')}}"></script>
<script src="{{asset('js/estimate-price.js')}}"></script>
<script type="text/javascript">
productInit();
// var product = $('.page-product');
// var dacdiemnoibat = product.find('.dacdiemnoibat');
// var thongsokythuat = product.find('.thongsokythuat');
// var thietke = product.find('.thietke');
// var thuvien = product.find('.thuvien');
// var phukien = product.find('.phukien');

// var menuProduct = product.find('.pr-menu');
// var bannerProduct = product.find('.pr-banner');
// productMenuHeight = product.find('.pr-navigation').offset().top;
// menuProduct.find('.top').click(function () {
//     scrollTo($('body'));
//     menuProduct.removeClass('scroll');
//     menuProduct.removeClass('scroll-show');
//     bannerProduct.removeClass('scroll');
// });
// menuProduct.find('.dropdown .dropdown-menu a').click(function () {
//     var alink = $(this);
//     menuProduct.find('.dropdown .dd-text').html('');
//     menuProduct.find('.dropdown .dd-text').html(alink.html());

//     var target = product.find(alink.attr('data-section'));
//     if (target.length > 0) {
//         scrollTo(target);
//     }

//     menuProduct.find('.dropdown').removeClass('open');
//     return false;
// });

// menuScrollBottom();
// $(window).scroll(function () {
//     var wTop = $(this).scrollTop();
//     if (phukien.length > 0 && wTop >= phukien.offset().top) {
//         menuProduct.find('.dropdown .dd-text').html('');
//         menuProduct.find('.dropdown .dd-text').html(menuProduct.find('[data-section=".phukien"]').html());
//     }

//     else if (thuvien.length > 0 && wTop >= thuvien.offset().top) {
//         menuProduct.find('.dropdown .dd-text').html('');
//         menuProduct.find('.dropdown .dd-text').html(menuProduct.find('[data-section=".thuvien"]').html());
//     }
//     else if (thietke.length > 0 && wTop >= thietke.offset().top) {
//         menuProduct.find('.dropdown .dd-text').html('');
//         menuProduct.find('.dropdown .dd-text').html(menuProduct.find('[data-section=".thietke"]').html());
//     }
//     else if (thongsokythuat.length > 0 && wTop >= thongsokythuat.offset().top) {
//         menuProduct.find('.dropdown .dd-text').html('');
//         menuProduct.find('.dropdown .dd-text').html(menuProduct.find('[data-section=".thongsokythuat"]').html());
//     }
//     else {
//         menuProduct.find('.dropdown .dd-text').html('');
//         menuProduct.find('.dropdown .dd-text').html(menuProduct.find('[data-section=".dacdiemnoibat"]').html());
//     }

//     didScroll = true;

//     if (wTop <= productMenuHeight) {
//         TweenMax.to(menuProduct, 0, { y: 0 });
//         menuProduct.removeClass('scroll-show');
//         menuProduct.removeClass('scroll');
//         menuProduct.removeClass('scroll-bottom');
//     }

//     menuScrollBottom();
// });

// function menuScrollBottom() {
//     var wTop = $(window).scrollTop();
//     var wHeight = $(window).height();
//     var navigationProduct = $('.pr-navigation');
//     var menuProduct = $('.pr-menu');

//     if ((wTop + wHeight) <= (navigationProduct.offset().top + menuProduct.outerHeight())) {
//         if (menuProduct.hasClass('scroll-bottom')) return;
//         menuProduct.addClass('scroll-bottom');
//     } else {
//         menuProduct.removeClass('scroll-bottom');
//     }
// }



// mở rộng thông số kỹ thuật
// product.find('.phienban-title').click(function () {
//     if ($(window).width() > 991) return;

//     var title = $(this);
//     if (title.hasClass('no-expand')) {
//         return false;
//     }

//     var row = title.parent();
//     var slide = row.parents('.phienban-slide');

//     if (!title.hasClass('expand')) {
//         slide.find('.expand').removeClass('expand');
//         var category = row.attr('data-specs');

//         slide.find('[data-specs="' + category + '"]').each(function (index, value) {
//             $(value).find('.phienban-title').addClass('expand');
//             $(value).find('.phienban-content').addClass('expand');
//         });
//     } else {
//         slide.find('.expand').removeClass('expand');
//     }
//     return false;
// })

// slide đặc điểm
// $('.slide-dacdiemnoibat').slick({
//     infinite: false,
//     slidesToShow: 3,
//     arrows: true,
//     speed: 300,
//     useTransform: true,
//     prevArrow: false,
//     nextArrow: false,
//     responsive: [
//       {
//           breakpoint: 992,
//           settings: {
//               centerMode: true,
//               centerPadding: '100px',
//               slidesToShow: 2,
//               initialSlide: 2
//           }
//       },
//       {
//           breakpoint: 480,
//           settings: {
//               centerMode: true,
//               centerPadding: '60px',
//               speed: 300,
//               slidesToShow: 1
//           }
//       }
//     ]
// }); 

// xoay 360
// $(".fancybox").fancybox({
//     openEffect: 'none',
//     closeEffect: 'none',
//     helpers: {
//         title: null
//     }
// });

// imgBegin = true;
// var car;
// $('[aria-controls="cl360"]').click(function () {
//     if (imgBegin) {
//         imgArray = JSON.parse($('#gl_360').attr('data-imgarray'));
//         car = $('#gl_360 .car').ThreeSixty({
//             totalFrames: 16,
//             endFrame: 16,
//             currentFrame: 1,
//             imgList: '.threesixty_images',
//             progress: '.spinner',
//             imgArray: imgArray,
//             filePrefix: '',
//             ext: '.png',
//             height: 292,
//             width: 784,
//             navigation: false,
//             responsive: true,
//             framerate: 8,
//             //disableSpin: true,
//             onReady: function () {
//                 var imgHeight = $('#gl_360 .threesixty .threesixty_images li:eq(0) img').height();
//                 $('#gl_360 .threesixty').css('height', imgHeight);
//             }
//         });
//     }
//     imgBegin = false;
// });

// $('#gl_360 .gl360-btn-prev').click(function (e) {
//     if (car != null) {
//         car.next();
//     }
// });

// $('#gl_360 .gl360-btn-next').click(function (e) {
//     if (car != null) {
//         car.previous();
//     }
// });    
</script>
@endsection
