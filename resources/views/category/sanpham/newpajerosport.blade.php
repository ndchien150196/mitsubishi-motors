@extends("master")
@section("js")
    <script src="{{asset('js/slick.min.js')}}"></script>
@endsection
@section("content") 
<div class="page-product">
    <!--Banner-->
    <div class="pr-banner">
        <picture>
            <!--if IE 9video(style='display: none;')-->
            <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/10/1920X800.jpg" alt="" media="(min-width: 992px)" />
            <!--if IE 9-->
            <img srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/10/750x600.jpg" alt="" class="res-img" />
        </picture>
    </div>
    <!--End - Banner-->

    <!--Navigation-->

    <div class="pr-navigation">
        <div class="pr-menu clearfix" style="transform: matrix(1, 0, 0, 1, 0, 0);">
            <a href="https://www.mitsubishiquangninh.com/" class="home">
                <span class="icon"><i class="svg-icon icon-home"></i></span>
            </a>
            <a href="javascript:;" class="top"> <span>Đầu trang</span><i class="fa fa-caret-up"></i> </a>
            <!-- Dropdown-->
            <div class="dropdown">
                <button type="button" id="dropdownMenuPr" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                    <div class="dd-text">
                        <span class="icon"><i class="svg-icon icon-star smoke"></i></span><span class="text">Đặc điểm nổi bật</span>
                    </div>
                    <div class="caret"><i class="fa fa-caret-down"></i></div>
                </button>
                <ul aria-labelledby="dropdownMenuPr" class="dropdown-menu">
                    <li class="dd-logo hidden-xs hidden-sm">
                        <img src="https://www.mitsubishiquangninh.com/san-pham/new-pajero-sport/" alt="New Pajero Sport" />
                    </li>
                    <li>
                        <a href="javascript:;" data-section=".dacdiemnoibat" data-tracking-click="Feature" data-tracking-click-cat="New Pajero Sport">
                            <span class="icon"><i class="svg-icon icon-star smoke"></i></span><span class="text">Đặc điểm nổi bật</span>
                        </a>
                    </li>

                    <li>
                        <a href="javascript:;" data-section=".thongsokythuat" data-tracking-click="Specification" data-tracking-click-cat="New Pajero Sport">
                            <span class="icon"><i class="svg-icon icon-setting smoke"></i></span><span class="text">Thông số, phiên bản &amp; giá</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-section=".thietke" data-tracking-click="Design" data-tracking-click-cat="New Pajero Sport">
                            <span class="icon"><i class="svg-icon icon-function smoke"></i></span><span class="text">Thiết kế &amp; tính năng</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-section=".thuvien" data-tracking-click="Gallery" data-tracking-click-cat="New Pajero Sport">
                            <span class="icon"><i class="svg-icon icon-gallery smoke"></i></span><span class="text">Thư viện hình ảnh &amp; Catalogue</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:;" data-section=".phukien" data-tracking-click="Accessory" data-tracking-click-cat="New Pajero Sport">
                            <span class="icon"><i class="svg-icon icon-accessories smoke"></i></span><span class="text">Phụ kiện</span>
                        </a>
                    </li>
                </ul>
                <!-- Dropdown menu-->
                <!-- End - Dropdown menu-->
                <!--===================-->
            </div>
            <!-- End - Dropdown-->
            <!--===================-->
        </div>
    </div>

    <!--End - Navigation-->
    <div class="pr-content">
        <!--Highlight-->
        <div class="normal-block dacdiemnoibat">
            <div class="grid-inner">
                <h3 class="title-block"><span>Đặc điểm nổi bật</span></h3>
                <div class="row">
                    <div class="col-md-4 col-lg-3">
                        <div class="pr-info">
                            <h1 class="title">New Pajero Sport</h1>
                            <p class="summary"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--End - Highlight-->
        <!--===================-->

        <!--Specs-->
        <div class="normal-block thongsokythuat">
            <div class="grid-inner">
                <h3 class="title-block pr-title-block">
                    <span>Thông số kỹ thuật</span>
                </h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="tab-content">
                            <div id="cl360" role="tabpanel" class="tab-pane fade">
                                <div id="gl_360" data-imgarray="[]" class="gl-360">
                                    <div class="threesixty car">
                                        <div class="spinner"><span>0%</span></div>
                                        <ol class="threesixty_images"></ol>
                                    </div>
                                    <img
                                        src="./New Pajero Sport _ Mitsubishi Quảng Ninh - Đại lý Mitsubishi Motors tại Việt Nam _ Phân phối xe Mitsubishi Mirage, Attrage, Triton, Outlander Sport, Pajero Sport chính hãng_files/line-360.png"
                                        alt="line-360.png"
                                        class="gl360-line"
                                    />
                                    <div class="gl360-direc">
                                        <a class="gl360-btn-prev">
                                            <i class="fa fa-caret-left"></i>
                                        </a>
                                        <span class="gl360-title">360<sup>o</sup></span>
                                        <a class="gl360-btn-next">
                                            <i class="fa fa-caret-right"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <!--#cl360-->

                            <!--.tab-pane-->
                        </div>
                        <div class="car-color-note text-center">
                            <span>Ghi chú: Hình ảnh minh họa có thể khác với thực tế.</span>
                        </div>
                        <!--.tab-content-->
                        <ul role="tablist" class="nav nav-tabs">
                            <li role="presentation" class="">
                                <a href="https://www.mitsubishiquangninh.com/san-pham/new-pajero-sport/#cl360" class="vehicle-color" aria-controls="cl360" role="tab" data-toggle="tab">
                                    <span class="text">360</span>
                                </a>
                            </li>
                        </ul>
                        <!--ul.nav-tabs-->
                    </div>
                    <div class="col-md-6">
                        <div class="phienban">
                            <div class="phienban-title">Phiên bản:</div>
                            <div class="phienban-select">
                                <ul></ul>
                            </div>
                        </div>
                        <div class="row hidden-md hidden-lg">
                            <div class="col-xs-12 phienban-note">Vuốt ngang để chọn và so sánh giữa các phiên bản</div>
                        </div>
                        <div class="row hidden-xs hidden-sm">
                            <div class="col-xs-12 phienban-note"><a href="javascript:;" data-href="#product_specs" class="view-detail modal-open"></a></div>
                        </div>
                        <div class="phienban-slide slick-initialized slick-slider">
                            <div aria-live="polite" class="slick-list"><div class="slick-track" style="opacity: 1; width: 0px; transform: translate(0px, 0px);" role="listbox"></div></div>
                        </div>
                        <!--.phienban-slide-->
                        <div class="phienban-btn">
                            <ul>
                                <li>
                                    <a
                                        href="javascript:;"
                                        data-href="#product_estimate"
                                        class="btn btn-icon-l modal-open d2-estimate"
                                        data-car-id="139"
                                        data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                        data-step="1"
                                        data-tracking-click="Estimated Cost"
                                        data-tracking-click-cat="New Pajero Sport"
                                    >
                                        <span class="icon"><i class="svg-icon icon-specs icon-specs-cost"></i></span><span class="text">Dự tính chi phí</span>
                                    </a>
                                </li>
                                <li>
                                    <a
                                        href="https://www.mitsubishiquangninh.com/mua-xe/bao-gia-chi-tiet/139/"
                                        class="btn btn-icon-l d2-estimate"
                                        data-car-id="139"
                                        data-tracking-click="Price Quotation By Dealer"
                                        data-tracking-click-cat="New Pajero Sport"
                                        data-step="2"
                                    >
                                        <span class="icon"><i class="svg-icon icon-specs icon-specs-price"></i></span><span class="text">Yêu cầu báo giá</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.mitsubishiquangninh.com/mua-xe/dang-ky-lai-thu/139/" class="btn btn-icon-l">
                                        <span class="icon"><i class="svg-icon icon-specs icon-specs-drive"></i></span><span class="text">Đăng ký lái thử</span>
                                    </a>
                                </li>
                                <li style="display: none;">
                                    <a href="https://www.mitsubishiquangninh.com/dai-ly/" class="btn btn-icon-l">
                                        <span class="icon"><i class="svg-icon icon-specs icon-specs-dealer"></i></span><span class="text">Tìm nhà phân phối</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <!--.chonphienban-->
                    </div>
                </div>
            </div>
        </div>
        <!--End - Specs-->
        <!--===================-->

        <!--End - Accessories-->
        <!--===================-->
        <!--===================-->
    </div>
    <!--.pr-content-->

    <!--===================-->
</div>

@endsection
@section('afftercontent')

<div d2-estimate-popup=""></div>
<!--#product_extimate-->

<section id="product_specs" class="mitsu-modal product-specs">
    <div class="grid-inner">
        <div data-href="#product_specs" class="modal-close"></div>
        <h3 class="title-block pr-title-block">
            <a title="So sánh giữa các phiên bản">So sánh giữa các phiên bản</a>
        </h3>
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <div class="list-steps">
                    <div class="steps-header">
                        <table>
                            <colgroup>
                                <col width="35%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <div class="stepes-pagination">
                                            <ul>
                                                <li><a href="javascript:;" data-href="#st_thongsokythuat" class="stepes-active active">Thông số kỹ thuật</a></li>
                                                <li><a href="javascript:;" data-href="#st_trangthietbi" class="stepes-active">Trang thiết bị</a></li>
                                            </ul>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="st_thongsokythuat" class="stepes step-1 active">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />
                                </colgroup>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                    <div id="st_trangthietbi" class="stepes step-2">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />
                                </colgroup>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                    <br />
                    <br />
                    <p class="text-left">(*) Mức tiêu hao nhiên liệu chứng nhận bởi Cục Đăng Kiểm Việt Nam. Các thông số kỹ thuật có thể thay đổi mà không báo trước</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!--end block modal-->


@endsection
@section('jsfooter')
<script src="{{asset('js/sanpham/TweenMax.min.js')}}"></script>
<script src="{{asset('js/sanpham/ScrollToPlugin.min.js')}}"></script>
<script src="{{asset('js/sanpham/jquery.fancybox.js')}}"></script>
<script src="{{asset('js/new-xpander.js')}}"></script>
<script src="{{asset('js/estimate-price.js')}}"></script>
<script type="text/javascript">
    productInit();
</script>
@endsection
