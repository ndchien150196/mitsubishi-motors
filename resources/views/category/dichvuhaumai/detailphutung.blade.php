@extends("master")
@section("content") 

<div class="second-block page-news-home">
    <div class="grid-fluid">
        <h3 class="title-block"><span>{{$title}}</span></h3>
        <div class="grid-content">
            <div class="row">
                <div class="col-md-8">
                    <div class="spare-detail-des editor">
                        <p></p>
                    </div>

                    <div class="tab-content editor spare-detail-tab-content">
                        <h3>CHỨC NĂNG CỦA NƯỚC LÀM MÁT</h3>
                        <p><strong>Ngăn ngừa sự ăn mòn trong hệ thống làm mát</strong></p>
                        <p>Bằng cách ngăn ngừa sự ăn mòn các chi tiết kim loại trong hệ thống làm mát, nước làm mát sẽ giúp việc trao đổi nhiệt được thực hiện đúng.</p>
                        <p><strong>Ngăn ngừa sự quá nhiệt</strong></p>
                        <p>
                            Khi nước làm mát bốc hơi và giảm xuống, sự quá nhiệt động cơ có thể xảy ra. Nước làm mát có điểm sôi cao nên khó bốc hơi. Đồng thời nước làm mát ngăn ngừa sự hình thành rỉ sét và cặn bên trong động cơ, mà những
                            yếu tố này có thể gây ra hiện tượng quá nhiệt.
                        </p>
                        <p><strong>Chống đông</strong></p>
                        <p>
                            Vào mùa đông, nước làm mát ngăn ngừa hư hỏng bên trong động cơ cũng như sự tắc nghẽn trong hệ thống làm mát. Điểm đông của nước làm mát sẽ thay đổi tùy theo nồng độ dung dịch làm mát. Điểm đông của nước làm mát
                            là âm 15oC khi nồng độ dung dịch làm mát là 30% và âm 35oC khi nồng độ dung dịch ở 50%.
                        </p>
                        <p>
                            <img
                                class="alignnone wp-image-295 size-full aligncenter"
                                src="{{asset('img/img-1.jpg')}}"
                                alt=""
                                width="520"
                                height="304"
                            />
                        </p>
                        <p>Nước làm mát sử dụng dễ dàng cho các loại xe xăng và dầu sản xuất bởi MITSUBISHI MOTORS.</p>
                        <p>Sau khi thay thế toàn bộ, thời gian sử dụng dài khoảng 4 năm hoặc ít nhất là 60.000km.</p>
                        <p>Ngăn ngừa sự ăn mòn các chi tiết kim loại trong hệ thống làm mát trong thời gian dài.</p>
                    </div>
                </div>
                <!-- End dầu nhớt và hóa chất content -->
                <div class="col-md-4">
                    <div class="news-sidebar">
                        <div class="sidebar-block link-block">
                            <h3 class="news-link-title">Dịch vụ hậu mãi</h3>
                            <!-- menu-->
                            <ul class="news-link">
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['gioi-thieu-dich-vu'])}}" title="Giới thiệu dịch vụ" class="sp-text">Giới thiệu dịch vụ</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['chinh-sach-bao-hanh'])}}" title="Chính sách bảo hành" class="sp-text">Chính sách bảo hành</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['bao-duong-dinh-ky'])}}" title="Bảo dưỡng định kỳ" class="sp-text">Bảo dưỡng định kỳ</a>
                                </li>
                                <li class="active">
                                    <a href="{{url('dich-vu-hau-mai',['phu-tung-chinh-hieu'])}}" title="Phụ tùng chính hiệu" class="sp-text">Phụ tùng chính hiệu</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['sua-chua'])}}" title="Sửa chữa" class="sp-text">Sửa chữa</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['dat-lich-bao-duong'])}}" title="Đặt lịch bảo dưỡng" class="sp-text">Đặt lịch bảo dưỡng</a>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-12">
                                <div class="sidebar-block drive-block">
                                    <ul class="drive-link">
                                        <li class="drive-item">
                                            <span class="icon"><i class="svg-icon icon-support"></i></span><span class="text">Hỗ trợ kỹ thuật</span><a href="tel:{{\App\Common::phonedv()}}" title="{{\App\Common::phonedv()}}" class="link">{{\App\Common::phonedv()}}</a>
                                        </li>
                                        <li class="drive-item">
                                            <span class="icon"><i class="svg-icon icon-customer"></i></span><span class="text">Chăm sóc khách hàng</span><a href="tel:{{\App\Common::phonelh()}}" title="{{\App\Common::phonelh()}}" class="link">{{\App\Common::phonelh()}}</a>
                                        </li>
                                        <li class="drive-item">
                                            <span class="icon"><i class="svg-icon icon-email"></i></span><span class="text">Email&nbsp; </span>
                                            <a href="mailto:{{\App\Common::email()}}" title="{{\App\Common::email()}}" class="link mail"> {{\App\Common::email()}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @include('//layouts/block-km')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
