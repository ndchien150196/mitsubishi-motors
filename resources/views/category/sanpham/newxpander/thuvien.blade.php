<!--Gallery-->
<div class="normal-block thuvien">
    <div class="grid-inner">
        <h3 class="title-block pr-title-block"><span>Thư viện hình ảnh &amp; catalogue</span></h3>
        <div class="row">
            <div class="col-md-6 catalogue-download">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2 col-md-offset-1">
                        <picture>
                            <!--if IE 9video(style='display: none;')
                                      -->
                            <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/08/Web-banner-MB.jpg" alt="All New Xpander" media="(min-width: 768px)" />
                            <!--if IE 9-->
                            <img
                                src="{{asset('img/new-xpander/Web-banner-MB.jpg')}}"
                                alt="Outlander Sport"
                            />
                        </picture>
                    </div>
                    <div class="col-sm-6 col-md-7">
                        <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/08/MMV_Xpander_2020_Thongsokythuat.pdf" title="Tải Catalogue" class="btn btn-icon-left" target="_blank">
                            <span class="icon"><i class="svg-icon icon-download"></i></span><span class="text">Tải Catalogue</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="video">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/WyxvZB8Dqw4"
                            frameborder="0"
                            allowfullscreen=""
                        ></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-photo"></div>
    </div>
</div>
<!--End - Gallery-->