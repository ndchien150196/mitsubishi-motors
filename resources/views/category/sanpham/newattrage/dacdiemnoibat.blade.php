<!--Highlight-->
<div class="normal-block dacdiemnoibat">
    <div class="grid-inner">
        <h3 class="title-block"><span>Đặc điểm nổi bật</span></h3>
        <div class="row">
            <div class="col-md-4 col-lg-3">
                <div class="pr-info">
                    <h1 class="title">New Attrage</h1>
                    <p class="summary">
                        Kế thừa những thế mạnh vốn có của một mẫu xe nhập khẩu nguyên chiếc từ Thái Lan: bền bỉ, tin cậy và tiết kiệm nhiên liệu hàng đầu phân khúc, Mitsubishi Attrage mới nay còn được khoác lên mình thiết kế
                        “Dynamic Shield” hiện đại và trẻ trung hơn giúp mang đến cho bạn một khởi đầu vững chắc cho hành trình mới.
                    </p>
                </div>
            </div>

            <!--.pr-info-->
            <div class="col-md-8 col-lg-9">
                <div class="slide-dacdiemnoibat">
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/new-attrage/Bi_LED.png')}}"
                                    alt="Đèn Bi-LED hiện đại"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Bi_LED.png"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">Đèn Bi-LED hiện đại</h4>
                                <p class="summary"></p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/new-attrage/Noi-that-rong-rai.png')}}"
                                    alt="Nội thất rộng rãi, tiện nghi"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Noi-that-rong-rai.png"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">Nội thất rộng rãi, tiện nghi</h4>
                                <p class="summary"></p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/new-attrage/ELL-Top-Feature.jpg')}}"
                                    alt="Tiết kiệm nhiên liệu vượt trội – 5,09L/100Km"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/ELL-Top-Feature.jpg"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">Tiết kiệm nhiên liệu vượt trội – 5,09L/100Km</h4>
                                <p class="summary">(*) Mức tiêu hao nhiên liệu hỗn hợp, chứng nhận bởi Cục Đăng Kiểm Việt Nam</p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <!--.slide-center-->
            </div>
        </div>
    </div>
</div>
<!--End - Highlight-->