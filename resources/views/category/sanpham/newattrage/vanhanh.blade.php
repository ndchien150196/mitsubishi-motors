<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>VẬN HÀNH HIỆU QUẢ</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide90"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="Vận hành hiệu quả" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> Vận hành hiệu quả</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Attrage-Van-hanh.png')}}"
                                    alt="Vận hành hiệu quả"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> Vận hành hiệu quả</span></h3>
                                <div class="opContent">
                                    <p>
                                        Attrage hoàn toàn là một mẫu xe thích hợp cho những khách hàng đặt khả năng tiết kiệm nhiên liệu làm tiêu chí hàng đầu khi mua xe. Động cơ MIVEC 1.2L kết hợp hoàn hảo với hợp số tự động vô cấp
                                        INVECS-III, cùng trọng lượng thân xe nhẹ, giúp cho Attrage đạt được mức tiêu thụ nhiên liệu tối ưu hàng đầu trong phân khúc​
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide91" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Động cơ 1.2L MIVEC" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Động cơ 1.2L MIVEC</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Động-cơ.jpg')}}"
                                    alt="Động cơ 1.2L MIVEC"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Động cơ 1.2L MIVEC</span></h3>
                                <div class="opContent">
                                    <p>Động cơ 1.2L thế hệ mới với công nghệ MIVEC tiên tiến giúp cải thiện công suất và mô-men xoắn, cho khả năng tăng tốc đáng ngạc nhiên và tiết kiệm nhiên liệu vượt trội.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide92" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Hộp số CVT INVECS-III" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Hộp số CVT INVECS-III</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/hop-so-cvt-invecs-iii2.jpg')}}"
                                    alt="Hộp số CVT INVECS-III"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Hộp số CVT INVECS-III</span></h3>
                                <div class="opContent">
                                    <p>
                                        Hộp số CVT cho phép chuyển số êm dịu và giảm tiêu hao nhiên liệu. Công nghệ điều khiển hộp số thông minh INVECS-III với khả năng ghi nhớ thao tác người lái sẽ đưa ra chương trình sang số phù
                                        hợp giúp nâng cao cảm giác lái và tiết kiệm nhiên liệu.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide93" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống ga tự động (Cruise Control)" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Hệ thống ga tự động (Cruise Control)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Cruise-Control.jpg')}}"
                                    alt="Hệ thống ga tự động (Cruise Control)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Hệ thống ga tự động (Cruise Control)</span></h3>
                                <div class="opContent">
                                    <p>Giúp gia tăng cảm giác lái của tài xế ở các khoảng tốc độ khác nhau. Đồng thời, giúp nâng cao độ ổn định của xe khi chạy tốc độ cao.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide94" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Bán kính quay vòng nhỏ" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Bán kính quay vòng nhỏ</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Ban-kinh-quay-vong.jpg')}}"
                                    alt="Bán kính quay vòng nhỏ"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Bán kính quay vòng nhỏ</span></h3>
                                <div class="opContent">
                                    <p>Việc xoay trở xe trong đô thị trở nên dễ dàng hơn rất nhiều đối với Attrage nhờ vào bán kính quay vòng chỉ 4,8m.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide95" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Khoảng sáng gầm xe 170mm" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Khoảng sáng gầm xe 170mm</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Attrage-Khoang-sang-gam.png')}}"
                                    alt="Khoảng sáng gầm xe 170mm"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Khoảng sáng gầm xe 170mm</span></h3>
                                <div class="opContent">
                                    <p>Khoảng sáng gầm tăng lên 170mm, cho khả năng vượt địa hình tốt trên cả những đoạn đường gồ ghề.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Attrage-Van-hanh.png')}}"
                        alt="Vận hành hiệu quả"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Vận hành hiệu quả</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Attrage hoàn toàn là một mẫu xe thích hợp cho những khách hàng đặt khả năng tiết kiệm nhiên liệu làm tiêu chí hàng đầu khi mua xe. Động cơ MIVEC 1.2L kết hợp hoàn hảo với hợp số tự động vô cấp INVECS-III,
                            cùng trọng lượng thân xe nhẹ, giúp cho Attrage đạt được mức tiêu thụ nhiên liệu tối ưu hàng đầu trong phân khúc​
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Động-cơ.jpg')}}"
                        alt="Động cơ 1.2L MIVEC"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Động cơ 1.2L MIVEC</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Động cơ 1.2L thế hệ mới với công nghệ MIVEC tiên tiến giúp cải thiện công suất và mô-men xoắn, cho khả năng tăng tốc đáng ngạc nhiên và tiết kiệm nhiên liệu vượt trội.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/hop-so-cvt-invecs-iii2.jpg')}}"
                        alt="Hộp số CVT INVECS-III"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hộp số CVT INVECS-III</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Hộp số CVT cho phép chuyển số êm dịu và giảm tiêu hao nhiên liệu. Công nghệ điều khiển hộp số thông minh INVECS-III với khả năng ghi nhớ thao tác người lái sẽ đưa ra chương trình sang số phù hợp giúp nâng
                            cao cảm giác lái và tiết kiệm nhiên liệu.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Cruise-Control.jpg')}}"
                        alt="Hệ thống ga tự động (Cruise Control)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống ga tự động (Cruise Control)</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Giúp gia tăng cảm giác lái của tài xế ở các khoảng tốc độ khác nhau. Đồng thời, giúp nâng cao độ ổn định của xe khi chạy tốc độ cao.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Ban-kinh-quay-vong.jpg')}}"
                        alt="Bán kính quay vòng nhỏ"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Bán kính quay vòng nhỏ</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Việc xoay trở xe trong đô thị trở nên dễ dàng hơn rất nhiều đối với Attrage nhờ vào bán kính quay vòng chỉ 4,8m.</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Attrage-Khoang-sang-gam.png')}}"
                        alt="Khoảng sáng gầm xe 170mm"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khoảng sáng gầm xe 170mm</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Khoảng sáng gầm tăng lên 170mm, cho khả năng vượt địa hình tốt trên cả những đoạn đường gồ ghề.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>