<!-- floating menu -->
<ul class="float-menu-block">
    <li>
        <a href="{{url('bang-gia')}}" alt="Bảng giá" class="left-icon-item">
            <span class="icon"><i class="svg-icon icon-price smoke"></i></span>
            <span class="text">Bảng giá</span>
        </a>
    </li>
    <li>
        <a href="{{asset('tin-tuc/tin-khuyen-mai')}}" alt="Khuyến mãi" class="left-icon-item">
            <span class="icon"><i class="svg-icon icon-promotion smoke"></i></span>
            <span class="text">Khuyến mãi</span>
        </a>
    </li>
    <li>
        <a href="{{url('mua-xe',['dang-ky-lai-thu'])}}" alt="" class="left-icon-item">
            <span class="icon"><i class="svg-icon icon-drive smoke"></i></span>
            <span class="text">Đăng ký lái thử</span>
        </a>
    </li>
</ul>
<!-- end floating menu -->
