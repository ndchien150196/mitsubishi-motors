<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class CategoryController 
{
    public function dangky($value)
    {
        $product = Product::pluck('amount','name');
    	$name = '';
    	if ($value == 'Xpander-Cross' || $value == 116) {
            $name = 'Xpander Cross';
    		$title = 'Xpander Cross';
    		$image = '/img/menu/Orange.jpg';
    	}
    	if ($value == 'NEW-PAJERO-SPORT' || $value == 139) {
            $title = 'NEW PAJERO SPORT';
    		$name = 'NEW PAJERO SPORT';
    		$image = '/img/menu/Pajero-sport.png';
    	}
    	if ($value == 'NEW-XPANDER' || $value == 132) {
            $title = 'NEW XPANDER';
    		$name = 'NEW XPANDER';
    		$image = '/img/menu/Xpander-Thumbnail.png';
    	}
    	if ($value == 'New-Attrage' || $value == 122) {
            $name = 'New Attrage';
    		$title = 'New Attrage';
    		$image = '/img/menu/PNG-ELL.png';
    	}
    	if ($value == 'New-Outlander' || $value == 121) {
            $name = 'New Outlander';
    		$title = 'New Outlander';
    		$image = '/img/menu/19OL_Gene_LHD_02white_FR_png.png';
    	}
    	if ($value == 'New-Triton' || $value == 22) {
            $name = 'New Triton';
    		$title = 'New Triton';
    		$image = '/img/menu/Triton1.png';
    	}
    	if (strlen($name) == 0) {
            return response()->view('errors', ['title' => 'Lỗi','product' => $product], 404);
    	}
    	return view('category.muaxe.dangkylaithu',compact('name','image','title','product'));
    }

    public function baogia($value)
    {
        $product = Product::pluck('amount','name');
    	$name = '';
    	if ($value == 'Xpander-Cross' || $value == 116) {
            $name = 'Xpander Cross';
    		$title = 'Xpander Cross';
    		$image = '/img/menu/Orange.jpg';
    	}
    	if ($value == 'NEW-PAJERO-SPORT' || $value == 139) {
            $name = 'NEW PAJERO SPORT';
    		$title = 'NEW PAJERO SPORT';
    		$image = '/img/menu/Pajero-sport.png';
    	}
    	if ($value == 'NEW-XPANDER' || $value == 132) {
            $name = 'NEW XPANDER';
    		$title = 'NEW XPANDER';
    		$image = '/img/menu/Xpander-Thumbnail.png';
    	}
    	if ($value == 'New-Attrage' || $value == 122) {
            $title = 'New Attrage';
    		$name = 'New Attrage';
    		$image = '/img/menu/PNG-ELL.png';
    	}
    	if ($value == 'New-Outlander' || $value == 121) {
            $name = 'New Outlander';
    		$title = 'New Outlander';
    		$image = '/img/menu/19OL_Gene_LHD_02white_FR_png.png';
    	}
    	if ($value == 'New-Triton' || $value == 22) {
            $name = 'New Triton';
    		$title = 'New Triton';
    		$image = '/img/menu/Triton1.png';
    	}
    	if (strlen($name) == 0) {
            return response()->view('errors', ['title' => 'Lỗi','product' => $product], 404);
    	}
    	return view('category.muaxe.baogiachitiet',compact('name','image','title','product'));
    }

    public function sanpham($value)
    {
        $product = Product::pluck('amount','name');
    	if ($value == 'xpander-cross') {
            $title = 'Xpander Cross';
    		return view('category.sanpham.xpandercross',compact('title','product'));
    	}elseif($value == 'new-xpander'){
            $title = 'NEW XPANDER';
            return view('category.sanpham.newxpander',compact('title','product'));
    	}elseif($value == 'new-pajero-sport'){
            $title = 'NEW PAJERO SPORT';
            return view('category.sanpham.newpajerosport',compact('title','product'));
        }elseif($value == 'new-attrage'){
            $title = 'New Attrage';
            return view('category.sanpham.newattrage',compact('title','product'));
    	}elseif($value == 'new-outlander'){
            $title = 'New Outlander';
            return view('category.sanpham.newoutlander',compact('title','product'));
    	}elseif($value == 'newtriton'){
            $title = 'New Triton';
            return view('category.sanpham.newtriton',compact('title','product'));
    	}else{
            return response()->view('errors', ['title' => 'Lỗi','product' => $product], 404);
    	}
    }

}
