<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>AN TOÀN</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide110"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="An toàn vượt trội với khung xe RISE thép gia cường" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> An toàn vượt trội với khung xe RISE thép gia cường</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Rise.jpg')}}"
                                    alt="An toàn vượt trội với khung xe RISE thép gia cường"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> An toàn vượt trội với khung xe RISE thép gia cường</span></h3>
                                <div class="opContent">
                                    <p>Công nghệ khung xe RISE của Mitsubishi sử dụng thép gia cường, được thiết kế để hấp thu va chạm và phân tán lực để bảo vệ tài xế và hành khách.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide111" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống phanh ABS – EBD" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Hệ thống phanh ABS – EBD</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/ABS-EBD.jpg')}}"
                                    alt="Hệ thống phanh ABS – EBD"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Hệ thống phanh ABS – EBD</span></h3>
                                <div class="opContent">
                                    <p>Các hệ thống phanh an toàn ABS, EBD kết hợp cùng khung xe chắc chắn mang lại khả năng an toàn chủ động vượt trội cho Xpander</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide112" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Hỗ trợ khởi hành ngang dốc (HSA)" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Hỗ trợ khởi hành ngang dốc (HSA)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/HSA1.jpg')}}"
                                    alt="Hỗ trợ khởi hành ngang dốc (HSA)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Hỗ trợ khởi hành ngang dốc (HSA)</span></h3>
                                <div class="opContent">
                                    <p>
                                        Hệ thống hỗ trợ khởi hành ngang dốc (Hill Start Assist – HSA) giúp xe không bị trôi về phía sau trong trường hợp dừng và khởi hành ở ngang dốc cao. Đặc biệt, Mitsubishi Motors trang bị hệ
                                        thống này trên cả phiên bản số sàn.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide113" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống hỗ trợ lực phanh khẩn cấp BA" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Hệ thống hỗ trợ lực phanh khẩn cấp BA</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/BA.jpg')}}"
                                    alt="Hệ thống hỗ trợ lực phanh khẩn cấp BA"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Hệ thống hỗ trợ lực phanh khẩn cấp BA</span></h3>
                                <div class="opContent">
                                    <p>
                                        Hệ thống hỗ trợ phanh khẩn cấp giúp tăng cường lực phanh cho người lái trong trường hợp đạp phanh khẩn cấp, đảm bảo an toàn cho người và xe.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide114" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống cân bằng điện tử ASC" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Hệ thống cân bằng điện tử ASC</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/ASC1.jpg')}}"
                                    alt="Hệ thống cân bằng điện tử ASC"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Hệ thống cân bằng điện tử ASC</span></h3>
                                <div class="opContent">
                                    <p>
                                        Hệ thống cân bằng điện tử (ASC) sử dụng các cảm biến để phân tích chuyển động và độ trượt của xe. Bằng cách kiểm soát công suất động cơ và lực phanh lên từng bánh xe riêng biệt, hệ thống ASC
                                        giúp duy trì sự ổn định của xe ngay cả trong điểu kiện trơn trượt.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide115" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống kiểm soát lực kéo (TCL)" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Hệ thống kiểm soát lực kéo (TCL)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/TCL.jpg')}}"
                                    alt="Hệ thống kiểm soát lực kéo (TCL)"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li><span>AT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Hệ thống kiểm soát lực kéo (TCL)</span></h3>
                                <div class="opContent">
                                    <p>
                                        Giúp tăng sự ổn định của xe và kiểm soát xe tốt hơn trong điều kiện thời tiết bất lợi và thiếu lực kéo.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="6" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide116" data-slick-index="6" aria-hidden="true">
                        <a href="javascript:;" title="Cảnh báo phanh khẩn cấp (ESS)" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">6. Cảnh báo phanh khẩn cấp (ESS)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/ESS1.jpg')}}"
                                    alt="Cảnh báo phanh khẩn cấp (ESS)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">6. Cảnh báo phanh khẩn cấp (ESS)</span></h3>
                                <div class="opContent">
                                    <p>Khi ABS được kích hoạt, đèn cảnh báo nguy hiểm sẽ tự động bật để báo hiệu cho các xe đi sau.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Rise.jpg')}}"
                        alt="An toàn vượt trội với khung xe RISE thép gia cường"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">An toàn vượt trội với khung xe RISE thép gia cường</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Công nghệ khung xe RISE của Mitsubishi sử dụng thép gia cường, được thiết kế để hấp thu va chạm và phân tán lực để bảo vệ tài xế và hành khách.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/ABS-EBD.jpg')}}"
                        alt="Hệ thống phanh ABS – EBD"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống phanh ABS – EBD</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Các hệ thống phanh an toàn ABS, EBD kết hợp cùng khung xe chắc chắn mang lại khả năng an toàn chủ động vượt trội cho Xpander</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/HSA1.jpg')}}"
                        alt="Hỗ trợ khởi hành ngang dốc (HSA)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hỗ trợ khởi hành ngang dốc (HSA)</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Hệ thống hỗ trợ khởi hành ngang dốc (Hill Start Assist – HSA) giúp xe không bị trôi về phía sau trong trường hợp dừng và khởi hành ở ngang dốc cao. Đặc biệt, Mitsubishi Motors trang bị hệ thống này trên
                            cả phiên bản số sàn.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/BA.jpg')}}"
                        alt="Hệ thống hỗ trợ lực phanh khẩn cấp BA"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống hỗ trợ lực phanh khẩn cấp BA</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Hệ thống hỗ trợ phanh khẩn cấp giúp tăng cường lực phanh cho người lái trong trường hợp đạp phanh khẩn cấp, đảm bảo an toàn cho người và xe.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/ASC1.jpg')}}"
                        alt="Hệ thống cân bằng điện tử ASC"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống cân bằng điện tử ASC</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Hệ thống cân bằng điện tử (ASC) sử dụng các cảm biến để phân tích chuyển động và độ trượt của xe. Bằng cách kiểm soát công suất động cơ và lực phanh lên từng bánh xe riêng biệt, hệ thống ASC giúp duy trì
                            sự ổn định của xe ngay cả trong điểu kiện trơn trượt.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/TCL.jpg')}}"
                        alt="Hệ thống kiểm soát lực kéo (TCL)"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li><span>AT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống kiểm soát lực kéo (TCL)</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Giúp tăng sự ổn định của xe và kiểm soát xe tốt hơn trong điều kiện thời tiết bất lợi và thiếu lực kéo.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="6" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/ESS1.jpg')}}"
                        alt="Cảnh báo phanh khẩn cấp (ESS)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Cảnh báo phanh khẩn cấp (ESS)</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Khi ABS được kích hoạt, đèn cảnh báo nguy hiểm sẽ tự động bật để báo hiệu cho các xe đi sau.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>