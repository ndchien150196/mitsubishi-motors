@extends("app")
@section('css')
<script src="//cdn.ckeditor.com/4.15.1/full/ckeditor.js"></script>
@endsection

@section("content")
<div class="content">
	<div class="row">
		<form method="POST" enctype="multipart/form-data" id="upload-form">
			@csrf	
			<div class="col-md-8 col-12">
				<label>Nội Dung</label>
				<textarea id="content" name="content">{{$tintuc->content}}</textarea>
			</div>
			<div class="col-md-4 col-12">
				<div class="form-group">
					<label>Tiêu Đề</label>
					<textarea class="form-control" id="title" name="title">{{$tintuc->title}}</textarea>
		            <input id="id" name="id" type="text" class="hidden" value="{{$tintuc->id}}">
	            </div>
	            <div>
	                <img id="image_preview_container" src="{{ $tintuc->avatar }}" alt="preview image" style="max-height: 150px;">
	            </div>
	            <div class="form-group">
				    <label for="image">Ảnh Đại Diện</label>
				    <input type="file" name="image" placeholder="Chọn Ảnh" id="image">
				</div>
	            <div class="form-group">
	                <label for="type">Chuyên Mục</label>
	                <div class="radio">
	                    <label><input type="radio" name="category" @if($tintuc->category === 'su-kien-noi-bat') checked @endif value="su-kien-noi-bat">Sự Kiện Nổi Bật</label><br/>
	                    <label><input type="radio" name="category" @if($tintuc->category === 'tin-khuyen-mai') checked @endif value="tin-khuyen-mai">Tin Khuyến Mại</label><br/>
	                    <label><input type="radio" name="category" @if($tintuc->category === 'tin-tong-hop') checked @endif value="tin-tong-hop">Tin Tổng Hợp</label><br/>
	                    <label><input type="radio" name="category" @if($tintuc->category === 'tin-tuyen-dung') checked @endif value="tin-tuyen-dung">Tin Tuyển Dụng</label>
	                </div>
	            </div>
	            <div class="form-group">
	                <label for="type">Trạng Thái</label>
	                <div class="radio">
	                    <label><input type="radio" name="status" @if($tintuc->status == 0) checked @endif value="0">Nháp / Gỡ Bài</label><br/>
	                    <label><input type="radio" name="status" @if($tintuc->status == 1) checked @endif value="1">Xuất Bản</label>
	                </div>
	            </div>
				<br/>
				<div class="form-group pull-right">
					<button type="submit" class="btn btn-success">Sửa Bài</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection
@section("js")
<script type="text/javascript">
	// editor
	CKEDITOR.replace('content', {
	    filebrowserUploadUrl: "{{route('admin.tintuc.uploadimage', ['_token' => csrf_token() ])}}",
        filebrowserUploadMethod: 'form'
	});
	
    $("#image").change(function () {
        let reader = new FileReader();

        reader.onload = (e) => {
            $("#image_preview_container").attr("src", e.target.result);
        };

        reader.readAsDataURL(this.files[0]);
    });

    $("#upload-form").submit(function (e) {
        e.preventDefault();
        let formData = new FormData(this);
        let edit= '{{ route("admin.tintuc.edit") }}';
		formData.append('content', CKEDITOR.instances['content'].getData());

        $.ajax({
            type: "POST",
            url: edit,
            data: formData,
            contentType: false,
            processData: false,
            success: (data) => {
                $.bootstrapGrowl(data.message, { type: 'success' });
	            setTimeout(function(){
		           window.location.href = '/admin/tin-tuc'; 
				}, 200); 
            },
            error: function (response) {
            	$.each(response.responseJSON.errors, function(i, item) {
				    $.bootstrapGrowl(item, { type: 'error' });
				});
            },
        });
    });

</script>

@endsection
