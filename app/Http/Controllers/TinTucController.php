<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\TinTuc;
use App\Models\Slide;
use App\Models\Product;
use App\Models\FileManager;

class TinTucController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $name = "Tin Tức";
        $action ="Danh sách";
        $tintuc = TinTuc::orderBy('created_at','desc')->get();
        return view('admin.tintuc.list',compact('name','action','tintuc'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $name = "Tin Tức";
        $action ="Thêm";
        return view('admin.tintuc.add',compact('name','action'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,
            [
                'title' => 'required',
                'content' => 'required',
                'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],

            [
                'required' => ':attribute Không được để trống',
            ],

            [
                'title' => 'Tiêu đề',
                'content' => 'Nội Dung',
                'image' => 'Hình Ảnh',
            ]

        );
        $tintuc = new TinTuc;
        $tintuc->user_id = \Auth::user()->id;
        $tintuc->category = $request->category;
        $tintuc->slug = $this->convertslug($request->title);
        $tintuc->title = $request->title;
        $tintuc->content = $request->content;
        $tintuc->status = $request->status;
        $file = $request->image;

        if (is_file($file)) {
            $originName = $request->file('image')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('image')->move(public_path('images'), $fileName);
            $url = asset('images/'.$fileName); 
            $tintuc->avatar = $url;
        }

        $tintuc->save();

        return response()->json([
            'status' => true,
            'message' => 'Thêm Thành Công'
        ], 200);
    }

    public function convertslug($value)
    {
        $str = trim(mb_strtolower($value));
        $str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
        $str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
        $str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
        $str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
        $str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
        $str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
        $str = preg_replace('/(đ)/', 'd', $str);
        $str = preg_replace('/[^a-z0-9-\s]/', '', $str);
        $str = preg_replace('/([\s]+)/', '-', $str);
        return $str;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $tintuc = TinTuc::where('slug',$request->slug)->first();
        $name = "Tin Tức";
        $action ="Sửa";
        return view('admin.tintuc.sua',compact('name','action','tintuc'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        // dd($request->all());
        $this->validate($request,
            [
                'title' => 'required',
                'content' => 'required',
                'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ],

            [
                'required' => ':attribute Không được để trống',
            ],

            [
                'title' => 'Tiêu đề',
                'content' => 'Nội Dung',
                'image' => 'Hình Ảnh',
            ]

        );
        $tintuc = TinTuc::findOrFail($request->id);
        $tintuc->user_id = \Auth::user()->id;
        $tintuc->category = $request->category;
        $tintuc->slug = $this->convertslug($request->title);
        $tintuc->title = $request->title;
        $tintuc->content = $request->content;
        $tintuc->status = $request->status;
        $file = $request->image;

        if (is_file($file)) {
            $originName = $request->file('image')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('image')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('image')->move(public_path('images'), $fileName);
            $url = asset('images/'.$fileName); 
            $tintuc->avatar = $url;
        }

        $tintuc->save();

        return response()->json([
            'status' => true,
            'message' => 'Sửa Thành Công'
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request)
    {
        TinTuc::where('slug',$request->slug)->delete();
        return redirect()->back();
    }

    public function uploadimage(Request $request)
    {
        if($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('upload')->move(public_path('images'), $fileName);
   
            $CKEditorFuncNum = $request->input('CKEditorFuncNum');
            $url = asset('images/'.$fileName); 
            $msg = 'Tải Thành Công'; 
            $response = "<script>window.parent.CKEDITOR.tools.callFunction($CKEditorFuncNum, '$url', '$msg')</script>";
               
            @header('Content-type: text/html; charset=utf-8'); 
            echo $response;
        }
    }

    public function uploadfile(Request $request)
    {
        $file = $request->upload_file;
       
        if (is_file($file)) {
            $new_name = rand().'.'.$file->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('editor', $file, $new_name);
            $img = new FileManager;
            $img->url = '/storage/editor/'.$new_name;
            $img->link = '/storage/editor/'.$new_name;
            $img->thumb = '/storage/editor/'.$new_name;
            $img->save();
        }
        return response()->json([
            'link' => '/storage/editor/'.$new_name,
        ]);
    }

    public function tuctuc($category)
    {
        $tintuc = TinTuc::where('category',$category)->get();
        return view('category.tintuc',compact('tintuc'));
    }

    public function slides()
    {
        $name = "Slide";
        $action ="Danh sách";
        $slide = Slide::orderBy('created_at','desc')->get();
        return view('admin.slide',compact('name','action','slide'));
    }

    public function slideadd(Request $request)
    {
        $data = new Slide;
        $data->name = $request->name;
        $data->link = $request->link;
        $file = $request->file;
        if (is_file($file)) {
            $originName = $request->file('file')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('file')->getClientOriginalExtension();
            $fileName = $fileName.'_'.time().'.'.$extension;
        
            $request->file('file')->move(public_path('images/slide'), $fileName);
            $url = asset('images/slide/'.$fileName); 
            $data->image = $url;
        }
        $data->save();
        return redirect()->back();

    }

    public function destroy(Request $request)
    {
        Slide::where('id',$request->id)->delete();
        return redirect()->back();
    }

    public function cars()
    {
        $name = "Giá Xe";
        $action ="Danh sách";
        $car = Product::orderBy('created_at','desc')->get();
        return view('admin.car',compact('name','action','car'));
    }

    public function caredit(Request $request)
    {
        $data = Product::findOrFail($request->id);
        $data->amount = $request->amount;
        $data->save();
        return response()->json([
            'status' => true,
            'message' => 'Thêm Thành Công'
        ], 200);
    }
}
