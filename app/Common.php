<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Cache;


class Common{

    public static function category($value){
        if ($value == 'su-kien-noi-bat') {
        	return 'Sự Kiện Nổi Bật';
        }
        if ($value == 'tin-khuyen-mai') {
        	return 'Tin Khuyến Mại';
        }
        if ($value == 'tin-tong-hop') {
        	return 'Tin Tổng Hợp';
        }
        if ($value == 'tin-tuyen-dung') {
        	return 'Tin Tuyển Dụng';
        }
    }

    public static function resethtml($value)
    {
        return strip_tags($value,'<p>');
    }

    public static function chensaotitle($value,$category)
    {
        if ($category == 'tin-khuyen-mai' || $category == 'tin-tong-hop') {
            return '💥'.$value.'💥';
        }else{
            return $value;
        }
    }

    public static function phonekd()
    {
        return '';
    }

    public static function phonelh()
    {
        return '1900986855';
    }

    public static function phonedv()
    {
        return '';
    }

    public static function email()
    {
        return '';
    }

    public static function fanpage()
    {
        return '';
    }

    public static function youtube()
    {
        return '';
    }
}
