<!-- footer -->
<footer id="footer" class="footer">
    <div class="footer-satsco hidden-xs">
        <div class="grid-inner">
            <div class="img">
                <img src="/" alt="" />
            </div>
            <div class="text"><strong>CÔNG TY CỔ PHẦN Ô TÔ HƯNG THỊNH PHÁT</strong> 140 Quang Trung, phường Quang Trung,TP Thái Bình</div>
        </div>
    </div>

    <div class="footer-top">
        <div class="grid-inner">
            <ul class="footer-top-list">
                <li>
                    <a href="{{asset('bang-gia')}}" title="Bảng giá xe" class="left-icon-link">
                        <span class="icon">
                            <i class="svg-icon svg-icon icon-price red red"></i>
                        </span>
                        <span class="text">Bảng giá xe</span>
                    </a>
                </li>
                <li>
                    <a href="{{asset('khuyen-mai')}}" title="Khuyến mãi" class="left-icon-link">
                        <span class="icon">
                            <i class="svg-icon svg-icon icon-promotion red red"></i>
                        </span>
                        <span class="text">Khuyến mãi</span>
                    </a>
                </li>
                <li>
                    <a href="{{asset('mua-xe/dang-ky-lai-thu')}}" title="Đăng ký lái thử" class="left-icon-link">
                        <span class="icon">
                            <i class="svg-icon svg-icon icon-drive red red"></i>
                        </span>
                        <span class="text">Đăng ký lái thử</span>
                    </a>
                </li>
            </ul>

            <div class="footer-top-contact">
                <div class="left-icon-link link-satsco hidden-sm hidden-md hidden-lg">
                    <div class="icon"></div>
                    <div class="text">
                        <span>MITSUBISHI THÁI BÌNH</span>
                        <strong><span>140 Quang Trung, phường Quang Trung,TP Thái Bình</span></strong>
                    </div>
                </div>
                <div class="left-icon-link">
                    <div class="icon">
                        <i class="svg-icon icon-phone-small"></i>
                    </div>
                    <div class="text">
                        <span>HOTLINE 24/7</span>
                        <strong><span>Liên Hệ:</span> <a href="tel:{{\App\Common::phonelh()}}">{{\App\Common::phonelh()}}</a></strong>
                    </div>
                </div>
            </div>

            <ul class="social-box hidden-xs">
                <li>
                    <a href="{{\App\Common::fanpage()}}" title="Facebook" class="icon-social facebook" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                </li>
                <li>
                    <a href="{{\App\Common::youtube()}}" title="Youtube" class="icon-social youtube" target="_blank">
                        <i class="fa fa-youtube-play"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <!-- Histats.com  START  (aync)-->
    <!-- phần mềm chống kích tặc -->
    <!-- Histats.com  END  -->
    <div class="footer-middle hidden-xs">
        <div class="grid-inner">
            <div class="footer-middle-col">
                <a href="/">
                    <h3 class="bold-title">TRANG CHỦ</h3>
                </a>
                <ul class="normal-list">
                    <li><a href="{{url('gioi-thieu')}}" title="Giới thiệu">Giới thiệu</a></li>
                    <li><a href="{{url('bang-gia')}}" title="Bảng giá">Bảng giá</a></li>
                    <li><a href="{{url('khuyen-mai')}}" title="Khuyến mãi">Khuyến mãi</a></li>
                    <li><a href="{{url('lien-he')}}" title="Liên hệ">Liên hệ</a></li>
                </ul>
            </div>
            <div class="footer-middle-col">
                <h3 class="bold-title">SẢN PHẨM</h3>
                <ul class="normal-list">
                    <li>
                        <a target="_blank" href="{{asset('san-pham/xpander-cross')}}" title="Xpander Cross">
                            Xpander Cross
                        </a>
                    </li>
                    <li><a target="_blank" href="http://newpajerosport.vn/?utm_source=Ha%20Noi%20Auto&amp;utm_medium=Click&amp;utm_campaign=Launch&amp;utm_term=&amp;utm_content=menu" title="New Pajero Sport">New Pajero Sport</a></li>
                    <li><a target="" href="{{asset('san-pham/new-xpander')}}" title="New Xpander">New Xpander</a></li>
                    <li><a target="" href="{{asset('san-pham/new-attrage')}}" title="New Attrage">New Attrage</a></li>
                    <li><a target="" href="{{asset('san-pham/new-outlander')}}" title="New Outlander">New Outlander</a></li>
                    <li><a target="" href="{{asset('san-pham/newtriton')}}" title="New Triton">New Triton</a></li>
                </ul>
            </div>
            <div class="footer-middle-col">
                <h3 class="bold-title">MUA XE</h3>
                <ul class="normal-list">
                    <li><a href="{{url('mua-xe',['mua-xe-tra-gop'])}}" title="Mua xe trả góp">Mua xe trả góp</a></li>
                    <li><a href="{{url('mua-xe',['quy-trinh-mua-xe'])}}" title="Quy trình mua xe ">Quy trình mua xe </a></li>
                    <li><a href="{{url('mua-xe',['dang-ky-lai-thu'])}}" title="Đăng ký lái thử">Đăng ký lái thử</a></li>
                    <li><a href="{{url('mua-xe',['bao-gia-chi-tiet'])}}" title="Báo giá chi tiết">Báo giá chi tiết</a></li>
                </ul>
            </div>
            <div class="footer-middle-col">
                <h3 class="bold-title">DỊCH VỤ HẬU MÃI</h3>
                <ul class="normal-list">
                    <li><a href="{{url('dich-vu-hau-mai',['gioi-thieu-dich-vu'])}}" title="Giới thiệu dịch vụ">Giới thiệu dịch vụ</a></li>
                    <li><a href="{{url('dich-vu-hau-mai',['bao-duong-dinh-ky'])}}" title="Bảo dưỡng định kỳ">Bảo dưỡng định kỳ</a></li>
                    <li><a href="{{url('dich-vu-hau-mai',['sua-chua'])}}" title="Sửa chữa">Sửa chữa</a></li>
                    <li><a href="{{url('dich-vu-hau-mai',['cuu-ho-giao-thong'])}}" title="Cứu hộ giao thông">Cứu hộ giao thông</a></li>
                    <li><a href="{{url('dich-vu-hau-mai',['chinh-sach-bao-hanh'])}}" title="Chính sách bảo hành">Chính sách bảo hành</a></li>
                    <li><a href="{{url('dich-vu-hau-mai',['phu-tung-chinh-hieu'])}}" title="Phụ tùng chính hiệu">Phụ tùng chính hiệu</a></li>
                    <li><a href="{{url('dich-vu-hau-mai',['dat-lich-bao-duong'])}}" title="Đặt lịch bảo dưỡng">Đặt lịch bảo dưỡng</a></li>
                </ul>
            </div>
            <div class="footer-middle-col">
                <h3 class="bold-title">TIN TỨC</h3>
                <ul class="normal-list">
                    <li><a href="{{asset('tin-tuc/su-kien-noi-bat')}}" title="Sự kiện nổi bật">Sự kiện nổi bật</a></li>
                    <li><a href="{{asset('tin-tuc/tin-khuyen-mai')}}" title="Tin khuyến mãi">Tin khuyến mãi</a></li>
                    <li><a href="{{asset('tin-tuc/tin-tong-hop')}}" title="Tin tổng hợp">Tin tổng hợp</a></li>
                    <li><a href="{{asset('tin-tuc/tin-tuyen-dung')}}" title="Tin tuyển dụng">Tin tuyển dụng</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="inner">
            <div class="copyright">©2020 Bản quyền thuộc về Mitsubishi Motors Vietnam Co.,Ltd.</div>
        </div>
    </div>
</footer>
<!-- end footer -->
