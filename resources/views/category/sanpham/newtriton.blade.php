@extends("master")
@section("js")
    <script src="{{asset('js/slick.min.js')}}"></script>
@endsection
@section("content") 
<div class="page-product">
    @include('category.sanpham.newtriton.banner')
    @include('category.sanpham.xpandercross.navigation')
    
    <div class="pr-content">
        @include('category.sanpham.newtriton.dacdiemnoibat')
        @include('category.sanpham.newtriton.thongsokythuat')
        @include('category.sanpham.newtriton.ngoaithat')
        @include('category.sanpham.newtriton.noithatrongrai')
        @include('category.sanpham.newtriton.vanhanh')
        @include('category.sanpham.newtriton.antoan')
        @include('category.sanpham.newtriton.thuvien')
        
    </div>
    <!--.pr-content-->

    <!--===================-->
</div>
@endsection
@section('afftercontent')
<div d2-estimate-popup=""></div>
<!--#product_extimate-->

<section id="product_specs" class="mitsu-modal product-specs">
    <div class="grid-inner">
        <div data-href="#product_specs" class="modal-close"></div>
        <h3 class="title-block pr-title-block">
            <a title="So sánh giữa các phiên bản">So sánh giữa các phiên bản</a>
        </h3>
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <div class="list-steps">
                    <div class="steps-header">
                        <table>
                            <colgroup>
                                <col width="35%" />
                                <col width="13%" />
                                <col width="13%" />
                                <col width="13%" />
                                <col width="13%" />
                                <col width="13%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <div class="stepes-pagination">
                                            <ul>
                                                <li><a href="javascript:;" data-href="#st_thongsokythuat" class="stepes-active active">Thông số kỹ thuật</a></li>
                                                <li><a href="javascript:;" data-href="#st_trangthietbi" class="stepes-active">Trang thiết bị</a></li>
                                            </ul>
                                        </div>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/newtriton/Xam1.png')}}"
                                            alt="New Triton &lt;small&gt;&lt;small&gt;4X2 MT (2020)&lt;/small&gt;&lt;/small&gt;"
                                        />
                                        <label>
                                            <small><small>4X2 MT (2020)</small></small>
                                        </label>
                                        <span>{{$product['MT']}} VNĐ</span>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/newtriton/Cam1.png')}}"
                                            alt="New Triton &lt;small&gt;&lt;small&gt;4X2 AT MIVEC (2020)&lt;/small&gt;&lt;/small&gt;"
                                        />
                                        <label>
                                            <small><small>4X2 AT MIVEC (2020)</small></small>
                                        </label>
                                        <span>{{$product['MT_MIVEC_2']}} VNĐ</span>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/newtriton/Xam1.png')}}"
                                            alt="New Triton &lt;small&gt;&lt;small&gt;4X4 MT MIVEC (2020)&lt;/small&gt;&lt;/small&gt;"
                                        />
                                        <label>
                                            <small><small>4X4 MT MIVEC (2020)</small></small>
                                        </label>
                                        <span>{{$product['MT_MIVEC_4']}} VNĐ</span>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/newtriton/Cam1.png')}}"
                                            alt="New Triton &lt;small&gt;&lt;small&gt;4X2 AT MIVEC PREMIUM (2020)&lt;/small&gt;&lt;/small&gt;"
                                        />
                                        <label>
                                            <small><small>4X2 AT MIVEC PREMIUM (2020)</small></small>
                                        </label>
                                        <span>{{$product['AT_MIVEC_Premium_2']}} VNĐ</span>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/newtriton/Xam1.png')}}"
                                            alt="New Triton &lt;small&gt;&lt;small&gt;4×4 AT MIVEC PREMIUM (2020)&lt;/small&gt;&lt;/small&gt;"
                                        />
                                        <label>
                                            <small><small>4×4 AT MIVEC PREMIUM (2020)</small></small>
                                        </label>
                                        <span>{{$product['AT_MIVEC_Premium_4']}} VNĐ</span>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="st_thongsokythuat" class="stepes step-1 active">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />
                                    <col width="13%" />
                                    <col width="13%" />
                                    <col width="13%" />
                                    <col width="13%" />
                                    <col width="13%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><b> KÍCH THƯỚC </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kích thước tổng thể (DxRxC) (mm)</td>
                                        <td>5.305 x 1.815 x 1.775</td>
                                        <td>5.305 x 1.815 x 1.780</td>
                                        <td>5.305 x 1.815 x 1.780</td>
                                        <td>5.305 x 1.815 x 1.795</td>
                                        <td>5.305 x 1.815 x 1.795</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kích thước thùng sau (DxRxC) (mm)</td>
                                        <td>1.520 x 1.470 x 475</td>
                                        <td>1.520 x 1.470 x 475</td>
                                        <td>1.520 x 1.470 x 475</td>
                                        <td>1.520 x 1.470 x 475</td>
                                        <td>1.520 x 1.470 x 475</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoảng cách hai cầu xe (mm)</td>
                                        <td>3.000</td>
                                        <td>3.000</td>
                                        <td>3.000</td>
                                        <td>3.000</td>
                                        <td>3.000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Bán kính quay vòng nhỏ nhất (m)</td>
                                        <td>5,9</td>
                                        <td>5,9</td>
                                        <td>5,9</td>
                                        <td>5,9</td>
                                        <td>5,9</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoảng sáng gầm xe (mm)</td>
                                        <td>200</td>
                                        <td>205</td>
                                        <td>205</td>
                                        <td>220</td>
                                        <td>220</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Trọng lượng không tải (kg)</td>
                                        <td>1.725</td>
                                        <td>1.740</td>
                                        <td>1915</td>
                                        <td>1810</td>
                                        <td>1925</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Số chỗ ngồi (người)</td>
                                        <td>5</td>
                                        <td>5</td>
                                        <td>5</td>
                                        <td>5</td>
                                        <td>5</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> ĐỘNG CƠ </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Loại động cơ</td>
                                        <td>2.4L Diesel DI-D</td>
                                        <td>2.4L Diesel MIVEC</td>
                                        <td>2.4L Diesel MIVEC</td>
                                        <td>2.4L Diesel MIVEC</td>
                                        <td>2.4L Diesel MIVEC</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống nhiên liệu</td>
                                        <td>Phun nhiên liệu điện tử</td>
                                        <td>Phun nhiên liệu điện tử</td>
                                        <td>Phun nhiên liệu điện tử</td>
                                        <td>Phun nhiên liệu điện tử</td>
                                        <td>Phun nhiên liệu điện tử</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Công suất cực đại (ps/rpm)</td>
                                        <td>136/3.500</td>
                                        <td>181/3.500</td>
                                        <td>181/3.500</td>
                                        <td>181/3.500</td>
                                        <td>181/3.500</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mômen xoắn cực đại (Nm/rpm)</td>
                                        <td>324/1.500-2500</td>
                                        <td>430/2.500</td>
                                        <td>430/2.500</td>
                                        <td>430/2.500</td>
                                        <td>430/2.500</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dung tích thùng nhiên liệu (L)</td>
                                        <td>75</td>
                                        <td>75</td>
                                        <td>75</td>
                                        <td>75</td>
                                        <td>75</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hộp số</td>
                                        <td>6MT</td>
                                        <td>6AT - Sport Mode</td>
                                        <td>6MT</td>
                                        <td>6AT - Sport Mode</td>
                                        <td>6AT - Sport Mode</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Truyền động</td>
                                        <td>Cầu sau</td>
                                        <td>Cầu sau</td>
                                        <td>Easy Select 4WD</td>
                                        <td>Cầu sau</td>
                                        <td>2 cầu Super Select 4WD-II</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gài cầu điện tử</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khóa vi sai cầu sau</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chế độ chọn địa hình Off-road</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Trợ lực lái</td>
                                        <td>Thủy lực</td>
                                        <td>Thủy lực</td>
                                        <td>Thủy lực</td>
                                        <td>Thủy lực</td>
                                        <td>Thủy lực</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống treo trước</td>
                                        <td>Độc lập, tay đòn kép, lò xo cuộn với thanh cân bằng</td>
                                        <td>Độc lập, tay đòn kép, lò xo cuộn với thanh cân bằng</td>
                                        <td>Độc lập, tay đòn kép, lò xo cuộn với thanh cân bằng</td>
                                        <td>Độc lập, tay đòn kép, lò xo cuộn với thanh cân bằng</td>
                                        <td>Độc lập, tay đòn kép, lò xo cuộn với thanh cân bằng</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống treo sau</td>
                                        <td>Nhíp lá</td>
                                        <td>Nhíp lá</td>
                                        <td>Nhíp lá</td>
                                        <td>Nhíp lá</td>
                                        <td>Nhíp lá</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lốp xe trước/sau</td>
                                        <td>245/70R16</td>
                                        <td>245/65R17</td>
                                        <td>245/65R17</td>
                                        <td>265/60R18</td>
                                        <td>265/60R18</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Phanh trước</td>
                                        <td>Đĩa thông gió 16"</td>
                                        <td>Đĩa thông gió 16"</td>
                                        <td>Đĩa thông gió 17"</td>
                                        <td>Đĩa thông gió 17"</td>
                                        <td>Đĩa thông gió 17"</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Phanh sau</td>
                                        <td>Tang trống</td>
                                        <td>Tang trống</td>
                                        <td>Tang trống</td>
                                        <td>Tang trống</td>
                                        <td>Tang trống</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="st_trangthietbi" class="stepes step-2">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />

                                    <col width="13%" />
                                    <col width="13%" />
                                    <col width="13%" />
                                    <col width="13%" />
                                    <col width="13%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><b> NGOẠI THẤT </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống đèn chiều sáng phía trước</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Đèn chiếu xa</i></td>
                                        <td>Halogen + Projector</td>
                                        <td>Halogen + Projector</td>
                                        <td>Halogen + Projector</td>
                                        <td>LED + Projector</td>
                                        <td>LED + Projector</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Đèn chiếu gần</i></td>
                                        <td>Halogen + Projector</td>
                                        <td>Halogen + Projector</td>
                                        <td>Halogen + Projector</td>
                                        <td>LED + Projector</td>
                                        <td>LED + Projector</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn LED chiếu sáng ban ngày</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cảm biến BẬT/TẮT đèn chiếu sáng phía trước</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn pha tự động</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn sương mù</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kính chiếu hậu</td>
                                        <td>Chỉnh điện, mạ crôm</td>
                                        <td>Chỉnh điện, mạ crôm</td>
                                        <td>Chỉnh/gập điện, mạ crôm, tích hợp đèn báo rẽ, sưởi gương</td>
                                        <td>Chỉnh/gập điện, mạ crôm, tích hợp đèn báo rẽ, sưởi gương</td>
                                        <td>Chỉnh/gập điện, mạ crôm, tích hợp đèn báo rẽ, sấy gương</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cảm biến gạt mưa tự động</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn phanh thứ ba lắp trên cao</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Sưởi kính sau</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mâm bánh xe</td>
                                        <td>16"</td>
                                        <td>17"</td>
                                        <td>17"</td>
                                        <td>18"</td>
                                        <td>18"</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Bệ bước hông xe</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Bệ bước cản sau dạng thể thao</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chắn bùn trước/sau</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> NỘI THẤT </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Vô lăng và cần số bọc da</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lẫy sang số trên vô lăng</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Điều chỉnh âm thanh trên vô lăng</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống kiểm soát hành trình</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tay lái điều chỉnh 4 hướng</td>
                                        <td>2 hướng</td>
                                        <td>2 hướng</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Điều hòa không khí</td>
                                        <td>Chỉnh tay</td>
                                        <td>Chỉnh tay</td>
                                        <td>Tự động</td>
                                        <td>Tự động 2 vùng độc lập</td>
                                        <td>Tự động 2 vùng độc lập</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cửa gió phía sau cho hành khách</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lọc gió điều hòa</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chất liệu ghế</td>
                                        <td>Nỉ</td>
                                        <td>Nỉ</td>
                                        <td>Nỉ cao cấp</td>
                                        <td>Da</td>
                                        <td>Da</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Ghế tài xế</td>
                                        <td>Chỉnh tay 4 hướng</td>
                                        <td>Chỉnh tay 4 hướng</td>
                                        <td>Chỉnh tay 6 hướng</td>
                                        <td>Chỉnh điện 8 hướng</td>
                                        <td>Chỉnh điện 8 hướng</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kính cửa điều khiển điện</td>
                                        <td>Kính cửa phía tài xế điều chỉnh một chạm xuống kính</td>
                                        <td>Kính cửa phía tài xế điều chỉnh một chạm xuống kính</td>
                                        <td>Kính cửa phía tài xế điều chỉnh một chạm, chống kẹt</td>
                                        <td>Kính cửa phía tài xế điều chỉnh một chạm, chống kẹt</td>
                                        <td>Kính cửa phía tài xế điều chỉnh một chạm, chống kẹt</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Màn hình hiển thị đa thông tin</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>LCD</td>
                                        <td>LCD</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống giải trí</td>
                                        <td>CD/USB/ Radio/Bluetooth</td>
                                        <td>CD/USB/ Radio/Bluetooth</td>
                                        <td>CD/USB/ Radio/Bluetooth</td>
                                        <td>Màn hình cảm ứng 6,75" với Android Auto, Apple CarPlay</td>
                                        <td>Màn hình cảm ứng 6,75" với Android Auto, Apple CarPlay</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Số lượng loa</td>
                                        <td>4</td>
                                        <td>4</td>
                                        <td>4</td>
                                        <td>6</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tựa tay hàng ghế sau với giá để ly</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> AN TOÀN </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Túi khí phía trước cho người lái và hành khách</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Túi khí bên</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Túi khí rèm dọc hai bên thân xe</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Túi khí đầu gối bảo vệ người lái</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cơ cấu căng đai tự động cho hàng ghế trước</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dây đai an toàn tất cả các ghế</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống chống bó cứng phanh (ABS)</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống phân phối lực phanh điện tử (EBD)</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống trợ lực phanh khẩn cấp (BA)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống cân bằng điện tử và kiểm soát lực kéo</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống hỗ trợ khởi hành ngang dốc (HSA)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống hỗ trợ xuống dốc (HDC)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chế độ chọn địa hình Off-Road mode</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cảm biến lùi</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cảm biến góc trước</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống giảm thiểu va chạm phía trước (FCM)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống chống tăng tốc ngoài ý muốn (UMS)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống cảnh báo điểm mù (BSW)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống hỗ trợ chuyển làn đường (LCA)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống cảnh báo phương tiện cắt ngang phía sau (RCTA)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gương chiếu hậu chống chói tự động</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chìa khóa thông minh (KOS)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khởi động bằng nút bấm (OSS)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoá cửa từ xa</td>
                                        <td>-</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống khóa cửa trung tâm &amp; Khóa an toàn trẻ em</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chìa khóa mã hóa chống trộm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br />
                    <br />
                    <p class="text-left">(*) Mức tiêu hao nhiên liệu chứng nhận bởi Cục Đăng Kiểm Việt Nam. Các thông số kỹ thuật có thể thay đổi mà không báo trước</p>
                </div>
            </div>
        </div>
    </div>
</section>



@endsection
@section('jsfooter')
<script src="{{asset('js/sanpham/TweenMax.min.js')}}"></script>
<script src="{{asset('js/sanpham/ScrollToPlugin.min.js')}}"></script>
<script src="{{asset('js/sanpham/jquery.fancybox.js')}}"></script>
<script src="{{asset('js/new-xpander.js')}}"></script>
<script src="{{asset('js/estimate-price.js')}}"></script>
<script type="text/javascript">
    productInit();
</script>
@endsection
