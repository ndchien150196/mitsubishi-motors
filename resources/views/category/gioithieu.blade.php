@extends("master")
@section("content") 
<div class="second-block page-news-home">
    <div class="grid-fluid">
        <h3 class="title-block">
            <span>Giới thiệu</span>
        </h3>
        <div class="grid-content">
            <div class="row">
                <div class="col-md-8">
                    <div class="editor">
                        <p>Lời đầu tiên, công ty chúng tôi xin gửi tới quý khách hàng lời chào trân trọng, lời chúc sức khỏe và thành công.</p>
                        <p>
                            Công ty cổ phần ô tô Hưng Thịnh Phát là đại lý 3s chính thức của Mitsubishi Motors Việt Nam tại khu vực Thái Bình. Với sự kết hợp của 3 chức năng: Bán hàng, dịch vụ sửa chữa, bảo hành và phụ tùng chính hãng.
                        </p>
                        <p>
                            Chính thức từ ngày 12/12/2020 tới đây, chúng tôi rất vinh dự và tự hào là Đại lý chính thức của Mitsubishi Motors Nhật Bản tại Việt Nam
                        </p>
                        <h3>Đại Lý Mitsubishi Thái Bình xin kính chào quý khách</h3>
                        <p>
                            Là nhà phân phối chính thức của Misubishi Motors Việt Nam, chúng tôi hân hạnh gởi đến quý khách hàng các sản phẩm của dòng xe <a href="/"><strong>Mitsubishi</strong></a> nhập khẩu và
                            lắp ráp như sau:&nbsp;&nbsp;<strong><a href="http://mitsubishi-thaibinh.vn/">Outlander</a>,</strong>&nbsp;<strong><a href="http://mitsubishi-thaibinh.vn/">Mirage</a></strong>,&nbsp;
                            <a href="http://mitsubishi-thaibinh.vn/">Attrage</a>,&nbsp;<a href="http://mitsubishi-thaibinh.vn/">Pajero&nbsp;Sport</a>,&nbsp;<a href="http://mitsubishi-thaibinh.vn/">Triton</a>,
                            <a href="http://mitsubishi-thaibinh.vn/">Xpander</a>
                        </p>
                        <p>
                            <a href="http://mitsubishi-thaibinh.vn/">Mitsubishi Thái Bình</a>, chúng tôi luôn nỗ lực nâng cao hình ảnh và chất lượng dịch vụ nhằm đem lại cho khách hàng những giá trị dịch vụ tốt nhất, uy tín và chuyên
                            nghiệp. Với đội ngũ bán hàng và kỹ thuật chuyên nghiệp, nhiều năm trong nghề, hệ thống showroom và xưởng sửa chữa bảo hành rộng rãi, trang bị theo tiêu chuẩn đại lý của Mitsubishi Motors Việt Nam.
                        </p>
                        <p>
                            Với mục tiêu “Mang lại sự tin tưởng và gắn bó dài lâu”,&nbsp;<strong><a href="http://mitsubishi-thaibinh.vn/">Mitsubishi Thái Bình</a>&nbsp;</strong>mong muốn sẽ là sự lựa&nbsp;chọn hàng đầu của quý khách.
                        </p>
                        <p>Hãy đến với chúng tôi để:</p>
                        <ul>
                            <li>Trực tiếp chiêm ngưỡng và cảm nhận.</li>
                            <li>Lái thử xe miễn phí.</li>
                            <li>Nhận nhiều ưu đãi khuyến mãi khi mua xe.</li>
                            <li>Tư vấn tài chính, hỗ trợ vay mua xe với lãi suất thấp.</li>
                        </ul>
                        <p>
                            <a href="http://mitsubishi-thaibinh.vn/"><strong>Mitsubishi Thái Bình</strong></a>
                        </p>
                        <ul>
                            <li>Add:&nbsp; 140 Quang Trung, phường Quang Trung,TP Thái Bình</li>
                            <li>Hotline : {{\App\Common::phonelh()}}&nbsp;</li>
                            <li>Website: http://mitsubishi-thaibinh.vn/</li>
                            <li>Fanpage: {{\App\Common::fanpage()}}</li>
                            <li>Rất hân hạnh được phục vụ quý khách hàng !</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="news-sidebar">
                        <div class="sidebar-block link-block">
                            <h3 class="news-link-title">Giới thiệu</h3>
                            <!-- menu-->
                            <ul class="news-link">
                                <li class="">
                                    <a href="{{url('home')}}" title="Trang chủ " class="sp-text">Trang chủ </a>
                                </li>
                                <li class="active">
                                    <a href="{{url('gioi-thieu')}}" title="Giới thiệu" class="sp-text">Giới thiệu</a>
                                </li>
                                <li class="">
                                    <a href="{{url('bang-gia')}}" title="Bảng giá" class="sp-text">Bảng giá</a>
                                </li>
                                <li class="">
                                    <a href="{{asset('tin-tuc/tin-khuyen-mai')}}" title="Khuyến mãi" class="sp-text">Khuyến mãi</a>
                                </li>
                                <li class="">
                                    <a href="{{url('lien-he')}}" title="Liên hệ" class="sp-text">Liên hệ</a>
                                </li>
                            </ul>
                        </div>
                        @include('/layouts/block-km')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection