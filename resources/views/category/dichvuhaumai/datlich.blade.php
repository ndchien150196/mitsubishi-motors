@extends("master")
@section("content") 
<div class="second-block page-news-home news-page service-pages booking-page">
    <div class="grid-fluid">
        <h3 class="title-block">
            <span>Đặt lịch bảo dưỡng</span>
        </h3>
        <div class="grid-content">
            <div class="row">
                <div class="col-md-8">
                    <div class="editor">
                        <div class="wrap">
                            <p>
                                <strong>Khuyến cáo về bảo dưỡng định kỳ và đặt lịch hẹn</strong> Quý khách nên đưa xe đi bảo dưỡng định kỳ ở 1.000 km đầu tiên và mỗi 5.000 km tiếp theo hoặc sau 4 tháng (tùy điều kiện nào đến trước) để đảm
                                bảo điều kiện hoạt động tốt nhất cho xe. Quý khách vui lòng đặt lịch hẹn trước khi đưa xe đến làm dịch vụ để được hưởng những ưu đãi từ Mitsubishi Quảng Ninh:
                            </p>
                            <ol>
                                <li>Được tiếp nhận ngay mà không phải chờ đợi</li>
                                <li>Tiết kiệm thời gian quý báu của mình</li>
                                <li>Nhận quà tặng</li>
                            </ol>
                            <div class="hotline">
                                <div>
                                    <b>Hotline tiếp nhận cuộc hẹn: <span class="red">{{\App\Common::phonedv()}}</span>.</b>
                                </div>
                                <div><b>Cám ơn Quý khách!</b></div>
                            </div>
                            <!-- Form input -->
                            <div id="booking_frm" class="booking-form reg-frm customer-survey">
                                <form accept-charset="UTF-8" class="mitsu-frm booking_frm" id="d2-booking_frm" data-url="/mail-dat-lich">
                                    <!-- //// -->
                                    <div class="wrap">
                                        <div class="item-wrap">
                                            <div class="hidden-xs hidden-sm item-label important" for="name">Họ và tên khách hàng <span>*</span></div>
                                            <div class="frm-item textfield-float-label" d2-val-target-parent="Name">
                                                @csrf
                                                <input id="name" name="Name" type="text" d2-val-required="" class="is-empty" />
                                                <label for="name" class="important">Họ và tên khách hàng <span>*</span></label>
                                                <p class="error-msg" d2-val-target="Name" d2-val-required-msg="">Vui lòng nhập tên</p>
                                                <p class="error-msg" d2-val-target="Name" d2-val-server-msg=""></p>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="hidden-xs hidden-sm item-label" for="name">Công ty</div>
                                            <div class="frm-item textfield-float-label" d2-val-target-parent="Company">
                                                <input id="firm-name" name="Company" type="text" class="is-empty" />
                                                <label for="name">Công ty</label>
                                                <p class="error-msg" d2-val-target="Firm-name" d2-val-required-msg="">Vui lòng nhập tên công ty</p>
                                                <p class="error-msg" d2-val-target="Firm-name" d2-val-server-msg=""></p>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="hidden-xs hidden-sm item-label important" for="name">Điện thoại di động <span>*</span></div>
                                            <div class="frm-item textfield-float-label" d2-val-target-parent="Phone">
                                                <input id="phone" name="Phone" type="text" d2-val-required="" d2-val-phone="" class="is-empty" />
                                                <label for="phone" class="important">Điện thoại di động <span>*</span></label>
                                                <p class="error-msg" d2-val-target="Phone" d2-val-required-msg="">Vui lòng nhập SĐT</p>
                                                <p class="error-msg" d2-val-target="Phone" d2-val-phone-msg="">SĐT chưa hợp lệ</p>
                                                <p class="error-msg" d2-val-target="Phone" d2-val-server-msg=""></p>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="hidden-xs hidden-sm item-label" for="name">Loại xe</div>
                                            <div class="frm-item textfield-float-label" d2-val-target-parent="Cartype">
                                                <input id="car_type" name="Cartype" type="text" class="is-empty" />
                                                <label for="car_type">Loại xe</label>
                                                <p class="error-msg" d2-val-target="Car-type" d2-val-required-msg="">Vui lòng nhập tên xe</p>
                                                <p class="error-msg" d2-val-target="Car-type" d2-val-server-msg=""></p>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="hidden-xs hidden-sm item-label important" for="name">Biển số xe <span>*</span></div>
                                            <div class="frm-item textfield-float-label" d2-val-target-parent="Licenseplate">
                                                <input id="license_plate" name="Licenseplate" type="text" d2-val-required="" class="is-empty" />
                                                <label for="license_plate" class="important">Biển số xe <span>*</span></label>
                                                <p class="error-msg" d2-val-target="Licenseplate" d2-val-required-msg="">Vui lòng nhập biển số xe</p>
                                                <p class="error-msg" d2-val-target="Licenseplate" d2-val-server-msg=""></p>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="hidden-xs hidden-sm item-label" for="name">Ngày yêu cầu dịch vụ <span>*</span></div>
                                            <div class="frm-item textfield-float-label" d2-val-target-parent="Bookingdate">
                                                <input id="booking_date" name="Bookingdate" class="picker is-empty hasDatepicker" type="text" />
                                                <label for="booking_date" class="important">Ngày yêu cầu dịch vụ <span>*</span></label>
                                                <p class="error-msg" d2-val-target="Booking-date" d2-val-required-msg="">Vui lòng nhập Ngày yêu cầu dịch vụ</p>
                                                <p class="error-msg" d2-val-target="Booking-date" d2-val-server-msg=""></p>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="hidden-xs hidden-sm item-label" for="name">Giờ yêu cầu dịch vụ</div>
                                            <div class="frm-item textfield-float-label" d2-val-target-parent="Booking-hour">
                                                <div class="selecter closed" tabindex="0">
                                                    <select name="HourService" tabindex="-1" class="selecter-element">
                                                        <option value="7:00 AM"> 7:00 AM</option>
                                                        <option value="8:00 AM"> 8:00 AM</option>
                                                        <option value="9:00 AM"> 9:00 AM</option>
                                                        <option value="10:00 AM"> 10:00 AM</option>
                                                        <option value="11:00 AM"> 11:00 AM</option>
                                                        <option value="12:00 AM"> 12:00 AM</option>
                                                        <option value="1:00 PM"> 1:00 PM</option>
                                                        <option value="2:00 PM"> 2:00 PM</option>
                                                        <option value="3:00 PM"> 3:00 PM</option>
                                                        <option value="4:00 PM"> 4:00 PM</option>
                                                        <option value="5:00 PM"> 5:00 PM</option>
                                                        <option value="6:00 PM"> 6:00 PM</option>
                                                    </select>
                                                    <span class="selecter-selected"> 7:00 AM</span>
                                                    <div class="selecter-options">
                                                        <span class="selecter-item selected" data-value="7:00 AM"> 7:00 AM</span><span class="selecter-item" data-value="8:00 AM"> 8:00 AM</span>
                                                        <span class="selecter-item" data-value="9:00 AM"> 9:00 AM</span><span class="selecter-item" data-value="10:00 AM"> 10:00 AM</span>
                                                        <span class="selecter-item" data-value="11:00 AM"> 11:00 AM</span><span class="selecter-item" data-value="12:00 AM"> 12:00 AM</span>
                                                        <span class="selecter-item" data-value="1:00 PM"> 1:00 PM</span><span class="selecter-item" data-value="2:00 PM"> 2:00 PM</span>
                                                        <span class="selecter-item" data-value="3:00 PM"> 3:00 PM</span><span class="selecter-item" data-value="4:00 PM"> 4:00 PM</span>
                                                        <span class="selecter-item" data-value="5:00 PM"> 5:00 PM</span><span class="selecter-item" data-value="6:00 PM"> 6:00 PM</span>
                                                    </div>
                                                </div>
                                                <label for="booking_hour">Giờ yêu cầu dịch vụ</label>
                                                <p class="error-msg" d2-val-target="Booking-hour" d2-val-required-msg="">Vui lòng nhập Giờ yêu cầu dịch vụ</p>
                                                <p class="error-msg" d2-val-target="Booking-hour" d2-val-server-msg=""></p>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="hidden-xs hidden-sm item-label" for="name">Số Km hiện tại</div>
                                            <div class="frm-item textfield-float-label" d2-val-target-parent="Kilometer">
                                                <input id="kilometer" name="Kilometer" type="text" class="is-empty" />
                                                <label for="kilometer">Số Km hiện tại</label>
                                                <p class="error-msg" d2-val-target="Kilometer" d2-val-required-msg="">Vui lòng nhập Số Km hiện tại</p>
                                                <p class="error-msg" d2-val-target="Kilometer" d2-val-server-msg=""></p>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="frm-item item-label">
                                                <div class="frm-hightline important">Nội dung làm dịch vụ <span>*</span></div>
                                            </div>
                                            <div class="frm-item checkboxfield">
                                                <ul class="frm-ul">
                                                    <li class="frm-li">
                                                        <label class="frm-label">
                                                            <input type="checkbox" name="noidunglamdichvu" data-code="" value=" Bảo dưỡng định kỳ" />
                                                            <span class="frm-text" data-answer=""> Bảo dưỡng định kỳ</span>
                                                        </label>
                                                    </li>
                                                    <li class="frm-li">
                                                        <label class="frm-label">
                                                            <input type="checkbox" name="noidunglamdichvu" data-code="" value=" Sửa chữa chung" />
                                                            <span class="frm-text" data-answer=""> Sửa chữa chung</span>
                                                        </label>
                                                    </li>
                                                    <li class="frm-li">
                                                        <label class="frm-label">
                                                            <input type="checkbox" name="noidunglamdichvu" data-code="" value=" Sửa chữa thân vỏ, sơn" />
                                                            <span class="frm-text" data-answer=""> Sửa chữa thân vỏ, sơn</span>
                                                        </label>
                                                    </li>
                                                    <li class="frm-li">
                                                        <label class="frm-label">
                                                            <input type="checkbox" name="noidunglamdichvu" data-code="" value=" Nội dung khác" />
                                                            <span class="frm-text" data-answer=""> Nội dung khác</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="frm-item item-label">
                                                <div class="frm-hightline">Cố vấn</div>
                                            </div>
                                            <div class="frm-item radiofield">
                                                <ul class="frm-ul">
                                                    <li class="frm-li">
                                                        <label class="frm-label">
                                                            <input type="radio" name="covan" data-code="" value="{{\App\Common::phonelh()}}" />
                                                            <span class="frm-text" data-answer="">{{\App\Common::phonedv()}}</span>
                                                        </label>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="hidden-xs hidden-sm item-label" for="name">Kỹ thuật viên</div>
                                            <div class="frm-item textfield-float-label" d2-val-target-parent="Ky-thuat-vien">
                                                <input id="ky_thuat_vien" name="kythuatvien" type="text" class="is-empty" />
                                                <label for="ky_thuat_vien">Kỹ thuật viên</label>
                                            </div>
                                        </div>
                                        <div class="item-wrap">
                                            <div class="hidden-xs hidden-sm item-label" for="name">Yêu cầu</div>
                                            <div class="frm-item textfield-float-label" d2-val-target-parent="Yeu-cau">
                                                <input id="yeu_cau" name="yeucau" type="text" class="is-empty" />
                                                <label for="yeu_cau">Yêu cầu</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- //// -->
                                    <div class="btn-wrap clearfix">
                                        <div class="frm-item frm-item-left">
                                            <a id="d2-submit-btn" href="javascript:;" class="frm-btn">Đăng ký</a>
                                        </div>
                                        <div class="frm-item frm-item-right">
                                            <a id="d2-reset" href="javascript:;" class="frm-btn btn-reset">Điền lại</a>
                                        </div>
                                    </div>
                                    <p class="notify-text red">Đặt lịch thành công</p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  Sidebar -->
                <div class="col-md-4">
                    <div class="news-sidebar">
                        <div class="sidebar-block link-block">
                            <h3 class="news-link-title">Dịch vụ hậu mãi</h3>
                            <!-- menu-->
                            <ul class="news-link">
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['gioi-thieu-dich-vu'])}}" title="Giới thiệu dịch vụ" class="sp-text">Giới thiệu dịch vụ</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['chinh-sach-bao-hanh'])}}" title="Chính sách bảo hành" class="sp-text">Chính sách bảo hành</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['bao-duong-dinh-ky'])}}" title="Bảo dưỡng định kỳ" class="sp-text">Bảo dưỡng định kỳ</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['phu-tung-chinh-hieu'])}}" title="Phụ tùng chính hiệu" class="sp-text">Phụ tùng chính hiệu</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['sua-chua'])}}" title="Sửa chữa" class="sp-text">Sửa chữa</a>
                                </li>
                                <li class="active">
                                    <a href="{{url('dich-vu-hau-mai',['dat-lich-bao-duong'])}}" title="Đặt lịch bảo dưỡng" class="sp-text">Đặt lịch bảo dưỡng</a>
                                </li>
                            </ul>
                        </div>
                        @include('//layouts/block-km')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
