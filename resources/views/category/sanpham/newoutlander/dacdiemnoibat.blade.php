<!--Highlight-->
<div class="normal-block dacdiemnoibat">
    <div class="grid-inner">
        <h3 class="title-block"><span>Đặc điểm nổi bật</span></h3>
        <div class="row">
            <div class="col-md-4 col-lg-3">
                <div class="pr-info">
                    <h1 class="title">New Attrage</h1>
                    <p class="summary">
                        Kế thừa những thế mạnh vốn có của một mẫu xe nhập khẩu nguyên chiếc từ Thái Lan: bền bỉ, tin cậy và tiết kiệm nhiên liệu hàng đầu phân khúc, Mitsubishi Attrage mới nay còn được khoác lên mình thiết kế
                        “Dynamic Shield” hiện đại và trẻ trung hơn giúp mang đến cho bạn một khởi đầu vững chắc cho hành trình mới.
                    </p>
                </div>
            </div>

            <!--.pr-info-->
            <div class="col-md-8 col-lg-9">
                <div class="slide-dacdiemnoibat">
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/newoutlander/Top-feature-1.png')}}"
                                    alt="THIẾT KẾ MỚI PHONG CÁCH HƠN"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Top-feature-1.png"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">THIẾT KẾ MỚI PHONG CÁCH HƠN</h4>
                                <p class="summary">Với triết lý "vẻ đẹp từ công năng ", thiết kế Dynamic Shield mang đến sự hài hòa giữa giữa thiết kế hiện đại, thanh lịch và khả năng vận hành an toàn đảm bảo.</p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/newoutlander/Top-feature-2.png')}}"
                                    alt="7 CHỖ RỘNG RÃI"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Top-feature-2.png"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">7 CHỖ RỘNG RÃI</h4>
                                <p class="summary">Nội thất bọc da cao cấp. Hàng ghế thứ hai có thể trượt linh hoạt lên đến 250mm.</p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/newoutlander/Top-feature-3.png')}}"
                                    alt="AN TOÀN VƯỢT TRỘI VỚI HỆ THỐNG AN TOÀN CHỦ ĐỘNG e-Assist"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Top-feature-3.png"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">AN TOÀN VƯỢT TRỘI VỚI HỆ THỐNG AN TOÀN CHỦ ĐỘNG e-Assist</h4>
                                <p class="summary">Hệ thống an toàn chủ động sử dụng radar và hệ thống camera để tăng cường an toàn và an tâm khi lái xe. Bao gồm các hệ thống: FCM, RCTA, LCA,...</p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <!--.slide-center-->
            </div>
        </div>
    </div>
</div>
<!--End - Highlight-->