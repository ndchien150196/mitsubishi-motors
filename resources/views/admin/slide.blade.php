@extends("app")

@section("content")
<div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row"></div>
    <div class="row">
        <div class="col-sm-12">
            <table id="mitsubishi" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="mitsubishi_info">
                <thead>
                    <tr role="row">
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">ID</th>
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Title</th>
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Ảnh</th>
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Link Bài Viết</th>
                        <th class="text-center">
                        	<a type="button" class="btn btn-default" data-toggle="modal" data-target="#modal-default"><span class="glyphicon glyphicon-plus"></span> Thêm </a>
                        </th>
                    </tr>
                </thead>
                <tbody class="">
                    @foreach($slide as $k => $u)
                        <tr role="row" class="odd">
                            <td class="text-center">{{$u->id}}</td>
                            <td class="text-center">{{$u->name}}</td>
                            <td class="text-center">
                                <a href="{{$u->image}}" target="_blank"><img src="{{$u->image}}" height="50"></a>
                            </td>
                            <td class="text-center">
                                <a href="{{$u->link}}" target="_blank">
                                    <i class="fa fa-eye" aria-hidden="true"></i>
                                </a>
                            </td>
                            <td class="text-center">
                                <a href="{{route('admin.slide.delete', ['id' => $u->id])}}"><span class="fa fa-trash"></span></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                	<span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title">Thêm Slide</h4>
            </div>
		    <form action="{{ asset('admin/slide/add') }}" method="post" enctype="multipart/form-data">
	            <div class="modal-body">
            		@csrf
                    <div class="form-group col-md-12">
                        <span class="control-fileupload">
                            <label for="name">Tên Ảnh (Không cần thiết đúng nhưng tốt cho seo):</label>
                            <input type="text" name="name" id="name" class="form-control">
                        </span>
                    </div>
                    <div class="form-group col-md-12">
                        <span class="control-fileupload">
                            <label for="link">Nhập Link :</label>
                            <input type="text" name="link" id="link" class="form-control">
                        </span>
                    </div>
			        <div class="form-group col-md-12">
						<span class="control-fileupload">
							<label for="file">Chọn Tệp :</label>
							<input type="file" name="file" id="file" class="form-control">
						</span>
					</div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-primary">Lưu</button>
	            </div>
	        </form>
        </div>
    </div>
</div>
@endsection

@section("js")
<script type="text/javascript">
	
</script>
@endsection
