<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>NGOẠI THẤT</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide80"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="NGÔN NGỮ THIẾT KẾ DYNAMIC SHIELD" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> NGÔN NGỮ THIẾT KẾ DYNAMIC SHIELD</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Dynamic-Shield.jpg')}}"
                                    alt="NGÔN NGỮ THIẾT KẾ DYNAMIC SHIELD"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> NGÔN NGỮ THIẾT KẾ DYNAMIC SHIELD</span></h3>
                                <div class="opContent">
                                    <p>Thiết kế DYNAMIC SHIELD với triết lý “Vẻ đẹp từ công năng”, mang lại sự hài hòa giữa hình ảnh mạnh mẽ, hiện đại và tính năng bảo vệ an toàn cùng khả năng vận hành vượt trội.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide81" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Đèn định vị dạng LED" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Đèn định vị dạng LED</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/xpander.jpg')}}"
                                    alt="Đèn định vị dạng LED"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Đèn định vị dạng LED</span></h3>
                                <div class="opContent">
                                    <p>Với thiết kế mới, đưa đèn định vị dạng LED lên trên, giúp xe tăng vẻ hiện đại và mạnh mẽ</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide82" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Đèn chiếu sáng phía trước LED" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Đèn chiếu sáng phía trước LED</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Den-LED.jpg')}}"
                                    alt="Đèn chiếu sáng phía trước LED"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Đèn chiếu sáng phía trước LED</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống đèn chiếu sáng phía trước công nghệ LED hiện đại với khả năng chiếu sáng tối ưu, bền bỉ và tiết kiệm năng lượng hơn.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide83" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Đèn hậu LED" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Đèn hậu LED</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Den-hau-LED.jpg')}}"
                                    alt="Đèn hậu LED"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Đèn hậu LED</span></h3>
                                <div class="opContent">
                                    <p>Thiết kế đèn xe đặc trưng mang lại ấn tượng mạnh cho phần đuôi xe.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide84" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Mâm bánh xe 16 inch thiết kế mới" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Mâm bánh xe 16 inch thiết kế mới</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Mam-xe.jpg')}}"
                                    alt="Mâm bánh xe 16 inch thiết kế mới"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Mâm bánh xe 16 inch thiết kế mới</span></h3>
                                <div class="opContent">
                                    <p>Mâm đúc hợp kim với thiết kế 2 tông màu (đen và bạc) được cắt và phay bóng, cho cảm giác mạnh mẽ.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide85" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Ăng ten vây cá" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Ăng ten vây cá</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Ang-ten-vay-ca.jpg')}}"
                                    alt="Ăng ten vây cá"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li><span>AT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Ăng ten vây cá</span></h3>
                                <div class="opContent">
                                    <p>Ăng ten vây cá giúp xe có ngoại thất hiện đại hơn.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Dynamic-Shield.jpg')}}"
                        alt="NGÔN NGỮ THIẾT KẾ DYNAMIC SHIELD"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">NGÔN NGỮ THIẾT KẾ DYNAMIC SHIELD</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Thiết kế DYNAMIC SHIELD với triết lý “Vẻ đẹp từ công năng”, mang lại sự hài hòa giữa hình ảnh mạnh mẽ, hiện đại và tính năng bảo vệ an toàn cùng khả năng vận hành vượt trội.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/xpander.jpg')}}"
                        alt="Đèn định vị dạng LED"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Đèn định vị dạng LED</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Với thiết kế mới, đưa đèn định vị dạng LED lên trên, giúp xe tăng vẻ hiện đại và mạnh mẽ</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Den-LED.jpg')}}"
                        alt="Đèn chiếu sáng phía trước LED"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Đèn chiếu sáng phía trước LED</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Hệ thống đèn chiếu sáng phía trước công nghệ LED hiện đại với khả năng chiếu sáng tối ưu, bền bỉ và tiết kiệm năng lượng hơn.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Den-hau-LED.jpg')}}"
                        alt="Đèn hậu LED"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Đèn hậu LED</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Thiết kế đèn xe đặc trưng mang lại ấn tượng mạnh cho phần đuôi xe.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Mam-xe.jpg')}}"
                        alt="Mâm bánh xe 16 inch thiết kế mới"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Mâm bánh xe 16 inch thiết kế mới</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Mâm đúc hợp kim với thiết kế 2 tông màu (đen và bạc) được cắt và phay bóng, cho cảm giác mạnh mẽ.</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Ang-ten-vay-ca.jpg')}}"
                        alt="Ăng ten vây cá"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li><span>AT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Ăng ten vây cá</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Ăng ten vây cá giúp xe có ngoại thất hiện đại hơn.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>