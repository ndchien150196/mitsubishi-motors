-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 23, 2020 lúc 02:58 PM
-- Phiên bản máy phục vụ: 10.4.13-MariaDB
-- Phiên bản PHP: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `mitsubishi`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `file_manager`
--

CREATE TABLE `file_manager` (
  `id` int(11) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Mitsubishi',
  `type` int(11) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `file_manager`
--

INSERT INTO `file_manager` (`id`, `url`, `thumb`, `link`, `tag`, `type`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '/storage/editor/891292216.jpeg', '/storage/editor/891292216.jpeg', '/storage/editor/891292216.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 09:44:43', '2020-11-19 09:44:43'),
(2, '/storage/editor/1322321523.jpeg', '/storage/editor/1322321523.jpeg', '/storage/editor/1322321523.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 09:45:06', '2020-11-19 09:45:06'),
(3, '/storage/editor/2060072898.jpeg', '/storage/editor/2060072898.jpeg', '/storage/editor/2060072898.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 09:45:31', '2020-11-19 09:45:31'),
(4, '/storage/editor/1854194877.jpeg', '/storage/editor/1854194877.jpeg', '/storage/editor/1854194877.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:01:52', '2020-11-19 10:01:52'),
(5, '/storage/editor/335515259.jpeg', '/storage/editor/335515259.jpeg', '/storage/editor/335515259.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:02:19', '2020-11-19 10:02:19'),
(6, '/storage/editor/1545012733.jpeg', '/storage/editor/1545012733.jpeg', '/storage/editor/1545012733.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:02:39', '2020-11-19 10:02:39'),
(7, '/storage/editor/1369666608.jpeg', '/storage/editor/1369666608.jpeg', '/storage/editor/1369666608.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:03:00', '2020-11-19 10:03:00'),
(8, '/storage/editor/1809686510.jpeg', '/storage/editor/1809686510.jpeg', '/storage/editor/1809686510.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:03:58', '2020-11-19 10:03:58'),
(9, '/storage/editor/970196546.jpeg', '/storage/editor/970196546.jpeg', '/storage/editor/970196546.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:04:22', '2020-11-19 10:04:22'),
(10, '/storage/editor/28129184.gif', '/storage/editor/28129184.gif', '/storage/editor/28129184.gif', 'Mitsubishi', 0, NULL, '2020-11-19 10:06:47', '2020-11-19 10:06:47'),
(11, '/storage/editor/1852135489.jpeg', '/storage/editor/1852135489.jpeg', '/storage/editor/1852135489.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:09:33', '2020-11-19 10:09:33'),
(12, '/storage/editor/631759047.jpeg', '/storage/editor/631759047.jpeg', '/storage/editor/631759047.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:10:35', '2020-11-19 10:10:35'),
(13, '/storage/editor/1187912419.jpeg', '/storage/editor/1187912419.jpeg', '/storage/editor/1187912419.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:10:52', '2020-11-19 10:10:52'),
(14, '/storage/editor/461949209.jpeg', '/storage/editor/461949209.jpeg', '/storage/editor/461949209.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:11:14', '2020-11-19 10:11:14'),
(15, '/storage/editor/1892320499.jpeg', '/storage/editor/1892320499.jpeg', '/storage/editor/1892320499.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:11:30', '2020-11-19 10:11:30'),
(16, '/storage/editor/1333963638.jpeg', '/storage/editor/1333963638.jpeg', '/storage/editor/1333963638.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:11:57', '2020-11-19 10:11:57'),
(17, '/storage/editor/1063077198.jpeg', '/storage/editor/1063077198.jpeg', '/storage/editor/1063077198.jpeg', 'Mitsubishi', 0, NULL, '2020-11-19 10:12:17', '2020-11-19 10:12:17'),
(18, '/storage/editor/149383149.jpg', '/storage/editor/149383149.jpg', '/storage/editor/149383149.jpg', 'Mitsubishi', 0, NULL, '2020-11-19 11:45:12', '2020-11-19 11:45:12'),
(19, '/storage/editor/984410500.jpg', '/storage/editor/984410500.jpg', '/storage/editor/984410500.jpg', 'Mitsubishi', 0, NULL, '2020-11-19 11:45:35', '2020-11-19 11:45:35'),
(20, '/storage/editor/64638621.jpg', '/storage/editor/64638621.jpg', '/storage/editor/64638621.jpg', 'Mitsubishi', 0, NULL, '2020-11-19 11:49:25', '2020-11-19 11:49:25'),
(21, '/storage/editor/845781800.png', '/storage/editor/845781800.png', '/storage/editor/845781800.png', 'Mitsubishi', 0, NULL, '2020-11-19 11:56:57', '2020-11-19 11:56:57'),
(22, '/storage/editor/1434022691.png', '/storage/editor/1434022691.png', '/storage/editor/1434022691.png', 'Mitsubishi', 0, NULL, '2020-11-19 11:58:53', '2020-11-19 11:58:53'),
(23, '/storage/editor/1442200859.jpg', '/storage/editor/1442200859.jpg', '/storage/editor/1442200859.jpg', 'Mitsubishi', 0, NULL, '2020-11-19 12:00:19', '2020-11-19 12:00:19'),
(24, '/storage/editor/906687554.png', '/storage/editor/906687554.png', '/storage/editor/906687554.png', 'Mitsubishi', 0, NULL, '2020-11-19 12:06:14', '2020-11-19 12:06:14'),
(25, '/storage/editor/1762664818.jpg', '/storage/editor/1762664818.jpg', '/storage/editor/1762664818.jpg', 'Mitsubishi', 0, NULL, '2020-11-19 12:20:23', '2020-11-19 12:20:23'),
(26, '/storage/editor/742904990.jpg', '/storage/editor/742904990.jpg', '/storage/editor/742904990.jpg', 'Mitsubishi', 0, NULL, '2020-11-19 12:43:47', '2020-11-19 12:43:47'),
(27, '/storage/editor/1799271517.jpg', '/storage/editor/1799271517.jpg', '/storage/editor/1799271517.jpg', 'Mitsubishi', 0, NULL, '2020-11-19 12:44:01', '2020-11-19 12:44:01'),
(28, '/storage/editor/1758807035.jpg', '/storage/editor/1758807035.jpg', '/storage/editor/1758807035.jpg', 'Mitsubishi', 0, NULL, '2020-11-19 12:50:51', '2020-11-19 12:50:51'),
(29, '/storage/editor/425679527.mp4', '/storage/editor/425679527.mp4', '/storage/editor/425679527.mp4', 'Mitsubishi', 1, NULL, '2020-11-21 04:32:30', '2020-11-21 04:32:30');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `password_resets`
--

INSERT INTO `password_resets` (`id`, `email`, `token`, `created_at`, `updated_at`) VALUES
(2, 'ndchien5@gmail.com', '$2y$10$E9C07FrM4IFNOHjaggpdxOYEsMkI3bMnaMJhnaUNrSeA664MnodDu', '2020-11-15 16:23:21', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tintuc`
--

CREATE TABLE `tintuc` (
  `id` int(11) UNSIGNED NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tintuc`
--

INSERT INTO `tintuc` (`id`, `title`, `slug`, `content`, `category`, `avatar`, `status`, `user_id`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'THÔNG BÁO Chiến', 'thong-bao-chien', '<p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">K&iacute;nh gửi Qu&yacute; Kh&aacute;ch h&agrave;ng<br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\">👉&nbsp;Nhằm phục vụ mỗi ng&agrave;y được tốt hơn &rdquo; Mitsubishi Motors Quảng Ninh&rdquo; tr&acirc;n trọng th&ocirc;ng b&aacute;o đến Qu&yacute; Kh&aacute;ch h&agrave;ng về thời gian tạm dừng hoạt động ng&agrave;y 27/06/2020 để chuyển đổi vận h&agrave;nh ch&iacute;nh thức hệ thống SAP S4/HANA như sau:</p><figure style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; display: block; margin: 0px auto 10px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; width: 652.391px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\"><img data-fr-image-pasted=\"true\" src=\"http://mitsubishiquangninh.com/w/wp-content/uploads/2020/06/Th%C3%B4ng-b%C3%A1o-891x1024.jpg\" alt=\"\" srcset=\"http://mitsubishiquangninh.com/w/wp-content/uploads/2020/06/Thông-báo-891x1024.jpg 891w, http://mitsubishiquangninh.com/w/wp-content/uploads/2020/06/Thông-báo-261x300.jpg 261w, http://mitsubishiquangninh.com/w/wp-content/uploads/2020/06/Thông-báo-768x883.jpg 768w, http://mitsubishiquangninh.com/w/wp-content/uploads/2020/06/Thông-báo-1336x1536.jpg 1336w, http://mitsubishiquangninh.com/w/wp-content/uploads/2020/06/Thông-báo-1781x2048.jpg 1781w\" sizes=\"(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px\" style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; border: 0px; padding: 0px; font-size: 16px; background: 0px 0px; font-weight: 400; height: auto; max-width: 100%; width: 652.391px;\" class=\"fr-fic fr-dii\"></figure><p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">⏰Thời gian nghỉ:<br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\">Thứ 7, ng&agrave;y 27/06/2020</p><p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">⏰Thời gian hoạt động trở lại:<br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\">Thứ 2, ng&agrave;y 29/06/2020</p><p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">👉Trong thời gian nghỉ, dừng to&agrave;n bộ hoạt động kinh doanh sửa chữa, bảo dưỡng trong 01 ng&agrave;y</p><p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">👉Bộ phận kinh doanh(hoạt động b&aacute;n xe) vẫn hoạt động b&igrave;nh thường</p><p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">Tr&acirc;n trọng cảm ơn!<br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&ndash;</p><p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">🚘MITSUBISHI MOTORS QUẢNG NINH🚘</p><p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">🎯Khu đ&ocirc; thị mới Nam Ga, Giếng Đ&aacute;y, Th&agrave;nh Phố Hạ Long &ndash; Quảng Ninh</p><p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">Hotline kinh doanh:<br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\">☎️0️⃣9️⃣0️⃣&nbsp;4️⃣0️⃣4️⃣&nbsp;9️⃣5️⃣5️⃣9️⃣</p><p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">Hotline dịch vụ:<br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\">☎️&nbsp;0️⃣9️⃣0️⃣4️⃣2️⃣3️⃣6️⃣2️⃣5️⃣6️⃣</p><p style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent; margin: 15px 0px 0px; padding: 0px; border: 0px; font-size: 16px; vertical-align: baseline; background: 0px 0px rgb(255, 255, 255); font-weight: 400; line-height: 28px; color: rgb(51, 51, 51); font-family: Roboto, Arial, Helvetica, sans-serif; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-thickness: initial; text-decoration-style: initial; text-decoration-color: initial;\">&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&mdash;&ndash;<br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\"><a href=\"https://www.facebook.com/hashtag/mitsubishiquangninh?__eep__=6&source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARDO0_VFdCAI401lPHQBLaAvYjXH0_xGLPpCdZZcrCy96nFg6eCEY8V7oGSntBIQAYjhv29NWvn_Ydibl_sdmxb5JL966ow_hoDHo6i6kJcwckQuNs5ZMo2rQvX1ud7owfIxnah6_0uLPqq8H_9lXLfoEeQ5DpCzBr9OAyjcHyHyNhpNLKFvhWOxMez8RwvRIvyqa3KPVf8cckK8r0EJO9SAXYO19iM8Wz30TXNXUT2F2T0Q994gFwYIW2vKHscKSZc-GlfIU_3zNt0DYV5Du7Qkwh28kPDA7-MqgnVn1X29IH7sjESU80qyfCiqEg6K__i99iOo3oavTYXFMzeudSU&__tn__=%2ANK-R\" style=\"box-sizing: border-box; outline: 0px !important; -webkit-tap-highlight-color: transparent; background: 0px 0px; color: rgb(69, 69, 69); text-decoration: none; margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: dotted; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(43, 43, 43); border-left-color: initial; border-image: initial; font-size: 16px; vertical-align: baseline; font-weight: 400; display: inline; max-width: 100%; transition: border 0.2s ease-in-out 0s;\">#Mitsubishiquangninh</a><br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\"><a href=\"https://www.facebook.com/hashtag/khuy%E1%BA%BFn_m%C3%A3i_d%E1%BB%8Bch_v%E1%BB%A5?__eep__=6&source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARDO0_VFdCAI401lPHQBLaAvYjXH0_xGLPpCdZZcrCy96nFg6eCEY8V7oGSntBIQAYjhv29NWvn_Ydibl_sdmxb5JL966ow_hoDHo6i6kJcwckQuNs5ZMo2rQvX1ud7owfIxnah6_0uLPqq8H_9lXLfoEeQ5DpCzBr9OAyjcHyHyNhpNLKFvhWOxMez8RwvRIvyqa3KPVf8cckK8r0EJO9SAXYO19iM8Wz30TXNXUT2F2T0Q994gFwYIW2vKHscKSZc-GlfIU_3zNt0DYV5Du7Qkwh28kPDA7-MqgnVn1X29IH7sjESU80qyfCiqEg6K__i99iOo3oavTYXFMzeudSU&__tn__=%2ANK-R\" style=\"box-sizing: border-box; outline: 0px !important; -webkit-tap-highlight-color: transparent; background: 0px 0px; color: rgb(69, 69, 69); text-decoration: none; margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: dotted; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(43, 43, 43); border-left-color: initial; border-image: initial; font-size: 16px; vertical-align: baseline; font-weight: 400; display: inline; max-width: 100%; transition: border 0.2s ease-in-out 0s;\">#Khuyến_m&atilde;i_dịch_vụ</a><br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\"><a href=\"https://www.facebook.com/hashtag/khuy%E1%BA%BFn_m%C3%A3i_th%C3%A1ng_6?__eep__=6&source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARDO0_VFdCAI401lPHQBLaAvYjXH0_xGLPpCdZZcrCy96nFg6eCEY8V7oGSntBIQAYjhv29NWvn_Ydibl_sdmxb5JL966ow_hoDHo6i6kJcwckQuNs5ZMo2rQvX1ud7owfIxnah6_0uLPqq8H_9lXLfoEeQ5DpCzBr9OAyjcHyHyNhpNLKFvhWOxMez8RwvRIvyqa3KPVf8cckK8r0EJO9SAXYO19iM8Wz30TXNXUT2F2T0Q994gFwYIW2vKHscKSZc-GlfIU_3zNt0DYV5Du7Qkwh28kPDA7-MqgnVn1X29IH7sjESU80qyfCiqEg6K__i99iOo3oavTYXFMzeudSU&__tn__=%2ANK-R\" style=\"box-sizing: border-box; outline: 0px !important; -webkit-tap-highlight-color: transparent; background: 0px 0px; color: rgb(69, 69, 69); text-decoration: none; margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: dotted; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(43, 43, 43); border-left-color: initial; border-image: initial; font-size: 16px; vertical-align: baseline; font-weight: 400; display: inline; max-width: 100%; transition: border 0.2s ease-in-out 0s;\">#Khuyến_m&atilde;i_th&aacute;ng_6</a><br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\"><a href=\"https://www.facebook.com/hashtag/mitsubishimotorsquangninh?__eep__=6&source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARDO0_VFdCAI401lPHQBLaAvYjXH0_xGLPpCdZZcrCy96nFg6eCEY8V7oGSntBIQAYjhv29NWvn_Ydibl_sdmxb5JL966ow_hoDHo6i6kJcwckQuNs5ZMo2rQvX1ud7owfIxnah6_0uLPqq8H_9lXLfoEeQ5DpCzBr9OAyjcHyHyNhpNLKFvhWOxMez8RwvRIvyqa3KPVf8cckK8r0EJO9SAXYO19iM8Wz30TXNXUT2F2T0Q994gFwYIW2vKHscKSZc-GlfIU_3zNt0DYV5Du7Qkwh28kPDA7-MqgnVn1X29IH7sjESU80qyfCiqEg6K__i99iOo3oavTYXFMzeudSU&__tn__=%2ANK-R\" style=\"box-sizing: border-box; outline: 0px !important; -webkit-tap-highlight-color: transparent; background: 0px 0px; color: rgb(69, 69, 69); text-decoration: none; margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: dotted; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(43, 43, 43); border-left-color: initial; border-image: initial; font-size: 16px; vertical-align: baseline; font-weight: 400; display: inline; max-width: 100%; transition: border 0.2s ease-in-out 0s;\">#Mitsubishimotorsquangninh</a><br style=\"box-sizing: border-box; outline: 0px; -webkit-tap-highlight-color: transparent;\"><a href=\"https://www.facebook.com/hashtag/khuy%E1%BA%BFn_m%C3%A3i?__eep__=6&source=feed_text&epa=HASHTAG&__xts__%5B0%5D=68.ARDO0_VFdCAI401lPHQBLaAvYjXH0_xGLPpCdZZcrCy96nFg6eCEY8V7oGSntBIQAYjhv29NWvn_Ydibl_sdmxb5JL966ow_hoDHo6i6kJcwckQuNs5ZMo2rQvX1ud7owfIxnah6_0uLPqq8H_9lXLfoEeQ5DpCzBr9OAyjcHyHyNhpNLKFvhWOxMez8RwvRIvyqa3KPVf8cckK8r0EJO9SAXYO19iM8Wz30TXNXUT2F2T0Q994gFwYIW2vKHscKSZc-GlfIU_3zNt0DYV5Du7Qkwh28kPDA7-MqgnVn1X29IH7sjESU80qyfCiqEg6K__i99iOo3oavTYXFMzeudSU&__tn__=%2ANK-R\" style=\"box-sizing: border-box; outline: 0px !important; -webkit-tap-highlight-color: transparent; background: 0px 0px; color: rgb(69, 69, 69); text-decoration: none; margin: 0px; padding: 0px; border-width: 0px 0px 1px; border-top-style: initial; border-right-style: initial; border-bottom-style: dotted; border-left-style: initial; border-top-color: initial; border-right-color: initial; border-bottom-color: rgb(43, 43, 43); border-left-color: initial; border-image: initial; font-size: 16px; vertical-align: baseline; font-weight: 400; display: inline; max-width: 100%; transition: border 0.2s ease-in-out 0s;\">#khuyến_m&atilde;i</a></p><p><br></p><p><span class=\"fr-video fr-deletable fr-fvc fr-dvb fr-draggable\" contenteditable=\"true\" draggable=\"true\"><iframe src=\"http://noithatzip.com.vn/00khtn/tailieu/chien.pdf\" title=\"chien.pdf\" class=\"fr-file fr-draggable\" style=\"width: 599px; height: 388px;\"></iframe></span></p>', 'tin-tong-hop', '/storage/tintuc/1383335722.jpg', 1, 1, NULL, '2020-11-19 12:06:30', '2020-11-19 13:14:11'),
(2, 'ádasdsa', 'adasdsa', '<p><span class=\"fr-video fr-dvb fr-draggable\" contenteditable=\"false\" draggable=\"true\"><video class=\"fr-draggable\" controls=\"\" src=\"http://noithatzip.com.vn/video/Dahlia.mp4\" style=\"width: 600px;\">Your browser does not support HTML5 video.</video></span></p><p>đ&igrave;nh chiến 1233324234</p>', 'su-kien-noi-bat', '/storage/tintuc/1426826982.jpg', 1, 1, NULL, '2020-11-21 04:02:47', '2020-11-21 04:05:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'mitsubishi', 'ndchien5@gmail.com', NULL, '$2y$10$nle./nHbHa8ycmtRfAj/wOkhcWw1q55/LtduCrhKgq7ITSgF7Dpf.', NULL, '2020-11-15 12:54:57', '2020-11-15 12:54:57');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `file_manager`
--
ALTER TABLE `file_manager`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `file_manager`
--
ALTER TABLE `file_manager`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tintuc`
--
ALTER TABLE `tintuc`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
