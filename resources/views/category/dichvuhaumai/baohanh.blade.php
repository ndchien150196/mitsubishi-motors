@extends("master")
@section("content") 
<div class="second-block page-news-home">
    <div class="grid-fluid">
        <h3 class="title-block">
            <span>Chính sách bảo hành</span>
        </h3>
        <div class="grid-content">
            <div class="row">
                <div class="col-md-8">
                    <div class="editor">
                        <p><em>Ghi chú: Chính sách bảo hành này có hiệu lực cho các xe Mitsubishi phân phối bởi Công ty Ôtô Mitsubishi Việt Nam (MMV) từ ngày 11/04/2016 trở về sau.</em></p>
                        <p><strong>1. CHÍNH SÁCH BẢO HÀNH Ô TÔ MỚI</strong></p>
                        <p>
                            MMV bảo đảm rằng tất cả các chi tiết thuộc CHI TIẾT BẢO HÀNH CHUNG &nbsp;và CHI TIẾT ĐẶC BIỆT (Vui lòng tham khảo CÁC CHI TIẾT ĐƯỢC BẢO HÀNH) của xe ô tô mới do MMV sản xuất hoặc phân phối (sau đây gọi là “xe ô
                            tô MMV mới”) mà được xác định là hư hỏng trong điều kiện sử dụng và bảo dưỡng bình thường và còn trong thời gian bảo hành sẽ được sửa chữa hoặc thay thế miễn phí tại bất kỳ đại lý ủy quyền của MMV.
                        </p>
                        <p><strong>2. NHỮNG GÌ ĐƯỢC BẢO HÀNH:</strong></p>
                        <p><strong>(1) CHI TIẾT ĐƯỢC BẢO HÀNH:</strong></p>
                        <p>CHI TIẾT BẢO HÀNH CHUNG và CHI TIẾT ĐẶC BIỆT là các chi tiết của xe ô tô MMV mới mà không thuộc nhóm “CHI TIẾT KHÔNG BẢO HÀNH” là những chi tiết được bảo hành.</p>
                        <p><strong>(2) THỜI GIAN BẢO HÀNH:</strong></p>
                        <ol>
                            <li>Thời gian bảo hành được tính từ thời điểm xe ô tô MMV mới được giao tới người mua đầu tiên ghi trong “Biên Bản Đăng Ký” (Sau đây gọi là “Ngày Bắt Đầu Bảo Hành”).</li>
                            <li>
                                Các CHI TIẾT BẢO HÀNH CHUNG là các chi tiết không thuộc nhóm CHI TIẾT ĐẶC BIỆT và CHI TIẾT KHÔNG BẢO HÀNH sẽ được bảo hành trong 36 tháng hoặc 100,000 km, tùy điều kiện nào đến trước kể từ “Ngày Bắt Đầu Bảo
                                Hành”.
                            </li>
                            <li>
                                Các CHI TIẾT ĐẶC BIỆT bao gồm:
                                <ol>
                                    <li>Thiết bị âm thanh (Radio, CD, DVD, màn hình) được lắp nguyên bản trên xe sẽ được bảo hành trong 36 tháng hoặc 100,000 km, tùy điều kiện nào đến trước kể từ “Ngày Bắt Đầu Bảo Hành”.</li>
                                    <li>Ắc-quy được lắp nguyên bản trên xe sẽ được bảo hành trong 12 tháng hoặc 20,000 km, tùy điều kiện nào đến trước kể từ “Ngày Bắt Đầu Bảo Hành”.</li>
                                </ol>
                            </li>
                        </ol>
                        <p><strong>3. NHỮNG GÌ KHÔNG ĐƯỢC BẢO HÀNH:</strong></p>
                        <p><strong>(1) &nbsp;CÁC CHI TIẾT KHÔNG ĐƯỢC BẢO HÀNH:</strong></p>
                        <p>
                            CÁC CHI TIẾT TRUNG GIAN ĐẶC BIỆT<br />
                            CÁC CHI TIẾT TRUNG GIAN ĐẶC BIỆT như lốp, săm, các chi tiết và thiết bị không phải do chính MMV lắp đặt sẽ không thuộc chính sách bảo hành này mà do nhà sản xuất liên quan đến chi tiết đó bảo hành.<br />
                            CÁC CHI TIẾT MAU HỎNG<br />
                            CÁC CHI TIẾT MAU HỎNG như Lõi lọc gió, Lõi lọc dầu bôi trơn, Lõi lọc nhiên liệu, Dây đai truyền động (ngoại trừ đai cam), Bu gi (ngoại trừ Bu gi có điện cực Platinum hoặc Iridium), mặt ma sát ly hợp, má phanh và
                            bố phanh, lưỡi gạt mưa, cầu chì, bóng đèn (trừ bóng đèn pha kín, bóng đèn Halogen, bóng đèn cao áp HID), các chi tiết mau hỏng khác sẽ không thuộc CHÍNH SÁCH BẢO HÀNH này.<br />
                            CÁC CHẤT BÔI TRƠN VÀ CÁC LOẠI KHÁC<br />
                            Các vật tư như các loại dầu, mỡ bôi trơn, nước làm mát, ga lạnh, điện dịch ắc quy, nhiên liệu sẽ không thuộc CHÍNH SÁCH BẢO HÀNH này.
                        </p>
                        <p><strong>(2) &nbsp;CÁC HƯ HỎNG VÀ ĐIỀU KIỆN KHÔNG ĐƯỢC BẢO HÀNH:</strong></p>
                        <p>Chính sách bảo hành này không áp dụng cho:</p>
                        <p>CÁC HAO MÒN HAY RÁCH HỎNG, SỰ XUỐNG CẤP VÀ TAI NẠN/THIÊN TAI</p>
                        <ol>
                            <li>Các hao mòn và rách hỏng của các chi tiết, các hạng mục <a href="http://mitsubishi-thaibinh.vn/">bảo dưỡng</a> thông thường.</li>
                            <li>Sự xuống cấp hay hư hỏng bình thường của các chi tiết tráng mạ, bề mặt sơn, chi tiết cao su, vật liệu phủ bọc và những chi tiết trang trí mềm.</li>
                            <li>Hư hỏng do các yếu tố bên ngoài như ô nhiễm hóa học, mưa a-xít, tai nạn giao thông, mưa đá, cát, muối, đá, hỏa hoạn hay thiên tai.</li>
                        </ol>
                        <p>SỬ DỤNG, BẢO DƯỠNG KHÔNG ĐÚNG CÁCH HOẶC SỬA ĐỔI</p>
                        <ol>
                            <li>
                                Hư hỏng do bảo dưỡng không đầy đủ, không đúng, sử dụng phụ tùng, vật tư và các chất bôi trơn, chất lỏng không được sự phê duyệt của MMV và các sửa chữa, thay thế bởi cơ sở nào khác ngoài MMV hoặc đại lý ủy
                                quyền của MMV.
                            </li>
                            <li>Hư hỏng thùng xe và cabi xe do chất hàng.</li>
                            <li>Hư hỏng do vận hành không đúng cách, sử dụng sai hoặc dùng xe ô tô MMV trong những điều kiện bất thường.</li>
                            <li>Hư hỏng do sửa đổi lại xe ô tô MMV mới.</li>
                        </ol>
                        <p>CÁC VẤN ĐỀ KHÁC</p>
                        <ol>
                            <li>Những biểu hiện hơi sai quy cách mà không ảnh hưởng đến chất lượng, sự vận hành hoặc tính năng của xe hay các chi tiết (ví dụ hơi ồn hoặc hơi rung).</li>
                            <li>Những thiệt hại đi kèm như không sử dụng được xe, mất thời gian và các chi phí hay các phí tổn liên quan đến tài sản cá nhân, hoạt động kinh doanh hay thu nhập.</li>
                            <li>Bất kỳ xe ô tô MMV mới nào mà chỉ số đồng hồ quãng đường sử dụng đã bị thay đổi nên không xác định được quãng đường xe đã chạy.</li>
                        </ol>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="news-sidebar">
                        <div class="sidebar-block link-block">
                            <h3 class="news-link-title">Dịch vụ hậu mãi</h3>
                            <!-- menu-->
                            <ul class="news-link">
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['gioi-thieu-dich-vu'])}}" title="Giới thiệu dịch vụ" class="sp-text">Giới thiệu dịch vụ</a>
                                </li>
                                <li class="active">
                                    <a href="{{url('dich-vu-hau-mai',['chinh-sach-bao-hanh'])}}" title="Chính sách bảo hành" class="sp-text">Chính sách bảo hành</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['bao-duong-dinh-ky'])}}" title="Bảo dưỡng định kỳ" class="sp-text">Bảo dưỡng định kỳ</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['phu-tung-chinh-hieu'])}}" title="Phụ tùng chính hiệu" class="sp-text">Phụ tùng chính hiệu</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['sua-chua'])}}" title="Sửa chữa" class="sp-text">Sửa chữa</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['dat-lich-bao-duong'])}}" title="Đặt lịch bảo dưỡng" class="sp-text">Đặt lịch bảo dưỡng</a>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-12">
                                <div class="sidebar-block drive-block">
                                    <ul class="drive-link">
                                        <li class="drive-item">
                                            <span class="icon"><i class="svg-icon icon-support"></i></span><span class="text">Hỗ trợ kỹ thuật</span><a href="tel:{{\App\Common::phonedv()}}" title="{{\App\Common::phonedv()}}" class="link">{{\App\Common::phonedv()}}</a>
                                        </li>
                                        <li class="drive-item">
                                            <span class="icon"><i class="svg-icon icon-customer"></i></span><span class="text">Chăm sóc khách hàng</span><a href="tel:{{\App\Common::phonelh()}}" title="{{\App\Common::phonelh()}}" class="link">{{\App\Common::phonelh()}}</a>
                                        </li>
                                        <li class="drive-item">
                                            <span class="icon"><i class="svg-icon icon-email"></i></span><span class="text">Email&nbsp; </span>
                                            <a href="mailto:{{\App\Common::email()}}" title="{{\App\Common::email()}}" class="link mail"> {{\App\Common::email()}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @include('//layouts/block-km')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
