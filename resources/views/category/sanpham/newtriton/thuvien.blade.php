<!--Gallery-->
<div class="normal-block thuvien">
    <div class="grid-inner">
        <h3 class="title-block pr-title-block"><span>Thư viện hình ảnh &amp; catalogue</span></h3>
        <div class="row">
            <div class="col-md-6 catalogue-download">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2 col-md-offset-1">
                        <picture>
                            <!--if IE 9video(style='display: none;')
                                      -->
                            <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/11/viber_image_2019-11-05_16-41-45.jpg" alt="New Triton" media="(min-width: 768px)" />
                            <!--if IE 9-->
                            <img
                                src="{{asset('img/newtriton/viber_image_2019-11-05_16-41-45.jpg')}}"
                                alt="Outlander Sport"
                            />
                        </picture>
                    </div>
                    <div class="col-sm-6 col-md-7">
                        <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/small_Final-MIT_TRITON_Brochure_30x24cm_FA_Final__0524.pdf" title="Tải Catalogue" class="btn btn-icon-left" target="_blank">
                            <span class="icon"><i class="svg-icon icon-download"></i></span><span class="text">Tải Catalogue</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="video">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/1Vg2y-7Enb4"
                            frameborder="0"
                            allowfullscreen=""
                        ></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-photo">
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/mainvis_05.jpg" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newtriton/mainvis_05-390x390.jpg')}}"
                        alt="mainvis_05"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/mainvis_03.jpg" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newtriton/mainvis_03-390x390.jpg')}}"
                        alt="mainvis_03"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/mainvis_02.jpg" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newtriton/mainvis_02-390x390.jpg')}}"
                        alt="mainvis_02"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/mainvis_01.jpg" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newtriton/mainvis_01-390x390.jpg')}}"
                        alt="mainvis_01"
                    />
                </a>
            </div>
        </div>
    </div>
</div>
<!--End - Gallery-->