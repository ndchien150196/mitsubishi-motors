<!--Highlight-->
<div class="normal-block dacdiemnoibat">
    <div class="grid-inner">
        <h3 class="title-block"><span>Đặc điểm nổi bật</span></h3>
        <div class="row">
            <div class="col-md-4 col-lg-3">
                <div class="pr-info">
                    <h1 class="title">New Xpander</h1>
                    <p class="summary">
                        Kế thừa những ưu điểm của phiên bản tiền nhiệm, Xpander mới với những nâng cấp hiện đại hơn sẽ tiếp tục mở ra các “HÀNH TRÌNH RỘNG MỞ” phía trước cùng các gia đình tại Việt Nam, những người vốn đang cân nhắc
                        lựa chọn cho mình một mẫu xe phù hợp với túi tiền với nhiều tiện ích và giá trị sử dụng.
                    </p>
                </div>
            </div>

            <!--.pr-info-->
            <div class="col-md-8 col-lg-9">
                <div class="slide-dacdiemnoibat">
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/new-xpander/Top-feature-1.jpg')}}"
                                    alt="Đèn chiếu sáng dạng LED"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/08/Top-feature-1.jpg"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">Đèn chiếu sáng dạng LED</h4>
                                <p class="summary"></p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/new-xpander/Top-feature-2.jpg')}}"
                                    alt="Nội thất 7 chỗ rộng rãi và tinh tế"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/08/Top-feature-2.jpg"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">Nội thất 7 chỗ rộng rãi và tinh tế</h4>
                                <p class="summary"></p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/new-xpander/Top-feature-3.jpg')}}"
                                    alt="Màn hình giải trí 7-inch"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/08/Top-feature-3.jpg"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">Màn hình giải trí 7-inch</h4>
                                <p class="summary">Kết nối Apple CarPlay &amp; Android Auto</p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <!--.slide-center-->
            </div>
        </div>
    </div>
</div>
<!--End - Highlight-->
<!--===================-->