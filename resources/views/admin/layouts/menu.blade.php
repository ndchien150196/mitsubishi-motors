<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header"></li>
      <li>
      	<a href="{{route('admin.home')}}">
      		<i class="fa fa-dashboard"></i>
      		<span>Dashboard</span>
      	</a>
      </li> 
      <li>
        <a href="{{route('admin.tintuc')}}">
          <i class="fa fa-newspaper-o"></i>
          <span>Tin Tức</span>
        </a>
      </li>
      <!-- <li>
        <a href="{{route('admin.tailieu')}}">
          <i class="fa fa-file-pdf-o"></i>
          <span>Tài Liệu</span>
        </a>
      </li> -->
      <li>
        <a href="{{route('admin.slide')}}">
          <i class="fa fa-sliders"></i>
          <span>Slide</span>
        </a>
      </li>
      <li>
        <a href="{{route('admin.car')}}">
          <i class="fa fa-car"></i>
          <span>Giá Xe</span>
        </a>
      </li>
      <li>
        <a href="{{route('logout')}}">
          <i class="fa fa-sign-out"></i>
          <span>Đăng Xuất</span>
        </a>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>