@extends("master")
@section("content") 
<div class="normal-block">
    <div class="grid-fluid">
        <h1 class="title-block"><span>Bảng giá xe &amp; khuyến mãi tháng 11/2020</span></h1>
        <div class="price-block">
            <div class="price-item" id="xpander-cross">
                <div class="price-box">
                    <div class="price-content">
                        <div class="price-header">
                            <div class="price-img">
                                <picture>
                                    <img src="{{asset('img/menu/Cross.png')}}" alt="Xpander Cross"/>
                                </picture>
                            </div>
                            <h3 class="price-title">Xpander Cross</h3>
                            <ul class="price-nav">
                                <li>
                                    <a href="{{asset('san-pham/xpander-cross/')}}" title="Xpander Cross"> <i class="icon icon-info"></i> Chi tiết sản phẩm </a>
                                </li>
                                <li>
                                    <a
                                        href="javascript:;"
                                        class="d2-estimate"
                                        data-step="1"
                                        data-car-id="116"
                                        data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                        title="Dự tính chi phí"
                                        data-tracking-click="Estimated Cost"
                                        data-tracking-click-cat="Xpander Cross"
                                    >
                                        <i class="icon icon-cost"></i> Dự tính chi phí
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/dang-ky-lai-thu/Xpander-Cross/')}}" title="Đăng ký lái thử"> <i class="icon icon-drive"></i> Đăng ký lái thử </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/bao-gia-chi-tiet/Xpander-Cross/')}}" title="Báo giá chi tiết"> <i class="icon icon-price"></i> Báo giá chi tiết </a>
                                </li>
                            </ul>
                        </div>
                        <div class="table-content">
                            <div class="table-area">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Phiên bản</th>
                                            <th>Giá bán lẻ</th>
                                            <th>Quà tặng đặc biệt tháng 11</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><strong>Xpander Cross</strong></td>
                                            <td>{{$product['Xpander_Cross']}}</td>
                                            <td>1 năm bảo hiểm vật chất (Trị giá 10 triệu VNĐ)</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="desc-area">
                                <p></p>
                                <p>Đơn vị: VNĐ</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="price-item" id="new-pajero-sport">
                <div class="price-box">
                    <div class="price-content">
                        <div class="price-header">
                            <div class="price-img">
                                <picture>
                                    <img src="{{asset('img/menu/Pajero-sport.png')}}" alt="New Pajero Sport"/>
                                </picture>
                            </div>
                            <h3 class="price-title">New Pajero Sport</h3>
                            <ul class="price-nav">
                                <li>
                                    <a href="{{asset('san-pham/new-pajero-sport/')}}" title="New Pajero Sport"> <i class="icon icon-info"></i> Chi tiết sản phẩm </a>
                                </li>
                                <li>
                                    <a
                                        href="javascript:;"
                                        class="d2-estimate"
                                        data-step="1"
                                        data-car-id="139"
                                        data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                        title="Dự tính chi phí"
                                        data-tracking-click="Estimated Cost"
                                        data-tracking-click-cat="New Pajero Sport"
                                    >
                                        <i class="icon icon-cost"></i> Dự tính chi phí
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/dang-ky-lai-thu/NEW-PAJERO-SPORT/')}}" title="Đăng ký lái thử"> <i class="icon icon-drive"></i> Đăng ký lái thử </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/bao-gia-chi-tiet/NEW-PAJERO-SPORT/')}}" title="Báo giá chi tiết"> <i class="icon icon-price"></i> Báo giá chi tiết </a>
                                </li>
                            </ul>
                        </div>
                        <div class="table-content">
                            <div class="table-area">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td style="width: 104px; text-align: center;"><strong>Phiên bản</strong></td>
                                            <td style="width: 107.66px; text-align: center;"><strong>Giá bán lẻ</strong></td>
                                            <td style="width: 227.34px; text-align: center;"><strong>Ưu đãi đặc biệt</strong></td>
                                        </tr>
                                        <tr>
                                            <td style="width: 104px;">Diesel 4×4 AT</td>
                                            <td style="width: 107.66px;">{{$product['Diesel_4×4_AT']}} VNĐ</td>
                                            <td style="width: 227.34px;" rowspan="2">
                                                Cho những khách hàng mua xe đầu tiên bao gồm:
                                                <p></p>
                                                <p>+ Bộ phụ kiện thể thao</p>
                                                <p>+ Iphone 11 Pro Max 64Gb</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Diesel 4×2 AT</td>
                                            <td>{{$product['Diesel_4×2_AT']}} VNĐ</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Gastroline 4×4 AT
                                                <p></p>
                                                <p><span style="font-size: 8pt;">(Chỉ áp dụng cho khách hàng dự án)</span></p>
                                            </td>
                                            <td>{{$product['Gastroline_4×4_AT']}} VNĐ</td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="desc-area">
                                <p></p>
                                <p>Đơn vị: VNĐ</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="price-item" id="new-xpander">
                <div class="price-box">
                    <div class="price-content">
                        <div class="price-header">
                            <div class="price-img">
                                <picture>
                                    <img src="{{asset('img/menu/Xpander-Thumbnail.png')}}" alt="New Xpander"/>
                                </picture>
                            </div>
                            <h3 class="price-title">New Xpander</h3>
                            <ul class="price-nav">
                                <li>
                                    <a href="{{asset('san-pham/new-xpander/')}}" title="New Xpander"> <i class="icon icon-info"></i> Chi tiết sản phẩm </a>
                                </li>
                                <li>
                                    <a
                                        href="javascript:;"
                                        class="d2-estimate"
                                        data-step="1"
                                        data-car-id="132"
                                        data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                        title="Dự tính chi phí"
                                        data-tracking-click="Estimated Cost"
                                        data-tracking-click-cat="New Xpander"
                                    >
                                        <i class="icon icon-cost"></i> Dự tính chi phí
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/dang-ky-lai-thu/NEW-XPANDER/')}}" title="Đăng ký lái thử"> <i class="icon icon-drive"></i> Đăng ký lái thử </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/bao-gia-chi-tiet/NEW-XPANDER/')}}" title="Báo giá chi tiết"> <i class="icon icon-price"></i> Báo giá chi tiết </a>
                                </li>
                            </ul>
                        </div>
                        <div class="table-content">
                            <div class="table-area">
                                <table style="width: 488px;">
                                    <thead>
                                        <tr>
                                            <th style="width: 133px; text-align: center;">Phiên bản</th>
                                            <th style="width: 145px; text-align: center;">Giá bán lẻ</th>
                                            <th style="width: 164px; text-align: center;">Quà tặng đặc biệt tháng 11</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="width: 133px; text-align: center;"><strong>Xpander AT – Nhập khẩu (CBU)</strong></td>
                                            <td style="width: 145px; text-align: center;">{{$product['Xpander_AT_NK']}}</td>
                                            <td style="width: 164px;">
                                                <ul>
                                                    <li>Ưu đãi 50% Thuế trước bạ (Trị giá 32 triệu VNĐ)</li>
                                                    <li>Tặng 1 năm bảo hiểm vật chất (Trị giá 10 triệu VNĐ)</li>
                                                </ul>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 133px; text-align: center;"><strong>Xpander AT – Lắp ráp trong nước (CKD)</strong></td>
                                            <td style="width: 145px; text-align: center;">{{$product['Xpander_AT_CKD']}}</td>
                                            <td style="width: 164px;">Tặng 1 năm bảo hiểm vật chất (Trị giá 10 triệu VNĐ)</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 133px; text-align: center;">
                                                <strong>Xpander MT</strong><strong><br /> </strong>
                                            </td>
                                            <td style="width: 145px; text-align: center;">{{$product['Xpander_MT']}}</td>
                                            <td style="width: 164px;">
                                                <ul>
                                                    <li>Ưu đãi 50% Thuế trước bạ (Trị giá 28 triệu VNĐ)</li>
                                                    <li>Tặng 1 năm bảo hiểm vật chất (Trị giá 8.5 triệu VNĐ)</li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="desc-area">
                                <p></p>
                                <p><span style="font-family: verdana, geneva, sans-serif; font-size: 10pt;">Đơn vị: VNĐ</span></p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="price-item" id="new-attrage">
                <div class="price-box">
                    <div class="price-content">
                        <div class="price-header">
                            <div class="price-img">
                                <picture>
                                    <img src="{{asset('img/menu/ELL-MY20_-final.png')}}" alt="New Attrage"/>
                                </picture>
                            </div>
                            <h3 class="price-title">New Attrage</h3>
                            <ul class="price-nav">
                                <li>
                                    <a href="{{asset('san-pham/new-attrage/')}}" title="New Attrage"> <i class="icon icon-info"></i> Chi tiết sản phẩm </a>
                                </li>
                                <li>
                                    <a
                                        href="javascript:;"
                                        class="d2-estimate"
                                        data-step="1"
                                        data-car-id="122"
                                        data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                        title="Dự tính chi phí"
                                        data-tracking-click="Estimated Cost"
                                        data-tracking-click-cat="New Attrage"
                                    >
                                        <i class="icon icon-cost"></i> Dự tính chi phí
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/dang-ky-lai-thu/New-Attrage/')}}" title="Đăng ký lái thử"> <i class="icon icon-drive"></i> Đăng ký lái thử </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/bao-gia-chi-tiet/New-Attrage/')}}" title="Báo giá chi tiết"> <i class="icon icon-price"></i> Báo giá chi tiết </a>
                                </li>
                            </ul>
                        </div>
                        <div class="table-content">
                            <div class="table-area">
                                <table style="width: 466px;">
                                    <thead>
                                        <tr>
                                            <th style="text-align: center; width: 76px;">Phiên bản</th>
                                            <th style="text-align: center; width: 168.402px;">Giá bán lẻ</th>
                                            <th style="text-align: center; width: 175.598px;">Ưu đãi đặc biệt tháng 11</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td style="text-align: center; width: 76px;"><b>Attrage MT</b></td>
                                            <td style="text-align: center; width: 168.402px;"><b>{{$product['Attrage_MT']}}</b></td>
                                            <td style="text-align: center; width: 175.598px;"><b>50% thuế trước bạ (Trị giá 19 triệu VNĐ)</b></td>
                                        </tr>
                                        <tr>
                                            <td style="text-align: center; width: 76px;"><b>Attrage CVT</b></td>
                                            <td style="text-align: center; width: 168.402px;"><b>{{$product['Attrage_CVT']}}</b></td>
                                            <td style="text-align: center; width: 168.402px;"><b>Bộ phụ kiện theo xe + 50% thuế trước bạ (Trị giá 23 triệu VNĐ)</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="desc-area">
                                <p></p>
                                <p>Đơn vị: VNĐ</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="price-item" id="new-outlander">
                <div class="price-box">
                    <div class="price-content">
                        <div class="price-header">
                            <div class="price-img">
                                <picture>
                                    <img src="{{asset('img/menu/RE-MY20.png')}}" alt="New Outlander"/>
                                </picture>
                            </div>
                            <h3 class="price-title">New Outlander</h3>
                            <ul class="price-nav">
                                <li>
                                    <a href="{{asset('san-pham/new-outlander/')}}" title="New Outlander"> <i class="icon icon-info"></i> Chi tiết sản phẩm </a>
                                </li>
                                <li>
                                    <a
                                        href="javascript:;"
                                        class="d2-estimate"
                                        data-step="1"
                                        data-car-id="121"
                                        data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                        title="Dự tính chi phí"
                                        data-tracking-click="Estimated Cost"
                                        data-tracking-click-cat="New Outlander"
                                    >
                                        <i class="icon icon-cost"></i> Dự tính chi phí
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/dang-ky-lai-thu/New-Outlander/')}}" title="Đăng ký lái thử"> <i class="icon icon-drive"></i> Đăng ký lái thử </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/bao-gia-chi-tiet/New-Outlander/')}}" title="Báo giá chi tiết"> <i class="icon icon-price"></i> Báo giá chi tiết </a>
                                </li>
                            </ul>
                        </div>
                        <div class="table-content">
                            <div class="table-area">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span style="color: #000000;"><strong>Phiên bản</strong></span>
                                            </td>
                                            <td>
                                                <span style="color: #000000;"><strong>Giá bán lẻ (VNĐ)</strong></span>
                                            </td>
                                            <td>
                                                <span style="color: #000000;"><strong>Ưu đãi đặc biệt tháng 11</strong></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span style="color: #000000;">CVT 2.0</span></td>
                                            <td><span style="color: #000000;">{{$product['CVT_2']}}</span></td>
                                            <td>
                                                – Ghế da cao cấp&nbsp;(Trị giá 8.5 triệu VNĐ)
                                                <p></p>
                                                <p>– Ưu đãi 50% thuế trước bạ còn lại (Trị giá 41 triệu VNĐ)</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span style="color: #000000;">CVT 2.0 Premium</span></td>
                                            <td><span style="color: #000000;">{{$product['CVT_2_P']}}</span></td>
                                            <td>
                                                – Camera 360&nbsp;(Trị giá 20 triệu VNĐ)
                                                <p></p>
                                                <p>– Ưu đãi 50% thuế trước bạ còn lại (Trị giá 47 triệu VNĐ)</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><span style="color: #000000;">CVT 2.4 Premium</span></td>
                                            <td><span style="color: #000000;">{{$product['CVT_24_P']}}</span></td>
                                            <td>
                                                – Camera 360&nbsp;(Trị giá 20 triệu VNĐ)
                                                <p></p>
                                                <p>– Bảo hiểm vật chất (Trị giá 16 triệu VNĐ)</p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="desc-area">
                                <p></p>
                                <p>Đơn vị giá: VNĐ</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="price-item" id="newtriton">
                <div class="price-box">
                    <div class="price-content">
                        <div class="price-header">
                            <div class="price-img">
                                <picture>
                                    <img src="{{asset('img/menu/Triton.png')}}" alt="New Triton"/>
                                </picture>
                            </div>
                            <h3 class="price-title">New Triton</h3>
                            <ul class="price-nav">
                                <li>
                                    <a href="{{asset('san-pham/newtriton/')}}" title="New Triton"> <i class="icon icon-info"></i> Chi tiết sản phẩm </a>
                                </li>
                                <li>
                                    <a
                                        href="javascript:;"
                                        class="d2-estimate"
                                        data-step="1"
                                        data-car-id="22"
                                        data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                        title="Dự tính chi phí"
                                        data-tracking-click="Estimated Cost"
                                        data-tracking-click-cat="New Triton"
                                    >
                                        <i class="icon icon-cost"></i> Dự tính chi phí
                                    </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/dang-ky-lai-thu/New-Triton/')}}" title="Đăng ký lái thử"> <i class="icon icon-drive"></i> Đăng ký lái thử </a>
                                </li>
                                <li>
                                    <a href="{{asset('mua-xe/bao-gia-chi-tiet/New-Triton/')}}" title="Báo giá chi tiết"> <i class="icon icon-price"></i> Báo giá chi tiết </a>
                                </li>
                            </ul>
                        </div>
                        <div class="table-content">
                            <div class="table-area">
                                <table style="width: 100%;">
                                    <tbody>
                                        <tr style="height: 36px;">
                                            <td style="height: 36px;" width="137">
                                                <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt;"><strong>Phiên bản</strong></span>
                                            </td>
                                            <td style="height: 36px;" width="178">
                                                <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt;"><strong>Giá bán lẻ đề xuất (VNĐ)</strong></span>
                                            </td>
                                            <td style="height: 36px;" width="337">
                                                <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt;"><strong>Ưu đãi đặc biệt tháng 10</strong></span>
                                            </td>
                                        </tr>
                                        <tr style="height: 64px;">
                                            <td style="height: 64px;" width="137">
                                                <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt; color: #000000;"><strong>4×4 AT&nbsp;MIVEC Premium</strong></span>
                                            </td>
                                            <td style="height: 64px;" width="178"><span style="color: #000000; font-family: verdana, geneva, sans-serif; font-size: 10pt;">{{$product['AT_MIVEC_Premium_4']}}</span></td>
                                            <td style="height: 240px;" rowspan="2" width="337">
                                                <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt; color: #000000;">–&nbsp; Nắp thùng&nbsp;&amp; Camera lùi</span>
                                                <p></p>
                                                <p><span style="color: #000000; font-family: verdana, geneva, sans-serif; font-size: 10pt;">Hoặc</span></p>
                                                <p><span style="font-family: verdana, geneva, sans-serif; font-size: 10pt; color: #000000;">– Bảo hiểm vật chất&nbsp;&amp; Camera lùi</span></p>
                                            </td>
                                        </tr>
                                        <tr style="height: 57px;">
                                            <td style="height: 57px;" width="137">
                                                <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt; color: #000000;"><strong>4×2 AT MIVEC Premium</strong></span>
                                            </td>
                                            <td style="height: 57px;" width="178"><span style="color: #000000; font-family: verdana, geneva, sans-serif; font-size: 10pt;">{{$product['AT_MIVEC_Premium_2']}}</span></td>
                                        </tr>
                                        <tr style="height: 26px;">
                                            <td style="height: 26px;" width="137">
                                                <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt; color: #000000;"><strong>4×4 MT&nbsp;MIVEC</strong></span>
                                            </td>
                                            <td style="height: 26px;" width="178"><span style="color: #000000; font-family: verdana, geneva, sans-serif; font-size: 10pt;">{{$product['MT_MIVEC_4']}}</span></td>
                                            <td style="height: 73px;" width="337"></td>
                                        </tr>
                                        <tr style="height: 21px;">
                                            <td style="height: 21px;" width="137">
                                                <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt; color: #000000;"><strong>4×2 AT MIVEC</strong></span>
                                            </td>
                                            <td style="height: 21px;" width="178"><span style="color: #000000; font-family: verdana, geneva, sans-serif; font-size: 10pt;">{{$product['MT_MIVEC_2']}}</span></td>
                                            <td style="height: 21px;" rowspan="2" width="178">
                                                –&nbsp; Nắp thùng
                                                <p></p>
                                                <p>Hoặc</p>
                                                <p>– Bảo hiểm vật chất</p>
                                            </td>
                                        </tr>
                                        <tr style="height: 26px;">
                                            <td style="height: 26px;" width="137">
                                                <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt; color: #000000;">&nbsp;<strong>4×2 MT</strong></span>
                                            </td>
                                            <td style="height: 26px;" width="178"><span style="font-family: verdana, geneva, sans-serif; font-size: 10pt; color: #000000;">&nbsp;{{$product['MT']}}</span></td>
                                        </tr>
                                        <tr style="height: 26px;">
                                            <td style="height: 26px; text-align: center;" colspan="3" width="137">
                                                <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt; color: #000000;"><em>* Ưu đãi đặc biệt cho các model xe sản xuất năm 2019.</em></span>
                                                <p></p>
                                                <p style="text-align: center;">
                                                    <span style="font-family: verdana, geneva, sans-serif; font-size: 10pt; color: #000000;">
                                                        <em>Vui lòng&nbsp;<a href="https://www.mitsubishi-motors.com.vn/nha-phan-phoi/">liên hệ NPP</a>&nbsp;để biết thông tin chi tiết</em>
                                                    </span>
                                                </p>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="desc-area">
                                <p></p>
                                <p>Đơn vị giá: VNĐ</p>
                                <p></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<div d2-estimate-popup=""></div>
@endsection
@section('jsfooter')
<script src="{{asset('js/product-page.js')}}"></script>
<script src="{{asset('js/estimate-price.js')}}"></script>
@endsection
