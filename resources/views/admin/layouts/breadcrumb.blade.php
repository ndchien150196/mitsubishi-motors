<section class="content-header">
  <h1>
    {{$name}}
    <small>{{$action}}</small>
  </h1>
</section>