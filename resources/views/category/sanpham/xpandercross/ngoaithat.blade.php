<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>NGOẠI THẤT</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide70"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="Thiết kế phong cách SUV" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> Thiết kế phong cách SUV</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Exterior1.jpg')}}" alt="Thiết kế phong cách SUV" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> Thiết kế phong cách SUV</span></h3>
                                <div class="opContent">
                                    <p>
                                        Ngôn ngữ thiết kế Dynamic Shield với triết lý “Vẻ đẹp từ công năng” kết hợp với mặt ca lăng và ốp cản thể thao mang đến sự hiện đại, cá tính đồng thời giúp nổi bật phong cách SUV của
                                        Xpander Cross.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide71" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Mặt ca-lăng và ốp cản thể thao" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Mặt ca-lăng và ốp cản thể thao</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Mat-ca-lang.jpg')}}" alt="Mặt ca-lăng và ốp cản thể thao" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Mặt ca-lăng và ốp cản thể thao</span></h3>
                                <div class="opContent">
                                    <p>Xpander Cross sở hữu mặt ca-lăng mới cùng ốp cản trước/sau&nbsp;thể thao mang đến một phong cách SUV mạnh mẽ.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide72" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Đèn chiếu sáng phía trước Full-LED" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Đèn chiếu sáng phía trước Full-LED</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Den-Full-LED.jpg')}}" alt="Đèn chiếu sáng phía trước Full-LED" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Đèn chiếu sáng phía trước Full-LED</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống đèn chiếu sáng phía trước công nghệ Full LED hiện đại với khả năng chiếu sáng tối ưu, bền bỉ và tiết kiệm năng lượng hơn.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide73" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Cụm đèn LED phía sau" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Cụm đèn LED phía sau</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Den-LED-sau.jpg')}}" alt="Cụm đèn LED phía sau" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Cụm đèn LED phía sau</span></h3>
                                <div class="opContent">
                                    <p>
                                        Thiết kế đèn hậu đặc trưng kết hợp cùng đuôi xe với 2 màu tương phản mang lại ấn tượng thể thao.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide74" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Thanh giá nóc thể thao" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Thanh giá nóc thể thao</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Thanh-gia-noc.jpg')}}" alt="Thanh giá nóc thể thao" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Thanh giá nóc thể thao</span></h3>
                                <div class="opContent">
                                    <p>Không chỉ mang đến dáng vẻ SUV cho xe, thanh giá nóc còn tạo thêm không gian chở đồ đạc trên nóc xe trong những chuyến đi xa, về quê, đi picnic…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide75" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Mâm xe 17-inch và ốp trang trí thể thao" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Mâm xe 17-inch và ốp trang trí thể thao</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Mam-17-inch.jpg')}}" alt="Mâm xe 17-inch và ốp trang trí thể thao" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Mâm xe 17-inch và ốp trang trí thể thao</span></h3>
                                <div class="opContent">
                                    <p>Mâm xe 17-inch 2 tông màu, cùng các ốp viền cửa xe và&nbsp;lốp bánh xe đem đến&nbsp;cái nhìn cá tính &amp; phong cách.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="6" class="option-item null slick-slide" tabindex="-1" role="option" aria-describedby="slick-slide76" data-slick-index="6" aria-hidden="true">
                        <a href="javascript:;" title="Ăng ten vây cá" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">6. Ăng ten vây cá</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Angten-vay-ca.jpg')}}" alt="Ăng ten vây cá" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">6. Ăng ten vây cá</span></h3>
                                <div class="opContent">
                                    <p></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Exterior1.jpg')}}" alt="Thiết kế phong cách SUV" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Thiết kế phong cách SUV</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Ngôn ngữ thiết kế Dynamic Shield với triết lý “Vẻ đẹp từ công năng” kết hợp với mặt ca lăng và ốp cản thể thao mang đến sự hiện đại, cá tính đồng thời giúp nổi bật phong cách SUV của Xpander Cross.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Mat-ca-lang.jpg')}}" alt="Mặt ca-lăng và ốp cản thể thao" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Mặt ca-lăng và ốp cản thể thao</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Xpander Cross sở hữu mặt ca-lăng mới cùng ốp cản trước/sau&nbsp;thể thao mang đến một phong cách SUV mạnh mẽ.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Den-Full-LED.jpg')}}" alt="Đèn chiếu sáng phía trước Full-LED" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Đèn chiếu sáng phía trước Full-LED</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Hệ thống đèn chiếu sáng phía trước công nghệ Full LED hiện đại với khả năng chiếu sáng tối ưu, bền bỉ và tiết kiệm năng lượng hơn.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Den-LED-sau.jpg')}}" alt="Cụm đèn LED phía sau" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Cụm đèn LED phía sau</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Thiết kế đèn hậu đặc trưng kết hợp cùng đuôi xe với 2 màu tương phản mang lại ấn tượng thể thao.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Thanh-gia-noc.jpg')}}" alt="Thanh giá nóc thể thao" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Thanh giá nóc thể thao</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Không chỉ mang đến dáng vẻ SUV cho xe, thanh giá nóc còn tạo thêm không gian chở đồ đạc trên nóc xe trong những chuyến đi xa, về quê, đi picnic…</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Mam-17-inch.jpg')}}" alt="Mâm xe 17-inch và ốp trang trí thể thao" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Mâm xe 17-inch và ốp trang trí thể thao</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Mâm xe 17-inch 2 tông màu, cùng các ốp viền cửa xe và&nbsp;lốp bánh xe đem đến&nbsp;cái nhìn cá tính &amp; phong cách.</p>
                    </div>
                </div>
            </div>
            <div data-item="6" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Angten-vay-ca.jpg')}}" alt="Ăng ten vây cá" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Ăng ten vây cá</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>