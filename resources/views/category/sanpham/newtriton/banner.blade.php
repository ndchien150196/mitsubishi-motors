<!--Banner-->
<div class="pr-banner">
    <picture>
        <!--if IE 9video(style='display: none;')-->
        <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/11/MIT_Triton_SU_MY20_WebBanner_1920x800px_FA.jpeg" alt="Sở hữu khả năng vận hành mạnh mẽ, vượt mọi địa hình." media="(min-width: 992px)" />
        <!--if IE 9-->
        <img srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/11/MIT_Triton_Su_MY20_WebBanner_750x600px_FA.jpeg" alt="Sở hữu khả năng vận hành mạnh mẽ, vượt mọi địa hình." class="res-img" />
    </picture>
    <div class="bn-content">
        <picture>
            <!--if IE 9video(style='display: none;')
                                    -->
            <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2017/06/logo-triton-white.png" alt="New Triton logo" media="(min-width: 992px)" />
            <!--if IE 9-->
            <img srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2017/06/logo-triton-white.png" alt="New Triton logo" />
        </picture>
        <h2 class="bn-title visible-xs visible-sm">Sở hữu khả năng vận hành mạnh mẽ, vượt mọi địa hình.</h2>
        <h2 class="bn-title hidden-xs hidden-sm">Sở hữu khả năng vận hành mạnh mẽ, vượt mọi địa hình.</h2>
        <p class="summary">
            Kế thừa và cải tiến hơn nữa định hướng thiết kế của thế hệ trước về một chiếc pick-up thể thao đa dụng đáp ứng trọn vẹn nhu cầu sử dụng cá nhân lẫn kinh doanh, mang đến sự thoải mái và tiện nghi của một chiếc xe du lịch
            nhưng vẫn đảm bảo tính bền bỉ và thực dụng của một chiếc xe bán tải.
        </p>
    </div>

    <div class="video hidden-xs hidden-sm video-full">
        <div class="video-show">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe
                    id="ytplayer"
                    type="text/html"
                    src="https://www.youtube.com/embed/LCumdv5l5Do"
                    frameborder="0"
                    allowfullscreen=""
                    volume="50"
                ></iframe>
            </div>
        </div>
    </div>
</div>
<!--End - Banner-->