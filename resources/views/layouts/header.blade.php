<header id="header" class="header">
  <div class="main-logo">
    <a target="_blank" href="/" title="Mitsubishi Motors Việt Nam">
      <img class="logo" alt="Mitsubishi Motors Việt Nam" src="{{asset('img/main-logo.svg')}}">
    </a>
  </div>
  <div class="mb-menu">
   <div class="logo-satsco">
      <a href="/" title="MITSUBISHI THÁI BÌNH" class="clearfix">
       <div class="col">
          <h2>MITSUBISHI THÁI BÌNH</h2>
          <span>140 Quang Trung, phường Quang Trung,TP Thái Bình </span>
       </div>
      </a>
    </div>
    <!-- Menu on top header: Top menu - Search - Language - Social-->
    <div class="top-header">
      <div class="header-inner clearfix" data-item="[
        {
           &quot;caption&quot;: &quot;Bảng giá&quot;,
           &quot;link&quot;: &quot;/bang-gia/&quot;,
           &quot;newWindow&quot;: false,
           &quot;internal&quot;: 1240,
           &quot;edit&quot;: false,
           &quot;isInternal&quot;: true,
           &quot;internalName&quot;: &quot;Bảng giá&quot;,
           &quot;type&quot;: &quot;internal&quot;,
           &quot;title&quot;: &quot;Bảng giá&quot;
        },
        {
           &quot;caption&quot;: &quot;Đăng ký lái thử&quot;,
           &quot;link&quot;: &quot;/dang-ky-lai-thu/&quot;,
           &quot;newWindow&quot;: false,
           &quot;internal&quot;: 1487,
           &quot;edit&quot;: false,
           &quot;isInternal&quot;: true,
           &quot;internalName&quot;: &quot;Đăng ký lái thử&quot;,
           &quot;type&quot;: &quot;internal&quot;,
           &quot;title&quot;: &quot;Đăng ký lái thử&quot;
        },
        {
           &quot;caption&quot;: &quot;Hệ thống đại lý&quot;,
           &quot;link&quot;: &quot;/dai-ly/&quot;,
           &quot;newWindow&quot;: false,
           &quot;internal&quot;: 1077,
           &quot;edit&quot;: false,
           &quot;isInternal&quot;: true,
           &quot;internalName&quot;: &quot;Đại lý&quot;,
           &quot;type&quot;: &quot;internal&quot;,
           &quot;title&quot;: &quot;Hệ thống đại lý&quot;
        }
     ]" data-item-type="Newtonsoft.Json.Linq.JArray">
          <div class="search-box visible-xs">
            <div class="search-toggle"></div>
            <form id="search-form" name="search-form" class="search-form" action="/" method="get" style="display: none;">
              <input name="q" type="text" placeholder="Tìm kiếm" autocomplete="off" class="input search-input">
              <button type="submit" title="Tìm kiếm" class="btn search-btn"><i class="fa fa-search"></i></button>
            </form>
          </div>
          <ul class="contact-box hidden-xs">
            <li>
              Liên Hệ:
              <a href="tel:{{\App\Common::phonelh()}}"><strong>{{\App\Common::phonelh()}}</strong></a>
            </li>
          </ul>
      </div>
    </div>
    <!-- End - Menu on top header: Top menu - Search - Language - Social-->
    <!-- Main menu-->
    <nav role="navigation" class="main-nav">
        <button class="btn nav-toggle">
            <span class="line line-1"></span>
            <span class="line line-2"></span>
            <span class="line line-3"></span>
        </button>
        <div class="nav-container">
            <ul class="nav-menu">
                <li class="menu-item" data-menu="trang-chu">
                    <a href="/" title="TRANG CHỦ" class="menu-link">TRANG CHỦ</a>
                </li>
                <li class="menu-item" data-menu="gioi-thieu">
                    <a href="{{url('gioi-thieu')}}" title="GIỚI THIỆU" class="menu-link">GIỚI THIỆU</a>
                </li>
                <li class="menu-item has-dropdown menu-item-sp" data-menu="san-pham">
                    <a href="javascript:;" title="SẢN PHẨM" class="menu-link">SẢN PHẨM</a>
                    <div class="nav-dropdown">
                        <div class="nav-dropdown-inner">
                            <ul class="menu">
                                <li class="menu-item">
                                    <a
                                        target="_blank"
                                        href="{{asset('san-pham/xpander-cross')}}"
                                        title="Xpander Cross"
                                        class="menu-link-sp"
                                    >
                                        <div class="img">
                                            <img src="{{asset('img/menu/Cross.png')}}" alt="Xpander Cross" />
                                        </div>
                                        <div class="text">
                                            <span class="menu-title">Xpander Cross</span>
                                            <span class="menu-price">Giá từ {{$product['Xpander_Cross']}} VNĐ</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <a target="_blank" href="http://newpajerosport.vn/?utm_source=Ha%20Noi%20Auto&amp;utm_medium=Click&amp;utm_campaign=Launch&amp;utm_term=&amp;utm_content=menu" title="New Pajero Sport" class="menu-link-sp">
                                        <div class="img">
                                            <img src="{{asset('img/menu/Pajero-sport.png')}}" alt="New Pajero Sport" />
                                        </div>
                                        <div class="text">
                                            <span class="menu-title">New Pajero Sport</span>
                                            <span class="menu-price">Giá từ {{$product['Diesel_4×2_AT']}} VNĐ</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <a target="" href="{{asset('san-pham/new-xpander')}}" title="New Xpander" class="menu-link-sp">
                                        <div class="img">
                                            <img src="{{asset('img/menu/Xpander-Thumbnail.png')}}" alt="New Xpander" />
                                        </div>
                                        <div class="text">
                                            <span class="menu-title">New Xpander</span>
                                            <span class="menu-price">Giá từ {{$product['Xpander_MT']}} VNĐ</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <a target="" href="{{asset('/san-pham/new-attrage')}}" title="New Attrage" class="menu-link-sp">
                                        <div class="img">
                                            <img src="{{asset('img/menu/ELL-MY20_-final.png')}}" alt="New Attrage" />
                                        </div>
                                        <div class="text">
                                            <span class="menu-title">New Attrage</span>
                                            <span class="menu-price">Giá từ {{$product['Attrage_MT']}} VND</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <a target="" href="{{asset('/san-pham/new-outlander')}}" title="New Outlander" class="menu-link-sp">
                                        <div class="img">
                                            <img src="{{asset('img/menu/RE-MY20.png')}}" alt="New Outlander" />
                                        </div>
                                        <div class="text">
                                            <span class="menu-title">New Outlander</span>
                                            <span class="menu-price">Giá từ {{$product['CVT_2']}} VNĐ</span>
                                        </div>
                                    </a>
                                </li>
                                <li class="menu-item">
                                    <a target="" href="{{asset('/san-pham/newtriton')}}" title="New Triton" class="menu-link-sp">
                                        <div class="img">
                                            <img src="{{asset('img/menu/Triton.png')}}" alt="New Triton" />
                                        </div>
                                        <div class="text">
                                            <span class="menu-title">New Triton</span>
                                            <span class="menu-price">Giá từ {{$product['MT']}} VNĐ</span>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </li>
                <li class="menu-item" data-menu="bang-gia">
                    <a href="{{url('bang-gia')}}" title="BẢNG GIÁ XE &amp; KHUYẾN MÃI" class="menu-link">BẢNG GIÁ XE &amp; KHUYẾN MÃI</a>
                </li>
                <li class="menu-item has-dropdown" data-menu="mua-xe">
                    <a href="javascript:;" title="MUA XE" class="menu-link">MUA XE</a>
                    <div class="nav-dropdown">
                        <ul class="nav-dropdown-menu">
                            <li class="nav-dropdown-item">
                                <a href="{{url('mua-xe',['mua-xe-tra-gop'])}}" title="Mua xe trả góp" class="nav-dropdown-link">Mua xe trả góp</a>
                            </li>
                            <li class="nav-dropdown-item">
                                <a href="{{url('mua-xe',['quy-trinh-mua-xe'])}}" title="Quy trình mua xe" class="nav-dropdown-link">Quy trình mua xe</a>
                            </li>
                            <li class="nav-dropdown-item">
                                <a href="{{url('mua-xe',['dang-ky-lai-thu'])}}" title="Đăng ký lái thử" class="nav-dropdown-link">Đăng ký lái thử</a>
                            </li>
                            <li class="nav-dropdown-item">
                                <a href="{{url('mua-xe',['bao-gia-chi-tiet'])}}" title="Báo giá chi tiết" class="nav-dropdown-link">Báo giá chi tiết</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="menu-item has-dropdown" data-menu="dich-vu-hau-mai">
                    <a href="javascript:;" title="DỊCH VỤ HẬU MÃI" class="menu-link">DỊCH VỤ HẬU MÃI</a>
                    <div class="nav-dropdown">
                        <ul class="nav-dropdown-menu">
                            <li class="nav-dropdown-item">
                                <a href="{{url('dich-vu-hau-mai',['gioi-thieu-dich-vu'])}}" title="Giới thiệu dịch vụ" class="nav-dropdown-link">Giới thiệu dịch vụ</a>
                            </li>
                            <li class="nav-dropdown-item">
                                <a href="{{url('dich-vu-hau-mai',['bao-duong-dinh-ky'])}}" title="Bảo dưỡng định kỳ" class="nav-dropdown-link">Bảo dưỡng định kỳ</a>
                            </li>
                            <li class="nav-dropdown-item">
                                <a href="{{url('dich-vu-hau-mai',['sua-chua'])}}" title="Sửa chữa" class="nav-dropdown-link">Sửa chữa</a>
                            </li>
                            <li class="nav-dropdown-item">
                                <a href="{{url('dich-vu-hau-mai',['cuu-ho-giao-thong'])}}" title="Cứu hộ giao thông" class="nav-dropdown-link">Cứu hộ giao thông</a>
                            </li>
                            <li class="nav-dropdown-item">
                                <a href="{{url('dich-vu-hau-mai',['chinh-sach-bao-hanh'])}}" title="Chính sách bảo hành" class="nav-dropdown-link">Chính sách bảo hành</a>
                            </li>
                            <li class="nav-dropdown-item">
                                <a href="{{url('dich-vu-hau-mai',['phu-tung-chinh-hieu'])}}" title="Phụ tùng chính hiệu" class="nav-dropdown-link">Phụ tùng chính hiệu</a>
                            </li>
                            <li class="nav-dropdown-item">
                                <a href="{{url('dich-vu-hau-mai',['dat-lich-bao-duong'])}}" title="Đặt lịch bảo dưỡng" class="nav-dropdown-link">Đặt lịch bảo dưỡng</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="menu-item has-dropdown" data-menu="tin-tuc">
                    <a href="javascript:;" title="TIN TỨC" class="menu-link">TIN TỨC</a>
                    <div class="nav-dropdown">
                        <ul class="nav-dropdown-menu">
                            <li class="nav-dropdown-item">
                                <a href="{{asset('tin-tuc/su-kien-noi-bat')}}" title="Sự kiện nổi bật" class="nav-dropdown-link">Sự kiện nổi bật</a>
                            </li>

                            <li class="nav-dropdown-item">
                                <a href="{{asset('tin-tuc/tin-khuyen-mai')}}" title="Tin khuyến mãi" class="nav-dropdown-link">Tin khuyến mãi</a>
                            </li>

                            <li class="nav-dropdown-item">
                                <a href="{{asset('tin-tuc/tin-tong-hop')}}" title="Tin tổng hợp" class="nav-dropdown-link">Tin tổng hợp</a>
                            </li>

                            <li class="nav-dropdown-item">
                                <a href="{{asset('tin-tuc/tin-tuyen-dung')}}" title="Tin tuyển dụng" class="nav-dropdown-link">Tin tuyển dụng</a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="menu-item" data-menu="lien-he">
                    <a href="{{url('lien-he')}}" title="LIÊN HỆ" class="menu-link">LIÊN HỆ</a>
                </li>
                <li class="menu-item">
                    <ul class="social-box">
                        <li>
                            <a href="{{\App\Common::fanpage()}}" title="Facebook" class="icon-social facebook" target="_blank">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{\App\Common::youtube()}}" title="Youtube" class="icon-social youtube" target="_blank">
                                <i class="fa fa-youtube-play"></i>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <div class="social-top-header visible-xs">
                <a class="text" href="/"> Về trang Mitsubishi Motors Việt Nam <i class="fa fa-angle-double-right" aria-hidden="true"></i> </a>
            </div>
        </div>
    </nav>

  </div>
</header>