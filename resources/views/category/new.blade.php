@extends("master")
@section('js')
<link href="{{asset('css/admin/froala_editor.pkgd.min.css')}}" rel="stylesheet" type="text/css" />
<style type="text/css">
    .tab-content img{
        object-fit: cover;
    }
</style>

@endsection
@section("content")
<div class="second-block page-news-home">
    <div class="grid-fluid">
        <h3 class="title-block"><span>{{\App\Common::category($tintuc->category)}}</span></h3>
        <div class="grid-content">
            <div class="row">
                <div class="col-md-8">
                    <div class="spare-detail-des editor">
                        <p class="content-title"><b>{{\App\Common::chensaotitle($tintuc->title,$tintuc->category)}}</b></p>
                    </div>
                    <div class="tab-content editor spare-detail-tab-content">
                        <div data-tab="0" role="tab-panel" class="tab-panel active fr-view">
                             {!! $tintuc->content !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="news-sidebar">
                        <div class="sidebar-block link-block">
                            <h3 class="news-link-title">Tin tức</h3>
                            <!-- menu-->
                            <ul class="news-link">
                                <li class="@if($tintuc->category == 'su-kien-noi-bat') active @endif">
                                    <a  href="{{asset('tin-tuc/su-kien-noi-bat')}}" title="Sự kiện nổi bật" class="sp-text">Sự kiện nổi bật</a>
                                </li>
                                <li class="@if($tintuc->category == 'tin-khuyen-mai') active @endif">
                                    <a href="{{asset('tin-tuc/tin-khuyen-mai')}}" title="Tin khuyến mãi" class="sp-text">Tin khuyến mãi</a>
                                </li>
                                <li class="@if($tintuc->category == 'tin-tong-hop') active @endif">
                                    <a href="{{asset('tin-tuc/tin-tong-hop')}}" title="Tin tổng hợp" class="sp-text">Tin tổng hợp</a>
                                </li>
                                <li class="@if($tintuc->category == 'tin-tuyen-dung') active @endif">
                                    <a href="{{asset('tin-tuc/tin-tuyen-dung')}}" title="Tin tuyển dụng" class="sp-text">Tin tuyển dụng</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
