<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'FrontEndController@home')->name('home');

Route::get('/gioi-thieu', 'FrontEndController@gioithieu')->name('gioi-thieu');


Route::get('/lien-he', 'FrontEndController@lienhe')->name('lien-he');

Route::get('/bang-gia', 'FrontEndController@banggia')->name('bang-gia');

Route::get('/san-pham/{xe}', 'CategoryController@sanpham')->name('muaxe.sanpham');
Route::get('/tin-tuc/{slug}.html', 'FrontEndController@new')->name('newdetail');
Route::get('/tin-tuc/{category}', 'FrontEndController@tuctuc')->name('tuctuc');
Route::get('/phu-tung-chinh-hieu/{slug}.html', 'FrontEndController@phutung');

Route::post('/mail-lien-he', 'FrontEndController@maillienhe')->name('maillienhe');
Route::post('/mail-dang-ky', 'FrontEndController@maildangky')->name('maildangky');
Route::post('/mail-bao-gia', 'FrontEndController@mailbaogia')->name('mailbaogia');
Route::post('/mail-dat-lich', 'FrontEndController@maildatlich')->name('maildatlich');


Route::group(['prefix'=>'/dich-vu-hau-mai'],function(){
    Route::get('/gioi-thieu-dich-vu', 'FrontEndController@gioithieudichvu')->name('gioi-thieu-dich-vu');

	Route::get('/bao-duong-dinh-ky', 'FrontEndController@baoduong')->name('bao-duong-dinh-ky');

	Route::get('/sua-chua', 'FrontEndController@suachua')->name('sua-chua');

	Route::get('/cuu-ho-giao-thong', 'FrontEndController@cuuho')->name('cuu-ho-giao-thong');

	Route::get('/chinh-sach-bao-hanh', 'FrontEndController@baohanh')->name('chinh-sach-bao-hanh');

	Route::get('/dat-lich-bao-duong', 'FrontEndController@datlich')->name('dat-lich-bao-duong');

	Route::get('/phu-tung-chinh-hieu', 'FrontEndController@phutungshow')->name('phu-tung-chinh-hieu');
});

Route::group(['prefix'=>'/mua-xe'],function(){
    Route::get('/mua-xe-tra-gop', 'FrontEndController@tragop' )->name('mua-xe-tra-gop');

	Route::get('/quy-trinh-mua-xe', 'FrontEndController@quytrinh' )->name('quy-trinh-mua-xe');

	Route::get('/dang-ky-lai-thu', 'FrontEndController@baogia' )->name('dang-ky-lai-thu');

	Route::get('/bao-gia-chi-tiet', 'FrontEndController@dangky' )->name('bao-gia-chi-tiet');

	Route::get('/dang-ky-lai-thu/{xe}', 'CategoryController@dangky')->name('muaxe.dangky');
	Route::get('/bao-gia-chi-tiet/{xe}', 'CategoryController@baogia')->name('muaxe.baogia');
});

Route::group(['prefix'=>'/admin','middleware'=>'Login'],function(){
	Route::get('/', 'DashboardController@index')->name('admin.home');

	Route::get('/tin-tuc', 'TinTucController@index')->name('admin.tintuc');
	Route::get('/tin-tuc-them', 'TinTucController@create')->name('admin.tintuc.them');
	Route::post('/tin-tuc-them', 'TinTucController@store')->name('admin.tintuc.add');
	Route::get('/tin-tuc-sua','TinTucController@edit')->name('admin.tintuc.sua');
	Route::post('/tin-tuc-sua','TinTucController@update')->name('admin.tintuc.edit');
	Route::get('/tin-tuc-delete','TinTucController@delete')->name('admin.tintuc.delete');
	Route::post('/upload_image','TinTucController@uploadimage')->name('admin.tintuc.uploadimage');
	Route::post('/upload_file','TinTucController@uploadfile')->name('admin.tintuc.uploadfile');
	Route::get('/tailieu','TaiLieuController@index')->name('admin.tailieu');
	Route::post('/tailieu','TaiLieuController@store')->name('admin.tailieu.import');
	
	Route::get('/slide','TinTucController@slides')->name('admin.slide');
	Route::post('/slide/add','TinTucController@slideadd')->name('admin.slide.add');
	Route::get('/slide-delete','TinTucController@destroy')->name('admin.slide.delete');

	Route::get('/car','TinTucController@cars')->name('admin.car');
	Route::post('/car/edit','TinTucController@caredit')->name('admin.car.edit');


});

Route::get('/reset', function () {
    return view('reset');
})->name('reset');
Route::post('/reset', 'ResetPasswordController@sendMail');
Route::get('/ok', 'ResetPasswordController@addFeedback');
// Route::put('reset-password/{token}', 'ResetPasswordController@reset')->name('password.reset');

Route::get('login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@index']);
Route::post('login', [ 'as' => 'login', 'uses' => 'Auth\LoginController@postLogin']);
Route::get('logout', [ 'as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
// Auth::routes();

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    return "Cache is cleared";
});