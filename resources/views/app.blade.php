<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>Mitsubishi Thái Bình</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="icon" type="image/x-icon" href="{{asset('img/favicon.png')}}">
  <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.png')}}">
  <link rel="apple-touch-icon" href="{{asset('img/favicon.png')}}">
  <link rel="stylesheet" href="{{asset('css/admin/bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/admin/font-awesome/css/font-awesome.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/admin/dataTables.bootstrap.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/admin/AdminLTE.min.css')}}">
  <link rel="stylesheet" href="{{asset('css/admin/_all-skins.min.css')}}">
  <script src="{{asset('js/admin/jquery.min.js')}}"></script>
  <script src="{{asset('js/admin/bootstrap.min.js')}}"></script>
  
  @yield('css')

</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">
  
  @include('admin.layouts.header')
  @include('admin.layouts.menu')

  <div class="content-wrapper">
    @include('admin.layouts.breadcrumb')
    <section class="content">
      @yield('content')
    </section>
  </div>
  @include('admin.layouts.footer')

  <div class="control-sidebar-bg"></div>
</div>

<script src="{{asset('js/admin/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/admin/dataTables.bootstrap.min.js')}}"></script>
<script src="{{asset('js/admin/jquery.slimscroll.min.js')}}"></script>
<script src="{{asset('js/admin/fastclick.js')}}"></script>
<script src="{{asset('js/admin/adminlte.min.js')}}"></script>
<script src="{{asset('js/admin/jquery-ui.min_1.js')}}"></script>
<script src="{{asset('js/admin/jquery.bootstrap-growl.min.js')}}"></script>
<script>
  var activeurl = window.location;
  $('.treeview-menu a[href="'+activeurl+'"]').parent('li').addClass('active');

  $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  $(function () {
    $('#mitsubishi').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })
  
</script>

@yield('js')
</body>
</html>
