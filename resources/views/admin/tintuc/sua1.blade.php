@extends("app")
@section('css')
<link href="https://cdn.jsdelivr.net/npm/froala-editor@latest/css/froala_editor.pkgd.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/froala-editor@latest/js/froala_editor.pkgd.min.js"></script>
@endsection

@section("content")
<div class="content">
	<div class="row">
		<form method="POST" enctype="multipart/form-data" id="upload-form">
			@csrf	
			<div class="col-md-8 col-12">
				<label>Nội Dung</label>
				<textarea id="content" name="content">{{$tintuc->content}}</textarea>
			</div>
			<div class="col-md-4 col-12">
				<div class="form-group">
					<label>Tiêu Đề</label>
					<textarea class="form-control" id="title" name="title">{{$tintuc->title}}</textarea>
		            <input id="id" name="id" type="text" class="hidden" value="{{$tintuc->id}}">
	            </div>
	            <div>
	                <img id="image_preview_container" src="{{ $tintuc->avatar }}" alt="preview image" style="max-height: 150px;">
	            </div>
	            <div class="form-group">
				    <label for="image">Ảnh Đại Diện</label>
				    <input type="file" name="image" placeholder="Chọn Ảnh" id="image">
				</div>
	            <div class="form-group">
	                <label for="type">Chuyên Mục</label>
	                <div class="radio">
	                    <label><input type="radio" name="category" @if($tintuc->category === 'su-kien-noi-bat') checked @endif value="su-kien-noi-bat">Sự Kiện Nổi Bật</label><br/>
	                    <label><input type="radio" name="category" @if($tintuc->category === 'tin-khuyen-mai') checked @endif value="tin-khuyen-mai">Tin Khuyến Mại</label><br/>
	                    <label><input type="radio" name="category" @if($tintuc->category === 'tin-tong-hop') checked @endif value="tin-tong-hop">Tin Tổng Hợp</label><br/>
	                    <label><input type="radio" name="category" @if($tintuc->category === 'tin-tuyen-dung') checked @endif value="tin-tuyen-dung">Tin Tuyển Dụng</label>
	                </div>
	            </div>
	            <div class="form-group">
	                <label for="type">Trạng Thái</label>
	                <div class="radio">
	                    <label><input type="radio" name="status" @if($tintuc->status == 0) checked @endif value="0">Nháp / Gỡ Bài</label><br/>
	                    <label><input type="radio" name="status" @if($tintuc->status == 1) checked @endif value="1">Xuất Bản</label>
	                </div>
	            </div>
				<br/>
				<div class="form-group pull-right">
					<button type="submit" class="btn btn-success">Sửa Bài</button>
				</div>
			</div>
		</form>
	</div>
</div>

@endsection
@section("js")
<script type="text/javascript">
	// editor
	new FroalaEditor('#content', {
	    heightMin: 580,
	    heightMax: 700,
	    // key request
	    imageUploadParam: 'file',

	    // Set the image upload URL.
	    imageUploadURL: '/admin/upload_image',

	    // Additional upload params.
	    imageUploadParams: {
			froala: 'true', // This allows us to distinguish between Froala or a regular file upload.
			_token: "{{ csrf_token() }}" // This passes the laravel token with the ajax request.
		},

	    // Set request type.
	    imageUploadMethod: 'POST',

	    // Set max image size to 5MB.
	    imageMaxSize: 5 * 1024 * 1024,

	    // Allow to upload PNG and JPG.
	    imageAllowedTypes: ['jpeg', 'jpg', 'png', 'gif'],
		imageInsertButtons: ['imageBack', '|', 'imageUpload', 'imageByURL'],
		imageEditButtons: ['imageReplace', 'imageAlign', 'imageCaption', 'imageRemove', '|', 'imageLink', 'linkOpen', 'linkEdit', 'linkRemove', '-', 'imageDisplay', 'imageStyle', 'imageAlt', 'imageSize'],
	    

	    events: {
			'image.beforeUpload': function (images) {
			// Return false if you want to stop the image upload.
			},
			'image.uploaded': function (response) {
			// Image was uploaded to the server.
			},
			'image.inserted': function ($img, response) {
			// Image was inserted in the editor.
			},
			'image.replaced': function ($img, response) {
			// Image was replaced in the editor.
			},
			'image.error': function (error, response) {
				console.log(error);
				if (error.code == 1) { 
				}

		        // No link in upload response.
		        else if (error.code == 2) { 
		        }

		        // Error during image upload.
		        else if (error.code == 3) { 
		        }

		        // Parsing response failed.
		        else if (error.code == 4) { 
		        	$.bootstrapGrowl('Lỗi không trả về ảnh trong khung nhập', { type: 'error' });
		        }

		        // Image too text-large.
		        else if (error.code == 5) { 
		        	$.bootstrapGrowl('Tập tin lớn hơn 5MB', { type: 'error' });
		        }

		        // Invalid image type.
		        else if (error.code == 6) { 
		        	$.bootstrapGrowl('Bạn chỉ được up ảnh dưới định dạng jpeg, jpg, png', { type: 'error' });
		        }

		        // Image can be uploaded only to same domain in IE 8 and IE 9.
		        else if (error.code == 7) { 
		        }
			},
		}
	});
	FroalaEditor.DefineIcon('insertHTML', { NAME: 'Collision', SVG_KEY: 'Collision' });
	
    $("#image").change(function () {
        let reader = new FileReader();

        reader.onload = (e) => {
            $("#image_preview_container").attr("src", e.target.result);
        };

        reader.readAsDataURL(this.files[0]);
    });

    $("#upload-form").submit(function (e) {
        e.preventDefault();
        let formData = new FormData(this);
        let edit= '{{ route("admin.tintuc.edit") }}';

        $.ajax({
            type: "POST",
            url: edit,
            data: formData,
            contentType: false,
            processData: false,
            success: (data) => {
                $.bootstrapGrowl(data.message, { type: 'success' });
	            setTimeout(function(){
		           window.location.href = '/admin/tin-tuc'; 
				}, 200); 
            },
            error: function (response) {
            	$.each(response.responseJSON.errors, function(i, item) {
				    $.bootstrapGrowl(item, { type: 'error' });
				});
            },
        });
    });

</script>

@endsection
