@extends("master")
@section("content") 
<div class="second-block page-news-home news-page quy-trinh-dang-ky">
    <div class="grid-fluid">
        <h3 class="title-block">
            <span>Quy trình mua xe</span>
        </h3>
        <div class="grid-content">
            <div class="row">
                <div class="col-md-8">
                    <div class="editor">
                        <div class="news-link-title">Quy trình đăng ký mua xe</div>
                        <p>
                            <b>Bước 1:</b>&nbsp;Đóng thuế trước bạ<br />
                            Các hồ sơ cần chuẩn bị
                        </p>
                        <ol>
                            <li>Phiếu kiểm tra chất lượng xuất xưởng (bản chính + 1 bản sao)</li>
                            <li>Hóa đơn Mitsubishi Motors Việt Nam xuất cho Đại lý (bản sao)</li>
                            <li>Hóa đơn Đại lý xuất cho Khách hàng (bản chính + 1sao)</li>
                            <li>Giấy chứng nhận an toàn kỹ thuật và bảo vệ môi trường ô tô sản xuất, lắp ráp ( bản sao)</li>
                            <li>Đăng ký kinh doanh của Đại lý xuất hóa đơn cho khách hàng ( bản sao).</li>
                            <li>
                                Tờ khai thuế trước bạ 2 bản: Khai theo hướng dẫn của phòng thuế.
                                <ol>
                                    <li>Đối với KH tư nhân, có thể lấy tờ khai và khai tại phòng thuế khi đi nộp thuế.</li>
                                    <li>Đối với công ty, ngoài ký tên, phải đóng dấu công ty trên tờ khai nên phải chuẩn bị tờ khai này trước khi đi đóng thuế.</li>
                                    <li>Người đi đăng ký cần có giấy giới thiệu đến phòng thuế.</li>
                                </ol>
                            </li>
                            <li>
                                Các giấy tờ khác:
                                <ol>
                                    <li>Đối với khách hàng tư nhân: Bản sao CMND và Hộ khẩu (mang theo bản chính để đối chiếu nếu cần)</li>
                                    <li>Đối với công ty tư nhân, DNTN: Bản sao Giấy chứng nhận đăng ký kinh doanh.</li>
                                    <li>Đối với công ty liên doanh nước ngoài: Bản phô tô Giấy phép đầu tư.</li>
                                </ol>
                            </li>
                        </ol>
                        <p>Tiến hành đóng thuế trước bạ</p>
                        <ul>
                            <li>
                                Mang toàn bộ hồ sơ trên đến phòng thuế.
                                <ul>
                                    <li>Nếu là tư nhân: Đến phòng thuế Quận/Huyện nơi chủ xe có HK thường trú.</li>
                                    <li>Nếu là công ty tư nhân, DNTN: Đến phòng thuế Quận/Huyện nơi có giấy phép đăng ký KD mới nhất</li>
                                    <li>Nếu là công ty LD/Người nước ngoài/VPĐDNN: Đến phòng thuế.</li>
                                </ul>
                            </li>
                            <li>Đóng tiền và nhận lại tờ biên lai đóng thuế và tờ khai thuế trước bạ.</li>
                            <li>Gửi lại phòng thuế 1 bộ hồ sơ bản sao</li>
                        </ul>
                        <p>
                            <b>Bước 2:</b>&nbsp;Đăng ký xe<br />
                            Các hồ sơ cần chuẩn bị
                        </p>
                        <ol>
                            <li>Phiếu kiểm tra chất lượng xuất xưởng (bản chính)</li>
                            <li>
                                Hóa đơn Đại lý xuất cho Khách hàng (bản chính)
                                <ol>
                                    <li>Đối với khách hàng tư nhân: mang theo CMND và Hộ khẩu (để đối chiếu nếu cần)</li>
                                    <li>Đối với công ty tư nhân, DNTN: Giấy giới thiệu, Bản sao y Giấy chứng nhận đăng ký kinh doanh (nếu DN có chức năng kinh doanh vận tải hành khách)</li>
                                    <li>Đối với công ty liên doanh nước ngoài: Bản phô tô Giấy phép đầu tư.</li>
                                </ol>
                            </li>
                            <li>Tờ khai thuế trước bạ &amp; biên lai đóng thuế trước bạ.</li>
                            <li>
                                Tờ khai đăng ký xe mới có dán số sườn, số máy (Đối với KH tư nhân, có thể lấy tờ khai và khai tại phòng CSGT khi đi đăng ký xe. Đối với công ty, ngoài ký tên, phải đóng dấu công ty trên tờ khai nên phải chuẩn
                                bị tờ khai này trước khi đi đăng ký. Đối với công ty: Người đi đăng ký cần có giấy giới thiệu đến phòng CSGT)
                            </li>
                        </ol>
                        <p>Các hồ sơ cần chuẩn bị</p>
                        <ul>
                            <li>Đưa xe cùng toàn bộ hồ sơ nói trên đến phòng CSGT.</li>
                            <li>Nộp hồ sơ</li>
                            <li>Chờ kiểm tra xe</li>
                            <li>Nộp tiền lệ phí đăng ký.</li>
                            <li>Bốc số tự động</li>
                            <li>Lấy biển số và giấy hẹn nhận giấy đăng ký xe.</li>
                        </ul>
                    </div>
                </div>
                <!--  Sidebar -->
                <div class="col-md-4">
                    <div class="news-sidebar">
                        <div class="sidebar-block link-block">
                            <h3 class="news-link-title">Tư vấn mua xe</h3>
                            <!-- menu-->
                            <ul class="news-link">
                                <li class="">
                                    <a href="{{url('mua-xe',['bao-gia-chi-tiet'])}}" title="Báo giá chi tiết" class="sp-text">Báo giá chi tiết</a>
                                </li>
                                <li class="">
                                    <a href="{{url('mua-xe',['mua-xe-tra-gop'])}}" title="Mua xe trả góp" class="sp-text">Mua xe trả góp</a>
                                </li>
                                <li class="active">
                                    <a href="{{url('mua-xe',['quy-trinh-mua-xe'])}}" title="Quy trình mua xe" class="sp-text">Quy trình mua xe</a>
                                </li>
                                <li class="">
                                    <a href="{{url('mua-xe',['dang-ky-lai-thu'])}}" title="Đăng ký lái thử" class="sp-text">Đăng ký lái thử</a>
                                </li>
                            </ul>
                        </div>
                        @include('//layouts/block-km')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
