<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>NGOẠI THẤT</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide70"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="Thiết kế Dynamic Shield" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> Thiết kế Dynamic Shield</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/Ngoại-thất.png')}}"
                                    alt="Thiết kế Dynamic Shield"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> Thiết kế Dynamic Shield</span></h3>
                                <div class="opContent">
                                    <p>
                                        Outlander&nbsp; tiên phong áp dụng ngôn ngữ thiết kế “Dynamic Shield” mới của Mitsubishi Motors với những đường nét đặc trưng ở phía trước của xe, mang lại ấn tượng về sự mạnh mẽ linh hoạt và
                                        khả năng bảo vệ toàn diện.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide71" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Lưới tản nhiệt thiết kế mới" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Lưới tản nhiệt thiết kế mới</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/Lưới-tản-nhiệt-mới.png')}}"
                                    alt="Lưới tản nhiệt thiết kế mới"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Lưới tản nhiệt thiết kế mới</span></h3>
                                <div class="opContent">
                                    <p>Thiết kế mới mang lại vẻ đẹp phong cách và hiện đại hơn cho phần đầu xe.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide72" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Đèn chiếu sáng phía trước công nghệ LED thiết kế mới" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Đèn chiếu sáng phía trước công nghệ LED thiết kế mới</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/headlamp.png')}}"
                                    alt="Đèn chiếu sáng phía trước công nghệ LED thiết kế mới"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Đèn chiếu sáng phía trước công nghệ LED thiết kế mới</span></h3>
                                <div class="opContent">
                                    <p>
                                        Hệ thống đèn chiếu sáng phía trước công nghệ LED thời thượng với khả năng chiếu sáng tối ưu, bền bỉ và tiết kiệm năng lượng hơn so với đèn pha xenon và đèn halogen. Hệ thống rửa đèn giúp loại
                                        sạch bụi bẩn bám vào đèn pha nhằm đảm bảo khả năng chiếu sáng ổn định (2.0 CVT Premium &amp; 2.4 CVT Premium)
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide73" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Cánh lướt gió đuôi xe" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Cánh lướt gió đuôi xe</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/cánh-lướt-gió.png')}}"
                                    alt="Cánh lướt gió đuôi xe"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Cánh lướt gió đuôi xe</span></h3>
                                <div class="opContent">
                                    <p>Trang bị theo xe giúp tăng tính hiện đại, thể thao</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide74" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Cửa sổ trời" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Cửa sổ trời</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/exterior_5.jpg')}}"
                                    alt="Cửa sổ trời"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Cửa sổ trời</span></h3>
                                <div class="opContent">
                                    <p>Mang đến trải nghiệm thú vị trong suốt hành trình, nhất là khi đi du lịch cùng cả gia đình</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide75" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Mâm đúc 18′ đa chấu hai tông màu mới" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Mâm đúc 18′ đa chấu hai tông màu mới</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/Mâm-xe-hai-tone-màu1.png')}}"
                                    alt="Mâm đúc 18′ đa chấu hai tông màu mới"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Mâm đúc 18′ đa chấu hai tông màu mới</span></h3>
                                <div class="opContent">
                                    <p>Mâm đúc 18″ với thiết kế hai tông màu mới tăng tính hiện đại và giúp ngoại thất Outlander trở nên ấn tượng và mạnh mẽ hơn.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/Ngoại-thất.png')}}"
                        alt="Thiết kế Dynamic Shield"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Thiết kế Dynamic Shield</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Outlander&nbsp; tiên phong áp dụng ngôn ngữ thiết kế “Dynamic Shield” mới của Mitsubishi Motors với những đường nét đặc trưng ở phía trước của xe, mang lại ấn tượng về sự mạnh mẽ linh hoạt và khả năng bảo
                            vệ toàn diện.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/Lưới-tản-nhiệt-mới.png')}}"
                        alt="Lưới tản nhiệt thiết kế mới"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Lưới tản nhiệt thiết kế mới</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Thiết kế mới mang lại vẻ đẹp phong cách và hiện đại hơn cho phần đầu xe.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/headlamp.png')}}"
                        alt="Đèn chiếu sáng phía trước công nghệ LED thiết kế mới"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Đèn chiếu sáng phía trước công nghệ LED thiết kế mới</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Hệ thống đèn chiếu sáng phía trước công nghệ LED thời thượng với khả năng chiếu sáng tối ưu, bền bỉ và tiết kiệm năng lượng hơn so với đèn pha xenon và đèn halogen. Hệ thống rửa đèn giúp loại sạch bụi bẩn
                            bám vào đèn pha nhằm đảm bảo khả năng chiếu sáng ổn định (2.0 CVT Premium &amp; 2.4 CVT Premium)
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/cánh-lướt-gió.png')}}"
                        alt="Cánh lướt gió đuôi xe"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Cánh lướt gió đuôi xe</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Trang bị theo xe giúp tăng tính hiện đại, thể thao</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/exterior_5.jpg')}}"
                        alt="Cửa sổ trời"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Cửa sổ trời</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Mang đến trải nghiệm thú vị trong suốt hành trình, nhất là khi đi du lịch cùng cả gia đình</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/Mâm-xe-hai-tone-màu1.png')}}"
                        alt="Mâm đúc 18′ đa chấu hai tông màu mới"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Mâm đúc 18′ đa chấu hai tông màu mới</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Mâm đúc 18″ với thiết kế hai tông màu mới tăng tính hiện đại và giúp ngoại thất Outlander trở nên ấn tượng và mạnh mẽ hơn.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>