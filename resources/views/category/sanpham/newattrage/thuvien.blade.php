<!--Gallery-->
<div class="normal-block thuvien">
    <div class="grid-inner">
        <h3 class="title-block pr-title-block"><span>Thư viện hình ảnh &amp; catalogue</span></h3>
        <div class="row">
            <div class="col-md-6 catalogue-download">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2 col-md-offset-1">
                        <picture>
                            <!--if IE 9video(style='display: none;')
                                      -->
                            <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Mitsu-Brochure_Front.jpg" alt="New Attrage." media="(min-width: 768px)" />
                            <!--if IE 9-->
                            <img
                                src="{{asset('img/new-attrage/Mitsu-Brochure_Front.jpg')}}"
                                alt="Outlander Sport"
                            />
                        </picture>
                    </div>
                    <div class="col-sm-6 col-md-7">
                        <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Mitsubishi-Attrage-2020-Leaflet.pdf" title="Tải Catalogue" class="btn btn-icon-left" target="_blank">
                            <span class="icon"><i class="svg-icon icon-download"></i></span><span class="text">Tải Catalogue</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="video">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/xm8nsNEDXjE"
                            frameborder="0"
                            allowfullscreen=""
                        ></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-photo">
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Untitled-1.jpg" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/new-attrage/Untitled-1-390x390.jpg')}}"
                        alt="Untitled-1"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Noi-that-rong-rai.jpg" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/new-attrage/Noi-that-rong-rai-390x390.jpg')}}"
                        alt="Noi-that-rong-rai"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/An-toan-ELL.jpg" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/new-attrage/An-toan-ELL-390x390.jpg')}}"
                        alt="An-toan-ELL"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Van-hanh.jpg" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/new-attrage/Van-hanh-390x390.jpg')}}"
                        alt="Van-hanh"
                    />
                </a>
            </div>
        </div>
    </div>
</div>
<!--End - Gallery-->