<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\FileManager;

class TaiLieuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $name = "Tài Liệu";
        $action ="Danh sách";
        $tailieu = FileManager::orderBy('created_at','desc')->where('type','>',0)->get();
        return view('admin.tailieu',compact('name','action','tailieu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file;
        if (is_file($file)) {
            $new_name = rand().'.'.$file->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('editor', $file, $new_name);
            $img = new FileManager;
            $img->url = '/storage/editor/'.$new_name;
            $img->link = '/storage/editor/'.$new_name;
            $img->thumb = '/storage/editor/'.$new_name;
            $img->type = 1;
            $img->save();
        }
        return redirect('admin/tailieu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
