<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>VẬN HÀNH</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide90"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="VẬN HÀNH ÊM ÁI CÁCH ÂM VƯỢT TRỘI" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> VẬN HÀNH ÊM ÁI CÁCH ÂM VƯỢT TRỘI</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/Van_hanh1.png')}}"
                                    alt="VẬN HÀNH ÊM ÁI CÁCH ÂM VƯỢT TRỘI"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> VẬN HÀNH ÊM ÁI CÁCH ÂM VƯỢT TRỘI</span></h3>
                                <div class="opContent">
                                    <p>
                                        New Mitsubishi Outlander được trang bị động cơ MIVEC hiện đại mang đến hiệu suất vận hành mạnh mẽ, tiết kiệm nhiêu liệu tối ưu và kết hợp cùng khả năng cách âm vượt trội mang đến sự thoải mái
                                        êm ái cho tất cả các hành khách , nâng tầm trải nghiệm.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide91" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Hộp Số CVT – INVECS III" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Hộp Số CVT – INVECS III</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/CVT-RE.jpg')}}"
                                    alt="Hộp Số CVT – INVECS III"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li><span>2.4 CVT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Hộp Số CVT – INVECS III</span></h3>
                                <div class="opContent">
                                    <p>Outlander được trang bị hộp số CVT thế hệ mới với tính năng kiểm soát tăng tốc và phản ứng nhạy hơn với chân ga giúp việc tăng tốc trở nên mượt mà và êm ái hơn</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide92" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Động cơ MIVEC" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Động cơ MIVEC</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/dong-co-mivec.jpg')}}"
                                    alt="Động cơ MIVEC"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Động cơ MIVEC</span></h3>
                                <div class="opContent">
                                    <p>
                                        Động cơ MIVEC tiên tiến của Mitsubishi giúp cải thiện công suất và mô men xoắn được trang bị trên Outlander với dung tích 2.0L &amp; 2.4L, giúp mang lại khả năng tăng tốc nhanh và tiết kiệm
                                        nhiên liệu vượt trội.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide93" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Lẫy sang số trên vô lăng" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Lẫy sang số trên vô lăng</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/padle_shif.png')}}"
                                    alt="Lẫy sang số trên vô lăng"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Lẫy sang số trên vô lăng</span></h3>
                                <div class="opContent">
                                    <p>Giúp việc sang số trên nên nhanh chóng và tiện lợi hơn trong các trường hợp cần thiết mà không phải rời tay khỏi vô lăng.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide94" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống ga tự động (Cruise Control)" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Hệ thống ga tự động (Cruise Control)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/he-thong-ga-tu-dong.jpg')}}"
                                    alt="Hệ thống ga tự động (Cruise Control)"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li></li>
                                        <li></li>
                                        <li><span>2.4 CVT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Hệ thống ga tự động (Cruise Control)</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống ga tự động giúp duy trì tốc độ ổn định mà không phải đặt chân trên bàn đạp ga, giúp việc lái xe trở nên thoải mái và thư giãn hơn, đặc biệt là các hành trình dài</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide95" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống dẫn động 2 cầu 4WD linh hoạt" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Hệ thống dẫn động 2 cầu 4WD linh hoạt</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/Vận-hành-4WD.png')}}"
                                    alt="Hệ thống dẫn động 2 cầu 4WD linh hoạt"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Hệ thống dẫn động 2 cầu 4WD linh hoạt</span></h3>
                                <div class="opContent">
                                    <p>
                                        New Outlander được trang bị hệ thống kiểm soát tất cả các bánh xe (All Wheel Control) giúp kiểm soát các bánh xe độc lập và đảm bảo độ cân bằng xe một cách hoàn hảo trong tất cả các điều kiện
                                        vận hành. Người lái có thể lựa chọn 3 chế độ vận hành linh hoạt: – 4WD ECO: Tiết kiệm nhiên liệu tối ưu, vận hành ở điều kiện đường thông thường – 4WD AUTO: Vận hành ở điều kiện trơn trượt, độ
                                        bám kém, tăng ổn định khi vào cua – 4WD LOCK: Vận hành ở điều kiện địa hình xấu, sa lầy, độ bám kém.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/Van_hanh1.png')}}"
                        alt="VẬN HÀNH ÊM ÁI CÁCH ÂM VƯỢT TRỘI"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">VẬN HÀNH ÊM ÁI CÁCH ÂM VƯỢT TRỘI</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            New Mitsubishi Outlander được trang bị động cơ MIVEC hiện đại mang đến hiệu suất vận hành mạnh mẽ, tiết kiệm nhiêu liệu tối ưu và kết hợp cùng khả năng cách âm vượt trội mang đến sự thoải mái êm ái cho
                            tất cả các hành khách , nâng tầm trải nghiệm.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/CVT-RE.jpg')}}"
                        alt="Hộp Số CVT – INVECS III"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li></li>
                            <li></li>
                            <li><span>2.4 CVT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hộp Số CVT – INVECS III</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Outlander được trang bị hộp số CVT thế hệ mới với tính năng kiểm soát tăng tốc và phản ứng nhạy hơn với chân ga giúp việc tăng tốc trở nên mượt mà và êm ái hơn</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/dong-co-mivec.jpg')}}"
                        alt="Động cơ MIVEC"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Động cơ MIVEC</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Động cơ MIVEC tiên tiến của Mitsubishi giúp cải thiện công suất và mô men xoắn được trang bị trên Outlander với dung tích 2.0L &amp; 2.4L, giúp mang lại khả năng tăng tốc nhanh và tiết kiệm nhiên liệu
                            vượt trội.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/padle_shif.png')}}"
                        alt="Lẫy sang số trên vô lăng"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Lẫy sang số trên vô lăng</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Giúp việc sang số trên nên nhanh chóng và tiện lợi hơn trong các trường hợp cần thiết mà không phải rời tay khỏi vô lăng.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/he-thong-ga-tu-dong.jpg')}}"
                        alt="Hệ thống ga tự động (Cruise Control)"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li></li>
                            <li></li>
                            <li><span>2.4 CVT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống ga tự động (Cruise Control)</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Hệ thống ga tự động giúp duy trì tốc độ ổn định mà không phải đặt chân trên bàn đạp ga, giúp việc lái xe trở nên thoải mái và thư giãn hơn, đặc biệt là các hành trình dài</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/Vận-hành-4WD.png')}}"
                        alt="Hệ thống dẫn động 2 cầu 4WD linh hoạt"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống dẫn động 2 cầu 4WD linh hoạt</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            New Outlander được trang bị hệ thống kiểm soát tất cả các bánh xe (All Wheel Control) giúp kiểm soát các bánh xe độc lập và đảm bảo độ cân bằng xe một cách hoàn hảo trong tất cả các điều kiện vận hành.
                            Người lái có thể lựa chọn 3 chế độ vận hành linh hoạt: – 4WD ECO: Tiết kiệm nhiên liệu tối ưu, vận hành ở điều kiện đường thông thường – 4WD AUTO: Vận hành ở điều kiện trơn trượt, độ bám kém, tăng ổn định
                            khi vào cua – 4WD LOCK: Vận hành ở điều kiện địa hình xấu, sa lầy, độ bám kém.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>