<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>VẬN HÀNH</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide100"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="VẬN HÀNH HIỆU QUẢ VÀ LINH HOẠT" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> VẬN HÀNH HIỆU QUẢ VÀ LINH HOẠT</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Van-hanh.jpg')}}"
                                    alt="VẬN HÀNH HIỆU QUẢ VÀ LINH HOẠT"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> VẬN HÀNH HIỆU QUẢ VÀ LINH HOẠT</span></h3>
                                <div class="opContent">
                                    <p>Tối đa sự linh hoạt trên mọi cung đường</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide101" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Động cơ MIVEC tiết kiệm nhiên liệu" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Động cơ MIVEC tiết kiệm nhiên liệu</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Dong-co.jpg')}}"
                                    alt="Động cơ MIVEC tiết kiệm nhiên liệu"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Động cơ MIVEC tiết kiệm nhiên liệu</span></h3>
                                <div class="opContent">
                                    <p>Công nghệ điều khiển van biến thiên điện tử MIVEC giúp tăng công suất cho phép xe vận hành hiệu quả nhưng vẫn đảm bảo tiết kiệm nhiên liệu tối ưu.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide102" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Khoảng sáng gầm cao, khả năng lội nước vượt trội" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Khoảng sáng gầm cao, khả năng lội nước vượt trội</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Khoang-sang-gam.jpg')}}"
                                    alt="Khoảng sáng gầm cao, khả năng lội nước vượt trội"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Khoảng sáng gầm cao, khả năng lội nước vượt trội</span></h3>
                                <div class="opContent">
                                    <p>Với khoảng sáng gầm lên tới 205mm, cho phép Xpander chinh phục cả những đoạn đường gồ ghề. Bên cạnh đó, Xpander có khả năng lội nước lên tới 400mm.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide103" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống treo ổn định, vững chãi" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Hệ thống treo ổn định, vững chãi</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/312.jpg')}}"
                                    alt="Hệ thống treo ổn định, vững chãi"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Hệ thống treo ổn định, vững chãi</span></h3>
                                <div class="opContent">
                                    <p>Tăng cường khả năng vận hành ổn định và đầm chắc trên đường.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide104" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Bán kính quay vòng nhỏ: 5.2m" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Bán kính quay vòng nhỏ: 5.2m</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Ban-kinh-quay-dau.jpg')}}"
                                    alt="Bán kính quay vòng nhỏ: 5.2m"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Bán kính quay vòng nhỏ: 5.2m</span></h3>
                                <div class="opContent">
                                    <p>Bán kính quay vòng tối thiểu chỉ 5.2m cùng với góc vát chéo ở đầu xe giúp xe dễ dàng xoay trở hơn</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Van-hanh.jpg')}}"
                        alt="VẬN HÀNH HIỆU QUẢ VÀ LINH HOẠT"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">VẬN HÀNH HIỆU QUẢ VÀ LINH HOẠT</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Tối đa sự linh hoạt trên mọi cung đường</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Dong-co.jpg')}}"
                        alt="Động cơ MIVEC tiết kiệm nhiên liệu"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Động cơ MIVEC tiết kiệm nhiên liệu</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Công nghệ điều khiển van biến thiên điện tử MIVEC giúp tăng công suất cho phép xe vận hành hiệu quả nhưng vẫn đảm bảo tiết kiệm nhiên liệu tối ưu.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Khoang-sang-gam.jpg')}}"
                        alt="Khoảng sáng gầm cao, khả năng lội nước vượt trội"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khoảng sáng gầm cao, khả năng lội nước vượt trội</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Với khoảng sáng gầm lên tới 205mm, cho phép Xpander chinh phục cả những đoạn đường gồ ghề. Bên cạnh đó, Xpander có khả năng lội nước lên tới 400mm.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/312.jpg')}}"
                        alt="Hệ thống treo ổn định, vững chãi"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống treo ổn định, vững chãi</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Tăng cường khả năng vận hành ổn định và đầm chắc trên đường.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Ban-kinh-quay-dau.jpg')}}"
                        alt="Bán kính quay vòng nhỏ: 5.2m"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Bán kính quay vòng nhỏ: 5.2m</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Bán kính quay vòng tối thiểu chỉ 5.2m cùng với góc vát chéo ở đầu xe giúp xe dễ dàng xoay trở hơn</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>