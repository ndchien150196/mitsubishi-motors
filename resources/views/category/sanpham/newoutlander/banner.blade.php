<!--Banner-->
<div class="pr-banner">
    <picture>
        <!--if IE 9video(style='display: none;')-->
        <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/07/1920x728-_RE2.4_Final.png" alt="" media="(min-width: 992px)" />
        <!--if IE 9-->
        <img srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/07/2.1000x800-_RE2.4.psd.png" alt="" class="res-img" />
    </picture>

    <div class="video hidden-xs hidden-sm video-full">
        <div class="video-show">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe
                    id="ytplayer"
                    type="text/html"
                    src="https://www.youtube.com/embed/efKSs5m5kpc"
                    frameborder="0"
                    allowfullscreen=""
                    volume="50"
                ></iframe>
            </div>
        </div>
    </div>
</div>
<!--End - Banner-->