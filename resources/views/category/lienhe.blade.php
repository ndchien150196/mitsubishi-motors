@extends("master")
@section("content") 
<div class="normal-block contact-block">
    <h3 class="title-block">
        <span>Liên hệ</span>
    </h3>
    <div class="content">
        <div class="map-block">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3738.63617374938!2d106.32466971492286!3d20.439047986320755!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135e4c31183f9cb%3A0x62d8493f1fbbd6b2!2s140%20Quang%20Trung%2C%20P.%20Quan%20Trung%2C%20Th%C3%A1i%20B%C3%ACnh!5e0!3m2!1svi!2s!4v1606242275325!5m2!1svi!2s" width="100%" height="450" frameborder="0" style="border: 0;" allowfullscreen=""></iframe>
        </div>
        <div class="side-block">
            <div class="contact-info">
                <div class="info-title">MITSUBISHI THÁI BÌNH</div>
                <ul class="list">
                    <li class="item">
                        <i class="fa fa-home"></i>
                        <span>140 Quang Trung, phường Quang Trung,TP Thái Bình </span>
                    </li>
                    <li class="item">
                        <i class="fa fa-phone"></i>
                        <span>ĐT: <a href="tel:{{\App\Common::phonelh()}}">{{\App\Common::phonelh()}}</a></span>
                    </li>
                    <li class="item">
                        <i class="fa fa-envelope"></i>
                        <span><a href="mailto:{{\App\Common::email()}}">{{\App\Common::email()}}</a></span>
                    </li>
                    <li class="item">
                        <i class="fa fa-globe"></i>
                        <span><a href="http://mitsubishi-thaibinh.vn/">http://mitsubishi-thaibinh.vn/</a></span>
                    </li>
                </ul>
            </div>
            <div id="contact_register_frm" class="contact-form reg-frm">
                <div id="alertComplete" class="form-header">Gửi yêu cầu đến đại lý</div>
                <form id="d2-register-test-drive" class="mitsu-frm contact-frm" data-url="/mail-lien-he">
                    @csrf
                    <div class="frm-item textfield-float-label" d2-val-target-parent="Name">
                        <input id="name" name="Name" type="text" d2-val-required="" class="is-empty" />
                        <label for="name">Họ tên</label>
                        <p class="error-msg" d2-val-target="Name" d2-val-required-msg="">Vui lòng nhập tên</p>
                        <p class="error-msg" d2-val-target="Name" d2-val-server-msg=""></p>
                    </div>
                    <div class="frm-item textfield-float-label" d2-val-target-parent="Email">
                        <input id="email" name="Email" type="text" d2-val-required="" d2-val-email="" class="is-empty" />
                        <label for="email">Email</label>
                        <p class="error-msg" d2-val-target="Email" d2-val-required-msg="">Vui lòng nhập email</p>
                        <p class="error-msg" d2-val-target="Email" d2-val-email-msg="">Email chưa hợp lệ</p>
                        <p class="error-msg" d2-val-target="Email" d2-val-server-msg=""></p>
                    </div>
                    <div class="frm-item textfield-float-label" d2-val-target-parent="Phone">
                        <input id="phone" name="Phone" type="text" d2-val-required="" d2-val-phone="" class="is-empty" />
                        <label for="phone">Số điện thoại</label>
                        <p class="error-msg" d2-val-target="Phone" d2-val-required-msg="">Vui lòng nhập SĐT</p>
                        <p class="error-msg" d2-val-target="Phone" d2-val-phone-msg="">SĐT chưa hợp lệ</p>
                        <p class="error-msg" d2-val-target="Phone" d2-val-server-msg=""></p>
                    </div>
                    <div class="frm-item textfield-float-label" d2-val-target-parent="Content">
                        <input id="content" name="Content" d2-val-required="" type="text" class="is-empty" />
                        <label for="content">Nội dung</label>
                        <p class="error-msg" d2-val-target="Content" d2-val-required-msg="">Vui lòng nhập nội dung</p>
                        <p class="error-msg" d2-val-target="Content" d2-val-server-msg=""></p>
                    </div>
                    <div class="frm-item">
                        <a id="d2-submit" href="javascript:;" class="frm-btn">Đăng ký ngay</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection
