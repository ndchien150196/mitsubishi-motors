<!--Highlight-->
<div class="normal-block dacdiemnoibat">
    <div class="grid-inner">
        <h3 class="title-block"><span>Đặc điểm nổi bật</span></h3>
        <div class="row">
            <div class="col-md-4 col-lg-3">
                <div class="pr-info">
                    <h1 class="title">Xpander Cross</h1>
                    <p class="summary">
                        Nổi bật với phong cách SUV cùng ngoại hình trẻ trung, khoang nội thất tiện nghi và khả năng vận hành linh hoạt, Mitsubishi Xpander Cross mới sẽ đáp ứng trọn vẹn những trải nghiệm phong phú, đặc biệt là cá
                        nhân yêu thích phiêu lưu, khám phá. XPANDER CROSS - BẢN LĨNH ĐỊNH PHONG CÁCH
                    </p>
                </div>
            </div>

            <!--.pr-info-->
            <div class="col-md-8 col-lg-9">
                <div class="slide-dacdiemnoibat">
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img class="res-img" alt="fsf" src="{{asset('img/xpander-cross/Top-Feature-1.jpg')}}" />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">Mặt ca lăng và ốp cản phong cách SUV</h4>
                                <p class="summary">Kết hợp cùng thiết kế Dynamic Shield đem đến vể ngoại đậm tính thể thao</p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img class="res-img" alt="fsf" src="{{asset('img/xpander-cross/Top-Feature-2.2.jpg')}}" />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">Hệ thống đèn chiếu sáng Full LED</h4>
                                <p class="summary"></p>
                            </figcaption>
                        </figure>
                    </div>
                    <div class="feature-news-box slider">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img class="res-img" alt="fsf" src="{{asset('img/xpander-cross/Top-Feature-3.jpg')}}" />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">Mâm xe kích thước 17-inch</h4>
                                <p class="summary"></p>
                            </figcaption>
                        </figure>
                    </div>
                </div>
                <!--.slide-center-->
            </div>
        </div>
    </div>
</div>
<!--End - Highlight-->