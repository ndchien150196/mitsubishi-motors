@extends("master")
@section("content") 
<div class="second-block page-news-home news-page service-pages">
    <div class="grid-fluid">
        <h3 class="title-block">
            <span>Sửa chữa</span>
        </h3>
        <div class="grid-content">
            <div class="row">
                <div class="col-md-8">
                    <div class="editor">
                        <p>Mục đích của công việc bảo dưỡng xe là giữ cho xe luôn trong điều kiện sử dụng tốt nhất và tránh được những “trục trặc trên đường”, cụ thể như sau:</p>
                        <ul>
                            <li>
                                Thay thế định kỳ những vật liệu và phụ tùng tiêu hao như dầu nhờn, lọc nhớt, lọc nhiên liệu, lọc gió,v..v.. . (Những loại thay thế này rất quan trọng nhằm giữ cho động cơ và các chi tiết quan trọng khác của
                                xe luôn trong tình trạng tốt nhất).
                            </li>
                            <li>Phát hiện hỏng hóc có thể xảy ra, từ đó có cách xử lý cần thiết, kịp thời để giảm thiểu chi phí sửa chữa thực tế.</li>
                        </ul>
                        <p><strong>Các loại bảo dưỡng</strong></p>
                        <p>Có hai loại hình bảo dưỡng như sau:</p>
                        <ul>
                            <li>Bảo dưỡng miễn phí công lao động: Áp dụng cho mức bảo dưỡng 5.000 km đầu tiên.</li>
                            <li>Bảo dưỡng định kỳ: Mỗi 5.000 Km sau lần bảo dưỡng miễn phí công lao động.</li>
                        </ul>
                        <p>Vui lòng tham khảo Lịch kiểm tra và bảo dưỡng định kỳ trong quyển Sổ tay hướng dẫn bảo dưỡng xe để thực hiện công việc bảo dưỡng xe tốt nhất.</p>
                            <!-- <p>
                            <img class="aligncenter wp-image-243 size-full" src="{{asset('img/dichvu/bao-duong-img.jpg')}}" alt="" width="902" height="632"
                            />
                        </p> -->
                    </div>
                </div>
                <!--  Sidebar -->
                <div class="col-md-4">
                    <div class="news-sidebar">
                        <div class="sidebar-block link-block">
                            <h3 class="news-link-title">Dịch vụ hậu mãi</h3>
                            <!-- menu-->
                            <ul class="news-link">
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['gioi-thieu-dich-vu'])}}" title="Giới thiệu dịch vụ" class="sp-text">Giới thiệu dịch vụ</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['chinh-sach-bao-hanh'])}}" title="Chính sách bảo hành" class="sp-text">Chính sách bảo hành</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['bao-duong-dinh-ky'])}}" title="Bảo dưỡng định kỳ" class="sp-text">Bảo dưỡng định kỳ</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['phu-tung-chinh-hieu'])}}" title="Phụ tùng chính hiệu" class="sp-text">Phụ tùng chính hiệu</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['sua-chua'])}}" title="Sửa chữa" class="sp-text">Sửa chữa</a>
                                </li>
                                <li class="active">
                                    <a href="{{url('dich-vu-hau-mai',['cuu-ho-giao-thong'])}}" title="Sửa chữa" class="sp-text">Cứu hộ giao thông</a>
                                </li>
                                <li class="">
                                    <a href="{{url('dich-vu-hau-mai',['dat-lich-bao-duong'])}}" title="Đặt lịch bảo dưỡng" class="sp-text">Đặt lịch bảo dưỡng</a>
                                </li>
                            </ul>
                        </div>
                        @include('//layouts/block-km')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
