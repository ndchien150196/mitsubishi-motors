@extends("master")
@section("js")
    <script src="{{asset('js/slick.min.js')}}"></script>
@endsection
@section("content") 
<div class="page-product">
    @include('category.sanpham.newoutlander.banner')
    @include('category.sanpham.xpandercross.navigation')
    <div class="pr-content">
        @include('category.sanpham.newoutlander.dacdiemnoibat')
        @include('category.sanpham.newoutlander.thongsokythuat')
        @include('category.sanpham.newoutlander.ngoaithat')
        @include('category.sanpham.newoutlander.noithatrongrai')
        @include('category.sanpham.newoutlander.vanhanh')
        @include('category.sanpham.newoutlander.antoan')
        @include('category.sanpham.newoutlander.thuvien')
    </div>
</div>


@endsection
@section('afftercontent')

<div d2-estimate-popup=""></div>

<section id="product_specs" class="mitsu-modal product-specs">
    <div class="grid-inner">
        <div data-href="#product_specs" class="modal-close"></div>
        <h3 class="title-block pr-title-block">
            <a title="So sánh giữa các phiên bản">So sánh giữa các phiên bản</a>
        </h3>
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <div class="list-steps">
                    <div class="steps-header">
                        <table>
                            <colgroup>
                                <col width="35%" />
                                <col width="21%" />
                                <col width="21%" />
                                <col width="21%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <div class="stepes-pagination">
                                            <ul>
                                                <li><a href="javascript:;" data-href="#st_thongsokythuat" class="stepes-active active">Thông số kỹ thuật</a></li>
                                                <li><a href="javascript:;" data-href="#st_trangthietbi" class="stepes-active">Trang thiết bị</a></li>
                                            </ul>
                                        </div>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/newoutlander/white.jpg')}}"
                                            alt="New Outlander 2.0 CVT"
                                        />
                                        <label>2.0 CVT</label><span>{{$product['CVT_2']}} VNĐ</span>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/newoutlander/red.jpg')}}"
                                            alt="New Outlander 2.0 CVT Premium"
                                        />
                                        <label>2.0 CVT Premium</label><span>{{$product['CVT_2_P']}} VNĐ</span>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/newoutlander/red.jpg')}}"
                                            alt="New Outlander 2.4 CVT Premium"
                                        />
                                        <label>2.4 CVT Premium</label><span>{{$product['CVT_24_P']}} VNĐ</span>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="st_thongsokythuat" class="stepes step-1 active">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />
                                    <col width="21%" />
                                    <col width="21%" />
                                    <col width="21%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><b> KÍCH THƯỚC </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kích thước tổng thể (DxRxC) (mm)</td>
                                        <td>4.695 x 1.810 x 1.710</td>
                                        <td>4.695 x 1.810 x 1.710</td>
                                        <td>4.695 x 1.810 x 1.710</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chiều dài cơ sở (mm)</td>
                                        <td>2.670</td>
                                        <td>2.670</td>
                                        <td>2.670</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chiều rộng cơ sở trước/sau (mm)</td>
                                        <td>1.540/1.540</td>
                                        <td>1.540/1.540</td>
                                        <td>1.540/1.540</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Bán kính quay vòng nhỏ nhất (m)</td>
                                        <td>5,3</td>
                                        <td>5,3</td>
                                        <td>5,3</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoảng sáng gầm xe (mm)</td>
                                        <td>190</td>
                                        <td>190</td>
                                        <td>190</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Trọng lượng không tải (Kg)</td>
                                        <td>1.500</td>
                                        <td>1.535</td>
                                        <td>1.610</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Số chỗ ngồi</td>
                                        <td>7 người</td>
                                        <td>7 người</td>
                                        <td>7 người</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> ĐỘNG CƠ </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Loại động cơ</td>
                                        <td>4B11 DOHC MIVEC</td>
                                        <td>4B11 DOHC MIVEC</td>
                                        <td>4B12 DOHC MIVEC</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dung Tích Xylanh (cc)</td>
                                        <td>1.998</td>
                                        <td>1.998</td>
                                        <td>2.360</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Công suất cực đại (ps/rpm)</td>
                                        <td>145/6.000</td>
                                        <td>145/6.000</td>
                                        <td>167/6.000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mômen xoắn cực đại (Nm/rpm)</td>
                                        <td>196/4.200</td>
                                        <td>196/4.200</td>
                                        <td>222/4.100</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dung tích thùng nhiên liệu (L)</td>
                                        <td>63</td>
                                        <td>63</td>
                                        <td>60</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hộp số</td>
                                        <td>Số tự động vô cấp (CVT) INVECS III</td>
                                        <td>Số tự động vô cấp (CVT) INVECS III</td>
                                        <td>Số tự động vô cấp (CVT) INVECS III</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Truyền động</td>
                                        <td>Cầu trước</td>
                                        <td>Cầu trước</td>
                                        <td>Hai cầu 4WD</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Trợ lực lái</td>
                                        <td>Trợ lực điện</td>
                                        <td>Trợ lực điện</td>
                                        <td>Trợ lực điện</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống treo trước</td>
                                        <td>Kiểu MacPherson với thanh cân bằng</td>
                                        <td>Kiểu MacPherson với thanh cân bằng</td>
                                        <td>Kiểu MacPherson với thanh cân bằng</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống treo sau</td>
                                        <td>Đa liên kết với thanh cân bằng</td>
                                        <td>Đa liên kết với thanh cân bằng</td>
                                        <td>Đa liên kết với thanh cân bằng</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lốp xe trước/sau</td>
                                        <td>225/55R18</td>
                                        <td>225/55R18</td>
                                        <td>225/55R18</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Phanh trước/sau</td>
                                        <td>Đĩa thông gió/Đĩa</td>
                                        <td>Đĩa thông gió/Đĩa</td>
                                        <td>Đĩa thông gió/Đĩa</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mức tiêu hao nhiên liệu (L/100Km)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Kết hợp</i></td>
                                        <td>7,2</td>
                                        <td>7,2</td>
                                        <td>7,7</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Trong đô thị</i></td>
                                        <td>9,7</td>
                                        <td>9,7</td>
                                        <td>10,3</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Ngoài đô thị</i></td>
                                        <td>5,8</td>
                                        <td>5,8</td>
                                        <td>6,2</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="st_trangthietbi" class="stepes step-2">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />

                                    <col width="21%" />
                                    <col width="21%" />
                                    <col width="21%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><b> NGOẠI THẤT </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống đèn chiếu sáng phía trước</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Đèn chiếu xa </i></td>
                                        <td>Clear Halogen</td>
                                        <td>LED</td>
                                        <td>LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Đèn chiếu gần </i></td>
                                        <td>Halogen &amp; Projector</td>
                                        <td>LED</td>
                                        <td>LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn pha điều chỉnh được độ cao</td>
                                        <td>Có</td>
                                        <td>Tự động</td>
                                        <td>Tự động</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn LED chiếu sáng ban ngày</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cảm biến đèn pha và gạt mưa tự động</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn sương mù trước/sau</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống rửa đèn</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn báo phanh thứ ba</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kính chiếu hậu</td>
                                        <td>Chỉnh điện/gập điện, tích hợp đèn báo rẽ và chức năng sưởi</td>
                                        <td>Chỉnh điện/gập điện, tích hợp đèn báo rẽ và chức năng sưởi</td>
                                        <td>Chỉnh điện/gập điện, tích hợp đèn báo rẽ và chức năng sưởi</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cửa sau đóng mở bằng điện</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tay nắm cửa ngoài</td>
                                        <td>Mạ crôm</td>
                                        <td>Mạ crôm</td>
                                        <td>Mạ crôm</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lưới tản nhiệt</td>
                                        <td>Mạ crôm</td>
                                        <td>Mạ crôm</td>
                                        <td>Mạ crôm</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kính cửa màu sậm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gạt nước kính trước</td>
                                        <td>Tốc độ thay đổi theo vận tốc xe</td>
                                        <td>Tự động</td>
                                        <td>Tự động</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gạt nước kính sau và sưởi kính sau</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mâm đúc hợp kim</td>
                                        <td>18"</td>
                                        <td>18"</td>
                                        <td>18"</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Anten vây cá</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Giá đỡ hành lý trên mui xe</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> NỘI THÂT </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Vô lăng bọc da</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Nút điều khiển âm thanh trên vô lăng</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Điện thoại rảnh tay trên vô lăng</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống kiểm soát hành trình</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lẫy sang số trên vô lăng</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Điều hòa nhiệt độ tự động</td>
                                        <td>Hai vùng nhiệt độ</td>
                                        <td>Hai vùng nhiệt độ</td>
                                        <td>Hai vùng nhiệt độ</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chất liệu ghế</td>
                                        <td>Nỉ cao cấp</td>
                                        <td>Da</td>
                                        <td>Da</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Ghế tài xế</td>
                                        <td>Chỉnh tay 6 hướng</td>
                                        <td>Chỉnh điện 10 hướng</td>
                                        <td>Chỉnh điện 10 hướng</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống sưởi ấm hàng ghế trước</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hàng ghế thứ hai gập 60:40</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hàng ghế thứ ba gập 50:50</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cửa sổ trời</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tay nắm cửa trong mạ crôm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Màn hình hiển thị đa thông tin</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Móc gắn ghế an toàn trẻ em</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn chiếu sáng hộp để đồ trung tâm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tấm ngăn khoang hành lý</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống âm thanh</td>
                                        <td>Bluetooth/USB/AUX/AM/FM - Apple CarPlay &amp; Android Auto</td>
                                        <td>Bluetooth/USB/AUX/AM/FM - Apple CarPlay &amp; Android Auto</td>
                                        <td>Bluetooth/USB/AUX/AM/FM - Apple CarPlay &amp; Android Auto</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Số lượng loa</td>
                                        <td>6</td>
                                        <td>6</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Ổ cắm điện phía sau xe</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> AN TOÀN </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Túi khí an toàn</td>
                                        <td>Túi khí đôi</td>
                                        <td>7 túi khí an toàn</td>
                                        <td>7 túi khí an toàn</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cơ cấu căng đai tự động</td>
                                        <td>Hàng ghế trước</td>
                                        <td>Hàng ghế trước</td>
                                        <td>Hàng ghế trước</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống chống bó cứng phanh (ABS)</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống phân phối lực phanh điện tử EBD</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống trợ lực phanh khẩn cấp BA</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Phanh tay điện tử &amp; chức năng giữ phanh tự động</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống cân bằng điện tử (ASC)</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống khởi hành ngang dốc (HSA)</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống giảm thiểu va chạm phía trước (FCM)</td>
                                        <td>Không</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống cảnh báo điểm mù (BSW)</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống cảnh báo phương tiện cắt ngang khi lùi xe (RCTA)</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống kiểm soát chân ga khi phanh</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chìa khóa thông minh và khởi động bằng nút bấm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoá cửa từ xa</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chức năng chống trộm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chìa khóa mã hóa chống trộm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Camera lùi</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cảm biến lùi</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br />
                    <br />
                    <p class="text-left">(*) Mức tiêu hao nhiên liệu chứng nhận bởi Cục Đăng Kiểm Việt Nam. Các thông số kỹ thuật có thể thay đổi mà không báo trước</p>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
@section('jsfooter')
<script src="{{asset('js/sanpham/TweenMax.min.js')}}"></script>
<script src="{{asset('js/sanpham/ScrollToPlugin.min.js')}}"></script>
<script src="{{asset('js/sanpham/jquery.fancybox.js')}}"></script>
<script src="{{asset('js/new-xpander.js')}}"></script>
<script src="{{asset('js/estimate-price.js')}}"></script>
<script type="text/javascript">
    productInit();
</script>
@endsection
