<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>NỘI THẤT</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide90"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="NỘI THẤT RỘNG RÃI NHỜ THIẾT KẾ J-LINE" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> NỘI THẤT RỘNG RÃI NHỜ THIẾT KẾ J-LINE</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-Interior.jpg')}}"
                                    alt="NỘI THẤT RỘNG RÃI NHỜ THIẾT KẾ J-LINE"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> NỘI THẤT RỘNG RÃI NHỜ THIẾT KẾ J-LINE</span></h3>
                                <div class="opContent">
                                    <p>Thiết kế J-line đem đến không gian nội thất rộng rãi hơn và ghế sau có độ nghiêng lưng ghế lớn nhất phân khúc lên đến 25 độ.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide91" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Ghế ngồi cao cấp" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Ghế ngồi cao cấp</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-seat.jpg')}}"
                                    alt="Ghế ngồi cao cấp"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Ghế ngồi cao cấp</span></h3>
                                <div class="opContent">
                                    <p>Ghế ngồi thiết kế thể thao mang đến sự thoải mái vượt trội trong từng chi tiết, kết hợp hệ thống chỉnh điện 8 hướng trên ghế lái</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide92" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Cửa gió sau bố trí trên trần" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Cửa gió sau bố trí trên trần</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/Untitled-2.png')}}"
                                    alt="Cửa gió sau bố trí trên trần"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Cửa gió sau bố trí trên trần</span></h3>
                                <div class="opContent">
                                    <p>Giúp hành khách thoải mái với nhiệt độ lý tưởng. Hệ thống sẽ tăng lượng gió lưu thông đến phía sau. Hướng gió và lưu lượng gió cũng có thể được điều chỉnh.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide93" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Vô lăng 4 chấu bọc da cao cấp" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Vô lăng 4 chấu bọc da cao cấp</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-Steeling-wheel.jpg')}}"
                                    alt="Vô lăng 4 chấu bọc da cao cấp"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Vô lăng 4 chấu bọc da cao cấp</span></h3>
                                <div class="opContent">
                                    <p>Vô lăng bốn chấu được bọc da sang trọng tích hợp nút điều khiển âm thanh. Ngoài ra, vô lăng được điều chỉnh 4 hướng tạo sự linh hoạt và thoải mái khi lái xe</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide94" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Bảng đồng hồ trung tâm" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Bảng đồng hồ trung tâm</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-MultiScreen.jpg')}}"
                                    alt="Bảng đồng hồ trung tâm"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Bảng đồng hồ trung tâm</span></h3>
                                <div class="opContent">
                                    <p>Màn hình hiển thị đa thông tin LCD và cụm đồng hồ thể thao sắc nét với độ tương phản cao, giúp người lái dễ dàng theo dõi thông tin ngay cả khi đi trong điều kiện ánh sáng yếu</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide95" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Gương chiếu hậu chống chói tự động" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Gương chiếu hậu chống chói tự động</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-Eletrochromic-Mirror.jpg')}}"
                                    alt="Gương chiếu hậu chống chói tự động"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li>
                                            <span>4X4<small class="small-text">AT MIVEC</small></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Gương chiếu hậu chống chói tự động</span></h3>
                                <div class="opContent">
                                    <p>Tăng cường khả năng quan sát và lái xe an toàn</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="6" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide96" data-slick-index="6" aria-hidden="true">
                        <a href="javascript:;" title="Điều hòa tự động 2 vùng độc lập" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">6. Điều hòa tự động 2 vùng độc lập</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-AirCon.jpg')}}"
                                    alt="Điều hòa tự động 2 vùng độc lập"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">6. Điều hòa tự động 2 vùng độc lập</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống điều hòa 2 vùng tự động độc lập với khả năng làm mát sâu, giúp tất cả các hành khách bên trong xe đều cảm thấy thoải mái.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="7" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide97" data-slick-index="7" aria-hidden="true">
                        <a href="javascript:;" title="Cách âm vượt trội" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">7. Cách âm vượt trội</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/cai-thien-cach-am.jpg')}}"
                                    alt="Cách âm vượt trội"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">7. Cách âm vượt trội</span></h3>
                                <div class="opContent">
                                    <p>Tận hưởng sự yên tĩnh tuyệt vời nhờ vào việc bố trí các vật liệu cách âm và hấp thụ âm khắp thân xe.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="8" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide98" data-slick-index="8" aria-hidden="true">
                        <a href="javascript:;" title="Hệ thống giải trí cao cấp" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">8. Hệ thống giải trí cao cấp</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-dvd.jpg')}}"
                                    alt="Hệ thống giải trí cao cấp"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">8. Hệ thống giải trí cao cấp</span></h3>
                                <div class="opContent">
                                    <p>Trải nghiệm tiện ích hiện đại với màn hình cảm ừng 6,75″ cùng 6 loa, kết nối Android Auto và Apple CarPlay</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-Interior.jpg')}}"
                        alt="NỘI THẤT RỘNG RÃI NHỜ THIẾT KẾ J-LINE"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">NỘI THẤT RỘNG RÃI NHỜ THIẾT KẾ J-LINE</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Thiết kế J-line đem đến không gian nội thất rộng rãi hơn và ghế sau có độ nghiêng lưng ghế lớn nhất phân khúc lên đến 25 độ.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-seat.jpg')}}"
                        alt="Ghế ngồi cao cấp"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Ghế ngồi cao cấp</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Ghế ngồi thiết kế thể thao mang đến sự thoải mái vượt trội trong từng chi tiết, kết hợp hệ thống chỉnh điện 8 hướng trên ghế lái</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/Untitled-2.png')}}"
                        alt="Cửa gió sau bố trí trên trần"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Cửa gió sau bố trí trên trần</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Giúp hành khách thoải mái với nhiệt độ lý tưởng. Hệ thống sẽ tăng lượng gió lưu thông đến phía sau. Hướng gió và lưu lượng gió cũng có thể được điều chỉnh.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-Steeling-wheel.jpg')}}"
                        alt="Vô lăng 4 chấu bọc da cao cấp"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Vô lăng 4 chấu bọc da cao cấp</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Vô lăng bốn chấu được bọc da sang trọng tích hợp nút điều khiển âm thanh. Ngoài ra, vô lăng được điều chỉnh 4 hướng tạo sự linh hoạt và thoải mái khi lái xe</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-MultiScreen.jpg')}}"
                        alt="Bảng đồng hồ trung tâm"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Bảng đồng hồ trung tâm</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Màn hình hiển thị đa thông tin LCD và cụm đồng hồ thể thao sắc nét với độ tương phản cao, giúp người lái dễ dàng theo dõi thông tin ngay cả khi đi trong điều kiện ánh sáng yếu</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-Eletrochromic-Mirror.jpg')}}"
                        alt="Gương chiếu hậu chống chói tự động"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li>
                                <span>4X4<small class="small-text">AT MIVEC</small></span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Gương chiếu hậu chống chói tự động</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Tăng cường khả năng quan sát và lái xe an toàn</p>
                    </div>
                </div>
            </div>
            <div data-item="6" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-AirCon.jpg')}}"
                        alt="Điều hòa tự động 2 vùng độc lập"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Điều hòa tự động 2 vùng độc lập</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Hệ thống điều hòa 2 vùng tự động độc lập với khả năng làm mát sâu, giúp tất cả các hành khách bên trong xe đều cảm thấy thoải mái.</p>
                    </div>
                </div>
            </div>
            <div data-item="7" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/cai-thien-cach-am.jpg')}}"
                        alt="Cách âm vượt trội"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Cách âm vượt trội</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Tận hưởng sự yên tĩnh tuyệt vời nhờ vào việc bố trí các vật liệu cách âm và hấp thụ âm khắp thân xe.</p>
                    </div>
                </div>
            </div>
            <div data-item="8" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-dvd.jpg')}}"
                        alt="Hệ thống giải trí cao cấp"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống giải trí cao cấp</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Trải nghiệm tiện ích hiện đại với màn hình cảm ừng 6,75″ cùng 6 loa, kết nối Android Auto và Apple CarPlay</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>