<!--Highlight-->
<div class="normal-block dacdiemnoibat">
    <div class="grid-inner">
        <h3 class="title-block"><span>Đặc điểm nổi bật</span></h3>
        <div class="row">
            <div class="col-md-4 col-lg-3">
                <div class="pr-info">
                    <h1 class="title">New Triton</h1>
                    <p class="summary">
                        Kế thừa và cải tiến hơn nữa định hướng thiết kế của thế hệ trước về một chiếc pick-up thể thao đa dụng đáp ứng trọn vẹn nhu cầu sử dụng cá nhân lẫn kinh doanh, mang đến sự thoải mái và tiện nghi của một chiếc
                        xe du lịch nhưng vẫn đảm bảo tính bền bỉ và thực dụng của một chiếc xe bán tải.
                    </p>
                </div>
            </div>

            <!--.pr-info-->
            <div class="col-md-8 col-lg-9">
                <div class="slide-dacdiemnoibat">
                    <div class="feature-news-box">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/newtriton/SU-Dynamic.jpg')}}"
                                    alt="NGÔN NGỮ THIẾT KẾ DYNAMIC SHIELD"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/SU-Dynamic.jpg"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">NGÔN NGỮ THIẾT KẾ DYNAMIC SHIELD</h4>
                                <p class="summary">Với triết lý “Vẻ đẹp từ công năng”, mang lại sự hài hòa giữa hình ảnh mạnh mẽ, hiện đại và tính năng bảo vệ an toàn</p>
                            </figcaption>
                        </figure>
                        <!--.feature-news-item-->
                    </div>
                    <div class="feature-news-box">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/newtriton/truyen-dong-2-cau-voi-khoa-vi-sai-trung-tam.jpg')}}"
                                    alt="SUPER SELECT 4WD-II"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2017/06/truyen-dong-2-cau-voi-khoa-vi-sai-trung-tam.jpg"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">SUPER SELECT 4WD-II</h4>
                                <p class="summary">Với 4 chế độ chọn địa hình cùng <b> vi sai trung tâm duy nhất phân khúc </b></p>
                            </figcaption>
                        </figure>
                        <!--.feature-news-item-->
                    </div>
                    <div class="feature-news-box">
                        <figure class="feature-news-item">
                            <div class="thumb">
                                <img
                                    src="{{asset('img/newtriton/SU-engine1.jpg')}}"
                                    alt="ĐỘNG CƠ DIESEL MIVEC 2.4L"
                                    class="res-img"
                                    data-src-mb="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/SU-engine1.jpg"
                                />
                            </div>
                            <figcaption class="content">
                                <h4 class="br-l-title">ĐỘNG CƠ DIESEL MIVEC 2.4L</h4>
                                <p class="summary">Động cơ Diesel bằng nhôm <b> đầu tiên trong phân khúc </b></p>
                            </figcaption>
                        </figure>
                        <!--.feature-news-item-->
                    </div>
                </div>
                <!--.slide-center-->
            </div>
        </div>
    </div>
</div>
<!--End - Highlight-->