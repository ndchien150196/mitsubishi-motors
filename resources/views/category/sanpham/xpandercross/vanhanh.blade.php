<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>VẬN HÀNH</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide90"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="Vận hành linh hoạt như một mẫu SUV thực thụ" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> Vận hành linh hoạt như một mẫu SUV thực thụ</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Performance1.jpg')}}" alt="Vận hành linh hoạt như một mẫu SUV thực thụ" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> Vận hành linh hoạt như một mẫu SUV thực thụ</span></h3>
                                <div class="opContent">
                                    <p>Xpander Cross sở hữu khả năng vận hành linh hoạt với hệ thống treo vững chãi, khoảng sáng gầm 225mm tốt nhất phân khúc, cùng khả năng lội nước vượt trội lên đến 400mm.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide91" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Động cơ MIVEC – Tiết kiệm nhiên liệu tối ưu" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Động cơ MIVEC – Tiết kiệm nhiên liệu tối ưu</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Dong-co.jpg')}}" alt="Động cơ MIVEC – Tiết kiệm nhiên liệu tối ưu" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Động cơ MIVEC – Tiết kiệm nhiên liệu tối ưu</span></h3>
                                <div class="opContent">
                                    <p>Công nghệ điều khiển van biến thiên điện tử MIVEC độc quyền giúp tăng công suất cho phép xe vận hành hiệu quả nhưng vẫn đảm bảo tiết kiệm nhiên liệu tối ưu.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide92" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Khoảng sáng gầm và khả năng lội nước vượt trội" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Khoảng sáng gầm và khả năng lội nước vượt trội</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Ngap-nuoc.jpg')}}" alt="Khoảng sáng gầm và khả năng lội nước vượt trội" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Khoảng sáng gầm và khả năng lội nước vượt trội</span></h3>
                                <div class="opContent">
                                    <p>Xpander Cross sở hữu khoảng sáng gầm 225mm, cho khả năng lội nước lên đến 400mm</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide93" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống treo vững trãi" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Hệ thống treo vững trãi</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/He-thong-treo.jpg')}}" alt="Hệ thống treo vững trãi" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Hệ thống treo vững trãi</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống treo cứng vững cho cảm giác lái chắc chắn khi đi trên con đường xấu.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" tabindex="-1" role="option" aria-describedby="slick-slide94" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Góc tiếp cận và góc thoát tốt" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Góc tiếp cận và góc thoát tốt</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img src="{{asset('img/xpander-cross/Goc-toi-goc-thoat.jpg')}}" alt="Góc tiếp cận và góc thoát tốt" />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Góc tiếp cận và góc thoát tốt</span></h3>
                                <div class="opContent">
                                    <p>Sở hữu góc tiếp cận và góc thoát tốt giúp xe dễ dàng vượt qua các địa hình phức tạp như một mẫu SUV thực thụ.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Performance1.jpg')}}" alt="Vận hành linh hoạt như một mẫu SUV thực thụ" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Vận hành linh hoạt như một mẫu SUV thực thụ</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Xpander Cross sở hữu khả năng vận hành linh hoạt với hệ thống treo vững chãi, khoảng sáng gầm 225mm tốt nhất phân khúc, cùng khả năng lội nước vượt trội lên đến 400mm.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Dong-co.jpg')}}" alt="Động cơ MIVEC – Tiết kiệm nhiên liệu tối ưu" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Động cơ MIVEC – Tiết kiệm nhiên liệu tối ưu</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Công nghệ điều khiển van biến thiên điện tử MIVEC độc quyền giúp tăng công suất cho phép xe vận hành hiệu quả nhưng vẫn đảm bảo tiết kiệm nhiên liệu tối ưu.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Ngap-nuoc.jpg')}}" alt="Khoảng sáng gầm và khả năng lội nước vượt trội" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khoảng sáng gầm và khả năng lội nước vượt trội</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Xpander Cross sở hữu khoảng sáng gầm 225mm, cho khả năng lội nước lên đến 400mm</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/He-thong-treo.jpg')}}" alt="Hệ thống treo vững trãi" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống treo vững trãi</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Hệ thống treo cứng vững cho cảm giác lái chắc chắn khi đi trên con đường xấu.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img src="{{asset('img/xpander-cross/Goc-toi-goc-thoat.jpg')}}" alt="Góc tiếp cận và góc thoát tốt" />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Góc tiếp cận và góc thoát tốt</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Sở hữu góc tiếp cận và góc thoát tốt giúp xe dễ dàng vượt qua các địa hình phức tạp như một mẫu SUV thực thụ.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>