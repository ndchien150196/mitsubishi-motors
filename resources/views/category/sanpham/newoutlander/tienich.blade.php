<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>TIỆN ÍCH THÂN THIỆN OMOTENASHI</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide120"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="45 ngăn chứa đồ tiện ích theo triết lý Omotenashi" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> 45 ngăn chứa đồ tiện ích theo triết lý Omotenashi</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Dung-do-2.jpg')}}"
                                    alt="45 ngăn chứa đồ tiện ích theo triết lý Omotenashi"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> 45 ngăn chứa đồ tiện ích theo triết lý Omotenashi</span></h3>
                                <div class="opContent">
                                    <p>Tiện ích thông minh ứng dụng triết lý “OMOTENASHI” của Nhật Bản, lấy người dùng làm trọng tâm, đem lại sự thoải mái từ những chi tiết nhỏ nhất.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide121" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Ngăn chứa đồ tiện ích" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Ngăn chứa đồ tiện ích</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Dung-do-1.jpg')}}"
                                    alt="Ngăn chứa đồ tiện ích"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Ngăn chứa đồ tiện ích</span></h3>
                                <div class="opContent">
                                    <p>Tiện ích thông minh ứng dụng triết lý “OMOTENASHI” của Nhật Bản, lấy người dùng làm trọng tâm, đem lại sự thoải mái từ những chi tiết nhỏ nhất.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide122" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Ngăn chứa nước tiện dụng" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Ngăn chứa nước tiện dụng</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Dung-nuoc.jpg')}}"
                                    alt="Ngăn chứa nước tiện dụng"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Ngăn chứa nước tiện dụng</span></h3>
                                <div class="opContent">
                                    <p>Tiện ích thông minh ứng dụng triết lý “OMOTENASHI” của Nhật Bản, lấy người dùng làm trọng tâm, đem lại sự thoải mái từ những chi tiết nhỏ nhất.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide123" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Ổ cắm điện tại cả 3 hàng ghế" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Ổ cắm điện tại cả 3 hàng ghế</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/O-cam-dien.jpg')}}"
                                    alt="Ổ cắm điện tại cả 3 hàng ghế"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Ổ cắm điện tại cả 3 hàng ghế</span></h3>
                                <div class="opContent">
                                    <p>Tiện ích thông minh ứng dụng triết lý “OMOTENASHI” của Nhật Bản, lấy người dùng làm trọng tâm, đem lại sự thoải mái từ những chi tiết nhỏ nhất.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide124" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Đèn chào mừng, dễ dàng tìm xe trong bãi đỗ" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Đèn chào mừng, dễ dàng tìm xe trong bãi đỗ</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Tim-xe.jpg')}}"
                                    alt="Đèn chào mừng, dễ dàng tìm xe trong bãi đỗ"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Đèn chào mừng, dễ dàng tìm xe trong bãi đỗ</span></h3>
                                <div class="opContent">
                                    <p>Đèn định vị xe sẽ sáng trong vòng 30s khi bấm mở khoá cửa trên chìa, hỗ trợ tìm xe trong bãi đỗ.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Dung-do-2.jpg')}}"
                        alt="45 ngăn chứa đồ tiện ích theo triết lý Omotenashi"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">45 ngăn chứa đồ tiện ích theo triết lý Omotenashi</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Tiện ích thông minh ứng dụng triết lý “OMOTENASHI” của Nhật Bản, lấy người dùng làm trọng tâm, đem lại sự thoải mái từ những chi tiết nhỏ nhất.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Dung-do-1.jpg')}}"
                        alt="Ngăn chứa đồ tiện ích"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Ngăn chứa đồ tiện ích</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Tiện ích thông minh ứng dụng triết lý “OMOTENASHI” của Nhật Bản, lấy người dùng làm trọng tâm, đem lại sự thoải mái từ những chi tiết nhỏ nhất.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Dung-nuoc.jpg')}}"
                        alt="Ngăn chứa nước tiện dụng"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Ngăn chứa nước tiện dụng</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Tiện ích thông minh ứng dụng triết lý “OMOTENASHI” của Nhật Bản, lấy người dùng làm trọng tâm, đem lại sự thoải mái từ những chi tiết nhỏ nhất.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/O-cam-dien.jpg')}}"
                        alt="Ổ cắm điện tại cả 3 hàng ghế"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Ổ cắm điện tại cả 3 hàng ghế</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Tiện ích thông minh ứng dụng triết lý “OMOTENASHI” của Nhật Bản, lấy người dùng làm trọng tâm, đem lại sự thoải mái từ những chi tiết nhỏ nhất.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Tim-xe.jpg')}}"
                        alt="Đèn chào mừng, dễ dàng tìm xe trong bãi đỗ"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #404040;">Đèn chào mừng, dễ dàng tìm xe trong bãi đỗ</h3>
                    <div class="opContent" style="color: #404040;">
                        <p>Đèn định vị xe sẽ sáng trong vòng 30s khi bấm mở khoá cửa trên chìa, hỗ trợ tìm xe trong bãi đỗ.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>