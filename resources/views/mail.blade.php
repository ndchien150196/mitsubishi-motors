<!DOCTYPE html>
<html>
<head>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>

<table>
  <tr>
    <th>Tên</th>
    <th>Số Điện Thoại</th>
    <th>Email</th>
    <th>Sản Phẩm</th>
  </tr>
  <tr>
    <td>{{$name}}</td>
    <td>{{$phone}}</td>
    <td>{{$email}}</td>
    <td>{{$product}}</td>
  </tr>
</table>

</body>
</html>
