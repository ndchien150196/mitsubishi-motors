<!--Banner-->
<div class="pr-banner">
    <picture>
        <!--if IE 9video(style='display: none;')-->
        <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/03/Attrage-PC.jpg" alt="" media="(min-width: 992px)" />
        <!--if IE 9-->
        <img srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/03/Attrage-MB.jpg" alt="" class="res-img" />
    </picture>
</div>
<!--End - Banner-->