<!--Specs-->
<div class="normal-block thongsokythuat">
    <div class="grid-inner">
        <h3 class="title-block pr-title-block">
            <span>Thông số kỹ thuật</span>
        </h3>
        <div class="row">
            <div class="col-md-6">
                <div class="tab-content">
                    <div id="cl360" role="tabpanel" class="tab-pane fade">
                        <div
                            id="gl_360"
                            data-imgarray='[                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/24.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/22.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/211.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/19.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/18.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/17.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/16.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/14.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/13.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/12.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/9.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/7.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/51.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/41.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/31.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/09/11.png"]'
                            class="gl-360"
                        >
                            <div class="threesixty car">
                                <div class="spinner"><span>0%</span></div>
                                <ol class="threesixty_images"></ol>
                            </div>
                            <img
                                src="{{asset('img/new-xpander/line-360.png')}}"
                                alt="line-360.png"
                                class="gl360-line"
                            />
                            <div class="gl360-direc">
                                <a class="gl360-btn-prev">
                                    <i class="fa fa-caret-left"></i>
                                </a>
                                <span class="gl360-title">360<sup>o</sup></span>
                                <a class="gl360-btn-next">
                                    <i class="fa fa-caret-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--#cl360-->

                    <div id="cl1" role="tabpanel" class="fade tab-pane active">
                        <img
                            src="{{asset('img/new-xpander/White.jpg')}}"
                            alt="New Xpander"
                        />
                    </div>
                    <div id="cl2" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/new-xpander/Gray.jpg')}}"
                            alt="New Xpander"
                        />
                    </div>
                    <div id="cl3" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/new-xpander/Black.jpg')}}"
                            alt="New Xpander"
                        />
                    </div>
                    <div id="cl4" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/new-xpander/Brown.jpg')}}"
                            alt="New Xpander"
                        />
                    </div>
                    <!--.tab-pane-->
                </div>
                <div class="car-color-note text-center">
                    <span>Ghi chú: Hình ảnh minh họa có thể khác với thực tế.</span>
                </div>
                <!--.tab-content-->
                <ul role="tablist" class="nav nav-tabs">
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-xpander/#cl360" class="vehicle-color" aria-controls="cl360" role="tab" data-toggle="tab">
                            <span class="text">360</span>
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-xpander/#cl1" class="vehicle-color" aria-controls="cl1 tab" data-toggle="tab" style="background-color: ;">
                            <span class="color-name">Trắng</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-xpander/#cl2" class="vehicle-color" aria-controls="cl2 tab" data-toggle="tab" style="background-color: #c4c4c4;">
                            <span class="color-name">Bạc</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-xpander/#cl3" class="vehicle-color" aria-controls="cl3 tab" data-toggle="tab" style="background-color: #000000;">
                            <span class="color-name">Đen</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-xpander/#cl4" class="vehicle-color" aria-controls="cl4 tab" data-toggle="tab" style="background-color: #562500;">
                            <span class="color-name">Nâu</span>
                        </a>
                    </li>
                </ul>
                <!--ul.nav-tabs-->
            </div>
            <div class="col-md-6">
                <div class="phienban">
                    <div class="phienban-title">Phiên bản:</div>
                    <div class="phienban-select">
                        <ul>
                            <li class="active"><a href="javascript:;" data-slide="0">MT</a></li>
                            <li class=""><a href="javascript:;" data-slide="1">AT (CBU)</a></li>
                            <li class=""><a href="javascript:;" data-slide="2">AT (CKD)</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row hidden-md hidden-lg">
                    <div class="col-xs-12 phienban-note">Vuốt ngang để chọn và so sánh giữa các phiên bản</div>
                </div>
                <div class="row hidden-xs hidden-sm">
                    <div class="col-xs-12 phienban-note"><a href="javascript:;" data-href="#product_specs" class="view-detail modal-open"></a></div>
                </div>
                <div class="phienban-slide">
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['Xpander_MT']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->
                        <!-- phien ban gia uu dai -->
                        <div class="row phienban-row phien-ban-gia">
                            <div class="col-md-6 phienban-title no-expand">Giá đặc biệt tháng 11</div>
                            <div class="col-md-6 phienban-gia">{{$product['Xpander_MT']}} VNĐ</div>
                        </div>

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification MT" data-tracking-click-cat="New Xpander">Thông số kỹ thuật</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right"></div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">4.475 x 1.750 x 1.730</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai cầu xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">2.775</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai bánh xe trước/sau (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.520/1.510</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,2</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">205</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (Kg)</div>
                                        <div class="col-md-6 phienban-content-right">1.235</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi</div>
                                        <div class="col-md-6 phienban-content-right">7</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right"></div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">1.5L MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung Tích Xylanh (cc)</div>
                                        <div class="col-md-6 phienban-content-right">1.499</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">104/6.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">141/4.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">45</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right"></div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">Số sàn 5 cấp</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">1 cầu - 2WD</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Điện</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">McPherson với lò xo cuộn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Thanh xoắn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">205/55R16</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa/Tang trống</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mức tiêu hao nhiên liệu (L/100km)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Kết hợp</i></div>
                                        <div class="col-md-6 phienban-content-right">6,9</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Trong đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">8,8</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Ngoài đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">5,9</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Xpander"
                                class="view-detail modal-open"
                                tabindex="0"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment MT" data-tracking-click-cat="New Xpander">Trang bị tiêu chuẩn</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right"></div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i> - Đèn chiếu xa </i></div>
                                        <div class="col-md-6 phienban-content-right">Clear Halogen</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i> - Đèn chiếu gần </i></div>
                                        <div class="col-md-6 phienban-content-right">Clear Halogen</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn định vị dạng LED</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn chào mừng và đèn hỗ trợ chiếu sáng khi rời xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn báo phanh thứ ba trên cao</div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Cùng màu với thân xe, chỉnh điện, Tích hợp đèn báo rẽ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa ngoài</div>
                                        <div class="col-md-6 phienban-content-right">Cùng màu thân xe</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lưới tản nhiệt</div>
                                        <div class="col-md-6 phienban-content-right">Đen bóng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính trước</div>
                                        <div class="col-md-6 phienban-content-right">Gạt mưa gián đoạn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính sau và sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm đúc hợp kim</div>
                                        <div class="col-md-6 phienban-content-right">16" - 2 tông màu</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THÂT </b></div>
                                        <div class="col-md-6 phienban-content-right"></div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng và cần số bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Nút điều khiển âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điện thoại rảnh tay trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng điều chỉnh 4 hướng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa nhiệt độ</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay, 2 giàn lạnh</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Nỉ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay 4 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ hai gập 60:40</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ ba gập 50:50</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa trong mạ crôm</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa điều khiển điện</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Móc gắn ghế an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống âm thanh</div>
                                        <div class="col-md-6 phienban-content-right">CD 2 DIN, USB, Bluetooth, AUX</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">4</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Sấy kính trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa gió điều hòa hàng ghế sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gương chiếu hậu trong chống chói chỉnh tay</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí an toàn</div>
                                        <div class="col-md-6 phienban-content-right">Túi khí đôi</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động</div>
                                        <div class="col-md-6 phienban-content-right">Hàng ghế trước</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử EBD</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp BA</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử (ASC)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh và khởi động bằng nút bấm</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảnh báo phanh khẩn cấp (ESS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chức năng chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Camera lùi</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khóa cửa trung tâm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Xpander"
                                class="view-detail modal-open"
                                tabindex="0"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['Xpander_AT_NK']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->
                        <!-- phien ban gia uu dai -->
                        <div class="row phienban-row phien-ban-gia">
                            <div class="col-md-6 phienban-title no-expand">Giá đặc biệt tháng 11</div>
                            <div class="col-md-6 phienban-gia">{{$product['Xpander_AT_NK']}} VNĐ</div>
                        </div>

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification AT (CBU)" data-tracking-click-cat="New Xpander">Thông số kỹ thuật</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">4.475 x 1.750 x 1.730</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai cầu xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">2.775</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai bánh xe trước/sau (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.520/1.510</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,2</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">205</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (Kg)</div>
                                        <div class="col-md-6 phienban-content-right">1.250</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi</div>
                                        <div class="col-md-6 phienban-content-right">7</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">1.5L MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung Tích Xylanh (cc)</div>
                                        <div class="col-md-6 phienban-content-right">1.499</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">104/6.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">141/4.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">45</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">Số tự động 4 cấp</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">1 cầu - 2WD</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Điện</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">McPherson với lò xo cuộn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Thanh xoắn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">205/55R16</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa/Tang trống</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mức tiêu hao nhiên liệu (L/100km)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Kết hợp</i></div>
                                        <div class="col-md-6 phienban-content-right">6,9</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Trong đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">8,5</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Ngoài đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">5,9</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Xpander"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment AT (CBU)" data-tracking-click-cat="New Xpander">Trang bị tiêu chuẩn</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i> - Đèn chiếu xa </i></div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i> - Đèn chiếu gần </i></div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn định vị dạng LED</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn chào mừng và đèn hỗ trợ chiếu sáng khi rời xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Trước</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn báo phanh thứ ba trên cao</div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Mạ crôm, gập điện, chỉnh điện, Tích hợp đèn báo rẽ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa ngoài</div>
                                        <div class="col-md-6 phienban-content-right">Mạ crôm</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lưới tản nhiệt</div>
                                        <div class="col-md-6 phienban-content-right">Crôm xám</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính trước</div>
                                        <div class="col-md-6 phienban-content-right">Gạt mưa gián đoạn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính sau và sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm đúc hợp kim</div>
                                        <div class="col-md-6 phienban-content-right">16" - 2 tông màu</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THÂT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng và cần số bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Nút điều khiển âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điện thoại rảnh tay trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng điều chỉnh 4 hướng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa nhiệt độ</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay, 2 giàn lạnh</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Da, tối màu</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay 6 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ hai gập 60:40</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ ba gập 50:50</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa trong mạ crôm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa điều khiển điện</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Móc gắn ghế an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống âm thanh</div>
                                        <div class="col-md-6 phienban-content-right">Màn hình cảm ứng 7", kết nối Apple CarPlay &amp; Android Auto</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">6</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Sấy kính trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa gió điều hòa hàng ghế sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gương chiếu hậu trong chống chói chỉnh tay</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí an toàn</div>
                                        <div class="col-md-6 phienban-content-right">Túi khí đôi</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động</div>
                                        <div class="col-md-6 phienban-content-right">Hàng ghế trước</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử EBD</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp BA</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử (ASC)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh và khởi động bằng nút bấm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảnh báo phanh khẩn cấp (ESS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chức năng chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Camera lùi</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khóa cửa trung tâm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Xpander"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['Xpander_AT_CKD']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->
                        <!-- phien ban gia uu dai -->
                        <div class="row phienban-row phien-ban-gia">
                            <div class="col-md-6 phienban-title no-expand">Giá đặc biệt tháng 11</div>
                            <div class="col-md-6 phienban-gia">{{$product['Xpander_AT_CKD']}} VNĐ</div>
                        </div>

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification AT (CKD)" data-tracking-click-cat="New Xpander">Thông số kỹ thuật</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">4.475 x 1.750 x 1.730</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai cầu xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">2.775</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai bánh xe trước/sau (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.520/1.510</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,2</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">205</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (Kg)</div>
                                        <div class="col-md-6 phienban-content-right">1.250</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi</div>
                                        <div class="col-md-6 phienban-content-right">7</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">1.5L MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung Tích Xylanh (cc)</div>
                                        <div class="col-md-6 phienban-content-right">1.499</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">104/6.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">141/4.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">45</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">Số tự động 4 cấp</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">1 cầu - 2WD</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Điện</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">McPherson với lò xo cuộn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Thanh xoắn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">205/55R16</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa/Tang trống</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mức tiêu hao nhiên liệu (L/100km)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Kết hợp</i></div>
                                        <div class="col-md-6 phienban-content-right">6,9</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Trong đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">8,5</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Ngoài đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">5,9</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Xpander"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment AT (CKD)" data-tracking-click-cat="New Xpander">Trang bị tiêu chuẩn</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i> - Đèn chiếu xa </i></div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i> - Đèn chiếu gần </i></div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn định vị dạng LED</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn chào mừng và đèn hỗ trợ chiếu sáng khi rời xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Trước</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn báo phanh thứ ba trên cao</div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Mạ crôm, gập điện, chỉnh điện, Tích hợp đèn báo rẽ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa ngoài</div>
                                        <div class="col-md-6 phienban-content-right">Mạ crôm</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lưới tản nhiệt</div>
                                        <div class="col-md-6 phienban-content-right">Crôm xám</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính trước</div>
                                        <div class="col-md-6 phienban-content-right">Gạt mưa gián đoạn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính sau và sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm đúc hợp kim</div>
                                        <div class="col-md-6 phienban-content-right">16" - 2 tông màu</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THÂT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng và cần số bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Nút điều khiển âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điện thoại rảnh tay trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng điều chỉnh 4 hướng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa nhiệt độ</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay, 2 giàn lạnh</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Da, tối màu</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay 6 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ hai gập 60:40</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ ba gập 50:50</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa trong mạ crôm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa điều khiển điện</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Móc gắn ghế an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống âm thanh</div>
                                        <div class="col-md-6 phienban-content-right">Màn hình cảm ứng 7", hỗ trợ kết nối Apple CarPlay &amp; Android Auto</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">6</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Sấy kính trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa gió điều hòa hàng ghế sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gương chiếu hậu trong chống chói chỉnh tay</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí an toàn</div>
                                        <div class="col-md-6 phienban-content-right">Túi khí đôi</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động</div>
                                        <div class="col-md-6 phienban-content-right">Hàng ghế trước</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử EBD</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp BA</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử (ASC)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh và khởi động bằng nút bấm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảnh báo phanh khẩn cấp (ESS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chức năng chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Camera lùi</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khóa cửa trung tâm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Xpander"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                </div>
                <!--.phienban-slide-->
                <div class="phienban-btn">
                    <ul>
                        <li>
                            <a
                                href="javascript:;"
                                data-href="#product_estimate"
                                class="btn btn-icon-l modal-open d2-estimate"
                                data-car-id="132"
                                data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                data-step="1"
                                data-tracking-click="Estimated Cost"
                                data-tracking-click-cat="New Xpander"
                            >
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-cost"></i></span><span class="text">Dự tính chi phí</span>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.mitsubishiquangninh.com/mua-xe/bao-gia-chi-tiet/132/"
                                class="btn btn-icon-l d2-estimate"
                                data-car-id="132"
                                data-tracking-click="Price Quotation By Dealer"
                                data-tracking-click-cat="New Xpander"
                                data-step="2"
                            >
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-price"></i></span><span class="text">Yêu cầu báo giá</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mitsubishiquangninh.com/mua-xe/dang-ky-lai-thu/132/" class="btn btn-icon-l">
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-drive"></i></span><span class="text">Đăng ký lái thử</span>
                            </a>
                        </li>
                        <li style="display: none;">
                            <a href="https://www.mitsubishiquangninh.com/dai-ly/" class="btn btn-icon-l">
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-dealer"></i></span><span class="text">Tìm nhà phân phối</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--.chonphienban-->
            </div>
        </div>
    </div>
</div>
<!--End - Specs-->
