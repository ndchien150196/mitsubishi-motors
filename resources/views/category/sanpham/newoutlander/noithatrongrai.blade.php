<div class="normal-block thietke hidden-border-bottom">
            <h3 class="title-block pr-title-block">
                <span>NỘI THẤT</span>
            </h3>
            <div class="option-slide">
                <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
                <div class="option-track slick-vertical slick-initialized slick-slider">
                    <div aria-live="polite" class="slick-list draggable">
                        <div class="slick-track" role="listbox">
                            <div
                                data-item="0"
                                class="option-item hightline active-show slick-slide slick-current slick-active"
                                style=""
                                tabindex="-1"
                                role="option"
                                aria-describedby="slick-slide80"
                                data-slick-index="0"
                                aria-hidden="false"
                            >
                                <a href="javascript:;" title="NỘI THẤT SANG TRỌNG VÀ KHÔNG GIAN RỘNG RÃI TỐI ƯU" class="opLink" tabindex="0">
                                    <div class="opLink-inner">
                                        <span class="opText"> NỘI THẤT SANG TRỌNG VÀ KHÔNG GIAN RỘNG RÃI TỐI ƯU</span>
                                    </div>
                                </a>
                                <div class="option-detail">
                                    <div class="option-img">
                                        <img
                                            src="{{asset('img/newoutlander/Nội-thất-7-chổ.png')}}"
                                            alt="NỘI THẤT SANG TRỌNG VÀ KHÔNG GIAN RỘNG RÃI TỐI ƯU"
                                        />
                                        <div class="option-listphienban"></div>
                                    </div>
                                    <div class="option-info">
                                        <h3 class="opTitle"><span class="opText"> NỘI THẤT SANG TRỌNG VÀ KHÔNG GIAN RỘNG RÃI TỐI ƯU</span></h3>
                                        <div class="opContent">
                                            <p>
                                                Được thiết kế là một mẫu xe cân bằng hài hòa giữa sự sang trọng hiện đại và không gian 7 chỗ thoải mái, Outlander chắc chắn sẽ đáp ứng tốt mọi nhu cầu của bạn dù là trong việc di chuyển hằng
                                                ngày hay là những chuyến du lịch cùng gia đình.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide81" data-slick-index="1" aria-hidden="false">
                                <a href="javascript:;" title="Ghế da chỉnh điện" class="opLink" tabindex="0">
                                    <div class="opLink-inner">
                                        <span class="opText">1. Ghế da chỉnh điện</span>
                                    </div>
                                </a>
                                <div class="option-detail">
                                    <div class="option-img">
                                        <img
                                            src="{{asset('img/newoutlander/Ghe_da.png')}}"
                                            alt="Ghế da chỉnh điện"
                                        />
                                        <div class="option-listphienban"></div>
                                    </div>
                                    <div class="option-info">
                                        <h3 class="opTitle"><span class="opText">1. Ghế da chỉnh điện</span></h3>
                                        <div class="opContent">
                                            <p>
                                                Outlander được trang bị ghế da màu đen sang trọng cùng các tiện ích khác như ghế lái chỉnh điện 10 hướng, hệ thống sưởi ấm cho hàng ghế trước. Đặc biệt, hàng ghế thứ hai có thể điều chỉnh độ
                                                nghiêng mang lại sự thoải mái cho hàng khách, nhất là trên các hành trình dài.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide82" data-slick-index="2" aria-hidden="false">
                                <a href="javascript:;" title="Khả năng gập ghế linh hoạt" class="opLink" tabindex="0">
                                    <div class="opLink-inner">
                                        <span class="opText">2. Khả năng gập ghế linh hoạt</span>
                                    </div>
                                </a>
                                <div class="option-detail">
                                    <div class="option-img">
                                        <img
                                            src="{{asset('img/newoutlander/Interior3.jpg')}}"
                                            alt="Khả năng gập ghế linh hoạt"
                                        />
                                        <div class="option-listphienban"></div>
                                    </div>
                                    <div class="option-info">
                                        <h3 class="opTitle"><span class="opText">2. Khả năng gập ghế linh hoạt</span></h3>
                                        <div class="opContent">
                                            <p>
                                                Với hàng ghế thứ hai và thứ ba có thể gập bằng sàn, Outlander mang đến khả năng sắp xếp ghế linh hoạt khi cần chở các hành lý có kích thước lớn và nhiều hành khách, tăng&nbsp;thể tích khoang
                                                hành lý lên 1.792 lít.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide83" data-slick-index="3" aria-hidden="false">
                                <a href="javascript:;" title="Chìa khóa thông minh và khởi động bằng nút bấm" class="opLink" tabindex="0">
                                    <div class="opLink-inner">
                                        <span class="opText">3. Chìa khóa thông minh và khởi động bằng nút bấm</span>
                                    </div>
                                </a>
                                <div class="option-detail">
                                    <div class="option-img">
                                        <img
                                            src="{{asset('img/newoutlander/chia-khoa-thong-minh-kos-va-khoi-dong-bang-nut-bam-oss-1.jpg')}}"
                                            alt="Chìa khóa thông minh và khởi động bằng nút bấm"
                                        />
                                        <div class="option-listphienban"></div>
                                    </div>
                                    <div class="option-info">
                                        <h3 class="opTitle"><span class="opText">3. Chìa khóa thông minh và khởi động bằng nút bấm</span></h3>
                                        <div class="opContent">
                                            <p>
                                                Hệ thống chìa khóa thông minh và khởi động nút bấm thao tác khóa/mở khóa cửa và khởi động xe trở nên đơn giản và hiện đại hơn bao giờ hết . Chỉ đơn giản là luôn mang theo chìa khóa bên mình.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide84" data-slick-index="4" aria-hidden="false">
                                <a href="javascript:;" title="Màn hình giải trí 7 inch" class="opLink" tabindex="0">
                                    <div class="opLink-inner">
                                        <span class="opText">4. Màn hình giải trí 7 inch</span>
                                    </div>
                                </a>
                                <div class="option-detail">
                                    <div class="option-img">
                                        <img
                                            src="{{asset('img/newoutlander/man_hinh_giai_tri.png')}}"
                                            alt="Màn hình giải trí 7 inch"
                                        />
                                        <div class="option-listphienban"></div>
                                    </div>
                                    <div class="option-info">
                                        <h3 class="opTitle"><span class="opText">4. Màn hình giải trí 7 inch</span></h3>
                                        <div class="opContent">
                                            <p>Outlander sở hữu màn hình cảm ứng kích thước 7 inch cho phép kết nối với hệ giải trí Android Auto và Apple CarPlay và được tích hợp camera lùi. (Hình ảnh có thể khác với thực tế)</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide85" data-slick-index="5" aria-hidden="false">
                                <a href="javascript:;" title="Cửa sau đóng mở điện" class="opLink" tabindex="0">
                                    <div class="opLink-inner">
                                        <span class="opText">5. Cửa sau đóng mở điện</span>
                                    </div>
                                </a>
                                <div class="option-detail">
                                    <div class="option-img">
                                        <img
                                            src="{{asset('img/newoutlander/cop_dong_mo_dien4.png')}}"
                                            alt="Cửa sau đóng mở điện"
                                        />
                                        <div class="option-listphienban"></div>
                                    </div>
                                    <div class="option-info">
                                        <h3 class="opTitle"><span class="opText">5. Cửa sau đóng mở điện</span></h3>
                                        <div class="opContent">
                                            <p>
                                                Tính năng đóng/mở cửa sau bằng điện giúp việc đóng/mở cửa sau trở nên đơn giản và dễ dàng bằng cách khác nhau: 1. Nhấn nút ở bên trái vô lăng 2. Chạm nhẹ vào nút ở trên cửa 3. Đóng/mở cửa sau
                                                bằng chìa khóa điều khiển từ xa
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-item="6" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide86" data-slick-index="6" aria-hidden="true">
                                <a href="javascript:;" title="Hệ thống điều hòa 2 vùng độc lập thiết kế mới" class="opLink" tabindex="-1">
                                    <div class="opLink-inner">
                                        <span class="opText">6. Hệ thống điều hòa 2 vùng độc lập thiết kế mới</span>
                                    </div>
                                </a>
                                <div class="option-detail">
                                    <div class="option-img">
                                        <img
                                            src="{{asset('img/newoutlander/dieu_hoa_2vung2.png')}}"
                                            alt="Hệ thống điều hòa 2 vùng độc lập thiết kế mới"
                                        />
                                        <div class="option-listphienban"></div>
                                    </div>
                                    <div class="option-info">
                                        <h3 class="opTitle"><span class="opText">6. Hệ thống điều hòa 2 vùng độc lập thiết kế mới</span></h3>
                                        <div class="opContent">
                                            <p>
                                                Điều hòa tự động hai vùng với khả năng lựa chọn nhiệt độ riêng biệt cho từng bên (trái/phải) giúp tất cả các hành khách bên trong xe đều cảm thấy thoải mái. Thiết kế mới hướng về người lái,
                                                với núm xoay giúp thao tác dễ dàng tránh gây mất tập trung khi đang lái xe.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-item="7" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide87" data-slick-index="7" aria-hidden="true">
                                <a href="javascript:;" title="Khoang hành lí lớn" class="opLink" tabindex="-1">
                                    <div class="opLink-inner">
                                        <span class="opText">7. Khoang hành lí lớn</span>
                                    </div>
                                </a>
                                <div class="option-detail">
                                    <div class="option-img">
                                        <img
                                            src="{{asset('img/newoutlander/khoang-hanh-ly-lon1.jpg')}}"
                                            alt="Khoang hành lí lớn"
                                        />
                                        <div class="option-listphienban"></div>
                                    </div>
                                    <div class="option-info">
                                        <h3 class="opTitle"><span class="opText">7. Khoang hành lí lớn</span></h3>
                                        <div class="opContent">
                                            <p>
                                                Outlander có thể tích khoang hành lý lên đến 1.792 lít khi gập cả hàng ghế thứ hai và thứ ba, giúp Outlander dễ dàng chở các hành lý kích thước lớn khi cần. Bên cạnh đó, Outlander được ngăn
                                                đựng hành lý và tấm che khoang hành lý tiện ích, giúp dễ dàng sắp đặt các hành lý một cách gọn gàng và an toàn.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div data-item="8" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide88" data-slick-index="8" aria-hidden="true">
                                <a href="javascript:;" title="Vô lăng thể thao sang trọng" class="opLink" tabindex="-1">
                                    <div class="opLink-inner">
                                        <span class="opText">8. Vô lăng thể thao sang trọng</span>
                                    </div>
                                </a>
                                <div class="option-detail">
                                    <div class="option-img">
                                        <img
                                            src="{{asset('img/newoutlander/vo_lang.png')}}"
                                            alt="Vô lăng thể thao sang trọng"
                                        />
                                        <div class="option-listphienban"></div>
                                    </div>
                                    <div class="option-info">
                                        <h3 class="opTitle"><span class="opText">8. Vô lăng thể thao sang trọng</span></h3>
                                        <div class="opContent">
                                            <p>
                                                Trang bị vô lăng thể thao 3 chấu, được bọc da, ốp nhựa piano đen bóng sang trọng. Bên cạnh đó vô lăng cũng được tích nút điều khiển âm thanh, hệ thống ga tự động và lẫy sang số sau vô lăng
                                                tăng cường tính thể thao khi vận hành.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

                <!-- Detail on Desktop-->
                <div class="option-list-detail">
                    <div data-item="0" class="option-detail show-detail">
                        <div class="option-img">
                            <img
                                src="{{asset('img/newoutlander/Nội-thất-7-chổ.png')}}"
                                alt="NỘI THẤT SANG TRỌNG VÀ KHÔNG GIAN RỘNG RÃI TỐI ƯU"
                            />
                            <div class="option-listphienban"></div>
                        </div>

                        <div class="option-info">
                            <h3 class="opTitle" style="color: #ffffff;">NỘI THẤT SANG TRỌNG VÀ KHÔNG GIAN RỘNG RÃI TỐI ƯU</h3>
                            <div class="opContent" style="color: #c0c0c0;">
                                <p>
                                    Được thiết kế là một mẫu xe cân bằng hài hòa giữa sự sang trọng hiện đại và không gian 7 chỗ thoải mái, Outlander chắc chắn sẽ đáp ứng tốt mọi nhu cầu của bạn dù là trong việc di chuyển hằng ngày hay là
                                    những chuyến du lịch cùng gia đình.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-detail">
                        <div class="option-img">
                            <img
                                src="{{asset('img/newoutlander/Ghe_da.png')}}"
                                alt="Ghế da chỉnh điện"
                            />
                            <div class="option-listphienban"></div>
                        </div>

                        <div class="option-info">
                            <h3 class="opTitle" style="color: #ffffff;">Ghế da chỉnh điện</h3>
                            <div class="opContent" style="color: #c0c0c0;">
                                <p>
                                    Outlander được trang bị ghế da màu đen sang trọng cùng các tiện ích khác như ghế lái chỉnh điện 10 hướng, hệ thống sưởi ấm cho hàng ghế trước. Đặc biệt, hàng ghế thứ hai có thể điều chỉnh độ nghiêng mang
                                    lại sự thoải mái cho hàng khách, nhất là trên các hành trình dài.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-detail">
                        <div class="option-img">
                            <img
                                src="{{asset('img/newoutlander/Interior3.jpg')}}"
                                alt="Khả năng gập ghế linh hoạt"
                            />
                            <div class="option-listphienban"></div>
                        </div>

                        <div class="option-info">
                            <h3 class="opTitle" style="color: #ffffff;">Khả năng gập ghế linh hoạt</h3>
                            <div class="opContent" style="color: #c0c0c0;">
                                <p>
                                    Với hàng ghế thứ hai và thứ ba có thể gập bằng sàn, Outlander mang đến khả năng sắp xếp ghế linh hoạt khi cần chở các hành lý có kích thước lớn và nhiều hành khách, tăng&nbsp;thể tích khoang hành lý lên
                                    1.792 lít.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-detail">
                        <div class="option-img">
                            <img
                                src="{{asset('img/newoutlander/chia-khoa-thong-minh-kos-va-khoi-dong-bang-nut-bam-oss-1.jpg')}}"
                                alt="Chìa khóa thông minh và khởi động bằng nút bấm"
                            />
                            <div class="option-listphienban"></div>
                        </div>

                        <div class="option-info">
                            <h3 class="opTitle" style="color: #ffffff;">Chìa khóa thông minh và khởi động bằng nút bấm</h3>
                            <div class="opContent" style="color: #c0c0c0;">
                                <p>Hệ thống chìa khóa thông minh và khởi động nút bấm thao tác khóa/mở khóa cửa và khởi động xe trở nên đơn giản và hiện đại hơn bao giờ hết . Chỉ đơn giản là luôn mang theo chìa khóa bên mình.</p>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-detail">
                        <div class="option-img">
                            <img
                                src="{{asset('img/newoutlander/man_hinh_giai_tri.png')}}"
                                alt="Màn hình giải trí 7 inch"
                            />
                            <div class="option-listphienban"></div>
                        </div>

                        <div class="option-info">
                            <h3 class="opTitle" style="color: #ffffff;">Màn hình giải trí 7 inch</h3>
                            <div class="opContent" style="color: #c0c0c0;">
                                <p>Outlander sở hữu màn hình cảm ứng kích thước 7 inch cho phép kết nối với hệ giải trí Android Auto và Apple CarPlay và được tích hợp camera lùi. (Hình ảnh có thể khác với thực tế)</p>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-detail">
                        <div class="option-img">
                            <img
                                src="{{asset('img/newoutlander/cop_dong_mo_dien4.png')}}"
                                alt="Cửa sau đóng mở điện"
                            />
                            <div class="option-listphienban"></div>
                        </div>

                        <div class="option-info">
                            <h3 class="opTitle" style="color: #ffffff;">Cửa sau đóng mở điện</h3>
                            <div class="opContent" style="color: #c0c0c0;">
                                <p>
                                    Tính năng đóng/mở cửa sau bằng điện giúp việc đóng/mở cửa sau trở nên đơn giản và dễ dàng bằng cách khác nhau: 1. Nhấn nút ở bên trái vô lăng 2. Chạm nhẹ vào nút ở trên cửa 3. Đóng/mở cửa sau bằng chìa
                                    khóa điều khiển từ xa
                                </p>
                            </div>
                        </div>
                    </div>
                    <div data-item="6" class="option-detail">
                        <div class="option-img">
                            <img
                                src="{{asset('img/newoutlander/dieu_hoa_2vung2.png')}}"
                                alt="Hệ thống điều hòa 2 vùng độc lập thiết kế mới"
                            />
                            <div class="option-listphienban"></div>
                        </div>

                        <div class="option-info">
                            <h3 class="opTitle" style="color: #ffffff;">Hệ thống điều hòa 2 vùng độc lập thiết kế mới</h3>
                            <div class="opContent" style="color: #c0c0c0;">
                                <p>
                                    Điều hòa tự động hai vùng với khả năng lựa chọn nhiệt độ riêng biệt cho từng bên (trái/phải) giúp tất cả các hành khách bên trong xe đều cảm thấy thoải mái. Thiết kế mới hướng về người lái, với núm xoay
                                    giúp thao tác dễ dàng tránh gây mất tập trung khi đang lái xe.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div data-item="7" class="option-detail">
                        <div class="option-img">
                            <img
                                src="{{asset('img/newoutlander/khoang-hanh-ly-lon1.jpg')}}"
                                alt="Khoang hành lí lớn"
                            />
                            <div class="option-listphienban"></div>
                        </div>

                        <div class="option-info">
                            <h3 class="opTitle" style="color: #ffffff;">Khoang hành lí lớn</h3>
                            <div class="opContent" style="color: #c0c0c0;">
                                <p>
                                    Outlander có thể tích khoang hành lý lên đến 1.792 lít khi gập cả hàng ghế thứ hai và thứ ba, giúp Outlander dễ dàng chở các hành lý kích thước lớn khi cần. Bên cạnh đó, Outlander được ngăn đựng hành lý
                                    và tấm che khoang hành lý tiện ích, giúp dễ dàng sắp đặt các hành lý một cách gọn gàng và an toàn.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div data-item="8" class="option-detail">
                        <div class="option-img">
                            <img
                                src="{{asset('img/newoutlander/vo_lang.png')}}"
                                alt="Vô lăng thể thao sang trọng"
                            />
                            <div class="option-listphienban"></div>
                        </div>

                        <div class="option-info">
                            <h3 class="opTitle" style="color: #ffffff;">Vô lăng thể thao sang trọng</h3>
                            <div class="opContent" style="color: #c0c0c0;">
                                <p>
                                    Trang bị vô lăng thể thao 3 chấu, được bọc da, ốp nhựa piano đen bóng sang trọng. Bên cạnh đó vô lăng cũng được tích nút điều khiển âm thanh, hệ thống ga tự động và lẫy sang số sau vô lăng tăng cường tính
                                    thể thao khi vận hành.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>