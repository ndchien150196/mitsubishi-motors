<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>NỘI THẤT</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide90"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="7 CHỖ RỘNG RÃI &amp; TINH TẾ HƠN" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> 7 CHỖ RỘNG RÃI &amp; TINH TẾ HƠN</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/7-cho.jpg')}}"
                                    alt="7 CHỖ RỘNG RÃI &amp; TINH TẾ HƠN"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> 7 CHỖ RỘNG RÃI &amp; TINH TẾ HƠN</span></h3>
                                <div class="opContent">
                                    <p>
                                        Thiết kế nội thất rộng rãi, hiện đại và tinh tế với vật liệu chất lượng không chỉ tạo ấn tượng ngay từ ánh nhìn đầu tiên mà còn mang lại sự thoải mái cho mọi hành khách ngay cả trên những hành
                                        trình dài.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide91" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Ghế da tối màu cao cấp" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Ghế da tối màu cao cấp</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Ghe-da-cao-cap.jpg')}}"
                                    alt="Ghế da tối màu cao cấp"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li><span>AT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Ghế da tối màu cao cấp</span></h3>
                                <div class="opContent">
                                    <p>Nội thất màu tối cùng ghế bọc da cho cảm giác sang trọng và tinh tế hơn.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide92" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Ghế sau gập phẳng sàn" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Ghế sau gập phẳng sàn</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Ghe-gap-phang-san.jpg')}}"
                                    alt="Ghế sau gập phẳng sàn"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Ghế sau gập phẳng sàn</span></h3>
                                <div class="opContent">
                                    <p>2 hàng ghế sau của Xpander có khả năng gập phẳng xuống sàn, mang lại không gian chở hàng rộng rãi và tiện ích.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide93" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống điều khiển hành trình Cruise Control" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Hệ thống điều khiển hành trình Cruise Control</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Cruise-Control1.jpg')}}"
                                    alt="Hệ thống điều khiển hành trình Cruise Control"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Hệ thống điều khiển hành trình Cruise Control</span></h3>
                                <div class="opContent">
                                    <p>
                                        Cruise Control giúp duy trì tốc độ ổn định mà không phải đặt chân lên bàn đạp ga, giúp việc lái xe trở nên thoải mái và thư giãn hơn, đặc biệt trên hành trình dài.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide94" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Màn hình cảm ứng 7-inch" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Màn hình cảm ứng 7-inch</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Man-hinh-AC-AA.jpg')}}"
                                    alt="Màn hình cảm ứng 7-inch"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Màn hình cảm ứng 7-inch</span></h3>
                                <div class="opContent">
                                    <p>
                                        Màn hình cảm ứng 7-inch có khả năng kết nối Apple CarPlay, Android Auto và Weblink (Android). Bên cạnh đó, hệ thống giải trí có thể kết nối Bluetooth, USB.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide95" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="7 chỗ với sắp xếp ghế linh hoạt" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. 7 chỗ với sắp xếp ghế linh hoạt</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-xpander/Sap-xep-linh-hoat.jpg')}}"
                                    alt="7 chỗ với sắp xếp ghế linh hoạt"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. 7 chỗ với sắp xếp ghế linh hoạt</span></h3>
                                <div class="opContent">
                                    <p>Với khả năng sắp xếp ghế linh hoạt, Xpander giúp bạn chủ động sắp xếp hành lý và chỗ ngồi cho những cuộc hành trình.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/7-cho.jpg')}}"
                        alt="7 CHỖ RỘNG RÃI &amp; TINH TẾ HƠN"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">7 CHỖ RỘNG RÃI &amp; TINH TẾ HƠN</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Thiết kế nội thất rộng rãi, hiện đại và tinh tế với vật liệu chất lượng không chỉ tạo ấn tượng ngay từ ánh nhìn đầu tiên mà còn mang lại sự thoải mái cho mọi hành khách ngay cả trên những hành trình dài.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Ghe-da-cao-cap.jpg')}}"
                        alt="Ghế da tối màu cao cấp"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li><span>AT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Ghế da tối màu cao cấp</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Nội thất màu tối cùng ghế bọc da cho cảm giác sang trọng và tinh tế hơn.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Ghe-gap-phang-san.jpg')}}"
                        alt="Ghế sau gập phẳng sàn"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Ghế sau gập phẳng sàn</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>2 hàng ghế sau của Xpander có khả năng gập phẳng xuống sàn, mang lại không gian chở hàng rộng rãi và tiện ích.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Cruise-Control1.jpg')}}"
                        alt="Hệ thống điều khiển hành trình Cruise Control"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống điều khiển hành trình Cruise Control</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Cruise Control giúp duy trì tốc độ ổn định mà không phải đặt chân lên bàn đạp ga, giúp việc lái xe trở nên thoải mái và thư giãn hơn, đặc biệt trên hành trình dài.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Man-hinh-AC-AA.jpg')}}"
                        alt="Màn hình cảm ứng 7-inch"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Màn hình cảm ứng 7-inch</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Màn hình cảm ứng 7-inch có khả năng kết nối Apple CarPlay, Android Auto và Weblink (Android). Bên cạnh đó, hệ thống giải trí có thể kết nối Bluetooth, USB.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-xpander/Sap-xep-linh-hoat.jpg')}}"
                        alt="7 chỗ với sắp xếp ghế linh hoạt"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">7 chỗ với sắp xếp ghế linh hoạt</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Với khả năng sắp xếp ghế linh hoạt, Xpander giúp bạn chủ động sắp xếp hành lý và chỗ ngồi cho những cuộc hành trình.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>