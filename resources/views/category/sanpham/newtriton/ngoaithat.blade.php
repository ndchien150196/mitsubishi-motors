<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>NGOẠI THẤT</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide80"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="UY MÃNH ĐẦY ẤN TƯỢNG" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> UY MÃNH ĐẦY ẤN TƯỢNG</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-Exterior.jpg')}}"
                                    alt="UY MÃNH ĐẦY ẤN TƯỢNG"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> UY MÃNH ĐẦY ẤN TƯỢNG</span></h3>
                                <div class="opContent">
                                    <p>Ngoại thất uy mãnh với ngôn ngữ thiết kế Dynamic Shield đặc trưng của Mitsubishi Motors, mang đến ấn tượng mạnh mẽ cùng tính năng bảo vệ an toàn.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide81" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Cụm đèn pha và định vị đạng LED" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Cụm đèn pha và định vị đạng LED</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-Bilead1.jpg')}}"
                                    alt="Cụm đèn pha và định vị đạng LED"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Cụm đèn pha và định vị đạng LED</span></h3>
                                <div class="opContent">
                                    <p>Đèn pha Bi-LED duy nhất trong phân khúc cho khả năng chiếu sáng vượt trội, với thiết kế sắc sảo, kết hợp tinh tế và đầy phong cách của thiết kế Dynamic Shield uy lực.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide82" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Cụm đèn hậu LED" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Cụm đèn hậu LED</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-Rearlight.jpg')}}"
                                    alt="Cụm đèn hậu LED"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Cụm đèn hậu LED</span></h3>
                                <div class="opContent">
                                    <p>Thiết kế đèn LED theo chiều dọc độc đáo và hiện đại.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide83" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Mâm hợp kim 18” 2 tông màu" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Mâm hợp kim 18” 2 tông màu</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-wheel.jpg')}}"
                                    alt="Mâm hợp kim 18” 2 tông màu"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Mâm hợp kim 18” 2 tông màu</span></h3>
                                <div class="opContent">
                                    <p>Mâm xe 18″ với 6 chấu kép cùng thiết kế thể thao giúp tăng nét trẻ trung mạnh mẽ của xe.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide84" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Bệ bước hông xe" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Bệ bước hông xe</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-Sidestep.jpg')}}"
                                    alt="Bệ bước hông xe"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Bệ bước hông xe</span></h3>
                                <div class="opContent">
                                    <p>Sự tinh tế “omotenashi” còn được chăm chút bằng việc Triton được bố trí bệ bước lên xe rộng hơn cùng các tay nắm hỗ trợ ra vào xe ở tất cả các vị trí ghế ngồi</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide85" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Thùng xe rộng rãi" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Thùng xe rộng rãi</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-thung.jpg')}}"
                                    alt="Thùng xe rộng rãi"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Thùng xe rộng rãi</span></h3>
                                <div class="opContent">
                                    <p>Thùng xe với kích thước lớn (1520 x 1470 x 475mm) giúp tăng khả năng chứa đồ và chở hàng.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-Exterior.jpg')}}"
                        alt="UY MÃNH ĐẦY ẤN TƯỢNG"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">UY MÃNH ĐẦY ẤN TƯỢNG</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Ngoại thất uy mãnh với ngôn ngữ thiết kế Dynamic Shield đặc trưng của Mitsubishi Motors, mang đến ấn tượng mạnh mẽ cùng tính năng bảo vệ an toàn.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-Bilead1.jpg')}}"
                        alt="Cụm đèn pha và định vị đạng LED"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Cụm đèn pha và định vị đạng LED</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Đèn pha Bi-LED duy nhất trong phân khúc cho khả năng chiếu sáng vượt trội, với thiết kế sắc sảo, kết hợp tinh tế và đầy phong cách của thiết kế Dynamic Shield uy lực.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-Rearlight.jpg')}}"
                        alt="Cụm đèn hậu LED"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Cụm đèn hậu LED</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Thiết kế đèn LED theo chiều dọc độc đáo và hiện đại.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-wheel.jpg')}}"
                        alt="Mâm hợp kim 18” 2 tông màu"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Mâm hợp kim 18” 2 tông màu</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Mâm xe 18″ với 6 chấu kép cùng thiết kế thể thao giúp tăng nét trẻ trung mạnh mẽ của xe.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-Sidestep.jpg')}}"
                        alt="Bệ bước hông xe"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Bệ bước hông xe</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Sự tinh tế “omotenashi” còn được chăm chút bằng việc Triton được bố trí bệ bước lên xe rộng hơn cùng các tay nắm hỗ trợ ra vào xe ở tất cả các vị trí ghế ngồi</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-thung.jpg')}}"
                        alt="Thùng xe rộng rãi"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Thùng xe rộng rãi</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Thùng xe với kích thước lớn (1520 x 1470 x 475mm) giúp tăng khả năng chứa đồ và chở hàng.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>