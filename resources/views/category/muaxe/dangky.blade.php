@extends("master")
@section("content") 
<div class="test-drive-page">
    <div class="normal-block">
        <div class="grid-fluid">
            <h1 class="title-block">
                <span>Đăng ký lái thử</span>
                <a style="display: none;" data-tracking-click="Back" data-tracking-click-cat="TestDrive" href="javascript:;" title="Quay lại" id="close-btn" class="close-btn"></a>
            </h1>
            <div class="grid-content">
                <div id="test_drive_block" class="mitsu-modal drive-block">
                    <!-- .st-1-->
                    <div class="step st-1" style="">
                        <div class="text-center">
                            <ul class="st-desc">
                                <li class="active"><strong>Bước 1:</strong> Chọn loại xe</li>
                                <li class="arrow">
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li><strong>Bước 2:</strong> Nhập thông tin</li>
                            </ul>
                        </div>

                        <div class="text-center">
                            <ul id="drive_product_select" class="product-select" get-url-id-product="">
                                <li data-select-value="1" data-car-id="116" data-url="/xpander-cross/" data-select-src="{{asset('img/menu/Orange.jpg')}}" data-select-name="Xpander Cross">
                                    <picture>
                                        <img src="{{asset('img/menu/Cross.png')}}" alt="Xpander Cross" />
                                    </picture>
                                    <p>Xpander Cross</p>
                                </li>
                                <li data-select-value="1" data-car-id="139" data-url="/new-pajero-sport/" data-select-src="{{asset('img/menu/Pajero-sport.png')}}" data-select-name="New Pajero Sport">
                                    <picture>
                                        <img src="{{asset('img/menu/Pajero-sport.png')}}" alt="New Pajero Sport" />
                                    </picture>
                                    <p>New Pajero Sport</p>
                                </li>
                                <li data-select-value="1" data-car-id="132" data-url="/new-xpander/" data-select-src="{{asset('img/menu/Xpander-Thumbnail.png')}}" data-select-name="New Xpander">
                                    <picture>
                                        <img src="{{asset('img/menu/Xpander-Thumbnail.png')}}" alt="New Xpander" />
                                    </picture>
                                    <p>New Xpander</p>
                                </li>
                                <li data-select-value="1" data-car-id="122" data-url="/new-attrage/" data-select-src="{{asset('img/menu/PNG-ELL.png')}}" data-select-name="New Attrage">
                                    <picture>
                                        <img src="{{asset('img/menu/ELL-MY20_-final.png')}}" alt="New Attrage" />
                                    </picture>
                                    <p>New Attrage</p>
                                </li>
                                <li
                                    data-select-value="1" data-car-id="121" data-url="/new-outlander/" data-select-src="{{asset('img/menu/19OL_Gene_LHD_02white_FR_png.png')}}" data-select-name="New Outlander"
                                >
                                    <picture>
                                        <img src="{{asset('img/menu/RE-MY20.png')}}" alt="New Outlander" />
                                    </picture>
                                    <p>New Outlander</p>
                                </li>
                                <li data-select-value="1" data-car-id="22" data-url="/newtriton/" data-select-src="{{asset('img/menu/Triton1.png')}}" data-select-name="New Triton">
                                    <picture>
                                        <img src="{{asset('img/menu/Triton.png')}}" alt="New Triton" />
                                    </picture>
                                    <p>New Triton</p>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <!-- .st-2-->
                    <div class="step st-2">
                        <div class="text-center">
                            <ul class="st-desc">
                                <li><strong>Bước 1:</strong> Chọn loại xe</li>
                                <li class="arrow">
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active"><strong>Bước 2:</strong> Nhập thông tin</li>
                            </ul>
                        </div>
                        <div class="text-center">
                            <div id="drive_product_selected" class="product-selected">
                                <img class="selected-img" />
                                <p class="selected-name"></p>
                                <div class="clear"></div>
                                <a class="selected-btn" href="javascript:;"> <i class="fa fa-arrow-left"></i> Chọn xe khác </a>
                            </div>

                            <div id="drive_register_frm" class="reg-frm">
                                <form id="d2-register-test-drive" class="mitsu-frm" data-url="/mail-dang-ky">
                                    <div class="frm-item textfield-float-label" d2-val-target-parent="Name">
                                        <input id="name" name="Name" type="text" d2-val-required="" class="is-empty" />
                                        <label for="name">Họ tên</label>
                                        <p class="error-msg" d2-val-target="Name" d2-val-required-msg="">Vui lòng nhập tên</p>
                                        <p class="error-msg" d2-val-target="Name" d2-val-server-msg=""></p>
                                    </div>
                                    <div class="frm-item textfield-float-label" d2-val-target-parent="Email">
                                        <input id="email" name="Email" type="text" d2-val-required="" d2-val-email="" class="is-empty" />
                                        <label for="email">Email</label>
                                        <p class="error-msg" d2-val-target="Email" d2-val-required-msg="">Vui lòng nhập email</p>
                                        <p class="error-msg" d2-val-target="Email" d2-val-email-msg="">Email chưa hợp lệ</p>
                                        <p class="error-msg" d2-val-target="Email" d2-val-server-msg=""></p>
                                    </div>
                                    <div class="frm-item textfield-float-label" d2-val-target-parent="Phone">
                                        <input id="phone" name="Phone" type="text" d2-val-required="" d2-val-phone="" class="is-empty" />
                                        <label for="phone">Số điện thoại</label>
                                        <p class="error-msg" d2-val-target="Phone" d2-val-required-msg="">Vui lòng nhập SĐT</p>
                                        <p class="error-msg" d2-val-target="Phone" d2-val-phone-msg="">SĐT chưa hợp lệ</p>
                                        <p class="error-msg" d2-val-target="Phone" d2-val-server-msg=""></p>
                                    </div>
                                    @csrf
                                    <input type="hidden" name="loaixe" id="getnameproduct"/>
                                    <div class="frm-item">
                                        <a
                                            id="d2-submit"
                                            href="javascript:;"
                                            class="frm-btn submit-form-dang-ky"
                                        >
                                            Đăng ký ngay
                                        </a>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- .st-3-->
                    <div class="step st-3 st-success">
                        <div class="success-content">
                            <div class="success-desc">
                                Thông tin của quý khách<br />
                                đã được cập nhật vào hệ thống.<br />
                                Bảng báo giá sẽ gửi đến quý khách<br />
                                trong thời gian sớm nhất.
                            </div>
                            <div class="desc-btn text-center">
                                <a href="javascript:history.go(-1)" class="btn btn-red" data-tracking-click="Back" data-tracking-click-cat="TestDrive">Về trang trước</a>
                                <a href="/" class="btn btn-red" data-tracking-click="Home" data-tracking-click-cat="TestDrive">Về trang chủ</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
