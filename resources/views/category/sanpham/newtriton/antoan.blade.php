<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>AN TOÀN CHỦ ĐỘNG TIÊN TIẾN</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide110"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="CÔNG NGHỆ AN TOÀN CHỦ ĐỘNG TIÊN TIẾN" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> CÔNG NGHỆ AN TOÀN CHỦ ĐỘNG TIÊN TIẾN</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-safety.jpg')}}"
                                    alt="CÔNG NGHỆ AN TOÀN CHỦ ĐỘNG TIÊN TIẾN"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> CÔNG NGHỆ AN TOÀN CHỦ ĐỘNG TIÊN TIẾN</span></h3>
                                <div class="opContent">
                                    <p>Mitsubishi Triton 2020 được trang bị hàng loạt các công nghệ an toàn tiên tiến như FCM, BWS, ABS, EBD, ASTC, HDC…</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide111" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống phanh ABS-EBD" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Hệ thống phanh ABS-EBD</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-ebd.jpg')}}"
                                    alt="Hệ thống phanh ABS-EBD"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Hệ thống phanh ABS-EBD</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống phanh ABS duy trì khả năng điều khiển xe khi phanh gấp. Hệ thống phân bổ lực phanh EBD giúp rút ngắn quãng đường phanh tối đa.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide112" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống hỗ trợ phanh (BA)" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Hệ thống hỗ trợ phanh (BA)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-BA.jpg')}}"
                                    alt="Hệ thống hỗ trợ phanh (BA)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Hệ thống hỗ trợ phanh (BA)</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống hỗ trợ phanh khẩn cấp giúp tăng cường lực phanh cho người lái trung trường hợp đạp phanh khẩn cấp, đảm bảo an toàn cho người và xe</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide113" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống cảnh báo điểm mù BSW" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Hệ thống cảnh báo điểm mù BSW</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/Untitled-1.png')}}"
                                    alt="Hệ thống cảnh báo điểm mù BSW"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Hệ thống cảnh báo điểm mù BSW</span></h3>
                                <div class="opContent">
                                    <p>
                                        Hệ thống sẽ sử dụng các cảm biến để phát hiện các vật thê nằm trong vùng điểm mù của xe ở bên trái và bên phải. Khi phát hiện có xe nằm trong điểm mù ở phía nào, cảnh báo sẽ xuất hiện trên
                                        gương chiếu hậu ở phía đó.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide114" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống giảm thiểu va chạm phía trước – FCM" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Hệ thống giảm thiểu va chạm phía trước – FCM</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/Untitled-4.png')}}"
                                    alt="Hệ thống giảm thiểu va chạm phía trước – FCM"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Hệ thống giảm thiểu va chạm phía trước – FCM</span></h3>
                                <div class="opContent">
                                    <p>
                                        Bằng cách sử dụng camera và cảm biến radar, hệ thống FCM có thể phát hiện các vật cản phía trước xe để giúp hạn chế va chạm phía trước hoặc giảm thiểu thiệt hại trong trường hợp có có va chạm
                                        không thể tránh khỏi.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide115" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống cảnh báo phương tiện cắt ngang phía sau – RCTA" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Hệ thống cảnh báo phương tiện cắt ngang phía sau – RCTA</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/Untitled-5.png')}}"
                                    alt="Hệ thống cảnh báo phương tiện cắt ngang phía sau – RCTA"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Hệ thống cảnh báo phương tiện cắt ngang phía sau – RCTA</span></h3>
                                <div class="opContent">
                                    <p>
                                        Nếu hệ thống phát hiện có phương tiện đến gần khi lùi xe, thông tin cảnh báo sẽ xuất hiện trên màn hình hiển thị đa thông tin, âm thanh cảnh báo sẽ phát ra và đèn hiển thị trên gương chiếu hậu
                                        sẽ nhấp nháy.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="6" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide116" data-slick-index="6" aria-hidden="true">
                        <a href="javascript:;" title="Hệ thống chống tăng tốc ngoài ý muốn – UMS" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">6. Hệ thống chống tăng tốc ngoài ý muốn – UMS</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/Untitled-6.png')}}"
                                    alt="Hệ thống chống tăng tốc ngoài ý muốn – UMS"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">6. Hệ thống chống tăng tốc ngoài ý muốn – UMS</span></h3>
                                <div class="opContent">
                                    <p>
                                        Khi tài xế chuyển sang “D” hoặc “R” và hệ thống phát hiện có vật cản phía trước/sau, hệ thống sẽ hoạt động bằng cách phát ra âm thanh cảnh báo đồng thời giảm công suất động cơ để giảm thiểu
                                        nguy cơ va chạm
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="7" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide117" data-slick-index="7" aria-hidden="true">
                        <a href="javascript:;" title="Active Stability &amp; Traction Control (ASTC)" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">7. Active Stability &amp; Traction Control (ASTC)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-ASTC.jpg')}}"
                                    alt="Active Stability &amp; Traction Control (ASTC)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">7. Active Stability &amp; Traction Control (ASTC)</span></h3>
                                <div class="opContent">
                                    <p>
                                        Hệ thống cân bằng điện tử và kiểm soát lực kéo (ASTC) sử dụng các cảm biến để phân tích chuyển động và độ trượt của xe. Bằng cách kiểm soát công suất động cơ và lực phanh lên từng bánh xe
                                        riêng biệt, hệ thống ASTC giúp duy trì sự ổn định của xe ngay cả trong điểu kiện trơn trượt.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="8" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide118" data-slick-index="8" aria-hidden="true">
                        <a href="javascript:;" title="Hệ thống khởi hành ngang dốc (HSA)" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">8. Hệ thống khởi hành ngang dốc (HSA)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-HSA.jpg')}}"
                                    alt="Hệ thống khởi hành ngang dốc (HSA)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">8. Hệ thống khởi hành ngang dốc (HSA)</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống tự động hỗ trợ phanh giúp xe không bị trượt về phía sau khi người lái chuyển từ chân phanh sang chân ga trong trường hợp dừng và khởi hành ngang dốc</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="9" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide119" data-slick-index="9" aria-hidden="true">
                        <a href="javascript:;" title="Hệ thống hỗ trợ xuống dốc" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">9. Hệ thống hỗ trợ xuống dốc</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-HDC.jpg')}}"
                                    alt="Hệ thống hỗ trợ xuống dốc"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li>
                                            <span>4X4<small class="small-text">AT MIVEC</small></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">9. Hệ thống hỗ trợ xuống dốc</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống tự động phanh giúp duy trình tốc độ ổn định và xuống dốc an toàn</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-safety.jpg')}}"
                        alt="CÔNG NGHỆ AN TOÀN CHỦ ĐỘNG TIÊN TIẾN"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">CÔNG NGHỆ AN TOÀN CHỦ ĐỘNG TIÊN TIẾN</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Mitsubishi Triton 2020 được trang bị hàng loạt các công nghệ an toàn tiên tiến như FCM, BWS, ABS, EBD, ASTC, HDC…</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-ebd.jpg')}}"
                        alt="Hệ thống phanh ABS-EBD"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống phanh ABS-EBD</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Hệ thống phanh ABS duy trì khả năng điều khiển xe khi phanh gấp. Hệ thống phân bổ lực phanh EBD giúp rút ngắn quãng đường phanh tối đa.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-BA.jpg')}}"
                        alt="Hệ thống hỗ trợ phanh (BA)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống hỗ trợ phanh (BA)</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Hệ thống hỗ trợ phanh khẩn cấp giúp tăng cường lực phanh cho người lái trung trường hợp đạp phanh khẩn cấp, đảm bảo an toàn cho người và xe</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/Untitled-1.png')}}"
                        alt="Hệ thống cảnh báo điểm mù BSW"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống cảnh báo điểm mù BSW</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Hệ thống sẽ sử dụng các cảm biến để phát hiện các vật thê nằm trong vùng điểm mù của xe ở bên trái và bên phải. Khi phát hiện có xe nằm trong điểm mù ở phía nào, cảnh báo sẽ xuất hiện trên gương chiếu hậu
                            ở phía đó.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/Untitled-4.png')}}"
                        alt="Hệ thống giảm thiểu va chạm phía trước – FCM"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống giảm thiểu va chạm phía trước – FCM</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Bằng cách sử dụng camera và cảm biến radar, hệ thống FCM có thể phát hiện các vật cản phía trước xe để giúp hạn chế va chạm phía trước hoặc giảm thiểu thiệt hại trong trường hợp có có va chạm không thể
                            tránh khỏi.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/Untitled-5.png')}}"
                        alt="Hệ thống cảnh báo phương tiện cắt ngang phía sau – RCTA"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống cảnh báo phương tiện cắt ngang phía sau – RCTA</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Nếu hệ thống phát hiện có phương tiện đến gần khi lùi xe, thông tin cảnh báo sẽ xuất hiện trên màn hình hiển thị đa thông tin, âm thanh cảnh báo sẽ phát ra và đèn hiển thị trên gương chiếu hậu sẽ nhấp
                            nháy.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="6" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/Untitled-6.png')}}"
                        alt="Hệ thống chống tăng tốc ngoài ý muốn – UMS"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống chống tăng tốc ngoài ý muốn – UMS</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Khi tài xế chuyển sang “D” hoặc “R” và hệ thống phát hiện có vật cản phía trước/sau, hệ thống sẽ hoạt động bằng cách phát ra âm thanh cảnh báo đồng thời giảm công suất động cơ để giảm thiểu nguy cơ va
                            chạm
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="7" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-ASTC.jpg')}}"
                        alt="Active Stability &amp; Traction Control (ASTC)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Active Stability &amp; Traction Control (ASTC)</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Hệ thống cân bằng điện tử và kiểm soát lực kéo (ASTC) sử dụng các cảm biến để phân tích chuyển động và độ trượt của xe. Bằng cách kiểm soát công suất động cơ và lực phanh lên từng bánh xe riêng biệt, hệ
                            thống ASTC giúp duy trì sự ổn định của xe ngay cả trong điểu kiện trơn trượt.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="8" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-HSA.jpg')}}"
                        alt="Hệ thống khởi hành ngang dốc (HSA)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống khởi hành ngang dốc (HSA)</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Hệ thống tự động hỗ trợ phanh giúp xe không bị trượt về phía sau khi người lái chuyển từ chân phanh sang chân ga trong trường hợp dừng và khởi hành ngang dốc</p>
                    </div>
                </div>
            </div>
            <div data-item="9" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-HDC.jpg')}}"
                        alt="Hệ thống hỗ trợ xuống dốc"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li>
                                <span>4X4<small class="small-text">AT MIVEC</small></span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống hỗ trợ xuống dốc</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Hệ thống tự động phanh giúp duy trình tốc độ ổn định và xuống dốc an toàn</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>