@extends("master")
@section("content") 
<div class="second-block page-sparepart">
    <div class="grid-fluid">
        <h1 class="title-block">
            <span>Phụ tùng chính hiệu</span>
        </h1>
        <div class="grid-content">
            <div class="mobile-block spare-content">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="spare-des">Bạn có thể tìm các phụ tùng chính hiệu Mitsubishi ở đâu tại Việt Nam? Không nơi nào khác ngoài hệ thống cung cấp phụ tùng của Mitsubishi Motors Việt Nam.</div>
                        <div class="btn-contact hidden-xs">
                            <a href="{{url('lien-he')}}" title="Hãy liên hệ ngay với chúng tôi" class="btn btn-red">Hãy liên hệ ngay với chúng tôi</a>
                        </div>
                        <div class="row">
                            <div class="col-xs-6 col-sm-4 col-md-3 item">
                                <a href="/phu-tung-chinh-hieu/dau-nhon-va-hoa-chat.html" title="DẦU NHỜN VÀ HÓA CHẤT">
                                    <img alt="DẦU NHỜN VÀ HÓA CHẤT" src="{{asset('img/dichvu/dau-nhon-va-hoa-chat.jpg')}}"/>
                                    <div class="item-name">DẦU NHỜN VÀ HÓA CHẤT</div>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 item">
                                <a href="/phu-tung-chinh-hieu/day-curoa.html" title="DÂY CUROA">
                                    <img alt="DÂY CUROA" src="{{asset('img/dichvu/day-curoa.jpg')}}"/>
                                    <div class="item-name">DÂY CUROA</div>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 item">
                                <a href="/phu-tung-chinh-hieu/loc-gio.html" title="LỌC GIÓ">
                                    <img alt="LỌC GIÓ" src="{{asset('img/dichvu/loc-gio.jpg')}}"/>
                                    <div class="item-name">LỌC GIÓ</div>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 item">
                                <a href="/phu-tung-chinh-hieu/loc-nhien-lieu.html" title="LỌC NHIÊN LIỆU">
                                    <img alt="LỌC NHIÊN LIỆU" src="{{asset('img/dichvu/loc-nhien-lieu.jpg')}}"/>
                                    <div class="item-name">LỌC NHIÊN LIỆU</div>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 item">
                                <a href="/phu-tung-chinh-hieu/loc-nhot.html" title="LỌC NHỚT">
                                    <img alt="LỌC NHỚT" src="{{asset('img/dichvu/loc-nhot.jpg')}}"/>
                                    <div class="item-name">LỌC NHỚT</div>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 item">
                                <a href="/phu-tung-chinh-hieu/ma-phanh.html" title="MÁ PHANH">
                                    <img alt="MÁ PHANH" src="{{asset('img/dichvu/ma-phanh.jpg')}}"/>
                                    <div class="item-name">MÁ PHANH</div>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 item">
                                <a href="/phu-tung-chinh-hieu/phu-kien.html" title="PHỤ KIỆN">
                                    <img alt="PHỤ KIỆN" src="{{asset('img/dichvu/phu-kien.jpg')}}"/>
                                    <div class="item-name">PHỤ KIỆN</div>
                                </a>
                            </div>
                            <div class="col-xs-6 col-sm-4 col-md-3 item">
                                <a href="/phu-tung-chinh-hieu/value-selection.html" title="VALUE SELECTION">
                                    <img alt="VALUE SELECTION" src="{{asset('img/dichvu/value-selection.jpg')}}"/>
                                    <div class="item-name">VALUE SELECTION</div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
