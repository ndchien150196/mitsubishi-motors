<!--Specs-->
<div class="normal-block thongsokythuat">
    <div class="grid-inner">
        <h3 class="title-block pr-title-block">
            <span>Thông số kỹ thuật</span>
        </h3>
        <div class="row">
            <div class="col-md-6">
                <div class="tab-content">
                    <div id="cl360" role="tabpanel" class="tab-pane fade">
                        <div
                            id="gl_360"
                            data-imgarray='[                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/360Su1jpg.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/360Su2.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/360Su3.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/360Su4.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/360Su5.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/360Su6.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2019/02/360Su8.jpg"]'
                            class="gl-360"
                        >
                            <div class="threesixty car">
                                <div class="spinner"><span>0%</span></div>
                                <ol class="threesixty_images"></ol>
                            </div>
                            <img
                                src="{{asset('img/newtriton/line-360.png')}}"
                                alt="line-360.png"
                                class="gl360-line"
                            />
                            <div class="gl360-direc">
                                <a class="gl360-btn-prev">
                                    <i class="fa fa-caret-left"></i>
                                </a>
                                <span class="gl360-title">360<sup>o</sup></span>
                                <a class="gl360-btn-next">
                                    <i class="fa fa-caret-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--#cl360-->

                    <div id="cl1" role="tabpanel" class="fade tab-pane active">
                        <img
                            src="{{asset('img/newtriton/SU-trang.jpg')}}"
                            alt="New Triton"
                        />
                    </div>
                    <div id="cl2" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/newtriton/SU-xam1.jpg')}}"
                            alt="New Triton"
                        />
                    </div>
                    <div id="cl3" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/newtriton/SU-cam.jpg')}}"
                            alt="New Triton"
                        />
                    </div>
                    <div id="cl4" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/newtriton/SU-bac.jpg')}}"
                            alt="New Triton"
                        />
                    </div>
                    <div id="cl5" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/newtriton/SU-nau.jpg')}}"
                            alt="New Triton"
                        />
                    </div>
                    <div id="cl6" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/newtriton/SU-den.jpg')}}"
                            alt="New Triton"
                        />
                    </div>
                    <!--.tab-pane-->
                </div>
                <div class="car-color-note text-center">
                    <span>Ghi chú: Hình ảnh minh họa có thể khác với thực tế.</span>
                </div>
                <!--.tab-content-->
                <ul role="tablist" class="nav nav-tabs">
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/newtriton/#cl360" class="vehicle-color" aria-controls="cl360" role="tab" data-toggle="tab">
                            <span class="text">360</span>
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/newtriton/#cl1" class="vehicle-color" aria-controls="cl1 tab" data-toggle="tab" style="background-color: #ffffff;">
                            <span class="color-name">Trắng</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/newtriton/#cl2" class="vehicle-color" aria-controls="cl2 tab" data-toggle="tab" style="background-color: #494949;">
                            <span class="color-name">Xám</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/newtriton/#cl3" class="vehicle-color" aria-controls="cl3 tab" data-toggle="tab" style="background-color: #d13e00;">
                            <span class="color-name">Cam</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/newtriton/#cl4" class="vehicle-color" aria-controls="cl4 tab" data-toggle="tab" style="background-color: #eaeaea;">
                            <span class="color-name">Bạc</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/newtriton/#cl5" class="vehicle-color" aria-controls="cl5 tab" data-toggle="tab" style="background-color: #8c4405;">
                            <span class="color-name">Nâu</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/newtriton/#cl6" class="vehicle-color" aria-controls="cl6 tab" data-toggle="tab" style="background-color: #212121;">
                            <span class="color-name">Đen</span>
                        </a>
                    </li>
                </ul>
                <!--ul.nav-tabs-->
            </div>
            <div class="col-md-6">
                <div class="phienban">
                    <div class="phienban-title">Phiên bản:</div>
                    <div class="phienban-select">
                        <ul>
                            <li class="active">
                                <a href="javascript:;" data-slide="0" data-tracking-click="Version &lt;small&gt;&lt;small&gt;4X2 MT (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                    <small><small>4X2 MT (2020)</small></small>
                                </a>
                            </li>
                            <li class="">
                                <a href="javascript:;" data-slide="1" data-tracking-click="Version &lt;small&gt;&lt;small&gt;4X2 AT MIVEC (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                    <small><small>4X2 AT MIVEC (2020)</small></small>
                                </a>
                            </li>
                            <li class="">
                                <a href="javascript:;" data-slide="2" data-tracking-click="Version &lt;small&gt;&lt;small&gt;4X4 MT MIVEC (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                    <small><small>4X4 MT MIVEC (2020)</small></small>
                                </a>
                            </li>
                            <li class="">
                                <a href="javascript:;" data-slide="3" data-tracking-click="Version &lt;small&gt;&lt;small&gt;4X2 AT MIVEC PREMIUM (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                    <small><small>4X2 AT MIVEC PREMIUM (2020)</small></small>
                                </a>
                            </li>
                            <li class="">
                                <a href="javascript:;" data-slide="4" data-tracking-click="Version &lt;small&gt;&lt;small&gt;4×4 AT MIVEC PREMIUM (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                    <small><small>4×4 AT MIVEC PREMIUM (2020)</small></small>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row hidden-md hidden-lg">
                    <div class="col-xs-12 phienban-note">Vuốt ngang để chọn và so sánh giữa các phiên bản</div>
                </div>
                <div class="row hidden-xs hidden-sm">
                    <div class="col-xs-12 phienban-note"><a href="javascript:;" data-href="#product_specs" class="view-detail modal-open"></a></div>
                </div>
                <div class="phienban-slide">
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['MT']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification &lt;small&gt;&lt;small&gt;4X2 MT (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                Thông số kỹ thuật
                            </div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">5.305 x 1.815 x 1.775</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước thùng sau (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.520 x 1.470 x 475</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai cầu xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">3.000</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,9</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">200</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (kg)</div>
                                        <div class="col-md-6 phienban-content-right">1.725</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi (người)</div>
                                        <div class="col-md-6 phienban-content-right">5</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">2.4L Diesel DI-D</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống nhiên liệu</div>
                                        <div class="col-md-6 phienban-content-right">Phun nhiên liệu điện tử</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">136/3.500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">324/1.500-2500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">75</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">6MT</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">Cầu sau</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gài cầu điện tử</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khóa vi sai cầu sau</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chế độ chọn địa hình Off-road</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Thủy lực</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">Độc lập, tay đòn kép, lò xo cuộn với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Nhíp lá</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">245/70R16</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa thông gió 16"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh sau</div>
                                        <div class="col-md-6 phienban-content-right">Tang trống</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Triton"
                                class="view-detail modal-open"
                                tabindex="0"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment &lt;small&gt;&lt;small&gt;4X2 MT (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                Trang bị tiêu chuẩn
                            </div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiều sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu xa</i></div>
                                        <div class="col-md-6 phienban-content-right">Halogen + Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu gần</i></div>
                                        <div class="col-md-6 phienban-content-right">Halogen + Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn LED chiếu sáng ban ngày</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến BẬT/TẮT đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn pha tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh điện, mạ crôm</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến gạt mưa tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn phanh thứ ba lắp trên cao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm bánh xe</div>
                                        <div class="col-md-6 phienban-content-right">16"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ bước hông xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ bước cản sau dạng thể thao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chắn bùn trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng và cần số bọc da</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lẫy sang số trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều chỉnh âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay lái điều chỉnh 4 hướng</div>
                                        <div class="col-md-6 phienban-content-right">2 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa không khí</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa gió phía sau cho hành khách</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lọc gió điều hòa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Nỉ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay 4 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa điều khiển điện</div>
                                        <div class="col-md-6 phienban-content-right">Kính cửa phía tài xế điều chỉnh một chạm xuống kính</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giải trí</div>
                                        <div class="col-md-6 phienban-content-right">CD/USB/ Radio/Bluetooth</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">4</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tựa tay hàng ghế sau với giá để ly</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí phía trước cho người lái và hành khách</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí bên</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí rèm dọc hai bên thân xe</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí đầu gối bảo vệ người lái</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động cho hàng ghế trước</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dây đai an toàn tất cả các ghế</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử (EBD)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp (BA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử và kiểm soát lực kéo</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ xuống dốc (HDC)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chế độ chọn địa hình Off-Road mode</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến lùi</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến góc trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giảm thiểu va chạm phía trước (FCM)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống tăng tốc ngoài ý muốn (UMS)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo điểm mù (BSW)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ chuyển làn đường (LCA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo phương tiện cắt ngang phía sau (RCTA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gương chiếu hậu chống chói tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh (KOS)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khởi động bằng nút bấm (OSS)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khóa cửa trung tâm &amp; Khóa an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Triton"
                                class="view-detail modal-open"
                                tabindex="0"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['MT_MIVEC_2']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification &lt;small&gt;&lt;small&gt;4X2 AT MIVEC (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                Thông số kỹ thuật
                            </div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">5.305 x 1.815 x 1.780</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước thùng sau (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.520 x 1.470 x 475</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai cầu xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">3.000</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,9</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">205</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (kg)</div>
                                        <div class="col-md-6 phienban-content-right">1.740</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi (người)</div>
                                        <div class="col-md-6 phienban-content-right">5</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">2.4L Diesel MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống nhiên liệu</div>
                                        <div class="col-md-6 phienban-content-right">Phun nhiên liệu điện tử</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">181/3.500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">430/2.500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">75</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">6AT - Sport Mode</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">Cầu sau</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gài cầu điện tử</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khóa vi sai cầu sau</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chế độ chọn địa hình Off-road</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Thủy lực</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">Độc lập, tay đòn kép, lò xo cuộn với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Nhíp lá</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">245/65R17</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa thông gió 16"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh sau</div>
                                        <div class="col-md-6 phienban-content-right">Tang trống</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Triton"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment &lt;small&gt;&lt;small&gt;4X2 AT MIVEC (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                Trang bị tiêu chuẩn
                            </div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiều sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu xa</i></div>
                                        <div class="col-md-6 phienban-content-right">Halogen + Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu gần</i></div>
                                        <div class="col-md-6 phienban-content-right">Halogen + Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn LED chiếu sáng ban ngày</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến BẬT/TẮT đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn pha tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh điện, mạ crôm</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến gạt mưa tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn phanh thứ ba lắp trên cao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm bánh xe</div>
                                        <div class="col-md-6 phienban-content-right">17"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ bước hông xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ bước cản sau dạng thể thao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chắn bùn trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng và cần số bọc da</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lẫy sang số trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều chỉnh âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay lái điều chỉnh 4 hướng</div>
                                        <div class="col-md-6 phienban-content-right">2 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa không khí</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa gió phía sau cho hành khách</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lọc gió điều hòa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Nỉ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay 4 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa điều khiển điện</div>
                                        <div class="col-md-6 phienban-content-right">Kính cửa phía tài xế điều chỉnh một chạm xuống kính</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giải trí</div>
                                        <div class="col-md-6 phienban-content-right">CD/USB/ Radio/Bluetooth</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">4</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tựa tay hàng ghế sau với giá để ly</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí phía trước cho người lái và hành khách</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí bên</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí rèm dọc hai bên thân xe</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí đầu gối bảo vệ người lái</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động cho hàng ghế trước</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dây đai an toàn tất cả các ghế</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử (EBD)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp (BA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử và kiểm soát lực kéo</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ xuống dốc (HDC)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chế độ chọn địa hình Off-Road mode</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến lùi</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến góc trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giảm thiểu va chạm phía trước (FCM)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống tăng tốc ngoài ý muốn (UMS)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo điểm mù (BSW)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ chuyển làn đường (LCA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo phương tiện cắt ngang phía sau (RCTA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gương chiếu hậu chống chói tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh (KOS)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khởi động bằng nút bấm (OSS)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khóa cửa trung tâm &amp; Khóa an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Triton"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['MT_MIVEC_4']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification &lt;small&gt;&lt;small&gt;4X4 MT MIVEC (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                Thông số kỹ thuật
                            </div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">5.305 x 1.815 x 1.780</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước thùng sau (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.520 x 1.470 x 475</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai cầu xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">3.000</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,9</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">205</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (kg)</div>
                                        <div class="col-md-6 phienban-content-right">1915</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi (người)</div>
                                        <div class="col-md-6 phienban-content-right">5</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">2.4L Diesel MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống nhiên liệu</div>
                                        <div class="col-md-6 phienban-content-right">Phun nhiên liệu điện tử</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">181/3.500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">430/2.500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">75</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">6MT</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">Easy Select 4WD</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gài cầu điện tử</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khóa vi sai cầu sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chế độ chọn địa hình Off-road</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Thủy lực</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">Độc lập, tay đòn kép, lò xo cuộn với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Nhíp lá</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">245/65R17</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa thông gió 17"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh sau</div>
                                        <div class="col-md-6 phienban-content-right">Tang trống</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Triton"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment &lt;small&gt;&lt;small&gt;4X4 MT MIVEC (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                Trang bị tiêu chuẩn
                            </div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiều sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu xa</i></div>
                                        <div class="col-md-6 phienban-content-right">Halogen + Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu gần</i></div>
                                        <div class="col-md-6 phienban-content-right">Halogen + Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn LED chiếu sáng ban ngày</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến BẬT/TẮT đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn pha tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh/gập điện, mạ crôm, tích hợp đèn báo rẽ, sưởi gương</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến gạt mưa tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn phanh thứ ba lắp trên cao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm bánh xe</div>
                                        <div class="col-md-6 phienban-content-right">17"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ bước hông xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ bước cản sau dạng thể thao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chắn bùn trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng và cần số bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lẫy sang số trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều chỉnh âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay lái điều chỉnh 4 hướng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa không khí</div>
                                        <div class="col-md-6 phienban-content-right">Tự động</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa gió phía sau cho hành khách</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lọc gió điều hòa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Nỉ cao cấp</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay 6 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa điều khiển điện</div>
                                        <div class="col-md-6 phienban-content-right">Kính cửa phía tài xế điều chỉnh một chạm, chống kẹt</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">LCD</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giải trí</div>
                                        <div class="col-md-6 phienban-content-right">CD/USB/ Radio/Bluetooth</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">4</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tựa tay hàng ghế sau với giá để ly</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí phía trước cho người lái và hành khách</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí bên</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí rèm dọc hai bên thân xe</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí đầu gối bảo vệ người lái</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động cho hàng ghế trước</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dây đai an toàn tất cả các ghế</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử (EBD)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp (BA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử và kiểm soát lực kéo</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ xuống dốc (HDC)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chế độ chọn địa hình Off-Road mode</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến lùi</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến góc trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giảm thiểu va chạm phía trước (FCM)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống tăng tốc ngoài ý muốn (UMS)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo điểm mù (BSW)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ chuyển làn đường (LCA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo phương tiện cắt ngang phía sau (RCTA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gương chiếu hậu chống chói tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh (KOS)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khởi động bằng nút bấm (OSS)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khóa cửa trung tâm &amp; Khóa an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Triton"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['AT_MIVEC_Premium_2']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div
                                class="col-md-12 phienban-title"
                                data-tracking-click="View Specification &lt;small&gt;&lt;small&gt;4X2 AT MIVEC PREMIUM (2020)&lt;/small&gt;&lt;/small&gt;"
                                data-tracking-click-cat="New Triton"
                            >
                                Thông số kỹ thuật
                            </div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">5.305 x 1.815 x 1.795</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước thùng sau (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.520 x 1.470 x 475</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai cầu xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">3.000</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,9</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">220</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (kg)</div>
                                        <div class="col-md-6 phienban-content-right">1810</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi (người)</div>
                                        <div class="col-md-6 phienban-content-right">5</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">2.4L Diesel MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống nhiên liệu</div>
                                        <div class="col-md-6 phienban-content-right">Phun nhiên liệu điện tử</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">181/3.500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">430/2.500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">75</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">6AT - Sport Mode</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">Cầu sau</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gài cầu điện tử</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khóa vi sai cầu sau</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chế độ chọn địa hình Off-road</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Thủy lực</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">Độc lập, tay đòn kép, lò xo cuộn với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Nhíp lá</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">265/60R18</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa thông gió 17"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh sau</div>
                                        <div class="col-md-6 phienban-content-right">Tang trống</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Triton"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment &lt;small&gt;&lt;small&gt;4X2 AT MIVEC PREMIUM (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                Trang bị tiêu chuẩn
                            </div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiều sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu xa</i></div>
                                        <div class="col-md-6 phienban-content-right">LED + Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu gần</i></div>
                                        <div class="col-md-6 phienban-content-right">LED + Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn LED chiếu sáng ban ngày</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến BẬT/TẮT đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn pha tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh/gập điện, mạ crôm, tích hợp đèn báo rẽ, sưởi gương</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến gạt mưa tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn phanh thứ ba lắp trên cao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm bánh xe</div>
                                        <div class="col-md-6 phienban-content-right">18"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ bước hông xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ bước cản sau dạng thể thao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chắn bùn trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng và cần số bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lẫy sang số trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều chỉnh âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay lái điều chỉnh 4 hướng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa không khí</div>
                                        <div class="col-md-6 phienban-content-right">Tự động 2 vùng độc lập</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa gió phía sau cho hành khách</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lọc gió điều hòa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Da</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh điện 8 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa điều khiển điện</div>
                                        <div class="col-md-6 phienban-content-right">Kính cửa phía tài xế điều chỉnh một chạm, chống kẹt</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">LCD</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giải trí</div>
                                        <div class="col-md-6 phienban-content-right">Màn hình cảm ứng 6,75" với Android Auto, Apple CarPlay</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">6</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tựa tay hàng ghế sau với giá để ly</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí phía trước cho người lái và hành khách</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí bên</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí rèm dọc hai bên thân xe</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí đầu gối bảo vệ người lái</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động cho hàng ghế trước</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dây đai an toàn tất cả các ghế</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử (EBD)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp (BA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử và kiểm soát lực kéo</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ xuống dốc (HDC)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chế độ chọn địa hình Off-Road mode</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến lùi</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến góc trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giảm thiểu va chạm phía trước (FCM)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống tăng tốc ngoài ý muốn (UMS)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo điểm mù (BSW)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ chuyển làn đường (LCA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo phương tiện cắt ngang phía sau (RCTA)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gương chiếu hậu chống chói tự động</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh (KOS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khởi động bằng nút bấm (OSS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khóa cửa trung tâm &amp; Khóa an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Triton"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['AT_MIVEC_Premium_4']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div
                                class="col-md-12 phienban-title"
                                data-tracking-click="View Specification &lt;small&gt;&lt;small&gt;4×4 AT MIVEC PREMIUM (2020)&lt;/small&gt;&lt;/small&gt;"
                                data-tracking-click-cat="New Triton"
                            >
                                Thông số kỹ thuật
                            </div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">5.305 x 1.815 x 1.795</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước thùng sau (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.520 x 1.470 x 475</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai cầu xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">3.000</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,9</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">220</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (kg)</div>
                                        <div class="col-md-6 phienban-content-right">1925</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi (người)</div>
                                        <div class="col-md-6 phienban-content-right">5</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">2.4L Diesel MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống nhiên liệu</div>
                                        <div class="col-md-6 phienban-content-right">Phun nhiên liệu điện tử</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">181/3.500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">430/2.500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">75</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">6AT - Sport Mode</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">2 cầu Super Select 4WD-II</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gài cầu điện tử</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khóa vi sai cầu sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chế độ chọn địa hình Off-road</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Thủy lực</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">Độc lập, tay đòn kép, lò xo cuộn với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Nhíp lá</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">265/60R18</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa thông gió 17"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh sau</div>
                                        <div class="col-md-6 phienban-content-right">Tang trống</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Triton"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment &lt;small&gt;&lt;small&gt;4×4 AT MIVEC PREMIUM (2020)&lt;/small&gt;&lt;/small&gt;" data-tracking-click-cat="New Triton">
                                Trang bị tiêu chuẩn
                            </div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiều sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu xa</i></div>
                                        <div class="col-md-6 phienban-content-right">LED + Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu gần</i></div>
                                        <div class="col-md-6 phienban-content-right">LED + Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn LED chiếu sáng ban ngày</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến BẬT/TẮT đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn pha tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh/gập điện, mạ crôm, tích hợp đèn báo rẽ, sấy gương</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến gạt mưa tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn phanh thứ ba lắp trên cao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm bánh xe</div>
                                        <div class="col-md-6 phienban-content-right">18"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ bước hông xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ bước cản sau dạng thể thao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chắn bùn trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng và cần số bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lẫy sang số trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều chỉnh âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay lái điều chỉnh 4 hướng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa không khí</div>
                                        <div class="col-md-6 phienban-content-right">Tự động 2 vùng độc lập</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa gió phía sau cho hành khách</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lọc gió điều hòa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Da</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh điện 8 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa điều khiển điện</div>
                                        <div class="col-md-6 phienban-content-right">Kính cửa phía tài xế điều chỉnh một chạm, chống kẹt</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giải trí</div>
                                        <div class="col-md-6 phienban-content-right">Màn hình cảm ứng 6,75" với Android Auto, Apple CarPlay</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">6</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tựa tay hàng ghế sau với giá để ly</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí phía trước cho người lái và hành khách</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí bên</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí rèm dọc hai bên thân xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí đầu gối bảo vệ người lái</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động cho hàng ghế trước</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dây đai an toàn tất cả các ghế</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử (EBD)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp (BA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử và kiểm soát lực kéo</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ xuống dốc (HDC)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chế độ chọn địa hình Off-Road mode</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến lùi</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến góc trước</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giảm thiểu va chạm phía trước (FCM)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống tăng tốc ngoài ý muốn (UMS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo điểm mù (BSW)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống hỗ trợ chuyển làn đường (LCA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo phương tiện cắt ngang phía sau (RCTA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gương chiếu hậu chống chói tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh (KOS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khởi động bằng nút bấm (OSS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khóa cửa trung tâm &amp; Khóa an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Triton"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                </div>
                <!--.phienban-slide-->
                <div class="phienban-btn">
                    <ul>
                        <li>
                            <a
                                href="javascript:;"
                                data-href="#product_estimate"
                                class="btn btn-icon-l modal-open d2-estimate"
                                data-car-id="22"
                                data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                data-step="1"
                                data-tracking-click="Estimated Cost"
                                data-tracking-click-cat="New Triton"
                            >
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-cost"></i></span><span class="text">Dự tính chi phí</span>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.mitsubishiquangninh.com/mua-xe/bao-gia-chi-tiet/22/"
                                class="btn btn-icon-l d2-estimate"
                                data-car-id="22"
                                data-tracking-click="Price Quotation By Dealer"
                                data-tracking-click-cat="New Triton"
                                data-step="2"
                            >
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-price"></i></span><span class="text">Yêu cầu báo giá</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mitsubishiquangninh.com/mua-xe/dang-ky-lai-thu/22/" class="btn btn-icon-l">
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-drive"></i></span><span class="text">Đăng ký lái thử</span>
                            </a>
                        </li>
                        <li style="display: none;">
                            <a href="https://www.mitsubishiquangninh.com/dai-ly/" class="btn btn-icon-l">
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-dealer"></i></span><span class="text">Tìm nhà phân phối</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--.chonphienban-->
            </div>
        </div>
    </div>
</div>
<!--End - Specs-->