<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>AN TOÀN CHUẨN MỰC</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide100"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="Lôi cuốn từ chuẩn mực an toàn" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> Lôi cuốn từ chuẩn mực an toàn</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/An-toàn1.jpg')}}"
                                    alt="Lôi cuốn từ chuẩn mực an toàn"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> Lôi cuốn từ chuẩn mực an toàn</span></h3>
                                <div class="opContent">
                                    <p>
                                        Attrage là một mẫu xe thực dụng phù hợp với giá trị sử dụng đồng thời được trang bị các tính năng an toàn chuẩn mực như khung xe RISE, hệ thống phanh ABS, phân phối lực phanh EBD,…bảo vệ tối
                                        đa cho hành khách.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide101" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Khung xe RISE thép siêu cường" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Khung xe RISE thép siêu cường</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/khung-xe-rise2.jpg')}}"
                                    alt="Khung xe RISE thép siêu cường"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Khung xe RISE thép siêu cường</span></h3>
                                <div class="opContent">
                                    <p>Khung xe RISE cứng vững, đạt tiêu chuẩn 5 sao của ANCAP, đảm bảo an toàn cao cho khoang hành khách khi va chạm.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide102" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống phanh chống bó cứng phanh ABS" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Hệ thống phanh chống bó cứng phanh ABS</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Phanh-ABS.jpg')}}"
                                    alt="Hệ thống phanh chống bó cứng phanh ABS"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Hệ thống phanh chống bó cứng phanh ABS</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống phanh ABS giúp&nbsp;chống&nbsp;hiện&nbsp;tượng&nbsp;bó&nbsp;cứng&nbsp;phanh&nbsp;duy trì khả năng điều khiển xe khi phanh gấp.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide103" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống phân phối lực phanh điện tử EBD" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Hệ thống phân phối lực phanh điện tử EBD</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Phanh-EBD-mới.jpg')}}"
                                    alt="Hệ thống phân phối lực phanh điện tử EBD"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Hệ thống phân phối lực phanh điện tử EBD</span></h3>
                                <div class="opContent">
                                    <p>Phân phối lực phanh tối ưu giúp làm chủ tay lái khi phanh gấp trên đường trơn trượt đồng thời rút ngắn quãng đường phanh.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide104" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống túi khí an toàn" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Hệ thống túi khí an toàn</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Túi-khí.jpg')}}"
                                    alt="Hệ thống túi khí an toàn"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Hệ thống túi khí an toàn</span></h3>
                                <div class="opContent">
                                    <p>Túi&nbsp;khí&nbsp;đôi cho&nbsp;hàng&nbsp;ghế&nbsp;trước,&nbsp;đảm&nbsp;bảo&nbsp;an&nbsp;toàn&nbsp;trong&nbsp;các&nbsp;va&nbsp;trạm&nbsp;trực&nbsp;diện.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide105" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống căng đai tự động" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Hệ thống căng đai tự động</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/he-thong-cang-dai-tu-dong1.jpg')}}"
                                    alt="Hệ thống căng đai tự động"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Hệ thống căng đai tự động</span></h3>
                                <div class="opContent">
                                    <p>Kết hợp với hệ thống túi khí đảm bảo an toàn cao khi xảy ra va chạm.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="6" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide106" data-slick-index="6" aria-hidden="true">
                        <a href="javascript:;" title="Cột lái tự đổ và bàn đạp phanh tự đổ" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">6. Cột lái tự đổ và bàn đạp phanh tự đổ</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/picture4.jpg')}}"
                                    alt="Cột lái tự đổ và bàn đạp phanh tự đổ"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">6. Cột lái tự đổ và bàn đạp phanh tự đổ</span></h3>
                                <div class="opContent">
                                    <p>Khi xảy ra các va chạm trực diện, cột lái và bàn đạp phanh sẽ tự đổ nhằm bảo vệ tối đa phần đầu và chân cho người lái.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/An-toàn1.jpg')}}"
                        alt="Lôi cuốn từ chuẩn mực an toàn"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Lôi cuốn từ chuẩn mực an toàn</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Attrage là một mẫu xe thực dụng phù hợp với giá trị sử dụng đồng thời được trang bị các tính năng an toàn chuẩn mực như khung xe RISE, hệ thống phanh ABS, phân phối lực phanh EBD,…bảo vệ tối đa cho hành
                            khách.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/khung-xe-rise2.jpg')}}"
                        alt="Khung xe RISE thép siêu cường"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #404040;">Khung xe RISE thép siêu cường</h3>
                    <div class="opContent" style="color: #404040;">
                        <p>Khung xe RISE cứng vững, đạt tiêu chuẩn 5 sao của ANCAP, đảm bảo an toàn cao cho khoang hành khách khi va chạm.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Phanh-ABS.jpg')}}"
                        alt="Hệ thống phanh chống bó cứng phanh ABS"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #404040;">Hệ thống phanh chống bó cứng phanh ABS</h3>
                    <div class="opContent" style="color: #404040;">
                        <p>Hệ thống phanh ABS giúp&nbsp;chống&nbsp;hiện&nbsp;tượng&nbsp;bó&nbsp;cứng&nbsp;phanh&nbsp;duy trì khả năng điều khiển xe khi phanh gấp.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Phanh-EBD-mới.jpg')}}"
                        alt="Hệ thống phân phối lực phanh điện tử EBD"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống phân phối lực phanh điện tử EBD</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Phân phối lực phanh tối ưu giúp làm chủ tay lái khi phanh gấp trên đường trơn trượt đồng thời rút ngắn quãng đường phanh.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Túi-khí.jpg')}}"
                        alt="Hệ thống túi khí an toàn"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống túi khí an toàn</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Túi&nbsp;khí&nbsp;đôi cho&nbsp;hàng&nbsp;ghế&nbsp;trước,&nbsp;đảm&nbsp;bảo&nbsp;an&nbsp;toàn&nbsp;trong&nbsp;các&nbsp;va&nbsp;trạm&nbsp;trực&nbsp;diện.</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/he-thong-cang-dai-tu-dong1.jpg')}}"
                        alt="Hệ thống căng đai tự động"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #404040;">Hệ thống căng đai tự động</h3>
                    <div class="opContent" style="color: #404040;">
                        <p>Kết hợp với hệ thống túi khí đảm bảo an toàn cao khi xảy ra va chạm.</p>
                    </div>
                </div>
            </div>
            <div data-item="6" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/picture4.jpg')}}"
                        alt="Cột lái tự đổ và bàn đạp phanh tự đổ"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #404040;">Cột lái tự đổ và bàn đạp phanh tự đổ</h3>
                    <div class="opContent" style="color: #404040;">
                        <p>Khi xảy ra các va chạm trực diện, cột lái và bàn đạp phanh sẽ tự đổ nhằm bảo vệ tối đa phần đầu và chân cho người lái.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>