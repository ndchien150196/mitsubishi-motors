@extends("master")
@section("js")
    <script src="{{asset('js/slick.min.js')}}"></script>
@endsection
@section("content") 
<div class="page-product">
    @include('category.sanpham.newattrage.banner')
    @include('category.sanpham.xpandercross.navigation')

    <!--End - Navigation-->
    <div class="pr-content">
    	@include('category.sanpham.newattrage.dacdiemnoibat')
    	@include('category.sanpham.newattrage.thongsokythuat')
    	@include('category.sanpham.newattrage.ngoaithat')
    	@include('category.sanpham.newattrage.noithatrongrai')
    	@include('category.sanpham.newattrage.vanhanh')
    	@include('category.sanpham.newattrage.antoan')
    	@include('category.sanpham.newattrage.thuvien')
        
        
    </div>
    <!--.pr-content-->

    <!--===================-->
</div>

@endsection
@section('afftercontent')
<div d2-estimate-popup=""></div>
<!--#product_extimate-->

<section id="product_specs" class="mitsu-modal product-specs">
    <div class="grid-inner">
        <div data-href="#product_specs" class="modal-close"></div>
        <h3 class="title-block pr-title-block">
            <a title="So sánh giữa các phiên bản">So sánh giữa các phiên bản</a>
        </h3>
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <div class="list-steps">
                    <div class="steps-header">
                        <table>
                            <colgroup>
                                <col width="35%" />
                                <col width="32%" />
                                <col width="32%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <div class="stepes-pagination">
                                            <ul>
                                                <li><a href="javascript:;" data-href="#st_thongsokythuat" class="stepes-active active">Thông số kỹ thuật</a></li>
                                                <li><a href="javascript:;" data-href="#st_trangthietbi" class="stepes-active">Trang thiết bị</a></li>
                                            </ul>
                                        </div>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/new-attrage/ELL-MY20.png')}}"
                                            alt="New Attrage MT"
                                        />
                                        <label>MT</label><span>{{$product['Attrage_MT']}} VNĐ</span>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/new-attrage/ELL-MY20.png')}}"
                                            alt="New Attrage CVT"
                                        />
                                        <label>CVT</label><span>{{$product['Attrage_CVT']}} VNĐ</span>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="st_thongsokythuat" class="stepes step-1 active">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />
                                    <col width="32%" />
                                    <col width="32%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><b> KÍCH THƯỚC </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kích thước tổng thể (DxRxC) (mm)</td>
                                        <td>4.305 x 1.670 x 1.515</td>
                                        <td>4.305 x 1.670 x 1.515</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chiều dài cơ sở (mm)</td>
                                        <td>2.550</td>
                                        <td>2.550</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Bán kính quay vòng nhỏ nhất (m)</td>
                                        <td>4,8</td>
                                        <td>4,8</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoảng sáng gầm xe (mm)</td>
                                        <td>170</td>
                                        <td>170</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Trọng lượng không tải (kg)</td>
                                        <td>875</td>
                                        <td>905</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tổng trọng lượng (kg)</td>
                                        <td>1.330</td>
                                        <td>1.350</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Số chỗ ngồi</td>
                                        <td>5</td>
                                        <td>5</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> ĐỘNG CƠ </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Loại động cơ</td>
                                        <td>1.2L MIVEC</td>
                                        <td>1.2L MIVEC</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống nhiên liệu</td>
                                        <td>Phun xăng đa điểm, điều khiển điện tử ECI-MULTI</td>
                                        <td>Phun xăng đa điểm, điều khiển điện tử ECI-MULTI</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dung Tích Xylanh (cc)</td>
                                        <td>1.193</td>
                                        <td>1.193</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Công suất cực đại (ps/rpm)</td>
                                        <td>78/6.000</td>
                                        <td>78/6.000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mômen xoắn cực đại (Nm/rpm)</td>
                                        <td>100/4.000</td>
                                        <td>100/4.000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tốc độ cực đại (Km/h)</td>
                                        <td>172</td>
                                        <td>172</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dung tích thùng nhiên liệu (L)</td>
                                        <td>42</td>
                                        <td>42</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> HỆ THỐNG DẪN ĐỘNG &amp; HỆ THỐNG TREO </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hộp số</td>
                                        <td>Số sàn 5 cấp</td>
                                        <td>Tự động vô cấp CVT INVECS-III</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống treo trước</td>
                                        <td>Kiểu MacPherson, lò xo cuộn với thanh cân bằng</td>
                                        <td>Kiểu MacPherson, lò xo cuộn với thanh cân bằng</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống treo sau</td>
                                        <td>Dầm xoắn</td>
                                        <td>Dầm xoắn</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mâm - Lốp</td>
                                        <td>Mâm hợp kim, 185/55R15</td>
                                        <td>Mâm hợp kim, 185/55R15</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Phanh trước</td>
                                        <td>Đĩa thông gió</td>
                                        <td>Đĩa thông gió</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Phanh sau</td>
                                        <td>Tang trống</td>
                                        <td>Tang trống</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mức tiêu hao nhiên liệu (L/100Km)</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Kết hợp</i></td>
                                        <td>5,09</td>
                                        <td>5,36</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Trong đô thị</i></td>
                                        <td>6,22</td>
                                        <td>6,47</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Ngoài đô thị</i></td>
                                        <td>4,42</td>
                                        <td>4,71</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="st_trangthietbi" class="stepes step-2">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />

                                    <col width="32%" />
                                    <col width="32%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><b> AN TOÀN </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Túi khí</td>
                                        <td>Túi khí đôi</td>
                                        <td>Túi khí đôi</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Căng đai tự động</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dây đai an toàn cho tất cả các ghế</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Móc gắn ghế trẻ em</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống chống bó cứng phanh ABS</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống phân phối lực phanh điện tử EBD</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoá cửa từ xa</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chìa khóa mã hóa chống trộm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chìa khóa thông minh</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống khởi động nút bấm</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> NGOẠI THẤT </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống đèn chiếu sáng phía trước</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Đèn chiếu xa</i></td>
                                        <td>Halogen</td>
                                        <td>LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Đèn chiếu gần</i></td>
                                        <td>Halogen</td>
                                        <td>LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn sương mù</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kính chiếu hậu</td>
                                        <td>Cùng màu với thân xe, chỉnh điện</td>
                                        <td>Cùng màu với thân xe, chỉnh điện, tích hợp đèn báo rẽ</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tay nắm cửa ngoài</td>
                                        <td>Cùng màu thân xe</td>
                                        <td>Cùng màu thân xe</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lưới tản nhiệt</td>
                                        <td>Viền chrome</td>
                                        <td>Viền đỏ</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gạt kính trước</td>
                                        <td>Tốc độ thay đổi theo vận tốc xe</td>
                                        <td>Tốc độ thay đổi theo vận tốc xe</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn phanh thứ 3 lắp trên cao</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> NỘI THẤT </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Vô lăng trợ lực điện</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Vô lăng bọc da</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Nút chỉnh âm thanh và thoại rảnh tay</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống ga tự động</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cần số bọc da</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Điều hòa không khí</td>
                                        <td>Chỉnh tay</td>
                                        <td>Tự động</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lọc gió điều hòa</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khóa cửa trung tâm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tay nắm cửa trong</td>
                                        <td>Cùng màu nội thất</td>
                                        <td>Mạ chrome</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kính cửa điều khiển điện</td>
                                        <td>Cửa kính phía người lái điều khiển một chạm với chức năng</td>
                                        <td>Cửa kính phía người lái điều khiển một chạm với chức năng</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Màn hình hiển thị đa thông tin</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn báo hiệu tiết kiệm nhiên liệu</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống âm thanh</td>
                                        <td>CD</td>
                                        <td>Màn hình cảm ứng 6.8", hỗ trợ kết nối Apple CarPlay/Android Auto</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống loa</td>
                                        <td>2</td>
                                        <td>4</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chất liệu ghế</td>
                                        <td>Nỉ</td>
                                        <td>Da</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Ghế tài xế chỉnh tay 6 hướng</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Bệ tỳ tay dành cho người lái</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tựa đầu hàng ghế sau</td>
                                        <td>3 vị trí</td>
                                        <td>3 vị trí</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tựa tay hàng ghế sau với giá để ly</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br />
                    <br />
                    <p class="text-left">(*) Mức tiêu hao nhiên liệu chứng nhận bởi Cục Đăng Kiểm Việt Nam. Các thông số kỹ thuật có thể thay đổi mà không báo trước</p>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
@section('jsfooter')
<script src="{{asset('js/sanpham/TweenMax.min.js')}}"></script>
<script src="{{asset('js/sanpham/ScrollToPlugin.min.js')}}"></script>
<script src="{{asset('js/sanpham/jquery.fancybox.js')}}"></script>
<script src="{{asset('js/new-xpander.js')}}"></script>
<script src="{{asset('js/estimate-price.js')}}"></script>
<script type="text/javascript">
    productInit();
</script>
@endsection
