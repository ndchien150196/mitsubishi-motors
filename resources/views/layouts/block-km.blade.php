<div class="row">
    <div class="col-sm-6 col-md-12">
        <div class="sidebar-block drive-block"></div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-12">
        <div class="sidebar-block promotion-block">
            <a href="{{asset('tin-tuc/tin-khuyen-mai')}}" title="💥 TRI ÂN VÀNG – TRÀN KHUYẾN MÃI 💥">
                <img class="img-full" alt="💥 TRI ÂN VÀNG – TRÀN KHUYẾN MÃI 💥" src="{{asset('img/promotion-banner.jpg')}}" />
            </a>
        </div>
    </div>
</div>