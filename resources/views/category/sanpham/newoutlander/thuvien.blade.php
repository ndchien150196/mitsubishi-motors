<!--Gallery-->
<div class="normal-block thuvien">
    <div class="grid-inner">
        <h3 class="title-block pr-title-block"><span>Thư viện hình ảnh &amp; catalogue</span></h3>
        <div class="row">
            <div class="col-md-6 catalogue-download">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-2 col-md-offset-1">
                        <picture>
                            <!--if IE 9video(style='display: none;')
                                      -->
                            <source srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Final_LEAFLET_OUTLANDER_200721_FA_OL_Small_Artboard-1-copy-2-2.png" alt="New Outlander" media="(min-width: 768px)" />
                            <!--if IE 9-->
                            <img
                                src="{{asset('img/newoutlander/Final_LEAFLET_OUTLANDER_200721_FA_OL_Small_Artboard-1-co.png')}}"
                                alt="Outlander Sport"
                            />
                        </picture>
                    </div>
                    <div class="col-sm-6 col-md-7">
                        <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Final_LEAFLET_OUTLANDER_200721_FA_OL_Small1.pdf" title="Tải Catalogue" class="btn btn-icon-left" target="_blank">
                            <span class="icon"><i class="svg-icon icon-download"></i></span><span class="text">Tải Catalogue</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="video">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe
                            width="560"
                            height="315"
                            src="https://www.youtube.com/embed/1md1W3MJGw0"
                            frameborder="0"
                            allowfullscreen=""
                        ></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="list-photo">
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/car1.png" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newoutlander/car1-390x390.png')}}"
                        alt="car1"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/car9.png" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newoutlander/car9-390x390.png')}}"
                        alt="car9"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/car8.png" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newoutlander/car8-390x390.png')}}"
                        alt="car8"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/car7.png" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newoutlander/car7-390x390.png')}}"
                        alt="car7"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/car6.png" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newoutlander/car6-390x390.png')}}"
                        alt="car6"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/car5.png" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newoutlander/car5-390x390.png')}}"
                        alt="car5"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/car4.png" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newoutlander/car4-390x390.png')}}"
                        alt="car4"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/car3.png" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newoutlander/car3-390x390.png')}}"
                        alt="car3"
                    />
                </a>
            </div>
            <div class="photo-item">
                <a href="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/car2.png" title="Outlander Sport" rel="gallery" class="fancybox">
                    <img
                        src="{{asset('img/newoutlander/car2-390x390.png')}}"
                        alt="car2"
                    />
                </a>
            </div>
        </div>
    </div>
</div>
<!--End - Gallery-->