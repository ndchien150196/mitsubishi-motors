<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>Mitsubishi Thái Bình</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
        <link rel="icon" type="image/x-icon" href="{{asset('img/favicon.png')}}" />
        <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.png')}}" />
        <link rel="apple-touch-icon" href="{{asset('img/favicon.png')}}" />
        <link rel="stylesheet" href="{{asset('css/admin/bootstrap.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/admin/font-awesome/css/font-awesome.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/admin/dataTables.bootstrap.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/admin/AdminLTE.min.css')}}" />
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="login-logo">
                <span><b>Mitsubishi </b>Thái Bình</span>
            </div>
            <div class="login-box-body">
                <!-- <p class="login-box-msg">Sign in to start your session</p> -->
                @if(count($errors) >0)
                <div class="alert alert-danger">
                    @foreach($errors->all() as $err) {{$err}}<br />
                    @endforeach
                </div>
                @endif @if(session('thongbao'))
                <div class="alert alert-danger">
                    {{session('thongbao')}}
                </div>
                @endif

                <form action="{{asset('reset')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <label>Nhập Email để tìm lại mật khẩu</label>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" name="email" placeholder="Email" />
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    </div>
                    
                    <div class="row">
                        <div class="col-xs-8">
                            <a href="{{asset('/login')}}">Đăng Nhập</a>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Xác Nhận</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
        <script src="{{asset('js/admin/jquery.min.js')}}"></script>
        <script src="{{asset('js/admin/bootstrap.min.js')}}"></script>
    </body>
</html>
