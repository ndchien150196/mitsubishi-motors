@extends("master")
@section("js")
    <script type="text/javascript" src="{{asset('js/slick.min.js')}}"></script>
@endsection
@section("content") 
    <div class="banner-slide">
        <div class="slick-single-item slider">
            @foreach($slide as $s)
            <div class="banner-slide-item">
                <a href="{{$s->link}}">
                    <img class="res-img" alt="{{$s->name}}" src="{{$s->image}}" />
                </a>
            </div>
            @endforeach
        </div>
        <span class="icon-box hidden-xs hidden-sm">
            <i class="svg-icon icon-arrow-down-white"></i>
        </span>
    </div>

    <div class="normal-block home-nav-block">
        <ul class="block-list">
            <li>
                <a href="{{url('bang-gia')}}">
                    <div class="text">BẢNG GIÁ &amp; KHUYẾN MÃI <i class="fa fa-angle-right" aria-hidden="true"></i></div>
                    <div class="img">
                        <img src="{{asset('img/home/1920x800-1.jpg')}}" alt="" />
                    </div>
                </a>
            </li>
            <li>
                <a href="{{asset('tin-tuc/su-kien-noi-bat')}}">
                    <div class="text">SỰ KIỆN ĐẠI LÝ <i class="fa fa-angle-right" aria-hidden="true"></i></div>
                    <div class="img">
                        <img src="{{asset('img/home/1920x800.jpg')}}" alt="" />
                    </div>
                </a>
            </li>
            <li>
                <a href="{{url('mua-xe',['bao-gia-chi-tiet'])}}">
                    <div class="text">YÊU CẦU BÁO GIÁ <i class="fa fa-angle-right" aria-hidden="true"></i></div>
                    <div class="img">
                        <img src="{{asset('img/home/link-3.jpg')}}" alt="" />
                    </div>
                </a>
            </li>

            <li class="hidden-xs head-red">
                <div class="text">
                    HOTLINE
                    <i class="fa fa-phone" aria-hidden="true"></i>
                </div>
                <div class="detail">
                    <div class="wrap">
                        <p>
                            Liên hệ:
                            <a href="tel:{{\App\Common::phonelh()}}"><strong>{{\App\Common::phonelh()}}</strong></a>
                        </p>
                    </div>
                </div>
            </li>
        </ul>
    </div>

    <div class="normal-block vehicle-block">
        <div class="grid-inner">
            <h2 class="title-block">
                <span>sản phẩm</span>
            </h2>
            <div class="vehicle-inner">
                <div class="vehicle-item">
                    <a target="_blank" href="{{asset('san-pham/xpander-cross')}}" title="Xpander Cross">
                        <div class="img-thumb">
                            <picture class="vehicle-item-img">
                                <source media="(min-width: 992px)" srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/07/Cross.png" alt="Xpander Cross" />
                                <img src="{{asset('img/home/Cross(1).png')}}" alt="Xpander Cross" />
                            </picture>
                        </div>
                        <h3 class="vehicle-item-title">
                            Xpander Cross
                        </h3>
                        <div class="vehicle-item-summary">
                            <ul class="vehicle-item-desc">
                                <li class="text">
                                    Tiêu thụ:
                                    <strong>6,90L/100Km</strong>
                                </li>
                                <li class="text">
                                    Giá từ:
                                    <strong>{{$product['Xpander_Cross']}} VNĐ</strong>
                                </li>
                            </ul>

                            <div class="more">
                                <div class="btn btn-grey">Tìm hiểu thêm</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="vehicle-item">
                    <a target="_blank" href="http://newpajerosport.vn/?utm_source=Ha%20Noi%20Auto&amp;utm_medium=Click&amp;utm_campaign=Launch&amp;utm_term=&amp;utm_content=menu" title="New Pajero Sport">
                        <div class="img-thumb">
                            <picture class="vehicle-item-img">
                                <source media="(min-width: 992px)" srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/10/Pajero-sport.png" alt="New Pajero Sport" />
                                <img src="{{asset('img/home/Pajero-sport(1).png')}}" alt="New Pajero Sport" />
                            </picture>
                        </div>
                        <h3 class="vehicle-item-title">
                            New Pajero Sport
                        </h3>
                        <div class="vehicle-item-summary">
                            <ul class="vehicle-item-desc">
                                <li class="text">
                                    Tiêu thụ: N/A
                                </li>
                                <li class="text">
                                    Giá từ:
                                    <strong>{{$product['Diesel_4×2_AT']}} VNĐ</strong>
                                </li>
                            </ul>

                            <div class="more">
                                <div class="btn btn-grey">Tìm hiểu thêm</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="vehicle-item">
                    <a target="" href="{{asset('san-pham/new-xpander')}}" title="New Xpander">
                        <div class="img-thumb">
                            <picture class="vehicle-item-img">
                                <source media="(min-width: 992px)" srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Xpander-Thumbnail.png" alt="New Xpander" />
                                <img src="{{asset('img/home/Xpander-Thumbnail(1).png')}}" alt="New Xpander" />
                            </picture>
                        </div>
                        <h3 class="vehicle-item-title">
                            New Xpander
                        </h3>
                        <div class="vehicle-item-summary">
                            <ul class="vehicle-item-desc">
                                <li class="text">
                                    Tiêu thụ:
                                    <strong>6,90L/100Km</strong>
                                </li>
                                <li class="text">
                                    Giá từ:
                                    <strong>{{$product['Xpander_MT']}} VNĐ</strong>
                                </li>
                            </ul>

                            <div class="more">
                                <div class="btn btn-grey">Tìm hiểu thêm</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="vehicle-item">
                    <a target="" href="{{asset('/san-pham/new-attrage')}}" title="New Attrage">
                        <div class="img-thumb">
                            <picture class="vehicle-item-img">
                                <source media="(min-width: 992px)" srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/03/ELL-MY20_-final.png" alt="New Attrage" />
                                <img src="{{asset('img/home/ELL-MY20_-final(1).png')}}" alt="New Attrage" />
                            </picture>
                        </div>
                        <h3 class="vehicle-item-title">
                            New Attrage
                        </h3>
                        <div class="vehicle-item-summary">
                            <ul class="vehicle-item-desc">
                                <li class="text">
                                    Tiêu thụ:
                                    <strong>5,09L/100Km</strong>
                                </li>
                                <li class="text">
                                    Giá từ:
                                    <strong>{{$product['Attrage_MT']}} VND</strong>
                                </li>
                            </ul>

                            <div class="more">
                                <div class="btn btn-grey">Tìm hiểu thêm</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="vehicle-item">
                    <a target="" href="{{asset('/san-pham/new-outlander')}}" title="New Outlander">
                        <div class="img-thumb">
                            <picture class="vehicle-item-img">
                                <source media="(min-width: 992px)" srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/RE-MY20.png" alt="New Outlander" />
                                <img src="{{asset('img/home/RE-MY20(1).png')}}" alt="New Outlander" />
                            </picture>
                        </div>
                        <h3 class="vehicle-item-title">
                            New Outlander
                        </h3>
                        <div class="vehicle-item-summary">
                            <ul class="vehicle-item-desc">
                                <li class="text">
                                    Tiêu thụ:
                                    <strong>7,70L/100Km</strong>
                                </li>
                                <li class="text">
                                    Giá từ:
                                    <strong>{{$product['CVT_2']}} VNĐ</strong>
                                </li>
                            </ul>

                            <div class="more">
                                <div class="btn btn-grey">Tìm hiểu thêm</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="vehicle-item">
                    <a target="" href="{{asset('/san-pham/newtriton')}}" title="New Triton">
                        <div class="img-thumb">
                            <picture class="vehicle-item-img">
                                <source media="(min-width: 992px)" srcset="https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2018/12/Triton.png" alt="New Triton" />
                                <img src="{{asset('img/home/Triton(1).png')}}" alt="New Triton" />
                            </picture>
                        </div>
                        <h3 class="vehicle-item-title">
                            New Triton
                        </h3>
                        <div class="vehicle-item-summary">
                            <ul class="vehicle-item-desc">
                                <li class="text">
                                    Tiêu thụ: N/A
                                </li>
                                <li class="text">
                                    Giá từ:
                                    <strong>{{$product['MT']}} VNĐ</strong>
                                </li>
                            </ul>

                            <div class="more">
                                <div class="btn btn-grey">Tìm hiểu thêm</div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="normal-block home-care-block">
        <div class="grid-inner">
            <h2 class="title-block">
                <span>DỊCH VỤ HẬU MÃI</span>
            </h2>
            <div class="content">
                <h3 class="number">
                    <i class="fa fa-phone" aria-hidden="true"></i>Hotline dịch vụ:<br />
                    <strong><a href="tel:{{\App\Common::phonedv()}}">{{\App\Common::phonedv()}}</a></strong>
                </h3>

                <div class="detail">
                    Dịch vụ tốt nhất từ Mitsubishi Motors Thái Bình phục vụ tối ưu cho chiếc xe của bạn. Chúng tôi duy trì tốt nhất chất lượng, độ tin cậy &amp; hiệu xuất hoạt động của xe. Hãy giữ gìn chiếc xe của bạn bằng việc luôn chọn
                    và sử dụng phụ dịch vụ từ Mitsubishi Motors Thái Bình .
                </div>
                <a href="{{url('dich-vu-hau-mai',['dat-lich-bao-duong'])}}" class="btn btn-red book-btn"> <i class="fa fa-calendar" aria-hidden="true"></i> Đặt lịch bảo dưỡng </a>
            </div>
        </div>
    </div>

    <div class="normal-block home-about-block">
        <div class="grid-inner">
            <h2 class="title-block">
                <span>Giới thiệu về đại lý</span>
            </h2>
            <div class="content">
                <div class="map">
                    <img class="img-full" src="{{asset('img/home/thaibinh_google_map.png')}}" alt="Map" />
                </div>
                <div class="detail" tabindex="0" style="outline: none; overflow: hidden;">
                    <div class="about">
                        <h2>MITSUBISHI THÁI BÌNH</h2>
                        <p>
                            Là nhà phân phối chính thức của Misubishi Motors,&nbsp;chúng tôi hân hạnh gởi đến quý khách hàng các sản phẩm của&nbsp;dòng xe&nbsp;
                            <a href="/">
                                <strong>Mitsubishi</strong>
                            </a>
                            &nbsp;nhập khẩu và lắp ráp như sau:&nbsp; <strong><a href="http://mitsubishi-thaibinh.vn/">Outlander</a>, </strong>&nbsp;<a href="http://mitsubishi-thaibinh.vn/">Mirage</a>,&nbsp;
                            <a href="http://mitsubishi-thaibinh.vn/">Attrage</a>,&nbsp; <a href="http://mitsubishi-thaibinh.vn/">Pajero&nbsp;Sport</a>,&nbsp; <a href="http://mitsubishi-thaibinh.vn/">Triton</a>,
                            <strong><a href="http://mitsubishi-thaibinh.vn/">Xpander</a></strong>
                        </p>
                        <p>
                            Được thành lập từ năm 2020, chúng tôi luôn nỗ lực nâng cao hình ảnh và chất lượng dịch vụ nhằm đem lại cho khách hàng những giá trị dịch vụ tốt nhất, uy tín và chuyên nghiệp. Với đội ngũ bán hàng và kỹ thuật
                            chuyên nghiệp, nhiều năm trong nghề, hệ thống showroom và xưởng sửa chữa bảo hành rộng rãi, trang bị theo tiêu chuẩn đại lý của <a href="http://mitsubishi-thaibinh.vn/">Mitsubishi&nbsp;Motors Việt Nam</a>.
                        </p>
                        <a href="{{url('gioi-thieu')}}"> <i class="fa fa-angle-double-right" aria-hidden="true"></i> Khám phá thêm </a>
                    </div>
                    <div class="address">
                        <h3>LIÊN HỆ:</h3>
                        <div class="list">
                            <div class="list-row">
                                <div class="list-col">Địa chỉ:</div>
                                <div class="list-col">140 Quang Trung, phường Quang Trung,TP Thái Bình</div>
                            </div>
                            <div class="list-row">
                                <div class="list-col">Website:</div>
                                <div class="list-col"><a href="http://mitsubishi-thaibinh.vn/">http://mitsubishi-thaibinh.vn/</a></div>
                            </div>
                            <div class="list-row">
                                <div class="list-col">Email:</div>
                                <div class="list-col"><a href="mailto:{{\App\Common::email()}}">{{\App\Common::email()}}</a></div>
                            </div>
                            <div class="list-row">
                                <div class="list-col">Điện thoại:</div>
                                <div class="list-col"><a href="tel:{{\App\Common::phonelh()}}">{{\App\Common::phonelh()}}</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="position: absolute; z-index: 1;">
                    <div class="enscroll-track vertical-track" style="height: 0px;">
                        <a href="/" class="vertical-handle">
                            <div class="top"></div>
                            <div class="bottom"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="normal-block home-intro-block">
        <div class="grid-inner">
            <h2 class="title-block">
                <span>Lý do chọn chúng tôi</span>
            </h2>
            <div class="content">
                <ul>
                    <li>
                        <img class="img-center" src="{{asset('img/home/intro-1.png')}}" alt="Intro 1" />
                        <h3>GIÁ ƯU ĐÃI - GIAO XE SỚM</h3>
                        <p>
                            Luôn cam kết mang lại mức giá ưu đãi nhất cho quý khách với thời gian giao xe nhanh nhất.
                        </p>
                    </li>
                    <li>
                        <img class="img-center" src="{{asset('img/home/intro-2.png')}}" alt="Intro 2" />
                        <h3>KHUYẾN MÃI TỐT NHẤT</h3>
                        <p>
                            Chúng tôi luôn cập nhật sớm nhất các chương trình khuyến mãi của hãng và đại lý nhằm mang đến lợi ích cao nhất cho khách hàng.
                        </p>
                    </li>
                    <li>
                        <img class="img-center" src="{{asset('img/home/intro-3.png')}}" alt="Intro 3" />
                        <h3>DỊCH VỤ HẬU MÃI CHU ĐÁO</h3>
                        <p>
                            Chúng tôi cam kết mang đến dịch vụ bảo dưỡng và sửa chữa theo tiêu chuẩn chất lượng của Mitsubishi trên toàn cầu.
                        </p>
                    </li>
                    <li>
                        <img class="img-center" src="{{asset('img/home/intro-4.png')}}" alt="Intro 4" />
                        <h3>LÁI THỬ TẬN NHÀ</h3>
                        <p>
                            Hỗ trợ quý khách dễ dàng trải nghiệm xe Mitsubishi mà không tốn thời gian.
                        </p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
@section("jsfooter")
    <script src="{{asset('js/home-page.js')}}"></script>
@endsection