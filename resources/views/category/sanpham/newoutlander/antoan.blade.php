<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>AN TOÀN VƯỢT TRỘI</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide100"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="AN TOÀN VƯỢT TRỘI – VỮNG BƯỚC THÀNH CÔNG" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> AN TOÀN VƯỢT TRỘI – VỮNG BƯỚC THÀNH CÔNG</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/An-toàn-vượt-trộ1.png')}}"
                                    alt="AN TOÀN VƯỢT TRỘI – VỮNG BƯỚC THÀNH CÔNG"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> AN TOÀN VƯỢT TRỘI – VỮNG BƯỚC THÀNH CÔNG</span></h3>
                                <div class="opContent">
                                    <p>
                                        Mitsubishi Outlander mới được nâng cập các tính năng an toàn vượt trội hơn và hiện đại hơn. Không chỉ mang đếm trải nghiệm vận hành êm ái, Outlander còn mang đến sự an tâm, tin cậy trong mọi
                                        hành trình của bạn, dù là di chuyển trong đô thị đông đúc hay là các chuyến đi đường xa.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide101" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống 7 túi khí an toàn" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Hệ thống 7 túi khí an toàn</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/Interior4.jpg')}}"
                                    alt="Hệ thống 7 túi khí an toàn"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Hệ thống 7 túi khí an toàn</span></h3>
                                <div class="opContent">
                                    <p>
                                        Trong trường hợp xảy ra va chạm, hệ thống 7 túi khí an toàn sẽ bảo vệ tất cả hành khách khỏi chấn thương do va đập mạnh. Hệ thống 7 túi khí bao gồm: 02 túi khí cho hành khách phía trước, 02
                                        túi khí bên cho hàng ghế trước, 02 túi khí rèm và 01 túi khí bảo vệ đầu gối người lái.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide102" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Phanh tay điện tử Auto Hold" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Phanh tay điện tử&nbsp;Auto Hold</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/AUtohold.png')}}"
                                    alt="Phanh tay điện tử Auto Hold"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Phanh tay điện tử&nbsp;Auto Hold</span></h3>
                                <div class="opContent">
                                    <p>
                                        Phanh tay điều khiển tự động thông minh, được thiết kế để giúp hạn chế việc người lái quên hạ hoặc kéo phanh tay. Bên cạnh đó hệ thống Auto Hold giúp người lái không phải&nbsp;liên tục đạp
                                        phanh khi dừng đèn đỏ.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide103" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống cảnh báo và giảm thiểu va chạm phía trước (FCM)" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Hệ thống cảnh báo và giảm thiểu va chạm phía trước (FCM)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/FCM1.png')}}"
                                    alt="Hệ thống cảnh báo và giảm thiểu va chạm phía trước (FCM)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Hệ thống cảnh báo và giảm thiểu va chạm phía trước (FCM)</span></h3>
                                <div class="opContent">
                                    <p>
                                        Bằng cách sử dụng camera và cảm biến radar, hệ thống FCM có thể phát hiện các vật cản phía trước xe để giúp hạn chế va chạm phía trước hoặc giảm thiểu thiệt hại trong trường hợp có va chạm
                                        không thể tránh khỏi.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide104" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống cảnh báo phương tiện cắt ngang khi lùi xe (RCTA)" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Hệ thống cảnh báo phương tiện cắt ngang khi lùi xe (RCTA)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/RCTA1.png')}}"
                                    alt="Hệ thống cảnh báo phương tiện cắt ngang khi lùi xe (RCTA)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Hệ thống cảnh báo phương tiện cắt ngang khi lùi xe (RCTA)</span></h3>
                                <div class="opContent">
                                    <p>Cải thiện khả năng phát hiện chướng ngại vật, giảm thiểu điểm mù và nguy cơ va chạm khi lùi xe.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide105" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống cảnh báo điểm mù (BSW) và Hỗ trợ chuyển làn (LCA)" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Hệ thống cảnh báo điểm mù (BSW) và Hỗ trợ chuyển làn (LCA)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/BSW1.png')}}"
                                    alt="Hệ thống cảnh báo điểm mù (BSW) và Hỗ trợ chuyển làn (LCA)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Hệ thống cảnh báo điểm mù (BSW) và Hỗ trợ chuyển làn (LCA)</span></h3>
                                <div class="opContent">
                                    <p>Nhận điện phương tiện trong vùng điểm mù. Cảnh báo trên gương chiếu hậu &amp; hỗ trợ cho người lái chuyển làn an toàn.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="6" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide106" data-slick-index="6" aria-hidden="true">
                        <a href="javascript:;" title="Hệ thống cân bằng điện tử (ASC)" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">6. Hệ thống cân bằng điện tử (ASC)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/ASC.png')}}"
                                    alt="Hệ thống cân bằng điện tử (ASC)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">6. Hệ thống cân bằng điện tử (ASC)</span></h3>
                                <div class="opContent">
                                    <p>Kiểm soát lực phanh, nâng cao sự ổn định trong điều kiện đường trơn trượt</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="7" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide107" data-slick-index="7" aria-hidden="true">
                        <a href="javascript:;" title="Hệ thống khởi hành ngang dốc (HSA)" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">7. Hệ thống khởi hành ngang dốc (HSA)</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/HSA.png')}}"
                                    alt="Hệ thống khởi hành ngang dốc (HSA)"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">7. Hệ thống khởi hành ngang dốc (HSA)</span></h3>
                                <div class="opContent">
                                    <p>
                                        Hệ thống hỗ trợ khởi hành ngang dốc (Hill Start Assist – HSA) giúp xe không bị trôi về phía sau trong trường hợp dừng và khởi hành ở ngang dốc cao khi người lái chuyển từ chân phanh sang chân
                                        ga.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="8" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide108" data-slick-index="8" aria-hidden="true">
                        <a href="javascript:;" title="Hệ thống phanh ABS- EBD- BA" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">8. Hệ thống phanh ABS- EBD- BA</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/ABS-ebd2.jpg.png')}}"
                                    alt="Hệ thống phanh ABS- EBD- BA"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">8. Hệ thống phanh ABS- EBD- BA</span></h3>
                                <div class="opContent">
                                    <p>Các hệ thống phanh an toàn ABS, EBD, BA kết hợp cùng 4 phanh đĩa mang lại khả năng an toàn chủ động vượt trội cho Outlander</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="9" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide109" data-slick-index="9" aria-hidden="true">
                        <a href="javascript:;" title="Khung xe RISE" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">9. Khung xe RISE</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newoutlander/khung-xe-rise.jpg')}}"
                                    alt="Khung xe RISE"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">9. Khung xe RISE</span></h3>
                                <div class="opContent">
                                    <p>Khung xe RISE mang lại khả năng bảo vệ tốt nhất cho hành khách trong trường hợp xảy ra va chạm nhờ sử dụng vật liệu thép tiên tiến giúp khung xe chắc chắn và cứng vững.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/An-toàn-vượt-trộ1.png')}}"
                        alt="AN TOÀN VƯỢT TRỘI – VỮNG BƯỚC THÀNH CÔNG"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">AN TOÀN VƯỢT TRỘI – VỮNG BƯỚC THÀNH CÔNG</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Mitsubishi Outlander mới được nâng cập các tính năng an toàn vượt trội hơn và hiện đại hơn. Không chỉ mang đếm trải nghiệm vận hành êm ái, Outlander còn mang đến sự an tâm, tin cậy trong mọi hành trình
                            của bạn, dù là di chuyển trong đô thị đông đúc hay là các chuyến đi đường xa.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/Interior4.jpg')}}"
                        alt="Hệ thống 7 túi khí an toàn"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống 7 túi khí an toàn</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Trong trường hợp xảy ra va chạm, hệ thống 7 túi khí an toàn sẽ bảo vệ tất cả hành khách khỏi chấn thương do va đập mạnh. Hệ thống 7 túi khí bao gồm: 02 túi khí cho hành khách phía trước, 02 túi khí bên
                            cho hàng ghế trước, 02 túi khí rèm và 01 túi khí bảo vệ đầu gối người lái.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/AUtohold.png')}}"
                        alt="Phanh tay điện tử Auto Hold"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Phanh tay điện tử&nbsp;Auto Hold</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Phanh tay điều khiển tự động thông minh, được thiết kế để giúp hạn chế việc người lái quên hạ hoặc kéo phanh tay. Bên cạnh đó hệ thống Auto Hold giúp người lái không phải&nbsp;liên tục đạp phanh khi dừng
                            đèn đỏ.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/FCM1.png')}}"
                        alt="Hệ thống cảnh báo và giảm thiểu va chạm phía trước (FCM)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống cảnh báo và giảm thiểu va chạm phía trước (FCM)</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Bằng cách sử dụng camera và cảm biến radar, hệ thống FCM có thể phát hiện các vật cản phía trước xe để giúp hạn chế va chạm phía trước hoặc giảm thiểu thiệt hại trong trường hợp có va chạm không thể tránh
                            khỏi.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/RCTA1.png')}}"
                        alt="Hệ thống cảnh báo phương tiện cắt ngang khi lùi xe (RCTA)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống cảnh báo phương tiện cắt ngang khi lùi xe (RCTA)</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Cải thiện khả năng phát hiện chướng ngại vật, giảm thiểu điểm mù và nguy cơ va chạm khi lùi xe.</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/BSW1.png')}}"
                        alt="Hệ thống cảnh báo điểm mù (BSW) và Hỗ trợ chuyển làn (LCA)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống cảnh báo điểm mù (BSW) và Hỗ trợ chuyển làn (LCA)</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Nhận điện phương tiện trong vùng điểm mù. Cảnh báo trên gương chiếu hậu &amp; hỗ trợ cho người lái chuyển làn an toàn.</p>
                    </div>
                </div>
            </div>
            <div data-item="6" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/ASC.png')}}"
                        alt="Hệ thống cân bằng điện tử (ASC)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống cân bằng điện tử (ASC)</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Kiểm soát lực phanh, nâng cao sự ổn định trong điều kiện đường trơn trượt</p>
                    </div>
                </div>
            </div>
            <div data-item="7" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/HSA.png')}}"
                        alt="Hệ thống khởi hành ngang dốc (HSA)"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống khởi hành ngang dốc (HSA)</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Hệ thống hỗ trợ khởi hành ngang dốc (Hill Start Assist – HSA) giúp xe không bị trôi về phía sau trong trường hợp dừng và khởi hành ở ngang dốc cao khi người lái chuyển từ chân phanh sang chân ga.</p>
                    </div>
                </div>
            </div>
            <div data-item="8" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/ABS-ebd2.jpg.png')}}"
                        alt="Hệ thống phanh ABS- EBD- BA"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống phanh ABS- EBD- BA</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Các hệ thống phanh an toàn ABS, EBD, BA kết hợp cùng 4 phanh đĩa mang lại khả năng an toàn chủ động vượt trội cho Outlander</p>
                    </div>
                </div>
            </div>
            <div data-item="9" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newoutlander/khung-xe-rise.jpg')}}"
                        alt="Khung xe RISE"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khung xe RISE</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Khung xe RISE mang lại khả năng bảo vệ tốt nhất cho hành khách trong trường hợp xảy ra va chạm nhờ sử dụng vật liệu thép tiên tiến giúp khung xe chắc chắn và cứng vững.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>