<!--Navigation-->

<div class="pr-navigation">
    <div class="pr-menu clearfix" style="transform: matrix(1, 0, 0, 1, 0, 0);">
        <a href="https://www.mitsubishiquangninh.com/" class="home">
            <span class="icon"><i class="svg-icon icon-home"></i></span>
        </a>
        <a href="javascript:;" class="top"> <span>Đầu trang</span><i class="fa fa-caret-up"></i> </a>
        <!-- Dropdown-->
        <div class="dropdown">
            <button type="button" id="dropdownMenuPr" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <div class="dd-text">
                    <span class="icon"><i class="svg-icon icon-star smoke"></i></span><span class="text">Đặc điểm nổi bật</span>
                </div>
                <div class="caret"><i class="fa fa-caret-down"></i></div>
            </button>
            <ul aria-labelledby="dropdownMenuPr" class="dropdown-menu">
                <li class="dd-logo hidden-xs hidden-sm">
                    <img
                        src="{{asset('img/new-xpander/Logo-Xpander-nho.png')}}"
                        alt="New Xpander"
                    />
                </li>
                <li>
                    <a href="javascript:;" data-section=".dacdiemnoibat" data-tracking-click="Feature" data-tracking-click-cat="New Xpander">
                        <span class="icon"><i class="svg-icon icon-star smoke"></i></span><span class="text">Đặc điểm nổi bật</span>
                    </a>
                </li>

                <li>
                    <a href="javascript:;" data-section=".thongsokythuat" data-tracking-click="Specification" data-tracking-click-cat="New Xpander">
                        <span class="icon"><i class="svg-icon icon-setting smoke"></i></span><span class="text">Thông số, phiên bản &amp; giá</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" data-section=".thietke" data-tracking-click="Design" data-tracking-click-cat="New Xpander">
                        <span class="icon"><i class="svg-icon icon-function smoke"></i></span><span class="text">Thiết kế &amp; tính năng</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" data-section=".thuvien" data-tracking-click="Gallery" data-tracking-click-cat="New Xpander">
                        <span class="icon"><i class="svg-icon icon-gallery smoke"></i></span><span class="text">Thư viện hình ảnh &amp; Catalogue</span>
                    </a>
                </li>
                <li>
                    <a href="javascript:;" data-section=".phukien" data-tracking-click="Accessory" data-tracking-click-cat="New Xpander">
                        <span class="icon"><i class="svg-icon icon-accessories smoke"></i></span><span class="text">Phụ kiện</span>
                    </a>
                </li>
            </ul>
            <!-- Dropdown menu-->
            <!-- End - Dropdown menu-->
            <!--===================-->
        </div>
        <!-- End - Dropdown-->
        <!--===================-->
    </div>
</div>

<!--End - Navigation-->