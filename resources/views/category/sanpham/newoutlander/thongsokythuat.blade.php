<!--Specs-->
<div class="normal-block thongsokythuat">
    <div class="grid-inner">
        <h3 class="title-block pr-title-block">
            <span>Thông số kỹ thuật</span>
        </h3>
        <div class="row">
            <div class="col-md-6">
                <div class="tab-content">
                    <div id="cl360" role="tabpanel" class="tab-pane fade">
                        <div
                            id="gl_360"
                            data-imgarray='[                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Angle-1.2.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Angle-2.21.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Angle-4.2.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Angle-6.2.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Angle-8.2.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Angle-7.2.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Angle-5.2.jpg",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/06/Angle-3.2.jpg"]'
                            class="gl-360"
                        >
                            <div class="threesixty car">
                                <div class="spinner"><span>0%</span></div>
                                <ol class="threesixty_images"></ol>
                            </div>
                            <img
                                src="{{asset('img/newoutlander/line-360.png')}}"
                                alt="line-360.png"
                                class="gl360-line"
                            />
                            <div class="gl360-direc">
                                <a class="gl360-btn-prev">
                                    <i class="fa fa-caret-left"></i>
                                </a>
                                <span class="gl360-title">360<sup>o</sup></span>
                                <a class="gl360-btn-next">
                                    <i class="fa fa-caret-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--#cl360-->

                    <div id="cl1" role="tabpanel" class="fade tab-pane active">
                        <img
                            src="{{asset('img/newoutlander/red.jpg')}}"
                            alt="New Outlander"
                        />
                    </div>
                    <div id="cl2" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/newoutlander/car-choose-1.png')}}"
                            alt="New Outlander"
                        />
                    </div>
                    <div id="cl3" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/newoutlander/white1.jpg')}}"
                            alt="New Outlander"
                        />
                    </div>
                    <div id="cl4" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/newoutlander/brown.jpg')}}"
                            alt="New Outlander"
                        />
                    </div>
                    <div id="cl5" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/newoutlander/titanium.jpg')}}"
                            alt="New Outlander"
                        />
                    </div>
                    <!--.tab-pane-->
                </div>
                <div class="car-color-note text-center">
                    <span>Ghi chú: Hình ảnh minh họa có thể khác với thực tế.</span>
                </div>
                <!--.tab-content-->
                <ul role="tablist" class="nav nav-tabs">
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-outlander#cl360" class="vehicle-color" aria-controls="cl360" role="tab" data-toggle="tab">
                            <span class="text">360</span>
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-outlander#cl1" class="vehicle-color" aria-controls="cl1 tab" data-toggle="tab" style="background-color: #9d333b;">
                            <span class="color-name">Đỏ</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-outlander#cl2" class="vehicle-color" aria-controls="cl2 tab" data-toggle="tab" style="background-color: #292828;">
                            <span class="color-name">Đen</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-outlander#cl3" class="vehicle-color" aria-controls="cl3 tab" data-toggle="tab" style="background-color: #ffffff;">
                            <span class="color-name">Trắng</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-outlander#cl4" class="vehicle-color" aria-controls="cl4 tab" data-toggle="tab" style="background-color: #59554d;">
                            <span class="color-name">Nâu</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-outlander#cl5" class="vehicle-color" aria-controls="cl5 tab" data-toggle="tab" style="background-color: #6f7274;">
                            <span class="color-name">Xám</span>
                        </a>
                    </li>
                </ul>
                <!--ul.nav-tabs-->
            </div>
            <div class="col-md-6">
                <div class="phienban">
                    <div class="phienban-title">Phiên bản:</div>
                    <div class="phienban-select">
                        <ul>
                            <li class="active"><a href="javascript:;" data-slide="0" data-tracking-click="Version 2.0 CVT" data-tracking-click-cat="New Outlander">2.0 CVT</a></li>
                            <li class=""><a href="javascript:;" data-slide="1" data-tracking-click="Version 2.0 CVT Premium" data-tracking-click-cat="New Outlander">2.0 CVT Premium</a></li>
                            <li class=""><a href="javascript:;" data-slide="2" data-tracking-click="Version 2.4 CVT Premium" data-tracking-click-cat="New Outlander">2.4 CVT Premium</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row hidden-md hidden-lg">
                    <div class="col-xs-12 phienban-note">Vuốt ngang để chọn và so sánh giữa các phiên bản</div>
                </div>
                <div class="row hidden-xs hidden-sm">
                    <div class="col-xs-12 phienban-note"><a href="javascript:;" data-href="#product_specs" class="view-detail modal-open"></a></div>
                </div>
                <div class="phienban-slide">
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['CVT_2']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification 2.0 CVT" data-tracking-click-cat="New Outlander">Thông số kỹ thuật</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">4.695 x 1.810 x 1.710</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Chiều dài cơ sở (mm)</div>
                                        <div class="col-md-6 phienban-content-right">2.670</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Chiều rộng cơ sở trước/sau (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.540/1.540</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,3</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">190</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (Kg)</div>
                                        <div class="col-md-6 phienban-content-right">1.500</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi</div>
                                        <div class="col-md-6 phienban-content-right">7 người</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">4B11 DOHC MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung Tích Xylanh (cc)</div>
                                        <div class="col-md-6 phienban-content-right">1.998</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">145/6.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">196/4.200</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">63</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">Số tự động vô cấp (CVT) INVECS III</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">Cầu trước</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Trợ lực điện</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">Kiểu MacPherson với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Đa liên kết với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">225/55R18</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa thông gió/Đĩa</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mức tiêu hao nhiên liệu (L/100Km)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Kết hợp</i></div>
                                        <div class="col-md-6 phienban-content-right">7,2</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Trong đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">9,7</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Ngoài đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">5,8</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Outlander"
                                class="view-detail modal-open"
                                tabindex="0"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment 2.0 CVT" data-tracking-click-cat="New Outlander">Trang bị tiêu chuẩn</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu xa </i></div>
                                        <div class="col-md-6 phienban-content-right">Clear Halogen</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu gần </i></div>
                                        <div class="col-md-6 phienban-content-right">Halogen &amp; Projector</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn pha điều chỉnh được độ cao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn LED chiếu sáng ban ngày</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến đèn pha và gạt mưa tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống rửa đèn</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn báo phanh thứ ba</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh điện/gập điện, tích hợp đèn báo rẽ và chức năng sưởi</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa sau đóng mở bằng điện</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa ngoài</div>
                                        <div class="col-md-6 phienban-content-right">Mạ crôm</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lưới tản nhiệt</div>
                                        <div class="col-md-6 phienban-content-right">Mạ crôm</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa màu sậm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính trước</div>
                                        <div class="col-md-6 phienban-content-right">Tốc độ thay đổi theo vận tốc xe</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính sau và sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm đúc hợp kim</div>
                                        <div class="col-md-6 phienban-content-right">18"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Anten vây cá</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Giá đỡ hành lý trên mui xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THÂT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Nút điều khiển âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điện thoại rảnh tay trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lẫy sang số trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa nhiệt độ tự động</div>
                                        <div class="col-md-6 phienban-content-right">Hai vùng nhiệt độ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Nỉ cao cấp</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay 6 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống sưởi ấm hàng ghế trước</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ hai gập 60:40</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ ba gập 50:50</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa sổ trời</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa trong mạ crôm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Móc gắn ghế an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn chiếu sáng hộp để đồ trung tâm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tấm ngăn khoang hành lý</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống âm thanh</div>
                                        <div class="col-md-6 phienban-content-right">Bluetooth/USB/AUX/AM/FM - Apple CarPlay &amp; Android Auto</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">6</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ổ cắm điện phía sau xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí an toàn</div>
                                        <div class="col-md-6 phienban-content-right">Túi khí đôi</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động</div>
                                        <div class="col-md-6 phienban-content-right">Hàng ghế trước</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử EBD</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp BA</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh tay điện tử &amp; chức năng giữ phanh tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử (ASC)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giảm thiểu va chạm phía trước (FCM)</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo điểm mù (BSW)</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo phương tiện cắt ngang khi lùi xe (RCTA)</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát chân ga khi phanh</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh và khởi động bằng nút bấm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chức năng chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Camera lùi</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến lùi</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Outlander"
                                class="view-detail modal-open"
                                tabindex="0"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['CVT_2_P']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification 2.0 CVT Premium" data-tracking-click-cat="New Outlander">Thông số kỹ thuật</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">4.695 x 1.810 x 1.710</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Chiều dài cơ sở (mm)</div>
                                        <div class="col-md-6 phienban-content-right">2.670</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Chiều rộng cơ sở trước/sau (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.540/1.540</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,3</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">190</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (Kg)</div>
                                        <div class="col-md-6 phienban-content-right">1.535</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi</div>
                                        <div class="col-md-6 phienban-content-right">7 người</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">4B11 DOHC MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung Tích Xylanh (cc)</div>
                                        <div class="col-md-6 phienban-content-right">1.998</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">145/6.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">196/4.200</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">63</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">Số tự động vô cấp (CVT) INVECS III</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">Cầu trước</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Trợ lực điện</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">Kiểu MacPherson với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Đa liên kết với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">225/55R18</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa thông gió/Đĩa</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mức tiêu hao nhiên liệu (L/100Km)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Kết hợp</i></div>
                                        <div class="col-md-6 phienban-content-right">7,2</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Trong đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">9,7</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Ngoài đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">5,8</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Outlander"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment 2.0 CVT Premium" data-tracking-click-cat="New Outlander">Trang bị tiêu chuẩn</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu xa </i></div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu gần </i></div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn pha điều chỉnh được độ cao</div>
                                        <div class="col-md-6 phienban-content-right">Tự động</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn LED chiếu sáng ban ngày</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến đèn pha và gạt mưa tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống rửa đèn</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn báo phanh thứ ba</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh điện/gập điện, tích hợp đèn báo rẽ và chức năng sưởi</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa sau đóng mở bằng điện</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa ngoài</div>
                                        <div class="col-md-6 phienban-content-right">Mạ crôm</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lưới tản nhiệt</div>
                                        <div class="col-md-6 phienban-content-right">Mạ crôm</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa màu sậm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính trước</div>
                                        <div class="col-md-6 phienban-content-right">Tự động</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính sau và sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm đúc hợp kim</div>
                                        <div class="col-md-6 phienban-content-right">18"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Anten vây cá</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Giá đỡ hành lý trên mui xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THÂT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Nút điều khiển âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điện thoại rảnh tay trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lẫy sang số trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa nhiệt độ tự động</div>
                                        <div class="col-md-6 phienban-content-right">Hai vùng nhiệt độ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Da</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh điện 10 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống sưởi ấm hàng ghế trước</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ hai gập 60:40</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ ba gập 50:50</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa sổ trời</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa trong mạ crôm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Móc gắn ghế an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn chiếu sáng hộp để đồ trung tâm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tấm ngăn khoang hành lý</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống âm thanh</div>
                                        <div class="col-md-6 phienban-content-right">Bluetooth/USB/AUX/AM/FM - Apple CarPlay &amp; Android Auto</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">6</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ổ cắm điện phía sau xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí an toàn</div>
                                        <div class="col-md-6 phienban-content-right">7 túi khí an toàn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động</div>
                                        <div class="col-md-6 phienban-content-right">Hàng ghế trước</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử EBD</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp BA</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh tay điện tử &amp; chức năng giữ phanh tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử (ASC)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giảm thiểu va chạm phía trước (FCM)</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo điểm mù (BSW)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo phương tiện cắt ngang khi lùi xe (RCTA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát chân ga khi phanh</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh và khởi động bằng nút bấm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chức năng chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Camera lùi</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến lùi</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Outlander"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['CVT_24_P']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification 2.4 CVT Premium" data-tracking-click-cat="New Outlander">Thông số kỹ thuật</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">4.695 x 1.810 x 1.710</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai cầu xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">2.670</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng cách hai bánh xe trước/sau (mm)</div>
                                        <div class="col-md-6 phienban-content-right">1.540/1.540</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">5,3</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">190</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (Kg)</div>
                                        <div class="col-md-6 phienban-content-right">1.610</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi</div>
                                        <div class="col-md-6 phienban-content-right">7 người</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">4B12 DOHC MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung Tích Xylanh (cc)</div>
                                        <div class="col-md-6 phienban-content-right">2.360</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">167/6.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">222/4.100</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">60</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">Số tự động vô cấp (CVT) INVECS III</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Truyền động</div>
                                        <div class="col-md-6 phienban-content-right">Hai cầu 4WD</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trợ lực lái</div>
                                        <div class="col-md-6 phienban-content-right">Trợ lực điện</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">Kiểu MacPherson với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Đa liên kết với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lốp xe trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">225/55R18</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa thông gió/Đĩa</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mức tiêu hao nhiên liệu (L/100Km)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Kết hợp</i></div>
                                        <div class="col-md-6 phienban-content-right">7,7</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Trong đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">10,3</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Ngoài đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">6,2</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Outlander"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment 2.4 CVT Premium" data-tracking-click-cat="New Outlander">Trang bị tiêu chuẩn</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu xa </i></div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu gần </i></div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Đèn pha điều chỉnh được độ cao</div>
                                        <div class="col-md-6 phienban-content-right">Tự động</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn LED chiếu sáng ban ngày</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến đèn pha và gạt mưa tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù trước/sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống rửa đèn</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn báo phanh thứ ba</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh điện/gập điện, tích hợp đèn báo rẽ và chức năng sưởi</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa sau đóng mở bằng điện</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa ngoài</div>
                                        <div class="col-md-6 phienban-content-right">Mạ crôm</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lưới tản nhiệt</div>
                                        <div class="col-md-6 phienban-content-right">Mạ crôm</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa màu sậm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính trước</div>
                                        <div class="col-md-6 phienban-content-right">Tự động</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt nước kính sau và sưởi kính sau</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm đúc hợp kim</div>
                                        <div class="col-md-6 phienban-content-right">18"</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Anten vây cá</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Giá đỡ hành lý trên mui xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THÂT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Nút điều khiển âm thanh trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điện thoại rảnh tay trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát hành trình</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lẫy sang số trên vô lăng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa nhiệt độ tự động</div>
                                        <div class="col-md-6 phienban-content-right">Hai vùng nhiệt độ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Da</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh điện 10 hướng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống sưởi ấm hàng ghế trước</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ hai gập 60:40</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hàng ghế thứ ba gập 50:50</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cửa sổ trời</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa trong mạ crôm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Móc gắn ghế an toàn trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn chiếu sáng hộp để đồ trung tâm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tấm ngăn khoang hành lý</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống âm thanh</div>
                                        <div class="col-md-6 phienban-content-right">Bluetooth/USB/AUX/AM/FM - Apple CarPlay &amp; Android Auto</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số lượng loa</div>
                                        <div class="col-md-6 phienban-content-right">6</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ổ cắm điện phía sau xe</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Túi khí an toàn</div>
                                        <div class="col-md-6 phienban-content-right">7 túi khí an toàn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cơ cấu căng đai tự động</div>
                                        <div class="col-md-6 phienban-content-right">Hàng ghế trước</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh (ABS)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử EBD</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống trợ lực phanh khẩn cấp BA</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh tay điện tử</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cân bằng điện tử (ASC)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khởi hành ngang dốc (HSA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống kiểm soát chân ga khi phanh</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống giảm thiểu va chạm phía trước (FCM)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh và khởi động bằng nút bấm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo điểm mù (BSW)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống cảnh báo phương tiện cắt ngang khi lùi xe (RCTA)</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chức năng chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Camera lùi</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cảm biến lùi</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Outlander"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                </div>
                <!--.phienban-slide-->
                <div class="phienban-btn">
                    <ul>
                        <li>
                            <a
                                href="javascript:;"
                                data-href="#product_estimate"
                                class="btn btn-icon-l modal-open d2-estimate"
                                data-car-id="121"
                                data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                data-step="1"
                                data-tracking-click="Estimated Cost"
                                data-tracking-click-cat="New Outlander"
                            >
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-cost"></i></span><span class="text">Dự tính chi phí</span>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.mitsubishiquangninh.com/mua-xe/bao-gia-chi-tiet/121/"
                                class="btn btn-icon-l d2-estimate"
                                data-car-id="121"
                                data-tracking-click="Price Quotation By Dealer"
                                data-tracking-click-cat="New Outlander"
                                data-step="2"
                            >
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-price"></i></span><span class="text">Yêu cầu báo giá</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mitsubishiquangninh.com/mua-xe/dang-ky-lai-thu/121/" class="btn btn-icon-l">
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-drive"></i></span><span class="text">Đăng ký lái thử</span>
                            </a>
                        </li>
                        <li style="display: none;">
                            <a href="https://www.mitsubishiquangninh.com/dai-ly/" class="btn btn-icon-l">
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-dealer"></i></span><span class="text">Tìm nhà phân phối</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--.chonphienban-->
            </div>
        </div>
    </div>
</div>
<!--End - Specs-->