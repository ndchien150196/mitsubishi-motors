@extends("master")
@section("content") 
<div class="second-block page-news-home news-page">
    <div class="grid-fluid">
        <h3 class="title-block">
            <span>Mua xe trả góp</span>
        </h3>
        <div class="grid-content">
            <div class="row">
                <div class="col-md-8">
                    <ul class="customer-list">
                        <li class="item active" data-tab="doanh_nghiep">
                            <span class="customer-icon">
                                <img src="{{asset('img/tragop/icon-doanh-nghiep.png')}}" alt="Khách hàng doanh nghiệp" />
                            </span>
                            <span class="hidden-md hidden-lg customer-text">
                                Khách hàng <br />
                                doanh nghiệp
                            </span>
                            <span class="hidden-xs hidden-sm customer-text">Khách hàng doanh nghiệp</span>
                        </li>
                        <li class="item" data-tab="ca_nhan">
                            <span class="customer-icon">
                                <img src="{{asset('img/tragop/icon-ca-nhan.png')}}" alt="Khách hàng cá nhân" />
                            </span>
                            <span class="hidden-md hidden-lg customer-text">
                                Khách hàng <br />
                                cá nhân
                            </span>
                            <span class="hidden-xs hidden-sm customer-text">Khách hàng cá nhân</span>
                        </li>
                    </ul>
                    <div class="editor">
                        <div id="doanh_nghiep" class="content-wrap active">
                            <div class="news-link-title">Công ty hoặc doanh nghiệp mua xe qua ngân hàng</div>

                            <div class="wrap">
                                <div class="wrap-head">Các hồ sơ cần chuẩn bị</div>
                                <ul class="content-list">
                                    <li>Đăng ký kinh doanh, Mẫu dấu, Mã số thuế</li>
                                    <li>Hợp đồng mua xe</li>
                                    <li>Báo cáo tài chính nộp thuế năm gần nhất có lãi (có dấu của Thuế)</li>
                                    <li>Tờ khai quyết toán thuế thu nhập doanh nghiệp năm gần nhất (có dấu của Thuế)</li>
                                    <li>Bộ tờ khai VAT 12 tháng gần nhất bao gồm cả đầu ra và đầu vào (có dấu của Thuế)</li>
                                    <li>Hóa đơn điện thoại cố định của Công ty tháng gần nhất</li>
                                    <li>Điều lệ Công ty</li>
                                    <li>Biên bản họp đại hội cổ đông bầu hội đồng quản trị (mẫu Ngân hàng)</li>
                                    <li>Biên bản họp hội đồng quản trị về việc vay vốn mua xe trả góp (mẫu Ngân hàng)</li>
                                    <li>Đơn vay (mẫu Ngân hàng).</li>
                                </ul>
                            </div>
                            <div class="wrap">
                                <div class="wrap-head">Chi tiết khoan vay</div>
                                <ul class="content-list">
                                    <li>Mức tài trợ thông thường 70% giá trị hóa đơn.</li>
                                    <li>Thời gian tối đa 60 tháng.</li>
                                    <li>Lãi suất: Tính vào thời điểm giải ngân và theo lãi suất của Ngân hàng nhà nước quy định.</li>
                                    <li>Thời gian thẩm định: Trong vòng 24 giờ sau khi nhận đầy đủ hồ sơ vay.</li>
                                    <li>Chi phí Thủ tục hành chánh tại ngân hàng gồm: phí đảm bảo tài sản, phí mở tài khoản, phí công chứng sao y, cà vẹt xe, mua bảo hiểm thân xe 1,5% trị giá xe trong thời gian vay.</li>
                                </ul>
                            </div>
                            <div class="wrap">
                                <div class="wrap-head important red">Trường hợp mua xe qua công ty cho thuê tài chính</div>
                                <div class="wrap-head">Các hồ sơ cần chuẩn bị</div>
                                <ul class="content-list">
                                    <li>Đăng ký kinh doanh, Mẫu dấu, Mã số thuế</li>
                                    <li>Hợp đồng mua xe</li>
                                    <li>Báo cáo tài chính trong 03 năm gần nhất (bảng cân đối kế toán, báo cáo kết quả hoạt động sản xuất kinh doanh, thuyết minh báo cáo tài chính, báo cáo lưu chuyển tiền tệ,…) (Bản chính hoặc sao y)</li>
                                    <li>Danh mục tài sản cố định (nguyên giá, giá trị còn lại) (Bản chính hoặc Sao y).</li>
                                    <li>Chi tiết các khoản phải thu, phải trả, hàng tồn kho (Bản chính hoặc sao y)</li>
                                    <li>Danh mục các khách hàng lớn của doanh nghiệp (Bản chính)</li>
                                    <li>Một số hợp đồng kinh tế liên quan đến đầu vào-đầu ra của doanh nghiệp (Bản chính hoặc Sao y).</li>
                                </ul>
                            </div>
                        </div>
                        <div id="ca_nhan" class="content-wrap">
                            <div class="wrap">
                                <div class="wrap-head red">Quy trình mua xe trả góp</div>
                                <ul class="content-list">
                                    <li>Khách hàng chuẩn bị đầy đủ hồ sơ theo hướng dẫn của nhân viên tín dụng.</li>
                                    <li>Nhân viên tín dụng thẩm định hồ sơ vay vốn.</li>
                                    <li>Sau khi có giấy tài trợ tín dụng và bộ hồ sơ xe.</li>
                                    <li>Khách hàng tiến hành tới đại lý Mitsubishi đóng tiền (tiền đối ứng) và tiến hành thủ tục làm đăng ký xe.</li>
                                    <li>Khi có biển số xe và có giấy hẹn lấy đăng ký xe, khách hàng tới Ngân hàng ký hợp đồng tín dụng và nhận nợ với ngân hàng</li>
                                    <li>Khi Ngân hàng chuyển tiền vay ôtô của khách hàng (Số tiền còn lại của Hợp đồng) vào tài khoản của Mitsubishi, khách hàng mang theo CMTND và làm thủ tục nhận xe.</li>
                                </ul>
                            </div>
                            <div class="wrap">
                                <div class="wrap-head red">Chi tiết khoản vay</div>
                                <ul class="content-list">
                                    <li>Mức tài trợ thông thường 70% giá trị hóa đơn mua xe.</li>
                                    <li>Thời gian vay tối đa 60 tháng.</li>
                                    <li>Lãi suất: Tính vào thời điểm giải ngân và theo lãi suất của Ngân hàng nhà nước quy định.</li>
                                    <li>Thời gian thẩm định: Trong vòng 24 giờ sau khi nhận đầy đủ hồ sơ vay.</li>
                                    <li>Chi phí Thủ tục hành chánh tại ngân hàng gồm: phí đảm bảo tài sản, phí mở tài khoản, phí công chứng sao y, cà vẹt xe, mua bảo hiểm thân xe 1,5% trị giá xe trong thời gian vay.</li>
                                </ul>
                            </div>
                            <div class="wrap">
                                <div class="wrap-head red">Cá nhân mua xe - Hồ sơ cần chuẩn bị</div>
                                <ul class="content-list">
                                    <li>Bản sao hộ khẩu</li>
                                    <li>Bản sao CMND (cả hai vợ chồng nếu đã kết hôn).</li>
                                    <li>Giấy chứng nhận độc thân hoặc giấy đăng ký kết hôn.</li>
                                    <li>Hợp đồng mua xe</li>
                                    <li>
                                        Chứng minh nguồn thu nhập từ:
                                        <ul class="sub-list">
                                            <li>Lương trả qua tài khoản Ngân hàng</li>
                                            <li>Lương tiền mặt có BHXH hoặc có trích nộp TNCN,</li>
                                            <li>Đích danh góp vốn vào Công ty (có tên trên ĐKKD)</li>
                                            <li>Hộ kinh doanh cá thể (Có đóng thuế môn bài, thuế khoán)</li>
                                            <li>Nguồn thu khác như: Có nhà cho thuê, có máy móc cho thuê…</li>
                                        </ul>
                                    </li>
                                    <li>Đơn vay (theo mẫu của Ngân hàng ).</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!--  Sidebar -->
                <div class="col-md-4">
                    <div class="news-sidebar">
                        <div class="sidebar-block link-block">
                            <h3 class="news-link-title">Tư vấn mua xe</h3>
                            <!-- menu-->
                            <ul class="news-link">
                                <li class="">
                                    <a href="{{url('mua-xe',['bao-gia-chi-tiet'])}}" title="Báo giá chi tiết" class="sp-text">Báo giá chi tiết</a>
                                </li>
                                <li class="active">
                                    <a href="{{url('mua-xe',['mua-xe-tra-gop'])}}" title="Mua xe trả góp" class="sp-text">Mua xe trả góp</a>
                                </li>
                                <li class="">
                                    <a href="{{url('mua-xe',['quy-trinh-mua-xe'])}}" title="Quy trình mua xe" class="sp-text">Quy trình mua xe</a>
                                </li>
                                <li class="">
                                    <a href="{{url('mua-xe',['dang-ky-lai-thu'])}}" title="Đăng ký lái thử" class="sp-text">Đăng ký lái thử</a>
                                </li>
                            </ul>
                        </div>
                        @include('//layouts/block-km')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section("jsfooter")
    <script src="{{asset('js/sales-page.js')}}"></script>
@endsection