<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>VẬN HÀNH</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide100"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="UY MÃNH VƯỢT ĐỊA HÌNH" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> UY MÃNH VƯỢT ĐỊA HÌNH</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-performance.jpg')}}"
                                    alt="UY MÃNH VƯỢT ĐỊA HÌNH"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> UY MÃNH VƯỢT ĐỊA HÌNH</span></h3>
                                <div class="opContent">
                                    <p>Kế thừa khả năng vận hành đỉnh cao mang đậm “chất Mitsubishi”, Triton mới mang lại khả năng vượt địa hình ấn tượng.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide101" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Động cơ Diesel MIVEC 2.4L" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Động cơ Diesel MIVEC 2.4L</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-engine.jpg')}}"
                                    alt="Động cơ Diesel MIVEC 2.4L"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Động cơ Diesel MIVEC 2.4L</span></h3>
                                <div class="opContent">
                                    <p>
                                        Động cơ Diesel MIVEC 2.4L bằng nhôm giúp giảm trọng lượng, tăng công suất và mô-men, tiết kiệm nhiên liệu với&nbsp;công nghệ điều khiển van bằng điện tử áp dụng duy nhất trên phân khúc
                                        pick-up.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide102" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Hộp số 6 cấp kết hợp lẫy chuyển số trên vô lăng" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Hộp số 6 cấp kết hợp lẫy chuyển số trên vô lăng</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-Tranmission.jpg')}}"
                                    alt="Hộp số 6 cấp kết hợp lẫy chuyển số trên vô lăng"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Hộp số 6 cấp kết hợp lẫy chuyển số trên vô lăng</span></h3>
                                <div class="opContent">
                                    <p>
                                        Hộp số 6 cấp chuyển số mượt mà và êm ái, kết hợp lẫy chuyển số trên vô lăng duy nhất trong phân khúc mang đến cảm giác điều khiển như trên một chiếc xe thể thao thực thụ và cho khả năng đi đèo
                                        dốc tốt hơn
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide103" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Khóa vi sai cầu sau" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Khóa vi sai cầu sau</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/Untitled-3.png')}}"
                                    alt="Khóa vi sai cầu sau"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Khóa vi sai cầu sau</span></h3>
                                <div class="opContent">
                                    <p>Khóa vi sai cầu sau điều khiển bằng điện tử. Hạn chế 2 bánh sau bị quay trơn, đảm bảo lực kéo được truyền đến 2 bánh sau, nâng cao khả năng vượt địa hình của xe.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide104" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Hệ thống gài cầu Super Select 4WD-II" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Hệ thống gài cầu Super Select 4WD-II</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-superselect.jpg')}}"
                                    alt="Hệ thống gài cầu Super Select 4WD-II"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Hệ thống gài cầu Super Select 4WD-II</span></h3>
                                <div class="opContent">
                                    <p>Hệ thống truyền động trứ danh Super Select II với 4 chế độ vận hành 2H-4H-4HLc-4LLc cùng nút chuyển cầu điện tử và vi sai trung tâm tăng khả năng vượt địa hình</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide105" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="4 chế độ chạy địa hình" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. 4 chế độ chạy địa hình</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-Offroad.jpg')}}"
                                    alt="4 chế độ chạy địa hình"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li>
                                            <span>4X4<small class="small-text">AT MIVEC</small></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. 4 chế độ chạy địa hình</span></h3>
                                <div class="opContent">
                                    <p>Tính năng lựa chọn địa hình (Off Road Mode) hỗ trợ tăng cường lực kéo khi chạy trên những địa hình phức tạp như sỏi, bùn, cát hay đá</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="6" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide106" data-slick-index="6" aria-hidden="true">
                        <a href="javascript:;" title="Vi sai trung tâm duy nhất trong phân khúc" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">6. Vi sai trung tâm duy nhất trong phân khúc</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-4h.jpg')}}"
                                    alt="Vi sai trung tâm duy nhất trong phân khúc"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">6. Vi sai trung tâm duy nhất trong phân khúc</span></h3>
                                <div class="opContent">
                                    <p>
                                        Vi sai trung tâm duy nhất phân khúc&nbsp;cho khả năng chuyển cầu từ 2H sang 4H khi xe đang di chuyển đến tốc độ 100Km/h phù hợp với mặt đường trơn trượt,&nbsp;ưu việt như dòng xe dẫn động toàn
                                        thời gian AWD.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="7" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide107" data-slick-index="7" aria-hidden="true">
                        <a href="javascript:;" title="Khả năng vượt địa hình vượt trội" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">7. Khả năng vượt địa hình vượt trội</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU-Angle.jpg')}}"
                                    alt="Khả năng vượt địa hình vượt trội"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">7. Khả năng vượt địa hình vượt trội</span></h3>
                                <div class="opContent">
                                    <p>Với khoảng sáng gầm xe lên đến 220mm cùng thiết kế các góc thoát lớn giúp Triton dễ dàng vượt mọi địa hình hiểm trở</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="8" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide108" data-slick-index="8" aria-hidden="true">
                        <a href="javascript:;" title="Khoảng sáng gầm cao, khả năng lội nước vượt trội" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">8. Khoảng sáng gầm cao, khả năng lội nước vượt trội</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/newtriton/SU_highground.jpg')}}"
                                    alt="Khoảng sáng gầm cao, khả năng lội nước vượt trội"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">8. Khoảng sáng gầm cao, khả năng lội nước vượt trội</span></h3>
                                <div class="opContent">
                                    <p>
                                        Khoảng sáng gầm tăng lên 220mm, cho khả năng vượt địa hình vượt trội trên cả những đoạn đường gồ ghề.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-performance.jpg')}}"
                        alt="UY MÃNH VƯỢT ĐỊA HÌNH"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">UY MÃNH VƯỢT ĐỊA HÌNH</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Kế thừa khả năng vận hành đỉnh cao mang đậm “chất Mitsubishi”, Triton mới mang lại khả năng vượt địa hình ấn tượng.</p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-engine.jpg')}}"
                        alt="Động cơ Diesel MIVEC 2.4L"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Động cơ Diesel MIVEC 2.4L</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Động cơ Diesel MIVEC 2.4L bằng nhôm giúp giảm trọng lượng, tăng công suất và mô-men, tiết kiệm nhiên liệu với&nbsp;công nghệ điều khiển van bằng điện tử áp dụng duy nhất trên phân khúc pick-up.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-Tranmission.jpg')}}"
                        alt="Hộp số 6 cấp kết hợp lẫy chuyển số trên vô lăng"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hộp số 6 cấp kết hợp lẫy chuyển số trên vô lăng</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Hộp số 6 cấp chuyển số mượt mà và êm ái, kết hợp lẫy chuyển số trên vô lăng duy nhất trong phân khúc mang đến cảm giác điều khiển như trên một chiếc xe thể thao thực thụ và cho khả năng đi đèo dốc tốt hơn
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/Untitled-3.png')}}"
                        alt="Khóa vi sai cầu sau"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khóa vi sai cầu sau</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Khóa vi sai cầu sau điều khiển bằng điện tử. Hạn chế 2 bánh sau bị quay trơn, đảm bảo lực kéo được truyền đến 2 bánh sau, nâng cao khả năng vượt địa hình của xe.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-superselect.jpg')}}"
                        alt="Hệ thống gài cầu Super Select 4WD-II"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Hệ thống gài cầu Super Select 4WD-II</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Hệ thống truyền động trứ danh Super Select II với 4 chế độ vận hành 2H-4H-4HLc-4LLc cùng nút chuyển cầu điện tử và vi sai trung tâm tăng khả năng vượt địa hình</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-Offroad.jpg')}}"
                        alt="4 chế độ chạy địa hình"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li>
                                <span>4X4<small class="small-text">AT MIVEC</small></span>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">4 chế độ chạy địa hình</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Tính năng lựa chọn địa hình (Off Road Mode) hỗ trợ tăng cường lực kéo khi chạy trên những địa hình phức tạp như sỏi, bùn, cát hay đá</p>
                    </div>
                </div>
            </div>
            <div data-item="6" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-4h.jpg')}}"
                        alt="Vi sai trung tâm duy nhất trong phân khúc"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Vi sai trung tâm duy nhất trong phân khúc</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Vi sai trung tâm duy nhất phân khúc&nbsp;cho khả năng chuyển cầu từ 2H sang 4H khi xe đang di chuyển đến tốc độ 100Km/h phù hợp với mặt đường trơn trượt,&nbsp;ưu việt như dòng xe dẫn động toàn thời gian
                            AWD.
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="7" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU-Angle.jpg')}}"
                        alt="Khả năng vượt địa hình vượt trội"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khả năng vượt địa hình vượt trội</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>Với khoảng sáng gầm xe lên đến 220mm cùng thiết kế các góc thoát lớn giúp Triton dễ dàng vượt mọi địa hình hiểm trở</p>
                    </div>
                </div>
            </div>
            <div data-item="8" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/newtriton/SU_highground.jpg')}}"
                        alt="Khoảng sáng gầm cao, khả năng lội nước vượt trội"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khoảng sáng gầm cao, khả năng lội nước vượt trội</h3>
                    <div class="opContent" style="color: #c0c0c0;">
                        <p>
                            Khoảng sáng gầm tăng lên 220mm, cho khả năng vượt địa hình vượt trội trên cả những đoạn đường gồ ghề.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>