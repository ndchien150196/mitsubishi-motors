<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Locale -->
    <meta http-equiv="Content-Language" content="en">

    <!-- To the Future! -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <!-- Meta -->

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <meta name="generator" content="">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- Facebook Meta Tags -->
    <meta property="og:image" content="">
    <meta property="og:title" content="">
    <meta property="og:url" content="">
    <meta property="og:site_name" content="">
    <meta property="og:type" content="website">

    <title>{{$title}} | Mitsubishi Thái Bình - Đại lý Mitsubishi Motors tại Việt Nam | Phân phối xe Mitsubishi Mirage, Attrage, Triton, Outlander Sport, Pajero Sport chính hãng</title>

    <!-- Favicons -->
    <link rel="icon" type="image/x-icon" href="{{asset('img/favicon.png')}}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('img/favicon.png')}}">
    <link rel="apple-touch-icon" href="{{asset('img/favicon.png')}}">

    <!-- style -->
    <!-- build:css css/combined.css -->
    <!--<link rel="stylesheet" href="/public/css/styles.css" />-->
    <link rel="stylesheet" href="{{asset('css/combined.css')}}">
    <script src="{{asset('js/jquery.js')}}"></script>
    <!-- endbuild -->
    @yield('js')

    <!-- Require script google recaptcha v3 -->
    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->
    <!-- <script src="{{asset('js/jquery.mousewheel.min.js')}}"></script> -->
    <!-- <style type="text/css">
        .fancybox-margin{
            margin-right:17px;
        }
        @font-face {
            font-family: 'rbicon';
            src: url(chrome-extension://dipiagiiohfljcicegpgffpbnjmgjcnf/fonts/rbicon.woff2) format("woff2");
            font-weight: normal;
            font-style: normal; 
        }
    </style> -->
</head>
<body>
    @include('layouts.header')
    <main id="main" class="main">
        @yield('content')
    </main>
    @yield('afftercontent')
    @include('layouts.footer')
    @include('layouts.contact')
    @include('layouts.tabright')
    @yield('jsfooter')
    <script src="{{asset('js/combined.js')}}"></script>

    <script>
        //1
        $(function () {
            $(".picker").datepicker({
                dateFormat: "dd-mm-yy",
            });
        });

        //2
        $("#BlockNews").html($("#dealernews").html());
        $(".phienban-btn li:eq(3)").hide();

        var carid = $(".phienban-btn li:eq(0) a").attr("data-car-id");
        var linklaithu = "/mua-xe/dang-ky-lai-thu/" + carid + "/";
        var linkbaogia = "/mua-xe/bao-gia-chi-tiet/" + carid + "/";
        $(".phienban-btn li:eq(2) a").attr("href", linklaithu);
        $(".phienban-btn li:eq(1) a").attr("href", linkbaogia);
        $(".phienban-btn li:eq(1) a").removeAttr("data-estimate-url");
        $(".phienban-btn li:eq(1) a").unbind().bind("click", function (e) {
            window.location = $(this).attr("href");
        });
        // productInit();

        //3
        var productApi = null;
        // getProductApi();
        // function getProductApi() {
        //     var url = "http://mitsubishi-motors.com.vn/api/getProductFromDealer.php";
        //     $.get(url, function (response) {
        //         if (!response.HasError) {
        //             productApi = response.Data;
        //             getProductLayout();
        //         } else {
        //             console.error(response.Error);
        //         }
        //     });
        // }
        function getProductLayout() {
            if (productApi) {
                var url = productApi[0].Url;
                $("#product_main").load(url, function () {
                    runningProductInit();
                });
            }
        }
        function runningProductInit() {
            if (typeof productInit != "undefined") {
                productInit();
            } else {
                setTimeout(runningProductInit, 100);
            }
        }
    </script>

</body>

</html>