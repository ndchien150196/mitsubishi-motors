@extends("master")
@section("content")
<div class="second-block page-news-home">
    <div class="grid-fluid">
        <h3 class="title-block">
            <span>Sự kiện nổi bật</span>
        </h3>
        <div class="grid-content">
            <div class="row">
                <div class="col-md-8">
                    <div class="news-block">
                    	@foreach ($tintuc as $t)
                        <div class="new-thumbtop">
                            <a href="/tin-tuc/{{$t->slug}}.html" title="{{$t->title}}" style="display: block;">
                                <div class="img-thumb">
                                    <img alt="{{$t->title}}" src="{{$t->avatar}}" width="100%" />
                                </div>
                                <div class="text-title">
                                    <h3>
                                        <p class="ni-title">{{\App\Common::chensaotitle($t->title,$t->category)}}</p>
                                    </h3>
                                    <div class="ni-desc cut-text">{!! \App\Common::resethtml($t->content) !!}</div>
                                </div>
                            </a>
                        </div>
                        @endforeach
                    </div>
                    <div class="cate-footer">
                        <div class="page-navigation">
                            <nav aria-label="Page navigation navigation-custom">
                                {{ $tintuc->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="news-sidebar">
                        <div class="sidebar-block link-block">
                            <h3 class="news-link-title">Tin tức</h3>
                            <!-- menu-->
                            <ul class="news-link">
                                <li class="@if($category == 'su-kien-noi-bat') active @endif">
                                    <a  href="{{asset('tin-tuc/su-kien-noi-bat')}}" title="Sự kiện nổi bật" class="sp-text">Sự kiện nổi bật</a>
                                </li>
                                <li class="@if($category == 'tin-khuyen-mai') active @endif">
                                    <a href="{{asset('tin-tuc/tin-khuyen-mai')}}" title="Tin khuyến mãi" class="sp-text">Tin khuyến mãi</a>
                                </li>
                                <li class="@if($category == 'tin-tong-hop') active @endif">
                                    <a href="{{asset('tin-tuc/tin-tong-hop')}}" title="Tin tổng hợp" class="sp-text">Tin tổng hợp</a>
                                </li>
                                <li class="@if($category == 'tin-tuyen-dung') active @endif">
                                    <a href="{{asset('tin-tuc/tin-tuyen-dung')}}" title="Tin tuyển dụng" class="sp-text">Tin tuyển dụng</a>
                                </li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-12">
                                <div class="sidebar-block drive-block"></div>
                            </div>
                        </div>
                         @include('/layouts/block-km')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
