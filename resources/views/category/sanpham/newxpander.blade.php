@extends("master")
@section("js")
    <script src="{{asset('js/slick.min.js')}}"></script>
@endsection
@section("content") 
<div class="page-product">
    @include('category.sanpham.newxpander.banner')
    @include('category.sanpham.xpandercross.navigation')
    
    <div class="pr-content">
        @include('category.sanpham.newxpander.dacdiemnoibat')

        @include('category.sanpham.newxpander.thongsokythuat')
        
        <!--===================-->

        @include('category.sanpham.newxpander.ngoaithat')

        @include('category.sanpham.newxpander.noithatrongrai')

        @include('category.sanpham.newxpander.vanhanh')
        
        @include('category.sanpham.newxpander.antoan')

        @include('category.sanpham.newxpander.tienich')

        @include('category.sanpham.newxpander.thuvien')
    </div>
    <!--.pr-content-->

    <!--===================-->
</div>
@endsection
@section('afftercontent')
<div d2-estimate-popup=""></div>
<!--#product_extimate-->

<section id="product_specs" class="mitsu-modal product-specs">
    <div class="grid-inner">
        <div data-href="#product_specs" class="modal-close"></div>
        <h3 class="title-block pr-title-block">
            <a title="So sánh giữa các phiên bản">So sánh giữa các phiên bản</a>
        </h3>
        <div class="row">
            <div class="col-md-12 col-lg-10 col-lg-offset-1">
                <div class="list-steps">
                    <div class="steps-header">
                        <table>
                            <colgroup>
                                <col width="35%" />
                                <col width="21%" />
                                <col width="21%" />
                                <col width="21%" />
                            </colgroup>
                            <thead>
                                <tr>
                                    <th>
                                        <div class="stepes-pagination">
                                            <ul>
                                                <li><a href="javascript:;" data-href="#st_thongsokythuat" class="stepes-active active">Thông số kỹ thuật</a></li>
                                                <li><a href="javascript:;" data-href="#st_trangthietbi" class="stepes-active">Trang thiết bị</a></li>
                                            </ul>
                                        </div>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/new-xpander/Xpander-Thumbnail.png')}}"
                                            alt="New Xpander MT"
                                        />
                                        <label>MT</label><span>{{$product['Xpander_MT']}} VNĐ</span>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/new-xpander/Xpander-Thumbnail.png')}}"
                                            alt="New Xpander AT (CBU)"
                                        />
                                        <label>AT (CBU)</label><span>{{$product['Xpander_AT_NK']}} VNĐ</span>
                                    </th>
                                    <th>
                                        <img
                                            src="{{asset('img/new-xpander/Xpander-Thumbnail.png')}}"
                                            alt="New Xpander AT (CKD)"
                                        />
                                        <label>AT (CKD)</label><span>{{$product['Xpander_AT_CKD']}} VNĐ</span>
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div id="st_thongsokythuat" class="stepes step-1 active">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />
                                    <col width="21%" />
                                    <col width="21%" />
                                    <col width="21%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><b> KÍCH THƯỚC </b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kích thước tổng thể (DxRxC) (mm)</td>
                                        <td>4.475 x 1.750 x 1.730</td>
                                        <td>4.475 x 1.750 x 1.730</td>
                                        <td>4.475 x 1.750 x 1.730</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoảng cách hai cầu xe (mm)</td>
                                        <td>2.775</td>
                                        <td>2.775</td>
                                        <td>2.775</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoảng cách hai bánh xe trước/sau (mm)</td>
                                        <td>1.520/1.510</td>
                                        <td>1.520/1.510</td>
                                        <td>1.520/1.510</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Bán kính quay vòng nhỏ nhất (m)</td>
                                        <td>5,2</td>
                                        <td>5,2</td>
                                        <td>5,2</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoảng sáng gầm xe (mm)</td>
                                        <td>205</td>
                                        <td>205</td>
                                        <td>205</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Trọng lượng không tải (Kg)</td>
                                        <td>1.235</td>
                                        <td>1.250</td>
                                        <td>1.250</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Số chỗ ngồi</td>
                                        <td>7</td>
                                        <td>7</td>
                                        <td>7</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> ĐỘNG CƠ </b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Loại động cơ</td>
                                        <td>1.5L MIVEC</td>
                                        <td>1.5L MIVEC</td>
                                        <td>1.5L MIVEC</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dung Tích Xylanh (cc)</td>
                                        <td>1.499</td>
                                        <td>1.499</td>
                                        <td>1.499</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Công suất cực đại (ps/rpm)</td>
                                        <td>104/6.000</td>
                                        <td>104/6.000</td>
                                        <td>104/6.000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mômen xoắn cực đại (Nm/rpm)</td>
                                        <td>141/4.000</td>
                                        <td>141/4.000</td>
                                        <td>141/4.000</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Dung tích thùng nhiên liệu (L)</td>
                                        <td>45</td>
                                        <td>45</td>
                                        <td>45</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> TRUYỀN ĐỘNG &amp; HỆ THỐNG TREO </b></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hộp số</td>
                                        <td>Số sàn 5 cấp</td>
                                        <td>Số tự động 4 cấp</td>
                                        <td>Số tự động 4 cấp</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Truyền động</td>
                                        <td>1 cầu - 2WD</td>
                                        <td>1 cầu - 2WD</td>
                                        <td>1 cầu - 2WD</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Trợ lực lái</td>
                                        <td>Điện</td>
                                        <td>Điện</td>
                                        <td>Điện</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống treo trước</td>
                                        <td>McPherson với lò xo cuộn</td>
                                        <td>McPherson với lò xo cuộn</td>
                                        <td>McPherson với lò xo cuộn</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống treo sau</td>
                                        <td>Thanh xoắn</td>
                                        <td>Thanh xoắn</td>
                                        <td>Thanh xoắn</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lốp xe trước/sau</td>
                                        <td>205/55R16</td>
                                        <td>205/55R16</td>
                                        <td>205/55R16</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Phanh trước/sau</td>
                                        <td>Đĩa/Tang trống</td>
                                        <td>Đĩa/Tang trống</td>
                                        <td>Đĩa/Tang trống</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mức tiêu hao nhiên liệu (L/100km)</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Kết hợp</i></td>
                                        <td>6,9</td>
                                        <td>6,9</td>
                                        <td>6,9</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Trong đô thị</i></td>
                                        <td>8,8</td>
                                        <td>8,5</td>
                                        <td>8,5</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i>- Ngoài đô thị</i></td>
                                        <td>5,9</td>
                                        <td>5,9</td>
                                        <td>5,9</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="st_trangthietbi" class="stepes step-2">
                        <div class="scroll-table">
                            <table>
                                <colgroup>
                                    <col width="35%" />

                                    <col width="21%" />
                                    <col width="21%" />
                                    <col width="21%" />
                                </colgroup>
                                <tbody>
                                    <tr>
                                        <td class="text-left"><b> NGOẠI THẤT </b></td>
                                        <td></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống đèn chiếu sáng phía trước</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i> - Đèn chiếu xa </i></td>
                                        <td>Clear Halogen</td>
                                        <td>LED</td>
                                        <td>LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><i> - Đèn chiếu gần </i></td>
                                        <td>Clear Halogen</td>
                                        <td>LED</td>
                                        <td>LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn định vị dạng LED</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn chào mừng và đèn hỗ trợ chiếu sáng khi rời xe</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn sương mù trước/sau</td>
                                        <td>Không</td>
                                        <td>Trước</td>
                                        <td>Trước</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Đèn báo phanh thứ ba trên cao</td>
                                        <td>LED</td>
                                        <td>LED</td>
                                        <td>LED</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kính chiếu hậu</td>
                                        <td>Cùng màu với thân xe, chỉnh điện, Tích hợp đèn báo rẽ</td>
                                        <td>Mạ crôm, gập điện, chỉnh điện, Tích hợp đèn báo rẽ</td>
                                        <td>Mạ crôm, gập điện, chỉnh điện, Tích hợp đèn báo rẽ</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tay nắm cửa ngoài</td>
                                        <td>Cùng màu thân xe</td>
                                        <td>Mạ crôm</td>
                                        <td>Mạ crôm</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Lưới tản nhiệt</td>
                                        <td>Đen bóng</td>
                                        <td>Crôm xám</td>
                                        <td>Crôm xám</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gạt nước kính trước</td>
                                        <td>Gạt mưa gián đoạn</td>
                                        <td>Gạt mưa gián đoạn</td>
                                        <td>Gạt mưa gián đoạn</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gạt nước kính sau và sưởi kính sau</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Mâm đúc hợp kim</td>
                                        <td>16" - 2 tông màu</td>
                                        <td>16" - 2 tông màu</td>
                                        <td>16" - 2 tông màu</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> NỘI THÂT </b></td>
                                        <td></td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Vô lăng và cần số bọc da</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Nút điều khiển âm thanh trên vô lăng</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Điện thoại rảnh tay trên vô lăng</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống kiểm soát hành trình</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Vô lăng điều chỉnh 4 hướng</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Điều hòa nhiệt độ</td>
                                        <td>Chỉnh tay, 2 giàn lạnh</td>
                                        <td>Chỉnh tay, 2 giàn lạnh</td>
                                        <td>Chỉnh tay, 2 giàn lạnh</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chất liệu ghế</td>
                                        <td>Nỉ</td>
                                        <td>Da, tối màu</td>
                                        <td>Da, tối màu</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Ghế tài xế</td>
                                        <td>Chỉnh tay 4 hướng</td>
                                        <td>Chỉnh tay 6 hướng</td>
                                        <td>Chỉnh tay 6 hướng</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hàng ghế thứ hai gập 60:40</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hàng ghế thứ ba gập 50:50</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tay nắm cửa trong mạ crôm</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Kính cửa điều khiển điện</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Màn hình hiển thị đa thông tin</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Móc gắn ghế an toàn trẻ em</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống âm thanh</td>
                                        <td>CD 2 DIN, USB, Bluetooth, AUX</td>
                                        <td>Màn hình cảm ứng 7", kết nối Apple CarPlay &amp; Android Auto</td>
                                        <td>Màn hình cảm ứng 7", hỗ trợ kết nối Apple CarPlay &amp; Android Auto</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Số lượng loa</td>
                                        <td>4</td>
                                        <td>6</td>
                                        <td>6</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Sấy kính trước/sau</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cửa gió điều hòa hàng ghế sau</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Gương chiếu hậu trong chống chói chỉnh tay</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left"><b> AN TOÀN </b></td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Túi khí an toàn</td>
                                        <td>Túi khí đôi</td>
                                        <td>Túi khí đôi</td>
                                        <td>Túi khí đôi</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cơ cấu căng đai tự động</td>
                                        <td>Hàng ghế trước</td>
                                        <td>Hàng ghế trước</td>
                                        <td>Hàng ghế trước</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống chống bó cứng phanh (ABS)</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống phân phối lực phanh điện tử EBD</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống trợ lực phanh khẩn cấp BA</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống cân bằng điện tử (ASC)</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Hệ thống khởi hành ngang dốc (HSA)</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chìa khóa thông minh và khởi động bằng nút bấm</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khoá cửa từ xa</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Cảnh báo phanh khẩn cấp (ESS)</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chức năng chống trộm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Chìa khóa mã hóa chống trộm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Camera lùi</td>
                                        <td>Không</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Khóa cửa trung tâm</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                        <td>Có</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <br />
                    <br />
                    <p class="text-left">(*) Mức tiêu hao nhiên liệu chứng nhận bởi Cục Đăng Kiểm Việt Nam. Các thông số kỹ thuật có thể thay đổi mà không báo trước</p>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('jsfooter')
<script src="{{asset('js/sanpham/TweenMax.min.js')}}"></script>
<script src="{{asset('js/sanpham/ScrollToPlugin.min.js')}}"></script>
<script src="{{asset('js/sanpham/jquery.fancybox.js')}}"></script>
<script src="{{asset('js/new-xpander.js')}}"></script>
<script src="{{asset('js/estimate-price.js')}}"></script>
<script type="text/javascript">
    productInit();
</script>
@endsection
