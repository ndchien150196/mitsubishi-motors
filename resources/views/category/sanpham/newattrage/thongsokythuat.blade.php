<!--Specs-->
<div class="normal-block thongsokythuat">
    <div class="grid-inner">
        <h3 class="title-block pr-title-block">
            <span>Thông số kỹ thuật</span>
        </h3>
        <div class="row">
            <div class="col-md-6">
                <div class="tab-content">
                    <div id="cl360" role="tabpanel" class="tab-pane fade">
                        <div
                            id="gl_360"
                            data-imgarray='[                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Attrage-Trắng-3.4.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Attrage-Trắng-5.4.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Attrage-Trắng-2.4.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Attrage-Trắng-1.4.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Attrage-Trắng.png",                                            "https://www.mitsubishi-motors.com.vn/w/wp-content/uploads/2020/02/Attrage-Trắng-4.4.png"]'
                            class="gl-360"
                        >
                            <div class="threesixty car">
                                <div class="spinner"><span>0%</span></div>
                                <ol class="threesixty_images"></ol>
                            </div>
                            <img
                                src="{{asset('img/new-attrage/line-360.png')}}"
                                alt="line-360.png')}}"
                                class="gl360-line"
                            />
                            <div class="gl360-direc">
                                <a class="gl360-btn-prev">
                                    <i class="fa fa-caret-left"></i>
                                </a>
                                <span class="gl360-title">360<sup>o</sup></span>
                                <a class="gl360-btn-next">
                                    <i class="fa fa-caret-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <!--#cl360-->

                    <div id="cl1" role="tabpanel" class="fade tab-pane active">
                        <img
                            src="{{asset('img/new-attrage/Attrage-Trắng.png')}}"
                            alt="New Attrage"
                        />
                    </div>
                    <div id="cl2" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/new-attrage/Attrage-Xám.png')}}"
                            alt="New Attrage"
                        />
                    </div>
                    <div id="cl3" role="tabpanel" class="fade tab-pane">
                        <img
                            src="{{asset('img/new-attrage/Attrage-Đỏ.png')}}"
                            alt="New Attrage"
                        />
                    </div>
                    <!--.tab-pane-->
                </div>
                <div class="car-color-note text-center">
                    <span>Ghi chú: Hình ảnh minh họa có thể khác với thực tế.</span>
                </div>
                <!--.tab-content-->
                <ul role="tablist" class="nav nav-tabs">
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-attrage#cl360" class="vehicle-color" aria-controls="cl360" role="tab" data-toggle="tab">
                            <span class="text">360</span>
                        </a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-attrage#cl1" class="vehicle-color" aria-controls="cl1 tab" data-toggle="tab" style="background-color: #ffffff;">
                            <span class="color-name">Trắng</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-attrage#cl2" class="vehicle-color" aria-controls="cl2 tab" data-toggle="tab" style="background-color: #828282;">
                            <span class="color-name">Xám</span>
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a href="https://www.mitsubishiquangninh.com/san-pham/new-attrage#cl3" class="vehicle-color" aria-controls="cl3 tab" data-toggle="tab" style="background-color: #d60c0c;">
                            <span class="color-name">Đỏ</span>
                        </a>
                    </li>
                </ul>
                <!--ul.nav-tabs-->
            </div>
            <div class="col-md-6">
                <div class="phienban">
                    <div class="phienban-title">Phiên bản:</div>
                    <div class="phienban-select">
                        <ul>
                            <li class="active"><a href="javascript:;" data-slide="0" data-tracking-click="Version MT" data-tracking-click-cat="New Attrage">MT</a></li>
                            <li class=""><a href="javascript:;" data-slide="1" data-tracking-click="Version CVT" data-tracking-click-cat="New Attrage">CVT</a></li>
                        </ul>
                    </div>
                </div>
                <div class="row hidden-md hidden-lg">
                    <div class="col-xs-12 phienban-note">Vuốt ngang để chọn và so sánh giữa các phiên bản</div>
                </div>
                <div class="row hidden-xs hidden-sm">
                    <div class="col-xs-12 phienban-note"><a href="javascript:;" data-href="#product_specs" class="view-detail modal-open"></a></div>
                </div>
                <div class="phienban-slide">
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['Attrage_MT']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification MT" data-tracking-click-cat="New Attrage">Thông số kỹ thuật</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">4.305 x 1.670 x 1.515</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Chiều dài cơ sở (mm)</div>
                                        <div class="col-md-6 phienban-content-right">2.550</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">4,8</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">170</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (kg)</div>
                                        <div class="col-md-6 phienban-content-right">875</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tổng trọng lượng (kg)</div>
                                        <div class="col-md-6 phienban-content-right">1.330</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi</div>
                                        <div class="col-md-6 phienban-content-right">5</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">1.2L MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống nhiên liệu</div>
                                        <div class="col-md-6 phienban-content-right">Phun xăng đa điểm, điều khiển điện tử ECI-MULTI</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung Tích Xylanh (cc)</div>
                                        <div class="col-md-6 phienban-content-right">1.193</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">78/6.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">100/4.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tốc độ cực đại (Km/h)</div>
                                        <div class="col-md-6 phienban-content-right">172</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">42</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> HỆ THỐNG DẪN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">Số sàn 5 cấp</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">Kiểu MacPherson, lò xo cuộn với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Dầm xoắn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm - Lốp</div>
                                        <div class="col-md-6 phienban-content-right">Mâm hợp kim, 185/55R15</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa thông gió</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh sau</div>
                                        <div class="col-md-6 phienban-content-right">Tang trống</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mức tiêu hao nhiên liệu (L/100Km)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Kết hợp</i></div>
                                        <div class="col-md-6 phienban-content-right">5,09</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Trong đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">6,22</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Ngoài đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">4,42</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Attrage"
                                class="view-detail modal-open"
                                tabindex="0"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment MT" data-tracking-click-cat="New Attrage">Trang bị tiêu chuẩn</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Túi khí</div>
                                        <div class="col-md-6 phienban-content-right">Túi khí đôi</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Căng đai tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Dây đai an toàn cho tất cả các ghế</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Móc gắn ghế trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh ABS</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử EBD</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khởi động nút bấm</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu xa</i></div>
                                        <div class="col-md-6 phienban-content-right">Halogen</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu gần</i></div>
                                        <div class="col-md-6 phienban-content-right">Halogen</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Cùng màu với thân xe, chỉnh điện</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa ngoài</div>
                                        <div class="col-md-6 phienban-content-right">Cùng màu thân xe</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lưới tản nhiệt</div>
                                        <div class="col-md-6 phienban-content-right">Viền chrome</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt kính trước</div>
                                        <div class="col-md-6 phienban-content-right">Tốc độ thay đổi theo vận tốc xe</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn phanh thứ 3 lắp trên cao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng trợ lực điện</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Nút chỉnh âm thanh và thoại rảnh tay</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống ga tự động</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cần số bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa không khí</div>
                                        <div class="col-md-6 phienban-content-right">Chỉnh tay</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lọc gió điều hòa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khóa cửa trung tâm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa trong</div>
                                        <div class="col-md-6 phienban-content-right">Cùng màu nội thất</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa điều khiển điện</div>
                                        <div class="col-md-6 phienban-content-right">Cửa kính phía người lái điều khiển một chạm với chức năng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn báo hiệu tiết kiệm nhiên liệu</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống âm thanh</div>
                                        <div class="col-md-6 phienban-content-right">CD</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống loa</div>
                                        <div class="col-md-6 phienban-content-right">2</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Nỉ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế chỉnh tay 6 hướng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ tỳ tay dành cho người lái</div>
                                        <div class="col-md-6 phienban-content-right">Không</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tựa đầu hàng ghế sau</div>
                                        <div class="col-md-6 phienban-content-right">3 vị trí</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tựa tay hàng ghế sau với giá để ly</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Attrage"
                                class="view-detail modal-open"
                                tabindex="0"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                    <div class="phienban-item">
                        <div class="row phienban-row">
                            <div class="col-md-6 phienban-title no-expand">Giá (đã bao gồm thuế VAT)</div>
                            <div class="col-md-6 phienban-gia">
                                {{$product['Attrage_CVT']}} VNĐ
                                <span> </span>
                            </div>
                            <div class="row phienban-content">
                                <div class="col-md-12 phienban-content-right">
                                    <small style="font-weight: bold; float: right; font-size: 12px;"></small>
                                </div>
                            </div>
                        </div>

                        <!--.phienban-row .phienban-gia-->

                        <!--.phienban-row .phienban-gia-->
                        <div data-specs="thongsokythuat" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Specification CVT" data-tracking-click-cat="New Attrage">Thông số kỹ thuật</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> KÍCH THƯỚC </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Kích thước tổng thể (DxRxC) (mm)</div>
                                        <div class="col-md-6 phienban-content-right">4.305 x 1.670 x 1.515</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Chiều dài cơ sở (mm)</div>
                                        <div class="col-md-6 phienban-content-right">2.550</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Bán kính quay vòng nhỏ nhất (m)</div>
                                        <div class="col-md-6 phienban-content-right">4,8</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Khoảng sáng gầm xe (mm)</div>
                                        <div class="col-md-6 phienban-content-right">170</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Trọng lượng không tải (kg)</div>
                                        <div class="col-md-6 phienban-content-right">905</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tổng trọng lượng (kg)</div>
                                        <div class="col-md-6 phienban-content-right">1.350</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Số chỗ ngồi</div>
                                        <div class="col-md-6 phienban-content-right">5</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> ĐỘNG CƠ </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Loại động cơ</div>
                                        <div class="col-md-6 phienban-content-right">1.2L MIVEC</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống nhiên liệu</div>
                                        <div class="col-md-6 phienban-content-right">Phun xăng đa điểm, điều khiển điện tử ECI-MULTI</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung Tích Xylanh (cc)</div>
                                        <div class="col-md-6 phienban-content-right">1.193</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Công suất cực đại (ps/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">78/6.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mômen xoắn cực đại (Nm/rpm)</div>
                                        <div class="col-md-6 phienban-content-right">100/4.000</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tốc độ cực đại (Km/h)</div>
                                        <div class="col-md-6 phienban-content-right">172</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Dung tích thùng nhiên liệu (L)</div>
                                        <div class="col-md-6 phienban-content-right">42</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> HỆ THỐNG DẪN ĐỘNG &amp; HỆ THỐNG TREO </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hộp số</div>
                                        <div class="col-md-6 phienban-content-right">Tự động vô cấp CVT INVECS-III</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo trước</div>
                                        <div class="col-md-6 phienban-content-right">Kiểu MacPherson, lò xo cuộn với thanh cân bằng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống treo sau</div>
                                        <div class="col-md-6 phienban-content-right">Dầm xoắn</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mâm - Lốp</div>
                                        <div class="col-md-6 phienban-content-right">Mâm hợp kim, 185/55R15</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh trước</div>
                                        <div class="col-md-6 phienban-content-right">Đĩa thông gió</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Phanh sau</div>
                                        <div class="col-md-6 phienban-content-right">Tang trống</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Mức tiêu hao nhiên liệu (L/100Km)</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Kết hợp</i></div>
                                        <div class="col-md-6 phienban-content-right">5,36</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Trong đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">6,47</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Ngoài đô thị</i></div>
                                        <div class="col-md-6 phienban-content-right">4,71</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_thongsokythuat"
                                data-tracking-click="View Specification All Version"
                                data-tracking-click-cat="New Attrage"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                        <div data-specs="trangbitieuchuan" class="row phienban-row">
                            <div class="col-md-12 phienban-title" data-tracking-click="View Equipment CVT" data-tracking-click-cat="New Attrage">Trang bị tiêu chuẩn</div>
                            <div class="row phienban-content">
                                <div class="col-xs-12">
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left"><b> AN TOÀN </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Túi khí</div>
                                        <div class="col-md-6 phienban-content-right">Túi khí đôi</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Căng đai tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Dây đai an toàn cho tất cả các ghế</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row">
                                        <div class="col-md-6 phienban-content-left">Móc gắn ghế trẻ em</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống chống bó cứng phanh ABS</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống phân phối lực phanh điện tử EBD</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khoá cửa từ xa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa mã hóa chống trộm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chìa khóa thông minh</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống khởi động nút bấm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NGOẠI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống đèn chiếu sáng phía trước</div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu xa</i></div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><i>- Đèn chiếu gần</i></div>
                                        <div class="col-md-6 phienban-content-right">LED</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn sương mù</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính chiếu hậu</div>
                                        <div class="col-md-6 phienban-content-right">Cùng màu với thân xe, chỉnh điện, tích hợp đèn báo rẽ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa ngoài</div>
                                        <div class="col-md-6 phienban-content-right">Cùng màu thân xe</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lưới tản nhiệt</div>
                                        <div class="col-md-6 phienban-content-right">Viền đỏ</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Gạt kính trước</div>
                                        <div class="col-md-6 phienban-content-right">Tốc độ thay đổi theo vận tốc xe</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn phanh thứ 3 lắp trên cao</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left"><b> NỘI THẤT </b></div>
                                        <div class="col-md-6 phienban-content-right">-</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng trợ lực điện</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Vô lăng bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Nút chỉnh âm thanh và thoại rảnh tay</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống ga tự động</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Cần số bọc da</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Điều hòa không khí</div>
                                        <div class="col-md-6 phienban-content-right">Tự động</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Lọc gió điều hòa</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Khóa cửa trung tâm</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tay nắm cửa trong</div>
                                        <div class="col-md-6 phienban-content-right">Mạ chrome</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Kính cửa điều khiển điện</div>
                                        <div class="col-md-6 phienban-content-right">Cửa kính phía người lái điều khiển một chạm với chức năng</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Màn hình hiển thị đa thông tin</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Đèn báo hiệu tiết kiệm nhiên liệu</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống âm thanh</div>
                                        <div class="col-md-6 phienban-content-right">Màn hình cảm ứng 6.8", hỗ trợ kết nối Apple CarPlay/Android Auto</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Hệ thống loa</div>
                                        <div class="col-md-6 phienban-content-right">4</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Chất liệu ghế</div>
                                        <div class="col-md-6 phienban-content-right">Da</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Ghế tài xế chỉnh tay 6 hướng</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Bệ tỳ tay dành cho người lái</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tựa đầu hàng ghế sau</div>
                                        <div class="col-md-6 phienban-content-right">3 vị trí</div>
                                    </div>
                                    <div class="row phienban-content-row visible-xs visible-sm">
                                        <div class="col-md-6 phienban-content-left">Tựa tay hàng ghế sau với giá để ly</div>
                                        <div class="col-md-6 phienban-content-right">Có</div>
                                    </div>
                                </div>
                            </div>
                            <a
                                href="javascript:;"
                                data-href="#product_specs"
                                data-alias="#st_trangthietbi"
                                data-tracking-click="View Equipment All Version"
                                data-tracking-click-cat="New Attrage"
                                class="view-detail modal-open"
                                tabindex="-1"
                            >
                                Xem chi tiết [+]
                            </a>
                        </div>
                        <!--.phienban-row-->
                    </div>
                </div>
                <!--.phienban-slide-->
                <div class="phienban-btn">
                    <ul>
                        <li>
                            <a
                                href="javascript:;"
                                data-href="#product_estimate"
                                class="btn btn-icon-l modal-open d2-estimate"
                                data-car-id="122"
                                data-estimate-url="https://www.mitsubishi-motors.com.vn/api/getCostEstimateDealer.php?language=vi"
                                data-step="1"
                                data-tracking-click="Estimated Cost"
                                data-tracking-click-cat="New Attrage"
                            >
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-cost"></i></span><span class="text">Dự tính chi phí</span>
                            </a>
                        </li>
                        <li>
                            <a
                                href="https://www.mitsubishiquangninh.com/mua-xe/bao-gia-chi-tiet/122/"
                                class="btn btn-icon-l d2-estimate"
                                data-car-id="122"
                                data-tracking-click="Price Quotation By Dealer"
                                data-tracking-click-cat="New Attrage"
                                data-step="2"
                            >
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-price"></i></span><span class="text">Yêu cầu báo giá</span>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.mitsubishiquangninh.com/mua-xe/dang-ky-lai-thu/122/" class="btn btn-icon-l">
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-drive"></i></span><span class="text">Đăng ký lái thử</span>
                            </a>
                        </li>
                        <li style="display: none;">
                            <a href="https://www.mitsubishiquangninh.com/dai-ly/" class="btn btn-icon-l">
                                <span class="icon"><i class="svg-icon icon-specs icon-specs-dealer"></i></span><span class="text">Tìm nhà phân phối</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <!--.chonphienban-->
            </div>
        </div>
    </div>
</div>
<!--End - Specs-->