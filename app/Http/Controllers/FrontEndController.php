<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\TinTuc;
use App\Models\MailData;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Session;
use App\Models\Slide;
use App\Models\Product;
use Mail;

class FrontEndController
{
    public function tuctuc($category)
    {
        $product = Product::pluck('amount','name');
        $tintuc = TinTuc::where('category',$category)->where('status',1)->orderBy('created_at','desc')->paginate(6);
        if ($category == 'su-kien-noi-bat') {
            $title = 'Sự kiện nổi bật';
        }elseif ($category == 'tin-khuyen-mai') {
            $title = 'Sự kiện nổi bật';
        }elseif ($category == 'tin-tong-hop') {
            $title = 'Sự kiện nổi bật';
        }elseif ($category == 'tin-tuyen-dung') {
            $title = 'Sự kiện nổi bật';
        }
        return view('category.tintuc',compact('tintuc','category','title','product'));
    }

    public function new($slug)
    {
        $product = Product::pluck('amount','name');
        $tintuc = TinTuc::where('slug',$slug)->where('status',1)->first();
        $title = $tintuc->title;
        return view('category.new',compact('tintuc','title','product'));
    }

    public function maillienhe(Request $r)
    {
        $new = new MailData;
        $new->type = 1;
        $new->name = $r->Name;
        $new->phone = $r->Phone;
        $new->content = $r->Content;
        $new->email = $r->Email;
        $new->link = '/lien-he';
        $new->save();

        $emails = ['ndchien150196@gmail.com', 'ndchien5@gmail.com','marketing@hyundaithaibinh.vn','duythachson@gmail.com'];
        Mail::send('maillienhe', array('name'=>$r->Name,'phone'=>$r->Phone,'email'=>$r->Email,'content'=>$r->Content), function($message) use ($emails){
            $message->to($emails)->subject('Khách hàng nhập thông tin tại trang liên hệ');
        });
        Session::flash('flash_message', 'Gửi Thành Công');
    }

    public function maildangky(Request $r)
    {
        $new = new MailData;
        $new->type = 2;
        $new->name = $r->Name;
        $new->product = $r->loaixe;
        $new->phone = $r->Phone;
        $new->email = $r->Email;
        $new->link = '/san-pham';
        $new->save();

        $emails = ['ndchien150196@gmail.com', 'ndchien5@gmail.com','marketing@hyundaithaibinh.vn','duythachson@gmail.com'];
        Mail::send('mail', array('name'=>$r->Name,'phone'=>$r->Phone,'email'=>$r->Email,'product'=>$r->loaixe), function($message) use ($emails){
            $message->to($emails)->subject('Khách hàng nhập thông tin Đăng Ký Lái Thử');
        });
        Session::flash('flash_message', 'Gửi Thành Công');
    }

    public function mailbaogia(Request $r)
    {
        $new = new MailData;
        $new->type = 3;
        $new->name = $r->Name;
        $new->product = $r->loaixe;
        $new->phone = $r->Phone;
        $new->email = $r->Email;
        $new->link = '/san-pham';
        $new->save();

        $emails = ['ndchien150196@gmail.com', 'ndchien5@gmail.com','marketing@hyundaithaibinh.vn','duythachson@gmail.com'];
        Mail::send('mail', array('name'=>$r->Name,'phone'=>$r->Phone,'email'=>$r->Email,'product'=>$r->loaixe), function($message) use ($emails){
            $message->to($emails)->subject('Khách hàng đặt lịch bảo dưỡng');
        });
        Session::flash('flash_message', 'Gửi Thành Công');
    }

    public function phutung($slug)
    {
        $product = Product::pluck('amount','name');
        if ($slug == 'dau-nhon-va-hoa-chat') {
            $title = 'DẦU NHỜN VÀ HÓA CHẤT';
            return view('category.dichvuhaumai.detailphutung',compact('title','product'));
        }elseif ($slug == 'day-curoa') {
            $title = 'DÂY CUROA';
            return view('category.dichvuhaumai.detailphutung',compact('title','product'));
        }elseif ($slug == 'loc-gio') {
            $title = 'LỌC GIÓ';
            return view('category.dichvuhaumai.detailphutung',compact('title','product'));
        }elseif ($slug == 'loc-nhien-lieu') {
            $title = 'LỌC NHIÊN LIỆU';
            return view('category.dichvuhaumai.detailphutung',compact('title','product'));
        }elseif ($slug == 'loc-nhot') {
            $title = 'LỌC NHỚT';
            return view('category.dichvuhaumai.detailphutung',compact('title','product'));
        }elseif ($slug == 'ma-phanh') {
            $title = 'MÁ PHANH';
            return view('category.dichvuhaumai.detailphutung',compact('title','product'));
        }elseif ($slug == 'phu-kien') {
            $title = 'PHỤ KIỆN';
            return view('category.dichvuhaumai.detailphutung',compact('title','product'));
        }elseif ($slug == 'value-selection') {
            $title = 'VALUE SELECTION';
            return view('category.dichvuhaumai.detailphutung',compact('title','product'));
        }else{
            return response()->view('errors', ['title' => 'Lỗi','product' => $product], 404);
        }
    }

    public function maildatlich(Request $r)
    {
        $new = new MailData;
        $new->type = 4;
        $new->name = $r->Name;
        $new->product = $r->Cartype;
        $new->phone = $r->Phone;
        $new->email = $r->Email;
        $new->content = 'Ngày đặt lịch:'.$r->Bookingdate.' vào lúc'.$r->HourService.'sẽ đến '.$r->noidunglamdichvu.'. Số Km hiện tại:'.$r->Kilometer;
        $new->link = '/dich-vu-hau-mai/dat-lich-bao-duong';
        $new->save();

        $emails = ['ndchien150196@gmail.com', 'ndchien5@gmail.com','marketing@hyundaithaibinh.vn','duythachson@gmail.com'];
        Mail::send('mail', array('name'=>$r->Name,'phone'=>$r->Phone,'email'=>$r->Email,'product'=>$r->Cartype), function($message) use ($emails){
            $message->to($emails)->subject('Khách hàng đặt lịch bảo dưỡng');
        });
        Session::flash('flash_message', 'Gửi Thành Công');
    }

    public function home()
    {
        $slide = Slide::orderBy('id','desc')->get();
        $product = Product::pluck('amount','name');
        return view('category.home', ['title' => 'Trang Chủ','slide' => $slide,'product' => $product]);
    }

    public function gioithieu()
    {
        $product = Product::pluck('amount','name');
        return view('category.gioithieu', ['title' => 'Giới Thiệu','product' => $product]);
    }

    public function lienhe()
    {
        $product = Product::pluck('amount','name');
        return view('category.lienhe', ['title' => 'Liên Hệ','product' => $product]);
    }

    public function gioithieudichvu()
    {
        $product = Product::pluck('amount','name');
        return view('category.dichvuhaumai.gioithieudichvu', ['title' => 'Giới thiệu dịch vụ','product' => $product]);
    }

    public function baoduong()
    {
        $product = Product::pluck('amount','name');
        return view('category.dichvuhaumai.baoduong', ['title' => 'Bảo dưỡng định kỳ','product' => $product]);
    }

    public function suachua()
    {
        $product = Product::pluck('amount','name');
        return view('category.dichvuhaumai.suachua', ['title' => 'Sửa chữa','product' => $product]);
    }

    public function cuuho()
    {
        $product = Product::pluck('amount','name');
        return view('category.dichvuhaumai.cuuho', ['title' => 'Cứu hộ giao thông','product' => $product]);
    }

    public function baohanh()
    {
        $product = Product::pluck('amount','name');
        return view('category.dichvuhaumai.baohanh', ['title' => 'Chính sách bảo hành','product' => $product]);
    }

    public function datlich()
    {
        $product = Product::pluck('amount','name');
        return view('category.dichvuhaumai.datlich', ['title' => 'Đặt lịch bảo dưỡng','product' => $product]);
    }

    public function phutungshow()
    {
        $product = Product::pluck('amount','name');
        return view('category.dichvuhaumai.phutung', ['title' => 'Phụ tùng chính hiệu','product' => $product]);
    }

    public function tragop()
    {
        $product = Product::pluck('amount','name');
        return view('category.muaxe.tragop', ['title' => 'Mua xe trả góp','product' => $product]);
    }

    public function quytrinh()
    {
        $product = Product::pluck('amount','name');
        return view('category.muaxe.quytrinh', ['title' => 'Quy trình mua xe','product' => $product]);
    }

    public function baogia()
    {
        $product = Product::pluck('amount','name');
        return view('category.muaxe.baogia', ['title' => 'Báo giá chi tiết','product' => $product]);
    }

    public function dangky()
    {
        $product = Product::pluck('amount','name');
        return view('category.muaxe.dangky', ['title' => 'Đăng ký lái thử','product' => $product]);
    }

    public function banggia()
    {
        $product = Product::pluck('amount','name');
        return view('category.banggia', ['title' => 'Bảng Giá','product' => $product]);
    }
}
