<div class="normal-block thietke hidden-border-bottom">
    <h3 class="title-block pr-title-block">
        <span>NỘI THẤT RỘNG RÃI &amp; TIỆN NGHI</span>
    </h3>
    <div class="option-slide">
        <div class="option-btn option-prev slick-disabled"><i class="fa fa-caret-up"></i></div>
        <div class="option-track slick-vertical slick-initialized slick-slider">
            <div aria-live="polite" class="slick-list draggable">
                <div class="slick-track" role="listbox">
                    <div
                        data-item="0"
                        class="option-item hightline active-show slick-slide slick-current slick-active"
                        style=""
                        tabindex="-1"
                        role="option"
                        aria-describedby="slick-slide80"
                        data-slick-index="0"
                        aria-hidden="false"
                    >
                        <a href="javascript:;" title="Không gian nội thất tiện nghi cho gia đình" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText"> Không gian nội thất tiện nghi cho gia đình</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Noi-that-rong-rai-ELL.jpg')}}"
                                    alt="Không gian nội thất tiện nghi cho gia đình"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText"> Không gian nội thất tiện nghi cho gia đình</span></h3>
                                <div class="opContent">
                                    <p>
                                        Khoang nội thất của Attrage sẽ làm hài lòng nhiều khách hàng nhờ cách bố trí thông minh và tối đa hóa không gian. Chất liệu da cao cấp của nội thất giúp cho hành khách luôn cảm thấy thoải mái
                                        và tiện nghi khi di chuyển hàng ngày cho công việc lẫn thư giãn cùng gia đình​
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="1" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide81" data-slick-index="1" aria-hidden="false">
                        <a href="javascript:;" title="Khởi động nút bấm" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">1. Khởi động nút bấm</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Khởi-động-nút-bấm.jpg')}}"
                                    alt="Khởi động nút bấm"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">1. Khởi động nút bấm</span></h3>
                                <div class="opContent">
                                    <p>Cho phép mở cửa mà không cần dùng chìa (KOS) và chức năng khởi động bằng nút bấm.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="2" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide82" data-slick-index="2" aria-hidden="false">
                        <a href="javascript:;" title="Ghế bọc da cao cấp" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">2. Ghế bọc da cao cấp</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Ghe-boc-da.jpg')}}"
                                    alt="Ghế bọc da cao cấp"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li><span>CVT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">2. Ghế bọc da cao cấp</span></h3>
                                <div class="opContent">
                                    <p>Ghế bọc da cao cấp mang đến sự tiện nghi vượt trội cho người lái và hành khách trên những hành trình dài.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="3" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide83" data-slick-index="3" aria-hidden="false">
                        <a href="javascript:;" title="Bệ tựa tay cho người lái" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">3. Bệ tựa tay cho người lái</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Tua-tay-ghe-lai.jpg')}}"
                                    alt="Bệ tựa tay cho người lái"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li><span>CVT</span></li>
                                        <li><span>MT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">3. Bệ tựa tay cho người lái</span></h3>
                                <div class="opContent">
                                    <p>Trang bị tạo nên sự khác biệt của Attrage trong phân khúc, giúp cho người lái đỡ mệt mỏi khi phải lái xe trong thời gian dài.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="4" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide84" data-slick-index="4" aria-hidden="false">
                        <a href="javascript:;" title="Nút điều chỉnh âm thanh" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">4. Nút điều chỉnh âm thanh</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Cụm-điều-khiển-âm-thanh.jpg')}}"
                                    alt="Nút điều chỉnh âm thanh"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li><span>CVT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">4. Nút điều chỉnh âm thanh</span></h3>
                                <div class="opContent">
                                    <p>Dễ dàng lựa chọn giai điệu yêu thích để hành trình nối tiếp niềm vui</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="5" class="option-item null slick-slide slick-active" style="" tabindex="-1" role="option" aria-describedby="slick-slide85" data-slick-index="5" aria-hidden="false">
                        <a href="javascript:;" title="Màn hình cảm ứng 7-inch" class="opLink" tabindex="0">
                            <div class="opLink-inner">
                                <span class="opText">5. Màn hình cảm ứng 7-inch</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Màn-hình-7-inch.jpg')}}"
                                    alt="Màn hình cảm ứng 7-inch"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">5. Màn hình cảm ứng 7-inch</span></h3>
                                <div class="opContent">
                                    <p>Màn&nbsp;hình&nbsp;cảm&nbsp;ứng&nbsp;7-inch&nbsp;với&nbsp;kết&nbsp;nối&nbsp;Android&nbsp;Auto&nbsp;và&nbsp;Apple&nbsp;CarPlay</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="6" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide86" data-slick-index="6" aria-hidden="true">
                        <a href="javascript:;" title="Điều hòa tự động" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">6. Điều hòa tự động</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Điều-hòa-tự-động.jpg')}}"
                                    alt="Điều hòa tự động"
                                />
                                <div class="option-listphienban">
                                    <ul>
                                        <li><span>CVT</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">6. Điều hòa tự động</span></h3>
                                <div class="opContent">
                                    <p>Duy trì nhiệt độ ổn định trong xe bất kể điều kiện bên ngoài thay đổi, mang đến cảm giác thoải mái dễ chịu suốt hành trình.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="7" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide87" data-slick-index="7" aria-hidden="true">
                        <a href="javascript:;" title="Nội thất tiện nghi Omotenashi" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">7. Nội thất tiện nghi Omotenashi</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/cac-hoc-tien-ich.jpg')}}"
                                    alt="Nội thất tiện nghi Omotenashi"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">7. Nội thất tiện nghi Omotenashi</span></h3>
                                <div class="opContent">
                                    <p>“Tiện ích thông minh ứng dụng triết lý “OMOTENASHI” của Nhật Bản, lấy người dùng làm trung tâm, đem lại sự thoải mái từ những chi tiết nhỏ nhất với hàng loạt ngăn chứa đa năng</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div data-item="8" class="option-item null slick-slide" style="" tabindex="-1" role="option" aria-describedby="slick-slide88" data-slick-index="8" aria-hidden="true">
                        <a href="javascript:;" title="Khoang hành lý dung tích lớn 450L" class="opLink" tabindex="-1">
                            <div class="opLink-inner">
                                <span class="opText">8. Khoang hành lý dung tích lớn 450L</span>
                            </div>
                        </a>
                        <div class="option-detail">
                            <div class="option-img">
                                <img
                                    src="{{asset('img/new-attrage/Khoang-hành-lý.jpg')}}"
                                    alt="Khoang hành lý dung tích lớn 450L"
                                />
                                <div class="option-listphienban"></div>
                            </div>
                            <div class="option-info">
                                <h3 class="opTitle"><span class="opText">8. Khoang hành lý dung tích lớn 450L</span></h3>
                                <div class="opContent">
                                    <p>Khoang hành lý có thể tích lớn đến 450L tối đa hóa khả năng chuyên chở, đồng thời giúp việc sắp xếp đồ đạc, vật dụng trở nên tiện lợi và dễ dàng</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="option-btn option-next"><i class="fa fa-caret-down"></i></div>

        <!-- Detail on Desktop-->
        <div class="option-list-detail">
            <div data-item="0" class="option-detail show-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Noi-that-rong-rai-ELL.jpg')}}"
                        alt="Không gian nội thất tiện nghi cho gia đình"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Không gian nội thất tiện nghi cho gia đình</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>
                            Khoang nội thất của Attrage sẽ làm hài lòng nhiều khách hàng nhờ cách bố trí thông minh và tối đa hóa không gian. Chất liệu da cao cấp của nội thất giúp cho hành khách luôn cảm thấy thoải mái và tiện nghi
                            khi di chuyển hàng ngày cho công việc lẫn thư giãn cùng gia đình​
                        </p>
                    </div>
                </div>
            </div>
            <div data-item="1" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Khởi-động-nút-bấm.jpg')}}"
                        alt="Khởi động nút bấm"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khởi động nút bấm</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Cho phép mở cửa mà không cần dùng chìa (KOS) và chức năng khởi động bằng nút bấm.</p>
                    </div>
                </div>
            </div>
            <div data-item="2" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Ghe-boc-da.jpg')}}"
                        alt="Ghế bọc da cao cấp"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li><span>CVT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Ghế bọc da cao cấp</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Ghế bọc da cao cấp mang đến sự tiện nghi vượt trội cho người lái và hành khách trên những hành trình dài.</p>
                    </div>
                </div>
            </div>
            <div data-item="3" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Tua-tay-ghe-lai.jpg')}}"
                        alt="Bệ tựa tay cho người lái"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li><span>CVT</span></li>
                            <li><span>MT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Bệ tựa tay cho người lái</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Trang bị tạo nên sự khác biệt của Attrage trong phân khúc, giúp cho người lái đỡ mệt mỏi khi phải lái xe trong thời gian dài.</p>
                    </div>
                </div>
            </div>
            <div data-item="4" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Cụm-điều-khiển-âm-thanh.jpg')}}"
                        alt="Nút điều chỉnh âm thanh"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li><span>CVT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Nút điều chỉnh âm thanh</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Dễ dàng lựa chọn giai điệu yêu thích để hành trình nối tiếp niềm vui</p>
                    </div>
                </div>
            </div>
            <div data-item="5" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Màn-hình-7-inch.jpg')}}"
                        alt="Màn hình cảm ứng 7-inch"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Màn hình cảm ứng 7-inch</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Màn&nbsp;hình&nbsp;cảm&nbsp;ứng&nbsp;7-inch&nbsp;với&nbsp;kết&nbsp;nối&nbsp;Android&nbsp;Auto&nbsp;và&nbsp;Apple&nbsp;CarPlay</p>
                    </div>
                </div>
            </div>
            <div data-item="6" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Điều-hòa-tự-động.jpg')}}"
                        alt="Điều hòa tự động"
                    />
                    <div class="option-listphienban">
                        <ul>
                            <li><span>CVT</span></li>
                        </ul>
                    </div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Điều hòa tự động</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Duy trì nhiệt độ ổn định trong xe bất kể điều kiện bên ngoài thay đổi, mang đến cảm giác thoải mái dễ chịu suốt hành trình.</p>
                    </div>
                </div>
            </div>
            <div data-item="7" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/cac-hoc-tien-ich.jpg')}}"
                        alt="Nội thất tiện nghi Omotenashi"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Nội thất tiện nghi Omotenashi</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>“Tiện ích thông minh ứng dụng triết lý “OMOTENASHI” của Nhật Bản, lấy người dùng làm trung tâm, đem lại sự thoải mái từ những chi tiết nhỏ nhất với hàng loạt ngăn chứa đa năng</p>
                    </div>
                </div>
            </div>
            <div data-item="8" class="option-detail">
                <div class="option-img">
                    <img
                        src="{{asset('img/new-attrage/Khoang-hành-lý.jpg')}}"
                        alt="Khoang hành lý dung tích lớn 450L"
                    />
                    <div class="option-listphienban"></div>
                </div>

                <div class="option-info">
                    <h3 class="opTitle" style="color: #ffffff;">Khoang hành lý dung tích lớn 450L</h3>
                    <div class="opContent" style="color: #ffffff;">
                        <p>Khoang hành lý có thể tích lớn đến 450L tối đa hóa khả năng chuyên chở, đồng thời giúp việc sắp xếp đồ đạc, vật dụng trở nên tiện lợi và dễ dàng</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>