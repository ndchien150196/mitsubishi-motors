@extends("app")

@section("content")
<div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
    <div class="row"></div>
    <div class="row">
        <div class="col-sm-12">
            <table id="mitsubishi" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="mitsubishi_info">
                <thead>
                    <tr role="row">
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">ID</th>
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Sản Phẩm</th>
                        <th class="text-center" tabindex="0" aria-controls="mitsubishi" rowspan="1" colspan="1">Giá</th>
                        <th class="text-center">
                            Sửa
                        </th>
                    </tr>
                </thead>
                <tbody class="">
                    @foreach($car as $k => $u)
                        <tr role="row" class="odd">
                            <td class="text-center">{{$u->id}}</td>
                            <td class="text-center">{{$u->product}}</td>
                            <td class="text-center">{{$u->amount}}</td>
                            <td class="text-center">
                                <a type="button" class="edit_data" data-toggle="modal" data-target="#edit" data-product="{{$u->product}}" data-amount="{{$u->amount}}"  data-id="{{$u->id}}">
                                    <span class="fa fa-edit"></span>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>


<div class="modal fade" id="edit">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title">Sửa Giá</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" class="form-control" id="edit_id">
                    <div class="form-group col-md-8 col-12">
                        <label>Name:</label>
                        <input type="text" class="form-control" id="edit_name" disabled="">
                        <input type="hidden" class="form-control" id="edit_id">
                    </div>
                    <div class="form-group col-md-4 col-12">
                        <label>Giá:</label>
                        <input type="text" class="form-control" id="edit_amount" placeholder="Nhập giá...">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <a href="#" onclick="edititem()"  type="button" class="btn btn-primary">Lưu</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section("js")
<script type="text/javascript">
	$('.edit_data').click(function () {
        $('#edit_name').val($(this).data('product'));
        $('#edit_amount').val($(this).data('amount'));
        $('#edit_id').val($(this).data('id'));
    });

    function edititem() {
        var edititem= '{{ route("admin.car.edit") }}';
        $.ajax({
            data: {
                amount   : $('#edit_amount').val(),
                id       : $('#edit_id').val(),
            },
            url: edititem,
            type: 'POST',
            success: function(data) {
                if(data.status === true) {
                    $.bootstrapGrowl(data.message, { type: 'success' });
                    window.location.reload();
                    setTimeout(function(){
                       window.location.reload(); 
                    }, 200); 
                }
            },
        });
    }

</script>
@endsection
